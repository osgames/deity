###############################################################################
#
# $Id$
#
# Deity Chimera Beastiary
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################



###############################################################################


BEAST _Chimera {
	ISA	_Any
}


###############################################################################
# Centaur (from HMG beastiary)
###############################################################################
BEAST Centaur {
	ISA	_Chimera
#	AKA

	# Location information is guessed, not official

	LOCATIONS HarnMaster3 {
		Skull		1
		Face		1
		Neck		2
		RightShoulder	3
		LeftShoulder	4
		RightArm	5
		LeftArm		6
		RightHand	11
		LeftHand	12
		RightForeQuarter 13
		LeftForeQuarter	14
		Thorax		15
		Abdomen		16
		RightHindQuarter 17
		LeftHindQuarter	18
		Tail		19
		RightForeLeg	20
		LeftForeLeg	21
		RightHindLeg	22
		LeftHindLeg	23
		RightForeFoot	24
		LeftForeFoot	25
		RightHindFoot	26
		LeftHindFoot	27
	}

	WIELD HarnMaster3 {
		RightHand	1
		LeftHand	2
	}

	HIT HarnMaster3 {
		5		Skull
		10		Face
		14		Neck
		21		(Left|Right)Shoulder
		30		(Left|Right)Arm
		34		(Left|Right)Hand
		44		Thorax
		54		(Left|Right)ForeQuarter
		70		(Left|Right)HindQuarter
		72		Tail
		82		(Left|Right)HindLeg
		86		(Left|Right)HindFoot
		96		(Left|Right)ForeLeg
		100		(Left|Right)ForeFoot
	}

	HIT "high" HarnMaster3 {
		15		Skull
		30		Face
		45		Neck
		57		(Left|Right)Shoulder
		81		(Left|Right)Arm
		85		(Left|Right)Hand
		95		Thorax
		100		(Left|Right)ForeQuarter
	}

	HIT "low" Harnmaster3 {
		6		(Left|Right)Arm
		12		(Left|Right)Hand
		19		Thorax
		29		(Left|Right)ForeQuarter
		37		(Left|Right)HindQuarter
		40		Tail
		60		(Left|Right)HindLeg
		65		(Left|Right)HindFoot
		92		(Left|Right)ForeLeg
		100		(Left|Right)ForeFoot
	}

	GENERATE all {
		# info
		I:Race		Centaur
		I:Frame		Medium
		I:Height	(3d6 + 66) * 2.54
		I:Weight	(500 + 100 * (I:Height / 2.54 - 66 - 3) / 15) / 2.2
		I:Age		5 + [35/1.1]
	}

	GENERATE HarnMaster3 {
		# attributes
		A:STR		3d6 + 8
		A:STA		3d6 + 7
		A:DEX		3d6 + 1
		A:AGL		3d6
		A:EYE		3d6 + 4
		A:HRG		3d6 + 1
		A:SML		3d6 + 1
		A:INT		3d6 + 1
		A:AUR		3d6 + 2
		A:WIL		3d6 + 1

		A:END		+ 0
		A:MOV		+ 0

		# skills (MLs based on average attrs + mod)
		S:Initiative	+ (60 - (12+11+11+0.5) // 3 * 4) + I:Age-5 + [25/1.2] - 5  # DEX+WIL+AGL
		S:Dodge		+ (55 - (11+11+11+0.5) // 3 * 5) + I:Age-5 + [25/1.2] - 5  # AGL+AGL+AGL
		S:Unarmed	+ (50 - (19+11+11+0.5) // 3 * 4) + I:Age-5                 # STR+DEX+AGL
		S:Daggers	+ (60 - (12+11+15+0.5) // 3 * 3) + I:Age-5 + [25/1.2] - 5  # DEX+DEX+EYE
		S:Hand/Unarmed	+ (50 - (19+11+11+0.5) // 3 * 4) + I:Age-5 + [25/1.2] - 5  # STR+DEX+AGL
		S:Hoof/Unarmed	+ (75 - (19+11+11+0.5) // 3 * 4) + I:Age-5 + [25/1.2] - 5  # STR+DEX+AGL
		S:Shortbow/Bows	+ (85 - (19+11+15+0.5) // 3 * 2) + I:Age-5 + [25/1.2] - 5  # STR+DEX+EYE
		S:Handaxe/Axes	+ (45 - (10+10+12+0.5) // 3 * 3) + I:Age-5 + [25/1.2] - 5  # STR+STR+DEX
	       S:Javelin/Spears	+ (65 - (19+18+12+0.5) // 3 * 3) + I:Age-5 + [25/1.2] - 5  # STR+STR+DEX
		S:Sling/Slings	+ (85 - (12+11+15+0.5) // 3 * 1) + I:Age-5 + [25/1.2] - 5  # DEX+DEX+EYE

		S:Awareness	+  0 + [ 5/1.2]
		S:Stealth	+ 10 + [ 5/1.2]
		S:Tracking	+  0 + [10/1.2]

		# armor (base)
		L:StartWear	_Centaur-Body

		# equipment
		L:StartHave	Shortbow | Sling
		L:StartHave	Javelin | Handaxe
		L:StartHave	Knife | Dagger
	}
}


EQUIP _Centaur-Hand {
	AKA			_Centaur-Arm

	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Blunt		1

		I:Skill		Hand
		A:Attack	0
		A:Defend	15
	}
}


EQUIP _Centaur-Foot {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Blunt		6

		I:Skill		Hoof
		A:Attack	0
		A:Defend	0
	}
}


EQUIP _Centaur-Body {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
	}

	COVER HarnMaster3 {
		RightHindQuarter 2,2,1,1
		LeftHindQuarter	2,2,1,1
		Tail		2,2,1,1
		RightForeLeg	2,2,1,1
		LeftForeLeg	2,2,1,1
		RightHindLeg	2,2,1,1
		LeftHindLeg	2,2,1,1
		RightForeFoot	2,2,1,1
		LeftForeFoot	2,2,1,1
		RightHindFoot	2,2,1,1
		LeftHindFoot	2,2,1,1
	}
}


###############################################################################
# Griffin (from HMG beastiary)
###############################################################################
BEAST Griffin {
	ISA	_Chimera,_WingAndTailedQuadruped
#	AKA

	GENERATE all {
		# info
		I:Race		Griffin
		I:Frame		Medium
		I:Height	(3d6 + 55) * 2.54
		I:Weight	(130 + 20 * (I:Height / 2.54 - 55 - 3) / 15) / 2.2
		I:Age		1 + [10/1.1]
	}

	GENERATE HarnMaster3 {
		# attributes
		A:STR		3d6 + 10
		A:STA		3d6 + 9
		#:DEX		-
		A:AGL		3d6 + 4
		A:EYE		3d6 + 8
		A:HRG		3d6 + 4
		A:SML		3d6 + 4
		A:INT		3d6 - 2
		A:AUR		1d3 - 1
		A:WIL		3d6 + 1

		A:END		+ 0
		A:MOV		+ 0

		# skills (MLs based on average attrs + mod)
		S:Initiative	+ (64 - (   11+15+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # DEX+WIL+AGL
		S:Dodge		+ (75 - (15+14+15+0.5) // 3 * 5) + I:Age + [25/1.2] - 5  # AGL+AGL+AGL
		S:Beak/Unarmed	+ (80 - (20   +15+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # STR+DEX+AGL
		S:Claw/Unarmed	+ (75 - (20   +15+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # STR+DEX+AGL

		S:Awareness	+ 0
		S:Stealth	+ 0

		# armor (base)
		L:StartWear	Natural(3,4,2,4)

		# equipment
	}
}


EQUIP _Griffin-Foot {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Point		10

		I:Skill		Claw
		A:Attack	0
		A:Defend	0
	}
}


EQUIP _Griffin-Face {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Point		12

		I:Skill		Beak
		A:Attack	0
		A:Defend	0
	}
}


###############################################################################
# Hippogriff (from HMG beastiary)
###############################################################################
BEAST Hippogriff {
	ISA	_Chimera,_WingAndTailedQuadruped
#	AKA

	GENERATE all {
		# info
		I:Race		Hippogriff
		I:Frame		Medium
		I:Height	(4d6 + 166) * 2.54
		I:Weight	(300 + 400 * (I:Height / 2.54 - 166 - 4) / 20) / 2.2
		I:Age		1 + [10/1.1]
	}

	GENERATE HarnMaster3 {
		# attributes
		A:STR		4d6 + 4
		A:STA		4d6 + 1
		#:DEX		-
		A:AGL		3d3 + 2
		A:EYE		4d6 + 5
		A:HRG		4d6 + 3
		A:SML		4d6 + 3
		#:INT		-
		#:AUR		-
		A:WIL		4d6 - 2

		A:END		+ 0
		A:MOV		+ 0

		# skills (MLs based on average attrs + mod)
		S:Initiative	+ (60 - (   12+ 8+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # DEX+WIL+AGL
		S:Dodge		+ (40 - ( 8+ 8+ 8+0.5) // 3 * 5) + I:Age + [25/1.2] - 5  # AGL+AGL+AGL
		S:Beak/Unarmed	+ (75 - (18   + 8+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # STR+DEX+AGL
		S:Hoof/Unarmed	+ (45 - (18   + 8+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # STR+DEX+AGL
		S:Talon/Unarmed	+ (85 - (18   + 8+0.5) // 2 * 4) + I:Age + [25/1.2] - 5  # STR+DEX+AGL

		S:Awareness	+ 0
		S:Stealth	+ 0

		# armor (base)
		L:StartWear	Natural(4,3,1,3)

		# equipment
	}
}


EQUIP _Hippogriff-FrontFoot {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Edge		6

		I:Skill		Talon
		A:Attack	0
		A:Defend	0
	}
}


EQUIP _Hippogriff-BackFoot {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Blunt		6

		I:Skill		Hoof
		A:Attack	0
		A:Defend	0
	}
}


EQUIP _Hippogriff-Face {
	IMAGE bitmap {
	}

	GENERATE HarnMaster3 {
		A:Point		8

		I:Skill		Beak
		A:Attack	0
		A:Defend	0
	}
}


###############################################################################


