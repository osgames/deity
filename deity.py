#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (mainline)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import gettext

#import deity
import deity.gettext


gettext.install('messages', './deity/locale', unicode=False,
                names=('gettext','ngettext'))
Deity_Lang_bw = gettext.translation('deity','deity/locale', languages=['bw'])


###############################################################################


import __builtin__
import bsddb
import sys

import deity.beastiary


__builtin__.__dict__['kDeityName'] = 'Deity'
__builtin__.__dict__['kDeityVersion'] = '0.8'
__builtin__.__dict__['kDeityCopyright'] = NP_('about:Copyright (C) 2005-2008 by Brian White <bcwhite@pobox.com>')
__builtin__.__dict__['kDeityDevelopers'] = [
    'Brian White <bcwhite@pobox.com>',
]
__builtin__.__dict__['kDeityDescription'] = NP_('''about:
Deity is a generic tool for game-masters (GMs) of role-playing games (RPGs).''')


###############################################################################


# special runs
if len(sys.argv) > 1:
    if sys.argv[1] == '--beastiary-load':
        import sys, glob, operator
        sys.argv = reduce(operator.add, map(glob.glob, sys.argv[2:]))

        bdb = bsddb.btopen(deity.kBeastiaryFilename, 'c')
        bld = deity.beastiary.Database(bdb)
        for arg in sys.argv:
            if arg[-1] == '~': continue
            if arg[0]  == '#': continue
            bld.LoadFile(arg)
        exit(0)

    if sys.argv[1] == '--beastiary-generate':
        bdb = bsddb.btopen(Deity.kBeastiaryFilename, 'c')
        gen = Deity.Beastiary.Generate(bdb, sys.argv[2], sys.argv[3])
        out = gen.keys()
        out.sort()
        for k in out:
            print "%14s: %s" % (k,gen[k])
        exit(0)


# args
testmode = False
for arg in sys.argv[1:]:
    if arg == "--test":
        testmode = True


###############################################################################


import wx
import locale

import deity.windows


###############################################################################


# open databases
BDB = bsddb.btopen(deity.kBeastiaryFilename,'r')
GDB = bsddb.btopen('saved.ddb','c')
GDB['/Rules/Options'] = 'CombatWeaponDamage;CombatWeaponWear;CombatBodyBlock;' \
                        'CombatKnockback;CombatFumble;CombatStumble;CombatMinorWoundShock=no;' \
                        'CombatAmputate;CombatBleeders;' \
                        'WoundInfections;' \
                        'ImperialMeasurements'
__builtin__.__dict__['BDB'] = BDB
__builtin__.__dict__['GDB'] = GDB


# startup (and output)
def DevNull(*opts): pass
__builtin__.__dict__['Output'] = DevNull
Deity_App = wx.PySimpleApp()
Deity_Locale = wx.Locale(wx.LANGUAGE_ENGLISH)
#locale.setlocale(locale.LC_ALL, 'EN')
Deity_Lang_bw.install()
Deity_Window = deity.windows.Main(testmode)
__builtin__.__dict__['Output'] = Deity_Window.Output
__builtin__.__dict__['Query']  = Deity_Window.Query
__builtin__.__dict__['Choose'] = Deity_Window.Choose
__builtin__.__dict__['Input']  = Deity_Window.Input
__builtin__.__dict__['Select'] = Deity_Window.Select


# do it!
Deity_App.MainLoop()
