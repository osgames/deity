###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program ("gettext" interface)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################
#
# This defines a dummy gettext interface as well as some custome functions for
# handing a "prefix" that defines where the original text originated (e.g.
# button text, beastiary, etc.).  Typically the main program will load this
# module and then load the real "gettext" to define functions that actually do
# something.  However, for the unit-tests, this simple interface is sufficient.
#
###############################################################################


import __builtin__


testxlate = {'test1':'Case1', 'test2':'Case2', 'test3':'Case3',
             'test:a':'B', 'test:c':'D', 'test:e':'F', 'test:g':'H',
             'test:aa':'BB', 'test:cc':'DD', 'test:ee':'FF',
             'test:1':'Z', 'test:2':'Y', 'test:3':'X', 'test:4':'W'}


def gettext(text):
    if text in testxlate:
        return testxlate[text]
    return text

def ngettext(single, plural, num):
    if num == 1:
        return gettext(single)
    else:
        return gettext(plural)

def gettext_p(prefix, value=None):
    #print 'P_',prefix,value,'->',
    if value is None:
        plen   = prefix.find(':') + 1
        if not plen: return _(prefix)
        value  = prefix
        prefix = prefix[0:plen]
    elif prefix == '':
        plen = 0
    else:
        if prefix[-1] != ':': prefix += ':'
        plen   = len(prefix)
        value  = prefix + value
    xlate = _(value)
    #print value,xlate,'/',plen
    if len(xlate) >= plen and xlate[0:plen] == prefix:
        return xlate[plen:]
    return xlate

def gettext_n(text):
    return text

def gettext_np(text):
    sep = text.index(':')
    return text[sep+1:]


__builtin__.__dict__['gettext']	= gettext
__builtin__.__dict__['ngettext']= ngettext
__builtin__.__dict__['_']	= gettext
__builtin__.__dict__['P_']	= gettext_p
__builtin__.__dict__['N_']	= gettext_n
__builtin__.__dict__['NP_']	= gettext_np
