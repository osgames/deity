###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity World Common (generic functions, constants, etc.)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import __builtin__
import re

from deity.exceptions import *
from deity.watcher import *
from deity.xtype import *


CurWorld = Watched(None)


###############################################################################


class World(object):
    '''The game "world" object handles all the mechanics of a living, breathing
    world.

    Everything here is gamesystem-independent.
    '''

    kDateTimeRe = re.compile('^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)\.(\d+)\s*(\d{3,4})?$')
    kTimeZoneRe = re.compile('^([\+\-]?)(\d{3,4})$')
    kTimeAdjRe  = re.compile('^\s*([\+\-]?)\s*(\d*)\s*(\w*)\s*(:?)\s*')
    kDateSetRe  = re.compile('^\s*(\d+)\D+(\d+)\D+(\d+)\s*$')
    kTimeSetRe  = re.compile('^\s*(\d+)\D+(\d+)(?:\D+(\d+)([\.\,]\d+)?)?\s*$')

    kTimeParts = ('year', 'month', 'day', 'hour', 'minute', 'second', 'nanosec')
    kTimeCodes = {
        NP_('time:nanos'):  'nseconds',
        NP_('time:ns'):     'nseconds',
        NP_('time:micros'): 'useconds',
        NP_('time:us'):     'useconds',
        NP_('time:millis'): 'mseconds',
        NP_('time:ms'):     'mseconds',
        NP_('time:second'): 'seconds',
        NP_('time:sec'):    'seconds',
        NP_('time:s'):      'seconds',
        NP_('time:minute'): 'minutes',
        NP_('time:min'):    'minutes',
        NP_('time:m'):      'minutes',
        NP_('time:hour'):   'hours',
        NP_('time:hr'):     'hours',
        NP_('time:h'):      'hours',
        NP_('time:day'):    'days',
        NP_('time:d'):      'days',
        NP_('time:month'):  'months',
        NP_('time:mo'):     'months',
        NP_('time:year'):   'years',
        NP_('time:y'):      'years',
    }

    # These should be defined in any derived class; there here for unit-tests.
    kMonthBase = 1
    kDayBase = 1
    kSecondsPerMinute = 60
    kMinutesPerHour = 60
    kHoursPerDay = 24
    kDaysPerMonth = 30
    kMonthsPerYear = 12
    kDaysPerYear = kDaysPerMonth * kMonthsPerYear
    kMonthNames = ('month01','month02','month03','month04','month05','month06','month07','month08','month09','month10','month11','month12')
    kMonthAbbrs = ('M01','M02','M03','M04','M05','M06','M07','M08','M09','M10','M11','M12')
    kEnvironments = xlist((
        'safe',
        'urban+', 'rural+', 'road+',
        'urban-', 'rural-', 'road-',
    ), xlate='abbr:')
    kEnvironInfo = {
        'safe':      { 'timestep': '1 days'    },
        'urban+':    { 'timestep': '1 minutes' },
        'urban-':    { 'timestep': '1 minutes' },
        'rural+':    { 'timestep': '4 hours'   },
        'rural-':    { 'timestep': '4 hours'   },
        'road+':     { 'timestep': '4 hours'   },
        'road-':     { 'timestep': '4 hours'   },
    }


    def __init__(self, prefix):
        self.gdbPrefix = '/World/%s' % prefix

        # Make instance easily available
        __builtin__.__dict__['WORLD'] = self
        CurWorld.Set(self)

        # Convert time-codes to local strings
        self.timeCodes = dict()
        for k,v in self.kTimeCodes.iteritems():
            self.timeCodes[P_(k).lower()] = v
        self.timeStrings = self.timeCodes.keys()
        self.timeStrings.sort(cmp = lambda a,b: cmp(len(b), len(a)))

        # Set other defaults
        ekey = '%s/EnvironCurrent' % self.gdbPrefix
        if ekey not in GDB:
            GDB[ekey] = self.kEnvironments[0]
        self._EnvironChanged(None)

        # Load timestamp
        self.timeKey = self.GdbGetPrefix('CurTime')
        self.timeCurrent = self.TimeImport(utc=GDB['%s/CurTime'  % self.timeKey], tz=GDB['%s/TimeZone' % self.timeKey])
        #print 'Reading UTC:','%s/CurTime'%self.timeKey,self.timeCurrent['utc']


    def GdbGetPrefix(self, key):
        prefix = self.gdbPrefix
        while '%s/%s' % (prefix,key) not in GDB:
            spos = prefix.rfind('/')
            if spos < 0:
                raise Exception('no prefix locates "%s" under "%s"' % (key,self.gdbPrefix))
            prefix = prefix[:spos]
        return prefix


    def _AbsoluteDay(self, year, doy, hour=0, minute=0, second=0, nanosec=0):
        secondsperday = self.kHoursPerDay * self.kMinutesPerHour * self.kSecondsPerMinute
        seconds = (hour * self.kMinutesPerHour + minute) * self.kSecondsPerMinute + second
        return year * self.kDaysPerYear + doy + (seconds + nanosec / 1.0e6) / secondsperday


    def _DaysPerMonth(self, month):
        return self.kDaysPerMonth


    def _DaysPerYear(self, year):
        return self.kDaysPerYear


    def _Daytime(self, month, day, hour, minute):
        if hour < self.kHoursPerDay/4 or hour >= self.kHoursPerDay*3/4:
            return 'night'
        else:
            return 'day'


    def _Season(self, year, doy):
        return 4 * (doy - self.kDayBase) / self._DaysPerYear(year)


    def _TimeOffset(self, adjustment, stamp=None, defunit=None, defmult=1, next=None, negate=1):
        #print "TimeOffset:", adjustment, stamp
        adjustment = str(adjustment).lower()
        if stamp is None:
            stamp = self.timeCurrent

        year    = stamp['year']
        month   = stamp['month'] - self.kMonthBase
        day     = stamp['day']   - self.kDayBase
        hour    = stamp['hour']
        minute  = stamp['minute']
        second  = stamp['second']
        nanosec = stamp['nanosec']
        #print 'year:',year,'month:',month,'day:',day,'hour:',hour,'minute:',minute,'second:',second,'nanosec:',nanosec

        yearadj    = 0
        monthadj   = 0
        dayadj     = 0
        houradj    = 0
        minuteadj  = 0
        secondadj  = 0
        nanosecadj = 0

        match = self.kTimeZoneRe.match(adjustment)
        if match:
            if match.group(1) == '-':
                sign = -1
            else:
                sign = +1
            houradj   = negate * sign * int(match.group(2)[0:-2])
            minuteadj = negate * sign * int(match.group(2)[-2:])
            #print 'timezone: hour:',houradj,'minute:',minuteadj
        else:
            sign = +1
            search = 0
            while search < len(adjustment) and not adjustment[search:].isspace():
                match = self.kTimeAdjRe.match(adjustment[search:])
                #print adjustment[search:],'=>',match.groups()
                if not match or ''.join(match.groups()) == '':
                    raise BadParameter(_('invalid time adjustment "%(adj)s" starting at "%(start)s" -- ignored')
                                       % {'adj':adjustment, 'start':adjustment[search:]})

                mult = 1
                if match.group(1) == '-':
                    sign = -1
                elif match.group(1) == '+':
                    sign = +1
                if match.group(2) == '':
                    num  = 1
                else:
                    num  = int(match.group(2))
                if match.group(4) == ':' or next:
                    trim = True
                else:
                    trim = False
                if match.group(3) == '':
                    unit = defunit
                    mult = defmult
                    trim = (next is None or next)
                else:
                    unit = match.group(3)
                    ulen = len(unit)
                    for s in self.timeStrings:
                        slen = len(s)
                        if ulen >= slen and s == unit[:slen]:
                            unit = self.timeCodes[s]
                            break
                    else:
                        raise BadParameter(_('invalid time unit "%(unit)s" in adjustment "%(adj)s" -- ignored')
                                           % {'adj':adjustment, 'unit':unit})

                sign *= negate
                #print adjustment[search:],'=>',sign,'*',num,unit
                if unit == 'years':
                    yearadj += sign * mult * num
                    if trim:
                        yearadj   -= (sign * year) % mult * sign
                        monthadj   = -month + self.kMonthBase
                        dayadj     = -day   + self.kDayBase
                        houradj    = -hour
                        minuteadj  = -minute
                        secondadj  = -second
                        nanosecadj = -nanosec
                        if sign < 0 and (sign * year) % mult == 0:
                            if monthadj or dayadj or houradj or minuteadj or secondadj or nanosecadj:
                                yearadj += mult * num
                elif unit == 'months':
                    monthadj += sign * mult * num
                    if trim:
                        monthadj  -= (sign * month) % mult * sign
                        dayadj     = -day   + self.kDayBase
                        houradj    = -hour
                        minuteadj  = -minute
                        secondadj  = -second
                        nanosecadj = -nanosec
                        if sign < 0 and (sign * month) % mult == 0:
                            if dayadj or houradj or minuteadj or secondadj or nanosecadj:
                                monthadj += mult * num
                elif unit == 'days':
                    dayadj += sign * mult * num
                    if trim:
                        dayadj    -= (sign * day) % mult * sign
                        houradj    = -hour
                        minuteadj  = -minute
                        secondadj  = -second
                        nanosecadj = -nanosec
                        if sign < 0 and (sign * day) % mult == 0:
                            if houradj or minuteadj or secondadj or nanosecadj:
                                dayadj += mult * num
                elif unit == 'hours':
                    houradj += sign * mult * num
                    #print 'hour:',hour,'houradj:',houradj
                    if trim:
                        houradj   -= (sign * hour) % mult * sign
                        minuteadj  = -minute
                        secondadj  = -second
                        nanosecadj = -nanosec
                        if sign < 0 and (sign * hour) % mult == 0:
                            if minuteadj or secondadj or nanosecadj:
                                houradj += mult
                elif unit == 'minutes':
                    minuteadj += sign * mult * num
                    if trim:
                        minuteadj -= (sign * minute) % mult * sign
                        secondadj  = -second
                        nanosecadj = -nanosec
                        if sign < 0 and (sign * minute) % mult == 0:
                            if secondadj or nanosecadj:
                                minuteadj += mult * num
                elif unit == 'seconds':
                    secondadj += sign * mult * num
                    if trim:
                        secondadj -= (sign * second) % mult * sign
                        nanosecadj = -nanosec
                        if sign < 0 and (sign * second) % mult == 0:
                            if nanosecadj:
                                secondadj += mult * num
                elif unit == 'mseconds':
                    nanosecadj += sign * mult * num * 1000000
                elif unit == 'useconds':
                    nanosecadj += sign * mult * num * 1000
                elif unit == 'nseconds':
                    nanosecadj += sign * mult * num
                else:
                    raise Exception('invalid time unit "%s"' % unit)

                search += match.end()

        #print 'nanosec:',nanosec,'nanoadj:',nanosecadj,'secondadj:',secondadj
        nanosec = nanosec + nanosecadj
        if nanosec < 0 or nanosec >= 1000000000:
            secondadj += nanosec // 1000000000
            nanosec = nanosec % 1000000000

        #print 'nanosec:',nanosec,'second:',second,'secondadj:',secondadj,'minuteadj:',minuteadj
        second = second + secondadj
        if second < 0 or second >= self.kSecondsPerMinute:
            minuteadj += second // self.kSecondsPerMinute
            second = second % self.kSecondsPerMinute

        #print 'second:',second,'minute:',minute,'minuteadj:',minuteadj,'houradj:',houradj
        minute = minute + minuteadj
        if minute < 0 or minute >= self.kMinutesPerHour:
            houradj += minute // self.kMinutesPerHour
            minute = minute % self.kMinutesPerHour

        #print 'minute:',minute,'hour:',hour,'houradj:',houradj,'dayadj:',dayadj
        hour = hour + houradj
        if hour < 0 or hour >= self.kHoursPerDay:
            dayadj += hour // self.kHoursPerDay
            hour = hour % self.kHoursPerDay

        #print 'hour:',hour,'day:',day,'dayadj:',dayadj,'monthadj:',monthadj
        day = day + dayadj
        try:
            if day < 0 or day >= self.kDaysPerMonth:
                monthadj += day // self.kDaysPerMonth
                day = day % self.kDaysPerMonth
        except NameError:
            raise Exception('untested code')
            while day < 0:
                month -= 1
                dayspermonth = self._DaysPerMonth(month % self.kMonthsPerYear)
                day += dayspermonth
            while True:
                dayspermonth = self._DaysPerMonth(month + self.kMonthBase)
                if day < dayspermonth:
                    break
                month += 1
                day -= dayspermonth

        #print 'day:',day,'month:',month,'monthadj:',monthadj,'monthadj:',yearadj
        month = month + monthadj
        if month < 0 or month >= self.kMonthsPerYear:
            yearadj += month // self.kMonthsPerYear
            month   = month % self.kMonthsPerYear

        #print 'month:',month,'year:',year,'yearadj:',yearadj
        year = year + yearadj

        try:
            doy = (month) * self.kDaysPerMonth + stamp['day']
        except NameError:
            raise Exception('untested code')
            doy = stamp['day']
            for m in xrange(month):
                doy += self._DaysPerMonth(m + self.kMonthBase)

        if self.timeUnitDefault == 'minutes':
            incr = (hour * self.kMinutesPerHour + minute) // self.timeIncDefault
        elif self.timeUnitDefault == 'hours':
            incr = hour // self.timeIncDefault
        elif self.timeUnitDefault == 'days':
            incr = 0
        else:
            raise Exception('unexpected default unit "%s" -- please add' % self.timeUnitDefault)

        stamp['year']    = year
        stamp['month']   = month + self.kMonthBase
        stamp['day']     = day   + self.kDayBase
        stamp['hour']    = hour
        stamp['minute']  = minute
        stamp['second']  = second
        stamp['nanosec'] = nanosec
        stamp['dayincr'] = incr
        stamp['abs']     = self._AbsoluteDay(year, doy, hour, minute, second, nanosec)
        stamp['doy']     = doy
        stamp['season']  = self._Season(year, doy)
        stamp['daytime'] = self._Daytime(month, day, hour, minute)


    def TimeCompare(self, stamp1, stamp2=None):
        if stamp2 is None:
            stamp2 = self.timeCurrent
        for p in ('abs', 'nanosec'):
            diff = stamp1[p] - stamp2[p]
            if diff < 0: return -1
            if diff > 0: return +1
        return 0


    def TimeAdjust(self, adjustment, stamp=None):
        if stamp is None:
            stamp = self.timeCurrent
        #print 'TimeAdjust:',adjustment

        match = self.kDateTimeRe.match(stamp['utc'])
        if not match:
            Output('invalid date/time "%s" -- ressetting' % stamp['utc'], '@error')
            stamp['utc'] = '0-%d-%d 00:00:00.0' % (self.kMonthBase,self.kDayBase)
            parts = (0,self.kMonthBase,self.kDayBase,0,0,0,0)
        else:
            parts = match.groups()

        match = self.kTimeZoneRe.match(stamp['tz'])
        if not match:
            Output('invalid timezone -- ressetting', '@error')
            stamp['tz'] = '+0000'

        stamp['year']    = int(parts[0])
        stamp['month']   = int(parts[1])
        stamp['day']     = int(parts[2])
        stamp['hour']    = int(parts[3])
        stamp['minute']  = int(parts[4])
        stamp['second']  = int(parts[5])
        stamp['nanosec'] = int(parts[6])
        if len(parts) > 7 and parts[7] is not None:
            stamp['tz'] = parts[7]

        if adjustment:
            # adjustment has to be made "in-Tz" or rounding will not work properly
            if 'tz' in stamp:
                self._TimeOffset(stamp['tz'], stamp)

            self._TimeOffset(adjustment, stamp, self.timeUnitDefault, self.timeIncDefault)

            # undo TZ adjustment for UTC storage
            if 'tz' in stamp:
                self._TimeOffset(stamp['tz'], stamp, negate=-1)

            stamp['utc'] = '%d-%02d-%02d %02d:%02d:%02d.%09d' % (stamp['year'], stamp['month'], stamp['day'],
                                                                 stamp['hour'], stamp['minute'], stamp['second'],
                                                                 stamp['nanosec'])

            if stamp is self.timeCurrent:
                #print 'Writing UTC:','%s/CurTime'%self.timeKey,self.timeCurrent['utc']
                GDB['%s/CurTime' % self.timeKey] = self.timeCurrent['utc']

        # move back to "in-TZ"
        if 'tz' in stamp:
            self._TimeOffset(stamp['tz'], stamp)


    def TimeImport(self, utc, tz='+0000'):
        stamp = dict(utc=utc, tz=tz)
        self.TimeAdjust(None, stamp)
        return stamp


    def TimeExport(self, stamp=None, withtz=False):
        if stamp is None:
            stamp = self.timeCurrent
        if withtz:
            return '%s %s' % (stamp['utc'], stamp['tz'])
        else:
            return stamp['utc']


    def TimeGetCurrent(self):
        return dict(self.timeCurrent)  # copy so two are independent


    def TimeSetDate(self, set):
        datematch = self.kDateSetRe.match(set)
        if datematch:
            # ^\s*(\d+)\D(\d+)\D(\d+)\s*$
            self.TimeAdjust('%+d%s %+d%s %+d%s' % (
                int(datematch.group(1)) - self.timeCurrent['year'],  P_('time:year'),
                int(datematch.group(2)) - self.timeCurrent['month'], P_('time:month'),
                int(datematch.group(3)) - self.timeCurrent['day'],   P_('time:day'),
            ))
        else:
            raise BadParameter(_('invalid time string "%(str)s"') % {'str':set})


    def TimeSetTime(self, set):
        timematch = self.kTimeSetRe.match(set)
        if timematch:
            # ^\s*(\d+)\D(\d+)(?:\D(\d+)(\.\d+)?)?\s*$
            adjust = '%+d%s %+d%s' % (
                int(timematch.group(1)) - self.timeCurrent['hour'],   P_('time:hour'),
                int(timematch.group(2)) - self.timeCurrent['minute'], P_('time:minute')
            )
            if timematch.group(3):
                adjust += ' %+d%s' % (
                    int(timematch.group(3)) - self.timeCurrent['second'], P_('time:second')
                )
            if timematch.group(4):
                partial = '.' + timematch.group(4)[1:] # could be "." or "," to start
                adjust += ' %+d%s' % (
                    int(float(partial) * 1e9) - self.timeCurrent['nanosec'], P_('time:nanos')
                )
            self.TimeAdjust(adjust)
        else:
            raise BadParameter(_('invalid time string "%(str)s"') % {'str':set})


    def TimeGetOutput(self, format, stamp=None):
        if stamp is None:
            stamp = self.timeCurrent

        output = ''
        escape = False
        style  = ''
        for f in format:
            if escape:
                if f == '%':
                   output += '%'
                elif f.isdigit():
                    style += f
                    continue
                elif f == 'Y':
                    output += (style+'d') % stamp['year']
                elif f == 'M':
                    output += (style+'d') % stamp['month']
                elif f == 'N':
                    output += (style+'s') % P_(self.kMonthNames[stamp['month']-self.kMonthBase])
                elif f == 'S':
                    output += (style+'s') % P_(self.kMonthAbbrs[stamp['month']-self.kMonthBase])
                elif f == 'D':
                    output += (style+'d') % stamp['day']
                elif f == 'h':
                    output += (style+'d') % stamp['hour']
                elif f == 'm':
                    output += (style+'d') % stamp['minute']
                elif f == 's':
                    output += (style+'d') % stamp['second']
                elif f == 'i':
                    output += (style+'d') % (stamp['nanosec'] // 1000000)
                elif f == 'u':
                    output += (style+'d') % (stamp['nanosec'] // 1000)
                elif f == 'n':
                    output += (style+'d') % (stamp['nanosec'])
                elif f == 'z':
                    output += (style+'s') % stamp['tz']
                else:
                    raise Exception('invalid date format "%s%s" in "%s"' % (style, f, format))
                escape = False

            else:
                if f == '%':
                    escape = True
                    style  = '%'
                else:
                    output += f

        return output


    #---------------------------------------------------------------------------


    def _EnvironChanged(self, env):
        if env is None:
            env = self.EnvironGet()
        step = self.kEnvironInfo[env]['timestep']
        mult,unit = step.split(' ')
        #print 'EnvironChanged:',env,mult,unit
        self.timeIncDefault  = int(mult)
        self.timeUnitDefault = unit


    def EnvironGet(self):
        return xstr(GDB['%s/EnvironCurrent' % self.gdbPrefix], xlate=self.kEnvironments.xlate)


    def EnvironSet(self, env):
        if env not in self.kEnvironments:
            raise Exception('unknown environment "%s"' % env)
        GDB['%s/EnvironCurrent' % self.gdbPrefix] = env
        self._EnvironChanged(env)


    def EnvironLookup(self, env):
        mod = env[-1]
        if mod == '+' or mod == '-':
            env = env[:-1]
            opts = XLookup(self.kEnvironments, env, typename="environment", multiple=True)
            if len(opts) == 1:
                return opts[0]
            match = None
            for o in opts:
                if o[-1] == mod:
                    # if multiple matches are found:
                    if match:
                        # this will just throw the proper exception
                        return XLookup(self.kEnvironments, env, typename="environment")
                    match = o
            if match:
                return match

        return XLookup(self.kEnvironments, env, typename="environment")


    def EnvironGetHtml(self):
        return ''

######################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################
