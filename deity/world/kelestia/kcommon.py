###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Kelestia Common (generic functions, constants, etc.)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import random
import re

import deity.world

from deity.xtype import *


###############################################################################


class Kelestia(deity.world.World):
    kMonthBase = 1
    kDayBase = 1
    kSecondsPerMinute = 60
    kMinutesPerHour = 60
    kHoursPerDay = 24
    kDaysPerMonth = 30
    kMonthsPerYear = 12
    kMonthNames = ('Nuzyael', 'Peonu', 'Kelen', 'Nolus', 'Larane', 'Agrazhar',
                   'Azura', 'Halane', 'Savor', 'Ilvin', 'Navek', 'Morgat')
    kMonthAbbrs = ('NUZ', 'PEO', 'KEL', 'NOL', 'LAR', 'AGR',
                   'AZU', 'HAL', 'SAV', 'ILV', 'NAV', 'MOR')
    kSeasons = xlist((
        NP_('time:spring'), NP_('time:summer'), NP_('time:autumn'), NP_('time:winter')
    ), xlate='time:')
    kStandardDateFormat = '%N %D, %Y'
    kStandardTimeFormat = '%02h:%02m'
    kStandardDateTimeFormat = '%N %D, %Y %02h:%02m'

    kWeatherRe   = re.compile(r'^(\w+) (\(\w+\))?\s*([NSEW]+):(\d)-(\d) (.*)$')
    kEventRe     = re.compile(r'^(\[.*?\])\s*(.*)$')
    kEncounterRe = re.compile(r'^(.*?)\s*(?:\[(.*?)\])?$')


    def __init__(self, prefix):
        deity.world.World.__init__(self, 'Kelestia/'+prefix)

        if '%s/CurMonth' % self.gdbPrefix not in GDB:
            GDB['%s/CurMonth' % self.gdbPrefix] = '-'

        self.weatherYear      = None
        self.weatherData      = None
        self.eventMonth       = None
        self.eventData        = dict()
        self.encounterCurrent = None
        self.encounterTime    = None


    #---------------------------------------------------------------------------


    def WeatherGetOutput(self, stamp=None):
        if stamp is None:
            stamp = self.timeCurrent

        if stamp['year'] != self.weatherYear:
            # load data for requested year
            wkey = '%s/WeatherData-%d' % (self.gdbPrefix, stamp['year'])
            wgen = len(self.kWeatherGen[0])
            if wkey in GDB:
                self.weatherData = GDB[wkey]
                #print 'load weather year:',wkey,self.weatherData
            else:
                ylkey = '%s/WeatherLastYear' % self.gdbPrefix
                if ylkey in GDB:
                    year = int(GDB[ylkey])
                    ykey = '%s/WeatherData-%d' % (self.gdbPrefix, year)
                    last = ord(GDB[ykey][-2:-1]) - 65
                else:
                    year = stamp['year'] - 1
                    last = random.randint(0, wgen-1)

                for y in xrange(min(year+1,stamp['year']), stamp['year']+1):
                    #print 'generate weather year:',y
                    next = last
                    daysperyear = self.kMonthsPerYear * self.kDaysPerMonth * self.kWeathersPerDay
                    yeardata = ''
                    for i in xrange(daysperyear):
                        # advance weather marker
                        rand = random.randint(1, 10)
                        if rand == 1:
                            next = (last - 1) % wgen
                        elif rand == 10:
                            next = (last + 2) % wgen
                        elif rand >= 8:
                            next = (last + 1) % wgen
                        else:
                            next = last
                        # choose wind velocity
                        rand = random.randint(1, 9+3+1)
                        if rand == 1:
                            wind = 2
                        elif rand <= 1+3:
                            wind = 1
                        else:
                            wind = 0
                        # append to stored data string
                        yeardata += chr(65 + next) + chr(65 + wind)
                        last = next

                    #print y,yeardata
                    ykey = '%s/WeatherData-%d' % (self.gdbPrefix, y)
                    GDB[ykey] = self.weatherData = yeardata
                    if y > year:
                        self.weatherYear = y
                        GDB[ylkey] = str(y)

        sdata = self.kWeatherGen[stamp['season']]
        wdidx = int((stamp['doy'] - self.kDayBase + stamp['abs'] % 1) * self.kWeathersPerDay) * 2
        wethr = ord(self.weatherData[wdidx]) - 65
        windi = ord(self.weatherData[wdidx+1]) - 65
        #print 'wdidx:',wdidx,'wethr:',wethr,'windi:',windi,'data:',self.weatherData[wdidx:wdidx+24]
        match = self.kWeatherRe.match(sdata[wethr])
        if not match:
            raise Exception('invalid weather specification "%s" (%d/%d)' % (sdata[wethr], stamp['season'], wethr))
        if match.group(2) and (stamp['hour'] < 8 or stamp['hour'] >= 20):
            temp = match.group(2)[1:-1]
        else:
            temp = match.group(1)
        wdir = match.group(3)
        wind = self.kWeatherWinds[int(match.group(4)) + windi]
        desc = match.group(6)

        return '%(temp)s, with %(dir)s %(wind)s, %(desc)s' % {
            'temp':P_('weather:',temp), 'dir': P_('weather:',wdir),
            'wind':P_('weather:',wind), 'desc':P_('weather:',desc),
        }


    def SpecialEventGetOutput(self, region=None):
        if not region:
            region = 'local'
        month = self.TimeGetOutput('%Y%02M')
        if month != self.eventMonth or region not in self.eventData:
            rkey = '%s/RegionalEvent-%s' % (self.gdbPrefix,region)
            mkey = '%s/CurMonth' % self.gdbPrefix
            if rkey in GDB and month == GDB[mkey]:
                #print 'SpecialEvent-Fetch:',region
                self.eventData[region] = GDB[rkey]
            else:
                #print 'SpecialEvent-Generate:',region
                event = deity.ListKeyLookup(self.kSpecialEvents[self.timeCurrent['season']],
                                            random.randint(1, 100))
                if event == '*':
                    event = deity.ListKeyLookup(self.kSpecialEvents[self.timeCurrent['season']],
                                                random.randint(1, 20)) + "\n" + \
                            deity.ListKeyLookup(self.kSpecialEvents[self.timeCurrent['season']],
                                                random.randint(1, 20))
                self.eventData[region] = event
                GDB[rkey] = event
                GDB[mkey] = month

        out = ''
        for e in self.eventData[region].split("\n"):
            if not e: continue
            if out:
                out += "\n"
            out += P_('event:', e)
        return out


    #---------------------------------------------------------------------------


    def _EncounterFindTable(self, etype, subtab):
        environ,daytime = etype.split('/')
        if environ[-1].isalnum():
            envs = (environ, '*')
        else:
            envs = (environ, environ[:-1], '*')
        if subtab:
            subtab = '/' + subtab
        else:
            subtab = ''
        for e in envs:
            for t in (daytime, '*'):
                table = '%s/%s%s' % (e, t, subtab)
                #print '- looking for:',table
                if table in self.kEncounterTables:
                    return table
        raise Exception('could not locate table for type "%s", subtab "%s"' % (etype,subtab))


    def _EncounterOutput(self, etype, subtab, random):
        table = self._EncounterFindTable(etype, subtab)
        rand  = random.randint(1, 100, table)
        item  = deity.ListKeyLookup(self.kEncounterTables[table], rand)
        eout  = ''
        match = self.kEncounterRe.match(item)
        if not match:
            raise Exception('invalid encounter entry "%s"' % item)
        if match.group(1) and match.group(1) != "":
            eout += P_('encounter:', match.group(1))
        if match.group(2):
            more = match.group(2)
            if more[0] == '*':
                goto = more[1:]
                if not goto:
                    goto = etype
                else:
                    if goto[-1] == '*':
                        environ,daytime = etype.split('/')
                        goto = goto[:-1] + daytime
                esub = self._EncounterOutput(goto, None, random)
                if esub:
                    if eout:
                        eout += P_('encounter:, ')
                    eout += esub
            else:
                for subtab in more.split('/'):
                    if not subtab: continue
                    esub = self._EncounterOutput(etype, subtab, random)
                    if esub:
                        eout += P_('encounter:, ')
                    eout += esub
        return eout


    def EncounterGetOutput(self, environ=None, stamp=None, random=None):
        if random is None:
            random = deity.trandom.Random()
        if stamp is None:
            stamp = self.timeCurrent
        if environ is None:
            environ = self.EnvironGet()
        etype = '%s/%s' % (environ, stamp['daytime'])
        return self._EncounterOutput(etype, None, random)


    #---------------------------------------------------------------------------


    def _EnvironGetEncounterOutput(self):
        enctime = '%d-%d-%d' % (self.timeCurrent['year'], self.timeCurrent['doy'], self.timeCurrent['dayincr'])
        #print 'EnvironGetEncounterOutput:',enctime,self.encounterTime
        if enctime == self.encounterTime:
            return self.encounterCurrent
        self.encounterTime = enctime
        rnum = random.randint(0, 20 * 13 - 1) % 20 + 1
        table = self.kEncounterTables['%s-chance' % self.timeCurrent['daytime']]
        if rnum < int(table[self.EnvironGet()]):
            self.encounterCurrent = _('(none)')
        else:
            self.encounterCurrent = self.EncounterGetOutput()
            if not self.encounterCurrent:
                self.encounterCurrent = _('(none)')
        return self.encounterCurrent


    def _EnvironGetEventOutput(self):
        date = self.TimeGetOutput('%02M-%02D')
        if date not in self.kSpecialDays:
            return _('(none)')
        out = ''
        for e in self.kSpecialDays[date].split("\n"):
            if not e: continue
            if out:
                out += "\n"
            match = self.kEventRe.match(e)
            if match:
                out += match.group(1) + ' '
                out += P_('event:',match.group(2))
        return out


    def _EnvironGetMoonOutput(self):
        day = self.timeCurrent['day']
        if self.timeCurrent['hour'] < 12:
            day = (day - 2) % 30 + 1
        if day == 15:
            return P_('moon:full')
        if day == 30:
            return P_('moon:new')
        if day < 15:
            return P_('moon:%d%% (waxing)') % (day * 100 // 15)
        else:
            return P_('moon:%d%% (waning)') % ((30-day) * 100 // 15)


    def _EnvironGetSeasonOutput(self):
        return P_(self.kSeasons[(self.timeCurrent['month'] - self.kMonthBase) / 3])


    def _EnvironGetSpecialEventOutput(self, region=None):
        event = self.SpecialEventGetOutput(region)
        if not event:
            return _('(none)')
        return event


    def _EnvironGetWeatherOutput(self):
        return self.WeatherGetOutput()


    def EnvironGetHtml(self):
        html = '<table border=0 cellpadding=0 cellspacing=2>'
        line = '<tr><td valign=top><b>%s:</b></td><td>%s</td></tr>'
        date = self.TimeGetOutput('%N %D, %Y')
        html+= line % (P_('heading:date'), date)
        time = self.TimeGetOutput('%02h:%02m')
        html+= line % (P_('heading:time'), time)
        html+= line % (P_('heading:season'), self._EnvironGetSeasonOutput())
        html+= line % (P_('heading:moon'), self._EnvironGetMoonOutput())
        html+= line % (P_('heading:weather'), self._EnvironGetWeatherOutput())

        item = self._EnvironGetEventOutput()
        if item != _('(none)'):
            item = '<font color="green">%s</font>' % item
        html+= line % (P_('heading:events'), item)

        item = self._EnvironGetSpecialEventOutput('local')
        if item != _('(none)'):
            item = '<font color="blue">%s</font>' % item
        html+= line % (P_('heading:local'), item)

        item = self._EnvironGetSpecialEventOutput('foreign')
        if item != _('(none)'):
            item = '<font color="blue">%s</font>' % item
        html+= line % (P_('heading:foreign'), item)

        item = self._EnvironGetEncounterOutput()
        if item != _('(none)'):
            item = '<font color="red">%s</font>' % item
        html+= line % (P_('heading:encounter'), item)

        html+= '</table>'
        html = html.replace("\n", "<br>")
        return html


###############################################################################
