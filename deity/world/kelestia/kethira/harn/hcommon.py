###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Kelestia/Kethira/Harn Common (generic functions, constants, etc.)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import deity.trandom
import deity.world.kelestia.kethira

from deity.xtype import *


###############################################################################


class Harn(deity.world.kelestia.kethira.Kethira):
    kEnvironments = xlist((
        NP_('abbr:safe'),
        NP_('abbr:urban')+'+', NP_('abbr:rural')+'+', NP_('abbr:road')+'+',
        NP_('abbr:urban')+'-', NP_('abbr:rural')+'-', NP_('abbr:road')+'-',
        NP_('abbr:sealane'),   NP_('abbr:opensea'),   NP_('abbr:river'),
        NP_('abbr:wilderness'),NP_('abbr:underworld'),
    ), xlate='env:')
    kEnvironInfo = {
        'safe':      { 'timestep': '1 days'    },
        'urban+':    { 'timestep': '1 minutes' },
        'urban-':    { 'timestep': '1 minutes' },
        'rural+':    { 'timestep': '4 hours'   },
        'rural-':    { 'timestep': '4 hours'   },
        'road+':     { 'timestep': '4 hours'   },
        'road-':     { 'timestep': '4 hours'   },
        'sealane':   { 'timestep': '4 hours'   },
        'opensea':   { 'timestep': '4 hours'   },
        'river':     { 'timestep': '4 hours'   },
        'wilderness':{ 'timestep': '4 hours'   },
        'underworld':{ 'timestep': '1 minutes' },
    }

    kWeatherGen = ((
        'cold N:1-3 '             + NP_('weather:overcast with flurries of sleet'),
        'cool NE:1-3 '            + NP_('weather:cloudy skies'),
        'warm (cool) SE:0-2 '     + NP_('weather:clear with fog/mist'),
        'warm SW:1-3 '            + NP_('weather:cloudy with light rain'),
        'cool NW:2-4 '            + NP_('weather:overcast with rain showers'),
        'cold NW:2-4 '            + NP_('weather:overcast skies'),
        'cold (freezing) SW:1-3 ' + NP_('weather:cloudy skies'),
        'cool SW:1-3 '            + NP_('weather:cloudy skies'),
        'cold NW:1-3 '            + NP_('weather:cloudy skies'),
        'freezing N:0-2 '         + NP_('weather:cloudy skies'),
        'cold (freezing) N:1-3 '  + NP_('weather:clear skies'),
        'cool (freezing) NE:1-3 ' + NP_('weather:cloudy skies'),
        'warm (cool) SE:0-2 '     + NP_('weather:clear with fog/mist'),
        'hot (warm) S:0-2 '       + NP_('weather:clear with fog/mist'),
        'warm SW:0-2 '            + NP_('weather:cloudy with thunderstorms (10% hail)'),
        'cool (cold) NW:1-3 '     + NP_('weather:clear skies'),
        'cool SW:2-4 '            + NP_('weather:cloudy with light rain'),
        'cool SW:2-4 '            + NP_('weather:overcast with heavy rain'),
        'cold NW:2-4 '            + NP_('weather:cloudy with flurries of sleet'),
        'cold NW:1-3 '            + NP_('weather:overcast skies'),
    ), (
        'cool N:0-2 '             + NP_('weather:cloudy skies'),
        'warm NE:0-2 '            + NP_('weather:cloudy with light rain'),
        'hot (cool) SE:0-2 '      + NP_('weather:clear skies'),
        'hot (warm) S:0-2 '       + NP_('weather:clear skies'),
        'warm SW:0-2 '            + NP_('weather:cloudy with thunderstorms (10% hail)'),
        'warm (cool) S:0-2 '      + NP_('weather:cloudy skies'),
        'cool SW:1-3 '            + NP_('weather:overcast with heavy rain'),
        'cool NW:2-4 '            + NP_('weather:overcast with rain showers'),
        'warm (cool) SW:1-3 '     + NP_('weather:cloudy skies'),
        'warm (cool) NW:1-3 '     + NP_('weather:clear skies'),
        'warm (cool) N:1-3 '      + NP_('weather:clear skies'),
        'hot (cool) NE:0-2 '      + NP_('weather:clear with fog/mist'),
        'hot (warm) SE:0-2 '      + NP_('weather:cloudy with thunderstorms (10% hail)'),
        'warm (cool) S:0-2 '      + NP_('weather:cloudy skies'),
        'warm SW:0-2 '            + NP_('weather:cloudy skies'),
        'warm (cool) SW:1-3 '     + NP_('weather:cloudy skies'),
        'cool SW:2-4 '            + NP_('weather:overcast skies'),
        'cool SW:1-3 '            + NP_('weather:overcast with rain showers'),
        'cool SW:1-3 '            + NP_('weather:overcast with steady rain'),
        'cold NW:0-2 '            + NP_('weather:overcast with heavy rain'),
    ), (
        'cool (cold) N:0-2 '      + NP_('weather:clear with fog/mist'),
        'warm (cool) N:0-2 '      + NP_('weather:cloudy skies'),
        'warm (cool) NE:0-2 '     + NP_('weather:clear with fog/mist'),
        'hot (warm) SE:0-2 '      + NP_('weather:clear with fog/mist'),
        'hot (warm) SW:0-2 '      + NP_('weather:cloudy with thunderstorms (10% hail)'),
        'warm SW:0-2 '            + NP_('weather:overcast with rain showers'),
        'cool NW:1-3 '            + NP_('weather:overcast with steady rain'),
        'cool SW:1-3 '            + NP_('weather:overcast with heavy rain'),
        'cool NW:2-4 '            + NP_('weather:overcast with steady rain'),
        'cold NW:1-3 '            + NP_('weather:overcast skies'),
        'cold N:2-4 '             + NP_('weather:overcast skies'),
        'cool NE:1-3 '            + NP_('weather:overcast with rain showers'),
        'warm (cool) SE:0-2 '     + NP_('weather:cloudy with fog/mist'),
        'cool S:1-3 '             + NP_('weather:overcast with steady rain'),
        'cool SW:2-4 '            + NP_('weather:overcast skies'),
        'warm (cool) S:1-3 '      + NP_('weather:cloudy with light rain'),
        'warm (cool) SW:1-3 '     + NP_('weather:clear skies'),
        'cool (cold) NW:2-4 '     + NP_('weather:cloudy skies'),
        'cold (freezing) SW:2-4 ' + NP_('weather:overcast with snow/sleet'),
        'freezing NW:1-3 '        + NP_('weather:cloudy skies'),
    ), (
        'cold (freezing) N:0-2 '  + NP_('weather:overcast with steady snow'),
        'freezing NW:1-3 '        + NP_('weather:overcast with snow flurries'),
        'cold N:2-4 '             + NP_('weather:overcast skies'),
        'cool (cold) NE:1-3 '     + NP_('weather:cloudy skies'),
        'warm (cold) SE:0-2 '     + NP_('weather:clear with fog/mist'),
        'cool (freezing) S:0-2 '  + NP_('weather:cloudy with light rain'),
        'cold SW:1-3 '            + NP_('weather:overcast with steady rain'),
        'cold NW:0-2 '            + NP_('weather:overcast with sleet flurries'),
        'cool SW:1-3 '            + NP_('weather:overcast with rain showers'),
        'cold NW:1-3 '            + NP_('weather:overcast with snow flurries'),
        'cold N:2-4 '             + NP_('weather:overcast with steady snow'),
        'freezing N:1-3 '         + NP_('weather:overcast skies'),
        'cool (freezing) NE:2-4 ' + NP_('weather:clear skies'),
        'cool (cold) SE:1-3 '     + NP_('weather:cloudy skies'),
        'cool (freezing) S:1-3 '  + NP_('weather:clear skies'),
        'cool (cold) SW:2-4 '     + NP_('weather:cloudy with light rain'),
        'cold (freezing) NW:1-3 ' + NP_('weather:overcast with steady snow'),
        'cold SW:2-4 '            + NP_('weather:overcast with snow flurries'),
        'cold SW:1-3 '            + NP_('weather:cloudy skies'),
        'cold NW:1-3 '            + NP_('weather:overcast skies'),
    ))

    kWeatherWinds = (NP_('weather:calm(0-8)'),    NP_('weather:breeze(8-24)'),
                     NP_('weather:winds(24-48)'), NP_('weather:gale(48-88)'),
                     NP_('weather:storm(88+)'))

    kSpecialDays = {
        #Nuzyael
        '01-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '01-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '01-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '01-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '01-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '01-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '01-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '01-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '01-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '01-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '01-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '01-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '01-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '01-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '01-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '01-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Peonu
        '02-04':'[Peoni] '      + NP_('event:Restoration Festival'),
        '02-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '02-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '02-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '02-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '02-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '02-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '02-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '02-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '02-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '02-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '02-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '02-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '02-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '02-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '02-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Kelen
        '03-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '03-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '03-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '03-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '03-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '03-10':'[Sarajin] '    + NP_('event:Ilbengaad Festival'),
        '03-11':'[Sarajin] '    + NP_('event:Ilbengaad Festival'),
        '03-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)') + "\n" + \
                '[Sarajin] '    + NP_('event:Ilbengaad Festival'),
        '03-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '03-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '03-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '03-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '03-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '03-19':'[Sarajin] '    + NP_('event:Bjarri\'s Feast (7 days)'),
        '03-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)') + "\n" + \
                '[Sarajin] '    + NP_('event:Bjarri\'s Feast (6 days)'),
        '03-21':'[Sarajin] '    + NP_('event:Bjarri\'s Feast (5 days)'),
        '03-22':'[Sarajin] '    + NP_('event:Bjarri\'s Feast (4 days)'),
        '03-23':'[Sarajin] '    + NP_('event:Bjarri\'s Feast (3 days)'),
        '03-24':'[Sarajin] '    + NP_('event:Bjarri\'s Feast (2 days)'),
        '03-25':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Sarajin] '    + NP_('event:Bjarri\'s Feast'),
        '03-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '03-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '03-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Nolus
        '04-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '04-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '04-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '04-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '04-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '04-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '04-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '04-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '04-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '04-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '04-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '04-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '04-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '04-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '04-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '04-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Larane
        '05-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '05-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '05-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '05-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '05-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '05-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '05-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '05-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '05-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '05-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '05-17':'[Larani] '     + NP_('event:Feast of Saint Ambrathas') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '05-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '05-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '05-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '05-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '05-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Agrazhar
        '06-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '06-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '06-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '06-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '06-08':'[Agrik] '      + NP_('event:Feast of Balefile'),
        '06-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '06-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '06-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '06-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '06-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '06-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '06-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '06-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '06-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '06-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '06-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Azura
        '07-04':'[Peoni] '      + NP_('event:Harvest Home Festival'),
        '07-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '07-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '07-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '07-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '07-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '07-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '07-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '07-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of Golden Twilight (high mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '07-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '07-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '07-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '07-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '07-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '07-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '07-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Halane
        '08-01':'[Halea] '      + NP_('event:The Banquet of Delight Festival (7 days)'),
        '08-02':'[Halea] '      + NP_('event:The Banquet of Delight Festival (6 days)'),
        '08-03':'[Halea] '      + NP_('event:The Banquet of Delight Festival (5 days)'),
        '08-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)') + "\n" + \
                '[Halea] '      + NP_('event:The Banquet of Delight Festival (4 days)'),
        '08-05':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Halea] '      + NP_('event:The Banquet of Delight Festival (3 days)'),
        '08-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)') + "\n" + \
                '[Halea] '      + NP_('event:The Banquet of Delight Festival (2 days)'),
        '08-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)') + "\n" + \
                '[Halea] '      + NP_('event:The Banquet of Delight Festival'),
        '08-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '08-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '08-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '08-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '08-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '08-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '08-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '08-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '08-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '08-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '08-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '08-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                                   '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Savor
        '09-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '09-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '09-06':"[Save-K'nor] " + NP_('event:Keserne (Saints mass)'),
        '09-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '09-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '09-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '09-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '09-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '09-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '09-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '09-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '09-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '09-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '09-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '09-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '09-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),

        # Ilvin
        '10-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '10-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '10-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '10-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '10-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '10-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '10-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '10-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '10-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '10-16':'[Ilvir] '      + NP_('event:The Araksin Festival (15 days)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '10-17':'[Ilvir] '      + NP_('event:The Araksin Festival (14 days)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '10-18':'[Ilvir] '      + NP_('event:The Araksin Festival (13 days)'),
        '10-19':'[Ilvir] '      + NP_('event:The Araksin Festival (12 days)'),
        '10-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)') + "\n" + \
                '[Ilvir] '      + NP_('event:The Araksin Festival (11 days)'),
        '10-21':'[Ilvir] '      + NP_('event:The Araksin Festival (10 days)'),
        '10-22':'[Ilvir] '      + NP_('event:The Araksin Festival (9 days)'),
        '10-23':'[Ilvir] '      + NP_('event:The Araksin Festival (8 days)'),
        '10-24':'[Ilvir] '      + NP_('event:The Araksin Festival (7 days)'),
        '10-25':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Ilvir] '      + NP_('event:The Araksin Festival (6 days)'),
        '10-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)') + "\n" + \
                '[Ilvir] '      + NP_('event:The Araksin Festival (5 days)'),
        '10-27':'[Ilvir] '      + NP_('event:The Araksin Festival (4 days)'),
        '10-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)') + "\n" + \
                '[Ilvir] '      + NP_('event:The Araksin Festival (3 days)'),
        '10-29':'[Ilvir] '      + NP_('event:The Araksin Festival (2 days)'),
        '10-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)') + "\n" + \
                '[Ilvir] '      + NP_('event:The Araksin Festival'),

        # Navek
        '11-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '11-05':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '11-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)'),
        '11-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)'),
        '11-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)'),
        '11-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '11-13':'[Morgath] '    + NP_('event:Degrees of Nyardath (high mass)'),
        '11-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '11-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '11-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '11-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '11-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '11-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '11-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '11-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '11-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Night of Shadows'),

        # Morgat
        '12-01':'[Morgath] '    + NP_('event:The Sacrament of Kobukrai (13 days)') + "\n" + \
                '[Naveh] '      + NP_('event:Shadowmath'),
        '12-02':'[Morgath] '    + NP_('event:The Feast of Bukrai (12 days)'),
        '12-03':'[Morgath] '    + NP_('event:The Feast of Bukrai (11 days)'),
        '12-04':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)') + "\n" + \
                '[Morgath] '    + NP_('event:The Feast of Bukrai (10 days)'),
        '12-05':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Morgath] '    + NP_('event:The Feast of Bukrai (9 days)'),
        '12-06':"[Save-K'nor] " + NP_('event:Velere (lay mass)') + "\n" + \
                '[Morgath] '    + NP_('event:The Feast of Bukrai (8 days)'),
        '12-07':'[Halea] '      + NP_('event:Shesneala Day (lay mass)') + "\n" + \
                '[Morgath] '    + NP_('event:The Feast of Bukrai (7 days)'),
        '12-08':'[Agrik] '      + NP_('event:Low Ceremony of the Balefile (lay mass)') + "\n" + \
                '[Morgath] '    + NP_('event:The Feast of Bukrai (6 days)'),
        '12-09':'[Morgath] '    + NP_('event:The Feast of Bukrai (5 days)'),
        '12-10':'[Morgath] '    + NP_('event:The Feast of Bukrai (4 days)'),
        '12-11':'[Morgath] '    + NP_('event:The Feast of Bukrai (3 days)'),
        '12-12':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)') + "\n" + \
                '[Morgath] '    + NP_('event:The Feast of Bukrai (2 days)'),
        '12-13':'[Morgath] '    + NP_('event:The Liturgy of Vabukrai'),
        '12-14':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 4 days)'),
        '12-15':'[Larani] '     + NP_('event:Soratir (lay mass)') + "\n" + \
                '[Siem] '       + NP_('event:Night of High Perspective (lay mass)') + "\n" + \
                '[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 3 days)'),
        '12-16':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual: 2 days)'),
        '12-17':'[Peoni] '      + NP_('event:Greater Sapelah (purification ritual)'),
        '12-20':'[Peoni] '      + NP_('event:Lesser Sapelah (lay mass)'),
        '12-25':'[Larani] '     + NP_('event:Soratir (lay mass)'),
        '12-26':'[Morgath] '    + NP_('event:Shadryn-Vars (lay mass)'),
        '12-28':'[Peoni] '      + NP_('event:Lesser Sepelah (lay mass)'),
        '12-30':'[Siem] '       + NP_('event:Night of Silent Renewal (lay mass)') + "\n" + \
                '[Naveh] '      + NP_('event:Dezenaka (high mass)'),
    }

    kSpecialEvents = ((
        # Spring
         '4=' + NP_('event:edict (inquisition/witch-hunt, persecution, royal proclamation, scandal, tax levy)'),
         '6=' + NP_('event:civil unrest (major intrigue, riots/rebellion)'),
         '7=' + NP_('event:death/illness (assasination attempt, sickness/death of personage)'),
        '12=' + NP_('event:war/raids (border/pirate/viking raid, civil war, invasion/foreign war)'),
        '15=' + NP_('event:terrorization (crime/crimewave, major brigandage, marauding creature)'),
        '17=' + NP_('event:epidemic (plague/infestation -- may spread)'),
        '19=' + NP_('event:disaster (avalanche, earthquake, fire, flood, landslide, meteor, mudslide, collapse/damage to major bridge/building)'),
        '20=' + NP_('event:freak weather (blizzard, cold snap, draught, heatwave, hurricate, lightnig strike, tornado)'),
        '22=*',
        '100='
    ), (
        # Summer
         '3=' + NP_('event:state occasion (wedding/birth, tournament/contenst, dignitary visit)'),
         '5=' + NP_('event:edict (inquisition/witch-hunt, persecution, royal proclamation, scandal, tax levy)'),
         '7=' + NP_('event:civil unrest (major intrigue, riots/rebellion)'),
         '8=' + NP_('event:death/illness (assasination attempt, sickness/death of personage)'),
        '11=' + NP_('event:war/raids (border/pirate/viking raid, civil war, invasion/foreign war)'),
        '14=' + NP_('event:terrorization (crime/crimewave, major brigandage, marauding creature)'),
        '17=' + NP_('event:epidemic (plague/infestation -- may spread)'),
        '19=' + NP_('event:disaster (avalanche, earthquake, fire, flood, landslide, meteor, mudslide, collapse/damage to major bridge/building)'),
        '20=' + NP_('event:freak weather (blizzard, cold snap, draught, heatwave, hurricate, lightnig strike, tornado)'),
        '22=*',
        '100='
    ), (
        # Fall
         '2=' + NP_('event:state occasion (wedding/birth, tournament/contenst, dignitary visit)'),
         '4=' + NP_('event:edict (inquisition/witch-hunt, persecution, royal proclamation, scandal, tax levy)'),
         '6=' + NP_('event:civil unrest (major intrigue, riots/rebellion)'),
         '7=' + NP_('event:death/illness (assasination attempt, sickness/death of personage)'),
         '9=' + NP_('event:war/raids (border/pirate/viking raid, civil war, invasion/foreign war)'),
        '12=' + NP_('event:terrorization (crime/crimewave, major brigandage, marauding creature)'),
        '13=' + NP_('event:epidemic (plague/infestation -- may spread)'),
        '15=' + NP_('event:good harvest (population growth, price collapse)'),
        '17=' + NP_('event:poor harvest (animal/crop blight, famine/shortage, food riots, high prices)'),
        '19=' + NP_('event:disaster (avalanche, earthquake, fire, flood, landslide, meteor, mudslide, collapse/damage to major bridge/building)'),
        '20=' + NP_('event:freak weather (blizzard, cold snap, draught, heatwave, hurricate, lightnig strike, tornado)'),
        '22=*',
        '100='
    ), (
        # Winter
         '1=' + NP_('event:state occasion (wedding/birth, tournament/contenst, dignitary visit)'),
         '3=' + NP_('event:edict (inquisition/witch-hunt, persecution, royal proclamation, scandal, tax levy)'),
         '5=' + NP_('event:civil unrest (major intrigue, riots/rebellion)'),
         '6=' + NP_('event:death/illness (assasination attempt, sickness/death of personage)'),
         '8=' + NP_('event:war/raids (border/pirate/viking raid, civil war, invasion/foreign war)'),
        '13=' + NP_('event:terrorization (crime/crimewave, major brigandage, marauding creature)'),
        '17=' + NP_('event:epidemic (plague/infestation -- may spread)'),
        '19=' + NP_('event:disaster (avalanche, earthquake, fire, flood, landslide, meteor, mudslide, collapse/damage to major bridge/building)'),
        '20=' + NP_('event:freak weather (blizzard, cold snap, draught, heatwave, hurricate, lightnig strike, tornado)'),
        '22=*',
        '100='
    ))

    #==========================================================================

    kEncounterTables = {
        'day-chance': {
            "urban+"     : 16,
            "urban-"     : 16,
            "rural+"     : 14,
            "rural-"     : 14,
            "road+"      : 17,
            "road-"      : 17,
            "wilderness" : 19,
            "river"      : 18,
            "sealane"    : 19,
            "opensea"    : 20,
            "underworld" : 20,
            "safe"       : 20,
        },

        'night-chance': {
            "urban+"     : 19,
            "urban-"     : 19,
            "rural+"     : 19,
            "rural-"     : 19,
            "road+"      : 19,
            "road-"      : 19,
            "wilderness" : 20,
            "river"      : 19,
            "sealane"    : 19,
            "opensea"    : 20,
            "underworld" : 20,
            "safe"       : 20,
        },

        #----------------------------------------------------------------------

        'safe/*': (
            '100=',
        ),

        #----------------------------------------------------------------------

        'urban+/day': (
            '1='+NP_('encounter:beggar/cripple/etc')+'[1]',
            '2='+NP_('encounter:cartographer/artist')+'[1]',
            '7='+NP_('encounter:cleric/etc')+'[4]',
            '9='+NP_('encounter:crier (news/edict/etc)'),
            '11='+NP_('encounter:servant/cook/etc')+'[1]',
            '25='+NP_('encounter:farmer/etc')+'[6]',
            '30='+NP_('encounter:fisherman/monger')+'[1]',
            '33='+NP_('encounter:foreigner')+'[*]',
            '55='+NP_('encounter:guildsman')+'[3]',
            '57='+NP_('encounter:hunter/trapper/etc')+'[1]',
            '58='+NP_('encounter:items from window...'),
            '63='+NP_('encounter:Lia-Kavair')+'[3a/5]',
            '68='+NP_('encounter:laborer/etc')+'[1]',
            '78='+NP_('encounter:military')+'[8]',
            '83='+NP_('encounter:mob/crowd/assembly')+'[2]',
            '85='+NP_('encounter:noble/personage')+'[7]',
            '88='+NP_('encounter:local official')+'[9]',
            '89='+NP_('encounter:mercantyler')+'[3a/1]',
            '90='+NP_('encounter:pimp procuring/etc'),
            '91='+NP_('encounter:prostitute'),
            '92='+NP_('encounter:ratter/scavenger')+'[1]',
            '93='+NP_('encounter:scribe/scholar')+'[1]',
            '94='+NP_('encounter:slaver-mercantyler')+'[1]',
            '95='+NP_('encounter:teamster')+'[3a/1]',
            '96='+NP_('encounter:toymaker')+'[1]',
            '97='+NP_('encounter:unguilded peddler')+'[1]',
            '98='+NP_('encounter:unguilded criminal')+'[5]',
            '100='+NP_('encounter:urchins/children')+'[1/2]',
        ),

        'urban+/night': (
            '1='+NP_('encounter:beggar/cripple/etc')+'[1]',
            '2='+NP_('encounter:cartographer/artist')+'[1]',
            '7='+NP_('encounter:cleric/etc')+'[4]',
            '8='+NP_('encounter:crier (news/edict/etc)'),
            '9='+NP_('encounter:dog/rats')+'[11]',
            '10='+NP_('encounter:servant/cook/etc')+'[1]',
            '13='+NP_('encounter:farmer/etc')+'[6]',
            '14='+NP_('encounter:fisherman/monger')+'[1]',
            '15='+NP_('encounter:foreigner')+'[*]',
            '26='+NP_('encounter:guildsman')+'[3]',
            '27='+NP_('encounter:hunter/trapper/etc')+'[1]',
            '32='+NP_('encounter:items from window...'),
            '44='+NP_('encounter:Lia-Kavair')+'[3a/5]',
            '45='+NP_('encounter:laborer/etc')+'[1]',
            '61='+NP_('encounter:military')+'[8]',
            '62='+NP_('encounter:mob/crowd/assembly')+'[2]',
            '63='+NP_('encounter:noble/personage')+'[7]',
            '64='+NP_('encounter:local official')+'[9]',
            '65='+NP_('encounter:mercantyler')+'[3a/1]',
            '67='+NP_('encounter:pimp procuring/etc'),
            '77='+NP_('encounter:prostitute'),
            '81='+NP_('encounter:ratter/scavenger')+'[1]',
            '83='+NP_('encounter:street ruffian')+'[1/5]',
            '84='+NP_('encounter:scribe/scholar')+'[1]',
            '86='+NP_('encounter:slaver-mercantyler')+'[1]',
            '91='+NP_('encounter:street cleaner'),
            '92='+NP_('encounter:teamster')+'[3a/1]',
            '93='+NP_('encounter:toymaker')+'[1]',
            '94='+NP_('encounter:unguilded peddler')+'[1]',
            '99='+NP_('encounter:unguilded criminal')+'[5]',
            '100='+NP_('encounter:urchins/children')+'[1/2]',
        ),

        'urban-/day': (
            '2='+NP_('encounter:beggar/cripple/etc')+'[1]',
            '3='+NP_('encounter:cartographer/artist')+'[1]',
            '8='+NP_('encounter:cleric/etc')+'[4]',
            '9='+NP_('encounter:crier (news/edict/etc)'),
            '10='+NP_('encounter:dog/rats')+'[11]',
            '11='+NP_('encounter:servant/cook/etc')+'[1]',
            '20='+NP_('encounter:farmer/etc')+'[6]',
            '24='+NP_('encounter:fisherman/monger')+'[1]',
            '27='+NP_('encounter:foreigner')+'[*]',
            '42='+NP_('encounter:guildsman')+'[3]',
            '44='+NP_('encounter:hunter/trapper/etc')+'[1]',
            '57='+NP_('encounter:items from window...'),
            '58='+NP_('encounter:Lia-Kavair')+'[3a/5]',
            '62='+NP_('encounter:laborer/etc')+'[1]',
            '70='+NP_('encounter:military')+'[8]',
            '76='+NP_('encounter:mob/crowd/assembly')+'[2]',
            '78='+NP_('encounter:noble/personage')+'[7]',
            '80='+NP_('encounter:local official')+'[9]',
            '81='+NP_('encounter:mercantyler')+'[3a/1]',
            '84='+NP_('encounter:pimp procuring/etc'),
            '88='+NP_('encounter:prostitute'),
            '89='+NP_('encounter:ratter/scavenger')+'[1]',
            '90='+NP_('encounter:street ruffian')+'[1/5]',
            '91='+NP_('encounter:scribe/scholar')+'[1]',
            '93='+NP_('encounter:slaver-mercantyler')+'[1]',
            '94='+NP_('encounter:street cleaner'),
            '95='+NP_('encounter:teamster')+'[3a/1]',
            '96='+NP_('encounter:toymaker')+'[1]',
            '97='+NP_('encounter:unguilded peddler')+'[1]',
            '98='+NP_('encounter:unguilded criminal')+'[5]',
            '100='+NP_('encounter:urchins/children')+'[1/2]',
        ),

        'urban-/night': (
            '3='+NP_('encounter:beggar/cripple/etc')+'[1]',
            '4='+NP_('encounter:cartographer/artist')+'[1]',
            '9='+NP_('encounter:cleric/etc')+'[4]',
            '10='+NP_('encounter:crier (news/edict/etc)'),
            '12='+NP_('encounter:dog/rats')+'[11]',
            '13='+NP_('encounter:servant/cook/etc')+'[1]',
            '15='+NP_('encounter:farmer/etc')+'[6]',
            '16='+NP_('encounter:fisherman/monger')+'[1]',
            '17='+NP_('encounter:foreigner')+'[*]',
            '27='+NP_('encounter:guildsman')+'[3]',
            '28='+NP_('encounter:hunter/trapper/etc')+'[1]',
            '34='+NP_('encounter:items from window...'),
            '54='+NP_('encounter:Lia-Kavair')+'[3a/5]',
            '55='+NP_('encounter:laborer/etc')+'[1]',
            '61='+NP_('encounter:military')+'[8]',
            '63='+NP_('encounter:mob/crowd/assembly')+'[2]',
            '64='+NP_('encounter:noble/personage')+'[7]',
            '65='+NP_('encounter:local official')+'[9]',
            '66='+NP_('encounter:mercantyler')+'[3a/1]',
            '69='+NP_('encounter:pimp procuring/etc'),
            '75='+NP_('encounter:prostitute'),
            '77='+NP_('encounter:ratter/scavenger')+'[1]',
            '86='+NP_('encounter:street ruffian')+'[1/5]',
            '87='+NP_('encounter:scribe/scholar')+'[1]',
            '88='+NP_('encounter:slaver-mercantyler')+'[1]',
            '91='+NP_('encounter:street cleaner'),
            '92='+NP_('encounter:teamster')+'[3a/1]',
            '93='+NP_('encounter:toymaker')+'[1]',
            '94='+NP_('encounter:unguilded peddler')+'[1]',
            '99='+NP_('encounter:unguilded criminal')+'[5]',
            '100='+NP_('encounter:urchins/children')+'[1/2]',
        ),

        #------------------------------------------------------------------

        'rural+/day': (
            '5='+NP_('encounter:local lord/escort')+'[1/8/10]',
            '55='+NP_('encounter:farmer/etc')+'[6]',
            '60='+NP_('encounter:forester')+'[1]',
            '62='+NP_('encounter:poacher/hunter')+'[1]',
            '75=[*road+/day]',
            '95=[*urban+/day]',
            '100=[*wilderness/day]',
        ),

        'rural+/night': (
            '2='+NP_('encounter:local lord/escort')+'[1/8/10]',
            '20='+NP_('encounter:farmer/etc')+'[6]',
            '40='+NP_('encounter:forester')+'[1]',
            '60='+NP_('encounter:poacher/hunter')+'[1]',
            '70=[*road+/night]',
            '90=[*urban+/night]',
            '100=[*wilderness/night]',
        ),

        'rural/day': (
            '5='+NP_('encounter:local lord/escort')+'[1/8/10]',
            '50='+NP_('encounter:farmer/etc')+'[6]',
            '55='+NP_('encounter:forester')+'[1]',
            '65='+NP_('encounter:poacher/hunter')+'[1]',
            '75=[*road-/day]',
            '90=[*urban-/day]',
            '100=[*wilderness/day]',
        ),

        'rural-/night': (
            '2='+NP_('encounter:local lord/escort')+'[1/8/10]',
            '20='+NP_('encounter:farmer/etc')+'[6]',
            '35='+NP_('encounter:forester')+'[1]',
            '60='+NP_('encounter:poacher/hunter')+'[1]',
            '70=[*road-/night]',
            '85=[*urban-/night]',
            '100=[*wilderness/night]',
        ),

        #----------------------------------------------------------------------

        'road+/day': (
            '10=[*wilderness/day]',
            '25=[*rural+/day]',
            '35=[*urban+/day]',
            '45='+NP_('encounter:journeyman')+'[3b]',
            '55='+NP_('encounter:caravan with escort'),
            '57='+NP_('encounter:peddler/mercantyler')+'[1]',
            '60='+NP_('encounter:peddler/mercantyler')+'[3b]',
            '65='+NP_('encounter:cleric/etc')+'[4]',
            '70='+NP_('encounter:forester')+'[1]',
            '80='+NP_('encounter:soldiers on patrol'),
            '85='+NP_('encounter:brigands/highwayman')+'[1]',
            '90='+NP_('encounter:military')+'[8]',
            '95='+NP_('encounter:personage')+'[7]',
            '100='+NP_('encounter:adventurer')+'[10]',
        ),

        'road+/night': (
            '35=[*wilderness/night]',
            '60=[*rural+/night]',
            '63=[*urban+/night]',
            '65='+NP_('encounter:journeyman')+'[3b]',
            '66='+NP_('encounter:caravan with escort'),
            '67='+NP_('encounter:peddler/mercantyler')+'[1]',
            '68='+NP_('encounter:peddler/mercantyler')+'[3b]',
            '70='+NP_('encounter:cleric/etc')+'[4]',
            '80='+NP_('encounter:forester')+'[1]',
            '85='+NP_('encounter:soldiers on patrol'),
            '94='+NP_('encounter:brigands/highwayman')+'[1]',
            '96='+NP_('encounter:military')+'[8]',
            '98='+NP_('encounter:personage')+'[7]',
            '100='+NP_('encounter:adventurer')+'[10]',
        ),

        'road-/day': (
            '15=[*wilderness/day]',
            '30=[*rural-/day]',
            '35=[*urban-/day]',
            '40='+NP_('encounter:journeyman')+'[3b]',
            '50='+NP_('encounter:caravan with escort'),
            '51='+NP_('encounter:peddler/mercantyler')+'[1]',
            '53='+NP_('encounter:peddler/mercantyler')+'[3b]',
            '57='+NP_('encounter:cleric/etc')+'[4]',
            '60='+NP_('encounter:forester')+'[1]',
            '70='+NP_('encounter:soldiers on patrol'),
            '90='+NP_('encounter:brigands/highwayman')+'[1]',
            '95='+NP_('encounter:military')+'[8]',
            '97='+NP_('encounter:personage')+'[7]',
            '100='+NP_('encounter:adventurer')+'[10]',
        ),

        'road-/night': (
            '40=[*wilderness/night]',
            '70=[*rural-/night]',
            '72=[*urban-/night]',
            '74='+NP_('encounter:journeyman')+'[3b]',
            '75='+NP_('encounter:caravan with escort'),
            '76='+NP_('encounter:peddler/mercantyler')+'[3b]',
            '79='+NP_('encounter:cleric/etc')+'[4]',
            '81='+NP_('encounter:forester')+'[1]',
            '83='+NP_('encounter:soldiers on patrol'),
            '94='+NP_('encounter:brigands/highwayman')+'[1]',
            '96='+NP_('encounter:military')+'[8]',
            '97='+NP_('encounter:personage')+'[7]',
            '100='+NP_('encounter:adventurer')+'[10]',
        ),

        #----------------------------------------------------------------------

        'wilderness/day': (
            '20='+NP_('encounter:tracks/spore/sounds')+'[*]',
            '45='+NP_('encounter:local tribe/patrol/etc'),
            '47='+NP_('encounter:wild dog/wolf/etc')+'[11]',
            '49='+NP_('encounter:mountain lion/wild cat/etc')+'[11]',
            '51='+NP_('encounter:bear')+'[11]',
            '54='+NP_('encounter:stag/deer/hind/etc')+'[11]',
            '55='+NP_('encounter:wild/stray cattle/etc')+'[11]',
            '57='+NP_('encounter:wild/stray sheep/goats/etc')+'[11]',
            '59='+NP_('encounter:wild/stray boar/pig/etc')+'[11]',
            '61='+NP_('encounter:eagle/hawk/falcon/etc')+'[11]',
            '62='+NP_('encounter:equine')+'[12]',
            '63='+NP_('encounter:reptile')+'[13]',
            '64='+NP_('encounter:Ivashu')+'[15]',
            '65='+NP_('encounter:ethereal')+'[16]',
            '65='+NP_('encounter:Dryad (forest only)'),
            '82='+NP_('encounter:human adventurer')+'[10]',
            '83='+NP_('encounter:Gargun (wandering band)')+'[10]',
            '84='+NP_('encounter:Gargun (wandering band)')+'[1]',
            '85='+NP_('encounter:Khuzdul adventurer')+'[10]',
            '86='+NP_('encounter:Sindarin adventurer')+'[10]',
            '88='+NP_('encounter:landslide/bog/etc'),
            '90='+NP_('encounter:one/more of party gets lost'),
            '92='+NP_('encounter:spoilage/loss of food/etc'),
            '94='+NP_('encounter:lame horse or equipment loss/failure'),
            '96='+NP_('encounter:sickness/food poisoning/etc'),
            '98='+NP_('encounter:mutiny/disennt/argument/etc'),
            '99='+NP_('encounter:plant hazard (poison ivy/etc)'),
            '100='+NP_('encounter:slime/mold/fungus')+'[14]',
        ),

        'wilderness/night': (
            '15='+NP_('encounter:tracks/spore/sounds')+'[*]',
            '45='+NP_('encounter:local tribe/patrol/etc'),
            '48='+NP_('encounter:wild dog/wolf/etc')+'[11]',
            '51='+NP_('encounter:mountain lion/wild cat/etc')+'[11]',
            '53='+NP_('encounter:bear')+'[11]',
            '55='+NP_('encounter:stag/deer/hind/etc')+'[11]',
            '57='+NP_('encounter:wild/stray cattle/etc')+'[11]',
            '59='+NP_('encounter:wild/stray sheep/goats/etc')+'[11]',
            '61='+NP_('encounter:wild/stray boar/pig/etc')+'[11]',
            '62='+NP_('encounter:eagle/hawk/falcon/etc')+'[11]',
            '63='+NP_('encounter:equine')+'[11/12]',
            '66='+NP_('encounter:reptile')+'[13/11]',
            '69='+NP_('encounter:Ivashu')+'[15/11]',
            '73='+NP_('encounter:ethereal')+'[16]',
            '75='+NP_('encounter:Dryad (forest only)'),
            '79='+NP_('encounter:human adventurer')+'[10]',
            '85='+NP_('encounter:Gargun (wandering band)')+'[10]',
            '88='+NP_('encounter:Gargun (wandering band)')+'[1]',
            '89='+NP_('encounter:Khuzdul adventurer')+'[10]',
            '91='+NP_('encounter:Sindarin adventurer')+'[10]',
            '92='+NP_('encounter:landslide/bog/etc'),
            '93='+NP_('encounter:one/more of party gets lost'),
            '94='+NP_('encounter:spoilage/loss of food/etc'),
            '95='+NP_('encounter:lame horse or equipment loss/failure'),
            '97='+NP_('encounter:sickness/food poisoning/etc'),
            '98='+NP_('encounter:mutiny/disennt/argument/etc'),
            '99='+NP_('encounter:plant hazard (poison ivy/etc)'),
            '100='+NP_('encounter:slime/mold/fungus')+'[14]',
        ),

        #----------------------------------------------------------------------

        'river/*': (
            '34='+NP_('encounter:local fishing boat'),
            '35='+NP_('encounter:foreign fishing boat'),
            '65='+NP_('encounter:local merchantman'),
            '75='+NP_('encounter:foreign merchantman'),
            '80='+NP_('encounter:pirate/privateer/etc'),
            '84='+NP_('encounter:local warship'),
            '85='+NP_('encounter:foreign warship'),
            '90='+NP_('encounter:mutiny/disent/argument'),
            '94='+NP_('encounter:maelstrom/feak current'),
            '96='+NP_('encounter:fire on board'),
            '98='+NP_('encounter:equipment failure'),
            '99='+NP_('encounter:food and/or water spoilage'),
            '99='+NP_('encounter:killer whale (orca)'),
            '99='+NP_('encounter:dolphin/porpoise'),
            '99='+NP_('encounter:gray/finback/sperm whale'),
            '99='+NP_('encounter:humpback/bowhead whale'),
            '99='+NP_('encounter:right whale/narwhal'),
            '99='+NP_('encounter:seal/walrus/sealion'),
            '100='+NP_('encounter:sea/water monster'),
        ),

        'sealane/*': (
            '25='+NP_('encounter:local fishing boat'),
            '30='+NP_('encounter:foreign fishing boat'),
            '55='+NP_('encounter:local merchantman'),
            '65='+NP_('encounter:foreign merchantman'),
            '70='+NP_('encounter:pirate/privateer/etc'),
            '74='+NP_('encounter:local warship'),
            '75='+NP_('encounter:foreign warship'),
            '80='+NP_('encounter:mutiny/disent/argument'),
            '82='+NP_('encounter:maelstrom/feak current'),
            '84='+NP_('encounter:fire on board'),
            '86='+NP_('encounter:equipment failure'),
            '90='+NP_('encounter:food and/or water spoilage'),
            '91='+NP_('encounter:killer whale (orca)'),
            '94='+NP_('encounter:dolphin/porpoise'),
            '95='+NP_('encounter:gray/finback/sperm whale'),
            '96='+NP_('encounter:humpback/bowhead whale'),
            '98='+NP_('encounter:right whale/narwhal'),
            '99='+NP_('encounter:seal/walrus/sealion'),
            '100='+NP_('encounter:sea/water monster'),
        ),

        'opensea/*': (
            '5='+NP_('encounter:local fishing boat'),
            '8='+NP_('encounter:foreign fishing boat'),
            '22='+NP_('encounter:local merchantman'),
            '29='+NP_('encounter:foreign merchantman'),
            '40='+NP_('encounter:pirate/privateer/etc'),
            '44='+NP_('encounter:local warship'),
            '45='+NP_('encounter:foreign warship'),
            '59='+NP_('encounter:mutiny/disent/argument'),
            '60='+NP_('encounter:maelstrom/feak current'),
            '62='+NP_('encounter:fire on board'),
            '65='+NP_('encounter:equipment failure'),
            '70='+NP_('encounter:food and/or water spoilage'),
            '72='+NP_('encounter:killer whale (orca)'),
            '77='+NP_('encounter:dolphin/porpoise'),
            '81='+NP_('encounter:gray/finback/sperm whale'),
            '84='+NP_('encounter:humpback/bowhead whale'),
            '90='+NP_('encounter:right whale/narwhal'),
            '98='+NP_('encounter:seal/walrus/sealion'),
            '100='+NP_('encounter:sea/water monster'),
        ),

        #----------------------------------------------------------------------

        'underworld/*': (
            '10='+NP_('encounter:track/spore/sounds')+'[*]',
            '40='+NP_('encounter:resident creatures (as applicable)'),
            '55=[*wilderness/*]',
            '65='+NP_('encounter:Gargun (probably wandering band)')+'[10]',
            '70='+NP_('encounter:snakes/lizards/dragon/etc')+'[13]',
            '78='+NP_('encounter:Ivashu')+'[15]',
            '85='+NP_('encounter:ethereal (ghost/demon/etc)')+'[16]',
            '90='+NP_('encounter:cave-in/bad floor/etc'),
            '92='+NP_('encounter:one/more of party gets lost'),
            '93='+NP_('encounter:failure/loss of equipment'),
            '95='+NP_('encounter:mutiny/disent/argument'),
            '99='+NP_('encounter:slime/mold/fungus')+'[14]',
            '100='+NP_('encounter:other/unique creature/etc'),
        ),

    #--------------------------------------------------------------------------

        '*/day/1': (
            '20='+NP_('encounter:eating/drinking/gambling/etc'),
            '25='+NP_('encounter:going to/from market/church/work'),
            '30='+NP_('encounter:to/from visiting friends/etc'),
            '35='+NP_('encounter:seeking/offering directions'),
            '40='+NP_('encounter:seeking/offering services'),
            '45='+NP_('encounter:camping/seeking lodgings/etc'),
            '55='+NP_('encounter:offering employment'),
            '85='+NP_('encounter:working/looking for work'),
            '90='+NP_('encounter:escaping the law/service/etc'),
            '95='+NP_('encounter:on errand/bearing message'),
            '100='+NP_('encounter:seeking/in a duel/fight/etc'),
        ),

        '*/night/1': (
            '24='+NP_('encounter:eating/drinking/gambling/etc'),
            '25='+NP_('encounter:going to/from market/church/work'),
            '30='+NP_('encounter:to/from visiting friends/etc'),
            '35='+NP_('encounter:seeking/offering directions'),
            '40='+NP_('encounter:seeking/offering services'),
            '75='+NP_('encounter:camping/seeking lodgings/etc'),
            '85='+NP_('encounter:offering employment'),
            '90='+NP_('encounter:working/looking for work'),
            '96='+NP_('encounter:escaping the law/service/etc'),
            '98='+NP_('encounter:on errand/bearing message'),
            '100='+NP_('encounter:seeking/in a duel/fight/etc'),
        ),

        #----------------------------------------------------------------------

        '*/day/2': (
            '5='+NP_('encounter:auction/impromptu market/sale'),
            '10='+NP_('encounter:brawl/looting/rioting'),
            '15='+NP_('encounter:fight/boxing match/duel'),
            '20='+NP_('encounter:hue and cry'),
            '35='+NP_('encounter:juggler/acrobat/jester/fool'),
            '40='+NP_('encounter:mob sport: soccer/greased pig/etc'),
            '55='+NP_('encounter:musician/bard/singer'),
            '60='+NP_('encounter:edict/proclamation/spectacle'),
            '70='+NP_('encounter:play/puppet show/animal show'),
            '75='+NP_('encounter:political orator/debate'),
            '80='+NP_('encounter:procession/funeral/etc'),
            '85='+NP_('encounter:public execution')+'[5]',
            '90='+NP_('encounter:public punishment')+'[5]',
            '100='+NP_('encounter:religious sermon/orator/debate')+'[4]',
        ),

        '*/night/2': (
            '1='+NP_('encounter:auction/impromptu market/sale'),
            '10='+NP_('encounter:brawl/looting/rioting'),
            '15='+NP_('encounter:fight/boxing match/duel'),
            '20='+NP_('encounter:hue and cry'),
            '35='+NP_('encounter:juggler/acrobat/jester/fool'),
            '35='+NP_('encounter:mob sport: soccer/greased pig/etc'),
            '50='+NP_('encounter:musician/bard/singer'),
            '55='+NP_('encounter:edict/proclamation/spectacle'),
            '60='+NP_('encounter:play/puppet show/animal show'),
            '67='+NP_('encounter:political orator/debate'),
            '82='+NP_('encounter:procession/funeral/etc'),
            '87='+NP_('encounter:public execution')+'[5]',
            '97='+NP_('encounter:public punishment')+'[5]',
            '100='+NP_('encounter:religious sermon/orator/debate')+'[4]',
        ),

        #----------------------------------------------------------------------

        'urban/*/3': (
            '1='+NP_('encounter:apathecary')+'[3a/3b]',
            '2='+NP_('encounter:alchemist')+'[3a/3b]',
            '3='+NP_('encounter:astrologer')+'[3a/3b]',
            '5='+NP_('encounter:chandler')+'[3a/3b]',
            '6='+NP_('encounter:charcoaler')+'[3a/3b]',
            '12='+NP_('encounter:clothier')+'[3a/3b]',
            '13='+NP_('encounter:courtesan')+'[3a/3b]',
            '14='+NP_('encounter:embalmer')+'[3a/3b]',
            '15='+NP_('encounter:glassworker')+'[3a/3b]',
            '17='+NP_('encounter:harper')+'[3a/3b]',
            '18='+NP_('encounter:herald')+'[3a/3b]',
            '23='+NP_('encounter:hideworker')+'[3a/3b]',
            '27='+NP_('encounter:innkeeper')+'[3a/3b]',
            '29='+NP_('encounter:jeweler')+'[3a/3b]',
            '30='+NP_('encounter:lexigrapher')+'[3a/3b]',
            '35='+NP_('encounter:litigant')+'[3a/3b]',
            '36='+NP_('encounter:locksmith')+'[3a/3b]',
            '40='+NP_('encounter:mason')+'[3a/3b]',
            '49='+NP_('encounter:mercantyler')+'[3a/3b]',
            '59='+NP_('encounter:metalsmith')+'[3a/3b]',
            '61='+NP_('encounter:miller')+'[3a/3b]',
            '62='+NP_('encounter:miner')+'[3a/3b]',
            '66='+NP_('encounter:ostler')+'[3a/3b]',
            '67='+NP_('encounter:perfumer')+'[3a/3b]',
            '69='+NP_('encounter:physician')+'[3a/3b]',
            '70='+NP_('encounter:pilot')+'[3a/3b]',
            '78='+NP_('encounter:potter')+'[3a/3b]',
            '80='+NP_('encounter:salter')+'[3a/3b]',
            '81='+NP_('encounter:seaman')+'[3a/3b]',
            '82='+NP_('encounter:Shek-Pvar')+'[3a/3b]',
            '83='+NP_('encounter:shipwright')+'[3a/3b]',
            '84='+NP_('encounter:tentmaker')+'[3a/3b]',
            '85='+NP_('encounter:thespian')+'[3a/3b]',
            '86='+NP_('encounter:timberwright')+'[3a/3b]',
            '89='+NP_('encounter:weaponcrafter')+'[3a/3b]',
            '100='+NP_('encounter:woodcrafter')+'[3a/3b]',
        ),

        '*/*/3': (
            '1='+NP_('encounter:apathecary')+'[3a/3b]',
            '2='+NP_('encounter:alchemist')+'[3a/3b]',
            '3='+NP_('encounter:astrologer')+'[3a/3b]',
            '4='+NP_('encounter:chandler')+'[3a/3b]',
            '6='+NP_('encounter:charcoaler')+'[3a/3b]',
            '8='+NP_('encounter:clothier')+'[3a/3b]',
            '9='+NP_('encounter:courtesan')+'[3a/3b]',
            '10='+NP_('encounter:embalmer')+'[3a/3b]',
            '11='+NP_('encounter:glassworker')+'[3a/3b]',
            '12='+NP_('encounter:harper')+'[3a/3b]',
            '13='+NP_('encounter:herald')+'[3a/3b]',
            '16='+NP_('encounter:hideworker')+'[3a/3b]',
            '18='+NP_('encounter:innkeeper')+'[3a/3b]',
            '19='+NP_('encounter:jeweler')+'[3a/3b]',
            '20='+NP_('encounter:lexigrapher')+'[3a/3b]',
            '21='+NP_('encounter:litigant')+'[3a/3b]',
            '22='+NP_('encounter:locksmith')+'[3a/3b]',
            '24='+NP_('encounter:mason')+'[3a/3b]',
            '31='+NP_('encounter:mercantyler')+'[3a/3b]',
            '41='+NP_('encounter:metalsmith')+'[3a/3b]',
            '59='+NP_('encounter:miller')+'[3a/3b]',
            '69='+NP_('encounter:miner')+'[3a/3b]',
            '72='+NP_('encounter:ostler')+'[3a/3b]',
            '73='+NP_('encounter:perfumer')+'[3a/3b]',
            '74='+NP_('encounter:physician')+'[3a/3b]',
            '75='+NP_('encounter:pilot')+'[3a/3b]',
            '79='+NP_('encounter:potter')+'[3a/3b]',
            '81='+NP_('encounter:salter')+'[3a/3b]',
            '82='+NP_('encounter:seaman')+'[3a/3b]',
            '83='+NP_('encounter:Shek-Pvar')+'[3a/3b]',
            '84='+NP_('encounter:shipwright')+'[3a/3b]',
            '85='+NP_('encounter:tentmaker')+'[3a/3b]',
            '86='+NP_('encounter:thespian')+'[3a/3b]',
            '89='+NP_('encounter:timberwright')+'[3a/3b]',
            '90='+NP_('encounter:weaponcrafter')+'[3a/3b]',
            '100='+NP_('encounter:woodcrafter')+'[3a/3b]',
        ),

        '*/*/3a': (
            '20='+NP_('encounter:apprentice'),
            '65='+NP_('encounter:journeyman'),
            '80='+NP_('encounter:bonded master'),
            '95='+NP_('encounter:freemaster'),
            '99='+NP_('encounter:syndic'),
            '100='+NP_('encounter:guildmaster'),
        ),

        '*/*/3b': (
            '30='+NP_('encounter:at/seeking work'),
            '40='+NP_('encounter:on errand'),
            '50='+NP_('encounter:seeking materials'),
            '55='+NP_('encounter:seeking employees'),
            '65='+NP_('encounter:delivering goods'),
            '80='+NP_('encounter:adventuring')+'[10]',
            '100=[1]',
        ),

        #----------------------------------------------------------------------

        '*/*/4': (
            '15='+NP_('encounter:acolyte/novice')+'[4a]',
            '30='+NP_('encounter:mendicant lay-brother/sister')+'[4a]',
            '40='+NP_('encounter:mendicant friar/etc')+'[4a]',
            '60='+NP_('encounter:deacon(ess)/etc')+'[4a]',
            '70='+NP_('encounter:mendicant priest(ess)')+'[4a]',
            '85='+NP_('encounter:temple priest(ess)')+'[4a]',
            '96='+NP_('encounter:high priest(ess) with attendants')+'[4a]',
            '99='+NP_('encounter:bishop(ess) with attendants')+'[4a]',
            '100='+NP_('encounter:primate/pontiff/archbishop(ess)')+'[4a]',
        ),

        '*/*/4a': (
            '10='+NP_('encounter:ministering flock/dispensing alms/etc'),
            '15='+NP_('encounter:inspecting church property/lands/etc'),
            '20='+NP_('encounter:preaching/about to preach/etc'),
            '30='+NP_('encounter:meditating/praying/etc'),
            '40='+NP_('encounter:seeking victims for rituals/etc'),
            '50='+NP_('encounter:inquisiting after heretics/apostates/etc'),
            '60='+NP_('encounter:begging/soliciting alms (ass applicable)'),
            '65='+NP_('encounter:on pilgrimage'),
            '100='+NP_('encounter:non-church-related activity')+'[1]',
        ),

        #----------------------------------------------------------------------

        '*/day/5': (
            '10='+NP_('encounter:collecting extortion/etc'),
            '15='+NP_('encounter:collecting dues/patrolling/etc'),
            '35='+NP_('encounter:pursecutting/stalking a mark/etc'),
            '45='+NP_('encounter:con job/gambling/touting/etc'),
            '46='+NP_('encounter:burgling/casing job/etc'),
            '47='+NP_('encounter:moving/smuggling goods/contraband'),
            '48='+NP_('encounter:assassin stalking prey/etc'),
            '75='+NP_('encounter:non-criminal activity')+'[3b]',
            '100='+NP_('encounter:non-criminal activity')+'[1]',
        ),

        '*/night/5': (
            '10='+NP_('encounter:collecting extortion/etc'),
            '11='+NP_('encounter:collecting dues/patrolling/etc'),
            '13='+NP_('encounter:pursecutting/stalking a mark/etc'),
            '20='+NP_('encounter:con job/gambling/touting/etc'),
            '45='+NP_('encounter:burgling/casing job/etc'),
            '60='+NP_('encounter:moving/smuggling goods/contraband'),
            '62='+NP_('encounter:assassin stalking prey/etc'),
            '70='+NP_('encounter:non-criminal activicy')+'[3b]',
            '100='+NP_('encounter:non-criminal activicy')+'[1]',
        ),

        #----------------------------------------------------------------------

        '*/*/6': (
            '20='+NP_('encounter:serf/poor thrall')+'[6a]',
            '45='+NP_('encounter:half-villein/average thrall')+'[6a]',
            '70='+NP_('encounter:villein/wealthy thrall')+'[6a]',
            '75='+NP_('encounter:reeve (chief serf/thrall)')+'[6a]',
            '80='+NP_('encounter:farm worker (freeman)')+'[6a]',
            '90='+NP_('encounter:freehold (tenant) farmer')+'[6a]',
            '95='+NP_('encounter:yeoman (freehold farmer)')+'[6a]',
            '96='+NP_('encounter:thatcher')+'[6a]',
            '98='+NP_('encounter:woodcutter/iceman')+'[6a]',
            '100='+NP_('encounter:forester')+'[6a]',
        ),

        '*/day/6a': (
            '5='+NP_('encounter:running away (seeking protection?)'),
            '30='+NP_('encounter:traveling to/from work/market'),
            '70='+NP_('encounter:at work (making/selling goods)'),
            '85='+NP_('encounter:herding livestock (as applicable)'),
            '100='+NP_('encounter:at leisure')+'[1]',
        ),

        '*/night/6a': (
            '15='+NP_('encounter:running away (seeking protection?)'),
            '20='+NP_('encounter:traveling to/from work/market'),
            '25='+NP_('encounter:at work (making/selling goods)'),
            '30='+NP_('encounter:herding livestock (as applicable)'),
            '100='+NP_('encounter:at leisure')+'[1]',
        ),

        #----------------------------------------------------------------------

        '*/*/7': (
            '75='+NP_('encounter:enfoeffed knight/patrician'),
            '85='+NP_('encounter:grandmaster/officer of fighting order'),
            '95='+NP_('encounter:duke/earl/baron/great patrician/etc'),
            '100='+NP_('encounter:king/emperor/tribal chieftain/etc'),
        ),

        #----------------------------------------------------------------------

        '*/*/8': (
            '50='+NP_('encounter:local garrison/guard/patrol/etc')+'[8a]',
            '60='+NP_('encounter:militiaman')+'[8a]',
            '75='+NP_('encounter:legionnaire/man at arms')+'[8a]',
            '84='+NP_('encounter:mercenary')+'[8a]',
            '88='+NP_('encounter:naval seaman/marine (as applicable)')+'[8a]',
            '93='+NP_('encounter:gladiator (free)')+'[8a]',
            '100='+NP_('encounter:knight-bachelor (landless)')+'[8a]',
        ),

        #----------------------------------------------------------------------

        '*/day/8a': (
            '45='+NP_('encounter:on guard/patrol'),
            '50='+NP_('encounter:bearing message/moving cargo'),
            '60='+NP_('encounter:training/on maneuvers/etc'),
            '65='+NP_('encounter:recruiting'),
            '70='+NP_('encounter:investigating crime/etc'),
            '80='+NP_('encounter:seeking employment'),
            '95='+NP_('encounter:off duty')+'[1]',
            '100='+NP_('encounter:absent without leave')+'[1]',
        ),

        '*/night/8a': (
            '60='+NP_('encounter:on guard/patrol'),
            '65='+NP_('encounter:bearing message/moving cargo'),
            '66='+NP_('encounter:training/on maneuvers/etc'),
            '69='+NP_('encounter:recruiting'),
            '70='+NP_('encounter:investigating crime/etc'),
            '75='+NP_('encounter:seeking employment'),
            '95='+NP_('encounter:off duty')+'[1]',
            '100='+NP_('encounter:absent without leave')+'[1]',
        ),

        #----------------------------------------------------------------------

        '*/*/9': (
            '20='+NP_('encounter:baliff/sheriff/constable')+'[9a]',
            '55='+NP_('encounter:mayor/alderman/town bureaucrat')+'[9a]',
            '70='+NP_('encounter:judge/magistrate/royal official')+'[9a]',
            '80='+NP_('encounter:gaoler/executioner (with prisoners?)')+'[9a]',
            '100='+NP_('encounter:reeve/inquisitor')+'[9a]',
        ),

        #----------------------------------------------------------------------

        '*/*/9a': (
            '25='+NP_('encounter:collecting/assessing taxes'),
            '60='+NP_('encounter:inspecting businesses/public works/etc'),
            '75='+NP_('encounter:investigating crime'),
            '100='+NP_('encounter:off duty')+'[1]',
        ),

        #----------------------------------------------------------------------

        '*/day/10': (
            '5='+NP_('encounter:camping/seeking accommodations/etc'),
            '23='+NP_('encounter:explorinrg/seeking adventure'),
            '58='+NP_('encounter:questing/crusading/geas/etc'),
            '63='+NP_('encounter:seeking directions/protection'),
            '70='+NP_('encounter:escaping persecution/the law/etc'),
            '80='+NP_('encounter:hunting criminals/runaways/game'),
            '90='+NP_('encounter:escorting cargo/treasure/etc'),
            '93='+NP_('encounter:in distress/under attack/etc'),
            '96='+NP_('encounter:caring for wounded/dead'),
            '98='+NP_('encounter:preparing/springing ambush/etc'),
            '100='+NP_('encounter:dividing loot after fight/etc'),
        ),

        '*/night/10': (
            '70='+NP_('encounter:camping/seeking accommodations/etc'),
            '72='+NP_('encounter:explorinrg/seeking adventure'),
            '74='+NP_('encounter:questing/crusading/geas/etc'),
            '76='+NP_('encounter:seeking directions/protection'),
            '86='+NP_('encounter:escaping persecution/the law/etc'),
            '93='+NP_('encounter:hunting criminals/runaways/game'),
            '94='+NP_('encounter:escorting cargo/treasure/etc'),
            '96='+NP_('encounter:in distress/under attack/etc'),
            '98='+NP_('encounter:caring for wounded/dead'),
            '99='+NP_('encounter:preparing/springing ambush/etc'),
            '100='+NP_('encounter:dividing loot after fight/etc'),
        ),

        #----------------------------------------------------------------------

        '*/*/11': (
            '50='+NP_('encounter:sleeping/hibernating/dormant'),
            '85='+NP_('encounter:stalking/hunting prey or fleeing predator'),
            '100='+NP_('encounter:eating kill/grazing/foraging/hunting/etc'),
        ),

        #----------------------------------------------------------------------

        '*/*/12': (
            '95='+NP_('encounter:wild pony')+'[11]',
            '96='+NP_('encounter:Centaurin')+'[1/11]',
            '97='+NP_('encounter:Unicorn')+'[11]',
            '99='+NP_('encounter:Hirenu (hippogriff)')+'[11]',
            '100='+NP_('encounter:unique/rare equine'),
        ),

        #----------------------------------------------------------------------

        '*/*/13': (
            '10='+NP_('encounter:ordinary reptile/lizard')+'[11]',
            '50='+NP_('encounter:ordinary snake (non-poisonous)')+'[11]',
            '80='+NP_('encounter:ordinary snake (poisonous)')+'[11]',
            '90='+NP_('encounter:ordinary snake (constrictor)')+'[11]',
            '96='+NP_('encounter:Yelgri (harpy)')+'[11]',
            '99='+NP_('encounter:wyvern/lime (as applicable)')+'[11]',
            '100='+NP_('encounter:dragon')+'[11]',
        ),

        #----------------------------------------------------------------------

        '*/*/14': (
            '35='+NP_('encounter:Langlah (gray ooze)'),
            '65='+NP_('encounter:Lurishi (ocher mold)'),
            '95='+NP_('encounter:M\'nogai (green slime)'),
            '100='+NP_('encounter:unique/rare fungus/slime'),
        ),

        #----------------------------------------------------------------------

        '*/day/15': (
            '25='+NP_('encounter:Aklash (Vessal of the Choking Wind)')+'[11]',
            '25='+NP_('encounter:Hru (rock giant)')+'[11]',
            '50='+NP_('encounter:Nolah (troll)')+'[11]',
            '60='+NP_('encounter:Umbathri (gargoyle)'),
            '95='+NP_('encounter:Vlasta (eater of eyes)')+'[11]',
            '100='+NP_('encounter:unique/rare ivashu')+'[11]',
        ),

        '*/night/15': (
            '15='+NP_('encounter:Aklash (Vessal of the Choking Wind)')+'[11]',
            '40='+NP_('encounter:Hru (rock giant)')+'[11]',
            '65='+NP_('encounter:Nolah (troll)')+'[11]',
            '75='+NP_('encounter:Umbathri (gargoyle)'),
            '95='+NP_('encounter:Vlasta (eater of eyes)')+'[11]',
            '100='+NP_('encounter:unique/rare ivashu')+'[11]',
        ),

        #----------------------------------------------------------------------

        '*/day/16': (
            '15='+NP_('encounter:Elmithri/water sprite/water elemental/etc'),
            '50='+NP_('encounter:Asiri/Aulamithri/air sprite/air elemental/etc'),
            '60='+NP_('encounter:earth elemental (as applicable)'),
            '70='+NP_('encounter:fire elemental (as applicable)'),
            '84='+NP_('encounter:shade/ghost/astral entity/etc'),
            '85='+NP_('encounter:Amorvin ("free" undead)'),
            '85='+NP_('encounter:Gulmorvin ("enslaved" undead)'),
            '90='+NP_('encounter:possessed entity/golem/zombie/etc'),
            '95='+NP_('encounter:demon/demigod (as applicable)'),
            '100='+NP_('encounter:unique/rare ethereal'),
        ),

        '*/night/16': (
            '10='+NP_('encounter:Elmithri/water sprite/water elemental/etc'),
            '20='+NP_('encounter:Asiri/Aulamithri/air sprite/air elemental/etc'),
            '30='+NP_('encounter:earth elemental (as applicable)'),
            '40='+NP_('encounter:fire elemental (as applicable)'),
            '60='+NP_('encounter:shade/ghost/astral entity/etc'),
            '70='+NP_('encounter:Amorvin ("free" undead)'),
            '80='+NP_('encounter:Gulmorvin ("enslaved" undead)'),
            '90='+NP_('encounter:possessed entity/golem/zombie/etc'),
            '95='+NP_('encounter:demon/demigod (as applicable)'),
            '100='+NP_('encounter:unique/rare ethereal'),
        ),
    }

    #==========================================================================


    def __init__(self):
        deity.world.kelestia.kethira.Kethira.__init__(self, 'Harn')


    def _Daytime(self, month, day, hour, minute):
        if month <= 3: # spring
            if hour >= 6 and hour < 18:
                return 'day'
            else:
                return 'night'
        elif month <= 6: # summer
            if hour >= 4 and hour < 20:
                return 'day'
            else:
                return 'night'
        elif month <= 9: # fall
            if hour >= 6 and hour < 18:
                return 'day'
            else:
                return 'night'
        else: # winter
            if hour >= 8 and hour < 16:
                return 'day'
            else:
                return 'night'


###############################################################################
