###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Kelestia/Kethira Common (generic functions, constants, etc.)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import deity.trandom
import deity.world.kelestia

from deity.xtype import *


###############################################################################


class Kethira(deity.world.kelestia.Kelestia):
    kSecondsPerMinute = 60
    kMinutesPerHour   = 60
    kHoursPerDay      = 24
    kDaysPerMonth     = 30
    kMonthsPerYear    = 12
    kWeathersPerDay   = 6


    def __init__(self, prefix):
        if '/World/Kelestia/Kethira/CurTime' not in GDB:
            GDB['/World/Kelestia/Kethira/CurTime'] = '720-01-01 08:00:00.0'

        if '/World/Kelestia/Kethira/TimeZone' not in GDB:
            GDB['/World/Kelestia/Kethira/TimeZone'] = '+0000'

        deity.world.kelestia.Kelestia.__init__(self, 'Kethira/'+prefix)


###############################################################################
