###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common (testable random numbers)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import random


###############################################################################


class Random(object):
    """Generate Random Numbers or Return Preset Ones (for testing)"""


    def __init__(self, **kwds):
        self.testmode = False
        self.testresults = dict()
        self.testend()


    def testset(self, name=None, val=None):
        self.testmode = True
        if name is None or val is None: return
        if name not in self.testresults:
            self.testresults[name] = list()
        try:
            for i in val:
                self.testresults[name].insert(0, i)
        except:
            self.testresults[name].insert(0, val)


    def testget(self, name, begin, end):
        if name not in self.testresults:
            raise KeyError, 'no test random value for "%s" (have: %s)' % (name, str(self.testresults))
        val = self.testresults[name].pop()
        if val < begin or val >= end:
            raise Exception, 'test random value "%s" is out of range (%s <= %s < %s)' % (name,str(begin),str(val),str(end))
        if len(self.testresults[name]) == 0:
            del self.testresults[name]
        return val


    def testend(self):
        self.testmode = False
        if self.testresults:
            remaining = str(self.testresults)
            self.testresults = dict()
            raise Exception, 'test random finished with values remaining: %s' % remaining


    def random(self, test=None):
        if test is not None and self.testmode:
            return self.testget(test, 0, 1)
        return random.random()


    def randint(self, a, b, test=None):
        if test is not None and self.testmode:
            return self.testget(test, a, b+1)
        return random.randint(a, b)
        #return random.randint(0, b * 13 * 7) // 7 % b + a


###############################################################################
