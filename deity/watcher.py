###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Windows
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


#WATCH_MAIN		= 0
#WATCH_MODE_EXPLORE	= 1
#WATCH_MODE_CHAREDIT	= 2
#WATCH_MODE_COMBAT	= 3
#WATCH_WIDGET_CHARDISPLAY= 4


###############################################################################


class Watched(object):
    def __init__(self, value):
        self.item = value
        self.seen = dict()


    def IsChanged(self, watcher):
        if self.seen.has_key(watcher):
            return False
        self.seen[watcher] = True
        return True


    def Get(self):
        return self.item


    def Set(self, value):
        self.seen = dict()
        self.item = value


    def Alter(self):
        self.seen = dict()
        return self.item


###############################################################################
