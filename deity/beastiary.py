###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Beastiary Database Management
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import deity
import re
import base64
#import random
#import math

Verbose         = False

kListSep	= "\x1C" # ASCII FS (field seperator)
kImageWidth	= 300
kImageHeight	= 400
kImageSize	= (kImageWidth,kImageHeight)

#kInvExpRe	= re.compile(r'\[(\d+)/([\d\.]+)\]')
#kCondOpRe	= re.compile(r'(==|!=|<|<=|>=|>)')
#kKeyRe		= re.compile(r'(([A-Z]):([A-Za-z0-9]+))')

kBeastListKey	= '/BeastList/'
kEquipListKey	= '/EquipList/'
kLocationsKey	= 'Locations'
kWieldKey	= 'Wield'
kGeneratorFuncs	= dict()
kTypeStringMax	= 30


###############################################################################


def DbGet(db, group, name, field, default=None, sections=None):
    source = deity.DbGet(db, '/%s/%s/Source'%(group,name))
#    print 'BDbGet:',group,name,field,default,sections,'->',source
    if source is None or len(source) < 2: return default
    if field == 'Source': return source

    # handle aliases
    if source[0] == '@':
        name = source[1:]

    # inheritence list
    bases = list()

    # go through inheritence looking for field
    while True:
        if sections is not None:
            for s in sections:
                value = deity.DbGet(db, '/%s/%s/%s/%s'%(group,name,field,s.lower()))
#                print ' -',group,name,field,s.lower(),'->',value
                if value is not None: return value
        value = deity.DbGet(db, '/%s/%s/%s'%(group,name,field))
#        print ' -',group,name,field,'->',value
        if value is not None: return value
        value = deity.DbGet(db, '/%s/%s/ISA'%(group,name))
#        print ' -',group,name,'ISA','->',value
        if value:
            bases.extend(value.split(';'))
        if not bases:
            return default
        name = bases[0]
        bases = bases[1:]


###############################################################################


class Database(object):
    def __init__(self, db):
        self.db	= db


    class Parser(object):
        kImageTypes	= r'bitmap'

        kWhitespaceRe	= re.compile(r'\s+')
        kNameRe		= re.compile(r'^_?[A-Z0-9\-]+$', re.I)
        kKeyRe		= re.compile(r'^([A-Z0-9]+:)?_?[A-Z0-9/\-]+$', re.I)
        kMultiNameRe	= re.compile(r'^(_?[A-Z0-9\:\-]+)(,(_?[A-Z0-9/\:\-]+))*$', re.I)
        kImageTypesRe	= re.compile('^('+kImageTypes+')$', re.I)
        kDieRollRe	= re.compile('^(\d*)d(\d+)$')

        def __init__(self, loader):
            self.loader = loader

        def Done(self):
            pass

        def Error(self, *msgs):
            self.loader.Error(*msgs)


    class List(Parser):
        def __init__(self, loader, name, systems=None):
            Database.Parser.__init__(self, loader)
            self.name	   = name
            self.systems   = systems
            self.itemlist  = list()

        def Done(self):
            if self.systems:
                if Verbose: print 'List:',self.name,'@',self.systems,'<-',self.itemlist
                for s in self.systems.split(','):
                    self.loader.db["%s/%s" % (self.name,s.lower())] = kListSep.join(self.itemlist)
            else:
                self.loader.db[self.name] = kListSep.join(self.itemlist)

            #maxlen = 0
            #maxkey = '%s/_MaxLen' % self.name
            #if self.loader.db.has_key(maxkey):
            #    maxlen = int(self.loader.db[maxkey])
            #for i in self.itemlist:
            #    s = P_('bdb:',i)
            #    l = len(s)
            #    if l > maxlen: maxlen = l
            #self.loader.db[maxkey] = str(maxlen)
            #print 'MaxLen:',maxkey,maxlen


    class KvList(List):
        kSplitRe = re.compile(r'^(\S+)\s+(\S.*)?$')

        def __init__(self, loader, name, systems):
            Database.List.__init__(self, loader, name, systems)

        def Load(self, line):
            match = self.kSplitRe.match(line)
            if not match:
                self.Error('invalid key/value pair (two items seperated by space)')
            key = match.group(1).lower()
            val = match.group(2).lower()
            if val is None: val = '0'
            #print "Key:",key,"Val:",val
            if not self.kKeyRe.match(key):
                self.Error('invalid key "%s" -- only alphanumeric and dashes' % (key))
            if val.find(kListSep) >= 0:
                self.Error('invalid value -- no field-seperators (0x1C) allowed')
            self.itemlist.append("%s=%s" % (key,val))


    class Image(Parser):
        def __init__(self, loader, name, itype):
            Database.Parser.__init__(self, loader)
            self.name  = name
            self.itype = itype
            self.idata = ''

        def Load(self, line):
            self.idata += line
            return None

        def Done(self):
            try:
                image = base64.b64decode(self.idata)
            except TypeError:
                self.Error('invalid image encoding')
            self.loader.db[self.name] = self.itype+':'+image


    class Beast(Parser):
        def __init__(self, loader, name):
            Database.Parser.__init__(self, loader)
            self.name	= name
            self.isa	= ''
            self.aka	= ''
            self.ranges = ''
            self.source	= None

        def Load(self, line):
            # record record start
            if not self.source:
                self.source = self.loader.lineinfo

            # split out individual tokens (first goes at end of list)
            tokens = line.split()
            tokens.reverse()
            # get record type
            record = tokens.pop()

            # handle ISA records (inheritence)
            if record == 'ISA':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'basetype[,basetype[...]]')
                basetypes = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(basetypes):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                for basetype in basetypes.split(','):
                    if basetype == self.name:
                        self.Error('base type "'+basetype+'" matches current defniition')
                    if not self.loader.db.has_key('/Beast/%s/Source'%(basetype)):
                        self.Error('unknown base type "%s"'%(basetype))
                    # append type to sub-types list
                    if self.isa: self.isa += ';'
                    self.isa += basetype
                return None # stay in this state

            # handle AKA records (aliases)
            elif record == 'AKA':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'basetype[,basetype[...]]')
                aliases = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(aliases):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                for alias in aliases.split(','):
                    if alias == self.name:
                        self.Error('alias "'+alias+'" matches current defniition')
                    akey = '/Beast/%s/Source'%(alias)
                    if self.loader.db.has_key(akey):
                        other = self.loader.db[akey]
                        if other != '@'+self.name:
                            self.Error('alias "%s" already defined'%(alias))
                    # append type to aliases list
                    if self.aka: self.aka += ';'
                    self.aka += alias
                return None # stay in this state

            # handle IMAGE records
            elif record == 'IMAGE':
                # validate format
                if len(tokens) != 2 or not self.kImageTypesRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'<'+kImageTypes+'> {')
                # parse image
                return Database.Image(self.loader, "/Beast/%s/%s"%(self.name,'Image'), tokens[1].lower())

            elif record == 'LOCATIONS':
                # validate format
                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
                # parse and store list
                return Database.KvList(self.loader, "/Beast/%s/%s"%(self.name,kLocationsKey), tokens[1])

            elif record == 'WIELD':
                # validate format
                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
                # parse and store list
                return Database.KvList(self.loader, "/Beast/%s/Wield"%(self.name), tokens[1])

            # handle HIT records (hit locations)
            elif record == 'HIT':
                # check for hit range (high, low, mid, etc.)
                subkey = 'Hit'
                if len(tokens) == 3:
                    range = tokens[-1]
                    if range[0] == '"' and range[-1] == '"':
                        tokens.pop()
                        range = range[1:-1].lower()
                        subkey += '-'+range
                        if self.ranges != '': self.ranges += kListSep
                        self.ranges += range
                # validate format
                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'["range"] GameSystem[,GameSystem[...]] {')
                # parse and store list
                return Database.KvList(self.loader, "/Beast/%s/%s"%(self.name,subkey), tokens[1])

            elif record == 'GENERATE':
                # validate format
                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
                # add to list of valid types
                if self.name[0] != '_':
                    for gs in tokens[1].split(','):
                        gs = gs.lower().strip()
                        key = '/Generate/Beast/%s' % gs
                        val = self.name
                        if self.aka:
                            val += ';%s' % self.aka
                        if key in self.loader.db:
                            self.loader.db[key] += ';%s' % val
                        else:
                            self.loader.db[key] = val
                # parse and store list
                return Database.KvList(self.loader, "/Beast/%s/Generate"%(self.name), tokens[1])

            else:
                self.Error('unknown record type:',record)


        def Done(self):
            # get current list of beasts
            if self.loader.db.has_key(kBeastListKey):
                blist = self.loader.db[kBeastListKey]
            else:
                blist = ''
            if blist != '': blist += kListSep
            # add master to list
            if Verbose: print "Done: add name:",self.name
            if self.name[0] != '_':
                blist += self.name + kListSep
            # save source info
            self.loader.db['/Beast/%s/Source' % (self.name)] = self.source
            # save inheritence info
            self.loader.db['/Beast/%s/ISA' % (self.name)] = self.isa
            # create aliases
            if Verbose: print "Done: add alias:",self.aka
            for a in self.aka.split(';'):
                if a == '': continue
                self.loader.db['/Beast/%s/Source' % (a)] = '@'+self.name
                if a[0] != '_':
                    blist += a + kListSep
            # save values
            if self.ranges != '':
                self.loader.db['/Beast/%s/Hitranges' % (self.name)] = self.ranges
            # save updated beast list
            self.loader.db[kBeastListKey] = blist[:-1] # omit last comma
            # say what we've done
            if Verbose: print self.name,"loaded"


    class Equip(Parser):
        def __init__(self, loader, name):
            Database.Parser.__init__(self, loader)
            self.name		= name
            self.isa		= ''
            self.aka		= ''
            self.canuse		= ''
            self.modifiers	= ''
            self.subtypes	= ''
            self.hands  	= None
            self.source		= None

        def Load(self, line):
            # record record start
            if not self.source:
                self.source = self.loader.lineinfo

            # split out individual tokens (first goes at end of list)
            tokens = line.split()
            tokens.reverse()
            # get record type
            record = tokens.pop()

            # handle ISA records (inheritence)
            if record == 'ISA':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'basetype[,basetype[...]]')
                basetypes = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(basetypes):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                for basetype in basetypes.split(','):
                    if basetype == self.name:
                        self.Error('base type "'+basetype+'" matches current defniition')
                    if not self.loader.db.has_key('/Equip/%s/Source'%(basetype)):
                        self.Error('unknown base type "%s"'%(basetype))
                    # append type to sub-types list
                    if self.isa: self.isa += ';'
                    self.isa += basetype
                return None # stay in this state

            # handle AKA records (aliases)
            elif record == 'AKA':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'basetype[,basetype[...]]')
                aliases = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(aliases):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                for alias in aliases.split(','):
                    if alias == self.name:
                        self.Error('alias "'+alias+'" matches current defniition')
                    akey = '/Equip/%s/Source'%(alias)
                    if self.loader.db.has_key(akey):
                        other = self.loader.db[akey]
                        if other != '@'+self.name:
                            self.Error('alias "%s" already defined'%(alias))
                    # append type to aliases list
                    if self.aka: self.aka += ';'
                    self.aka += alias
                return None # stay in this state

            # handle CANUSE records (who can use this)
            elif record == 'CANUSE':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'chartype[,chartype[...]]')
                types = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(types):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                # append type to can-use list
                if self.canuse: self.canuse += kListSep
                self.canuse += types
                return None # stay in this state

            # handle MODIFIERS records (who can use this)
            elif record == 'MODIFIERS':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'attribute[,attribute[...]]')
                types = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(types):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                # append type to can-use list
                if self.modifiers: self.modifiers += kListSep
                self.modifiers += types
                return None # stay in this state

            # handle SUBTYPES records (who can use this)
            elif record == 'SUBTYPES':
                # must have exactly one option
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'subtype[,subtype[...]]')
                types = tokens.pop().lower()
                # verify each type in comma-separated list
                if not self.kMultiNameRe.match(types):
                    self.Error('invalid',record,'line -- only alphanumeric and dashes in types')
                # append type to can-use list
                if self.subtypes: self.subtypes += kListSep
                self.subtypes += types
                return None # stay in this state

            # handle IMAGE records
            elif record == 'IMAGE':
                # validate format
                if len(tokens) != 2 or not self.kImageTypesRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'<'+kImageTypes+'> {')
                # parse image
                return Database.Image(self.loader, "/Equip/%s/Image"%self.name, tokens[1].lower())

            elif record == 'COVER':
                # validate format
                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
                # parse and store list
                return Database.KvList(self.loader, "/Equip/%s/Cover"%(self.name), tokens[1])

#            elif record == 'DAMAGE':
#                # validate format
#                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
#                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
#                # parse and store list
#                return Database.KvList(self.loader, "/Equip/%s/Damage"%(self.name), tokens[1])

#            elif record == 'DATA':
#                # validate format
#                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
#                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
#                # parse and store list
#                return Database.KvList(self.loader, "/Equip/%s/Data"%(self.name), tokens[1])

            elif record == 'HANDS':
                # validate format
                if len(tokens) != 1:
                    self.Error('invalid',record,'line -- use:',record,'number-of-hands-required-for-use')
                self.hands = int(tokens.pop())
                return None # stay in this state

            elif record == 'GENERATE':
                # validate format
                if len(tokens) != 2 or not self.kMultiNameRe.match(tokens[1]) or tokens[0] != '{':
                    self.Error('invalid',record,'line -- use:',record,'GameSystem[,GameSystem[...]] {')
                # add to list of valid types
                if self.name[0] != '_':
                    for gs in tokens[1].split(','):
                        gs = gs.lower().strip()
                        key = '/Generate/Equip/%s' % gs
                        val = self.name
                        if self.aka:
                            val += ';%s' % self.aka
                        if key in self.loader.db:
                            self.loader.db[key] += ';%s' % val
                        else:
                            self.loader.db[key] = val
                # parse and store list
                return Database.KvList(self.loader, "/Equip/%s/Generate"%(self.name), tokens[1])

            else:
                self.Error('unknown record type:',record)


        def Done(self):
            # get current list of equipment
            if self.loader.db.has_key(kEquipListKey):
                clist = self.loader.db[kEquipListKey]
            else:
                clist = ''
            if clist != '': clist += kListSep
            for subtype in self.subtypes.split(','):
                name = self.name
                if subtype: name += ':' + subtype
                if Verbose: print "Done: add name:",name
                # add master to list
                if name[0] != '_':
                    clist += name + kListSep
                # save source info
                self.loader.db['/Equip/%s/Source' % (name)] = self.source
                # save inheritence info
                isa = self.isa
                if subtype:
                    if isa: isa += ';'
                    isa += '_SUBTYPE_' + subtype
                self.loader.db['/Equip/%s/ISA' % (name)] = isa
                # save can-use info
                self.loader.db['/Equip/%s/CanUse' % (name)] = self.canuse
                # save modifiers info
                self.loader.db['/Equip/%s/Modifiers' % (name)] = self.modifiers
                # create aliases
                if Verbose: print "Done: add alias:",self.aka
                for a in self.aka.split(';'):
                    if a == '': continue
                    for s in self.subtypes.split(','):
                        if s: a += ':' + s
                        self.loader.db['/Equip/%s/Source' % (a)] = '@'+name
                        if a[0] != '_':
                            clist += a + kListSep
                # save values
                if self.hands is not None:
                    self.loader.db['/Equip/%s/Hands' % (name)] = str(self.hands)
                # save updated equipment list
                self.loader.db[kEquipListKey] = clist[:-1] # omit last comma
            # say what we've done
            if Verbose: print name,"loaded"


    class Idle(Parser):
        kItemRe = re.compile(r'(\w+)\s+(\S+)\s+{')

        def __init__(self, loader):
            Database.Parser.__init__(self, loader)

        def Load(self, line):
            match = self.kItemRe.match(line)
            if match:
                (token,name) = match.groups()
                name = name
                if token == 'BEAST':
                    if name.find('_',1) > 0:
                        self.Error('invalid beast name -- undescore only allowed for first character')
                    if not self.kNameRe.match(name):
                        self.Error('invalid beast name -- only alphanumeric and dashes')
                    if len(name) > kTypeStringMax:
                        self.Error('invalid beast name -- must be %d characters or less' % kTypeStringMax)
                    return Database.Beast(self.loader, name.lower())
                elif token == 'EQUIP':
                    if name.find('_',1) > 0:
                        self.Error('invalid equipment name -- undescore only allowed for first character')
                    if not self.kNameRe.match(name):
                        self.Error('invalid equipment name -- only alphanumeric and dashes')
                    if len(name) > kTypeStringMax:
                        self.Error('invalid equipment name -- must be %d characters or less' % kTypeStringMax)
                    return Database.Equip(self.loader, name.lower())
                else:
                    self.Error('unknown definition type "'+token+'"')
            else:
                self.Error('invalid definition start -- use TYPE name[,name[...]] {')


    def Error(self, *text):
        print self.lineinfo+':',
        for t in text:
            print t,
        print
        exit(1)


    def LoadFile(self, file):
        parserstack = list()
        parser = self.Idle(self)

        lineno = 0
        fd = open(file,'r')
        for line in fd.xreadlines():
            # count lines
            lineno += 1
            self.lineinfo = file+':'+str(lineno)

            # remove comments
            i = line.find('#')
            if i >= 0:
                line = line[:i]
            # remove leading/trailing whitespace
            line = line.strip()
            # skip blank lines
            if len(line) == 0: continue

            # catch block termination
            if line[0] == '}':
                if not parserstack:
                    self.Error('unmatched closing brace')
                if Verbose: print "leaving state",parser.__class__.__name__,"-- back in",parserstack[-1].__class__.__name__
                parser.Done()
                parser = parserstack.pop()
                continue

            # process this line appropriately
            newparser = parser.Load(line)
            if newparser is not None:
                parserstack.append(parser)
                parser = newparser
                if Verbose: print "entered new state",parser.__class__.__name__

        self.lineinfo = file+':eof'
        if parserstack:
            self.Error('missing closing brace')
        parser.Done()
        parser = None

        if self.db.has_key(kBeastListKey): print 'Beasts:',self.db[kBeastListKey]
        if self.db.has_key(kEquipListKey): print 'Equip: ',self.db[kEquipListKey]
        fd.close()
        if '/Generate/Beast/gamesystem' in self.db: print 'Generate:',self.db['/Generate/Beast/gamesystem']
        if '/Generate/Beast/harnmaster3' in self.db: print 'Generate:',self.db['/Generate/Beast/harnmaster3']


###############################################################################
