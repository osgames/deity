###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Windows
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import datetime
import gettext
import pdb
import traceback
import wx

import deity
import deity.commands
import deity.modes
import deity.gamesystem
import deity.gamesystem.harnmaster
import deity.world.kelestia.kethira.harn

from deity.exceptions import *
from deity.watcher import *
from deity.xtype import *


###############################################################################


class UserIo(wx.Panel):
    kOutputLogFile = 'deity.output'
    kOutputLineFudge = 1.5  # reduce line count by this factor for proper page scrolling


    def __init__(self, parent, id=wx.ID_ANY):
        self.promptLen = 0;

        # create a log file for a copy of all our output
        self.logfile = open(self.kOutputLogFile, 'a')
        self.logfile.write(('=' * 79) + '\n')
        self.logfile.write(datetime.datetime.now().ctime() + '\n')
        self.logfile.write(('=' * 79) + '\n')

        wx.Panel.__init__(self, parent, id, style=wx.SUNKEN_BORDER)
        # setup
        self.inputFont = wx.Font(pointSize=wx.DEFAULT, family=wx.FONTFAMILY_DEFAULT,
                                 style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        # output window
        self.output = wx.TextCtrl(self, wx.ID_ANY,
                                  style=wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_RICH2,
                                  size=(1200,100))
        (width, height, descent, leading) = self.output.GetFullTextExtent('AgBy')
        self.outputLineHeight = height + 2 * descent
        self.output.Bind(wx.EVT_SET_FOCUS, self.OnOutputFocus)
        self.sizer.Add(self.output, 1, wx.EXPAND)
        # input box
        self.input = wx.TextCtrl(self, wx.ID_ANY,
                                 style=wx.TE_PROCESS_ENTER|wx.TE_CHARWRAP)
        self.input.SetFont(self.inputFont)
        (width, height, descent, leading) = self.input.GetFullTextExtent('AgBy')
        charheight = height + 2*descent + 1
        self.input.SetSizeHints(-1, charheight, -1, charheight, -1, 0)
        self.input.Bind(wx.EVT_CHAR, self.OnChar)  # EVT_CHAR_HOOK doesn't work
        self.sizer.Add(self.input, 0, wx.EXPAND|wx.FIXED_MINSIZE)
        # display it
        self.SetSizerAndFit(self.sizer)


    def SetFocus(self):
        self.input.SetFocus()
        self.input.SetInsertionPointEnd()


    def OnChar(self, ev):
        key = ev.GetKeyCode()
        if key == wx.WXK_BACK or key == wx.WXK_LEFT:
            pos = self.input.GetInsertionPoint()
            if pos <= self.promptLen:
                return  # don't go back if will overwrite prompt
        elif key == wx.WXK_HOME:
            self.input.SetInsertionPoint(self.promptLen)
            return
        elif key == wx.WXK_UP:
            if ev.ShiftDown():
                self.output.ScrollLines(-1)
                return
        elif key == wx.WXK_DOWN:
            if ev.ShiftDown():
                self.output.ScrollLines(1)
                return
        elif key == wx.WXK_PAGEUP:
            if ev.ShiftDown():
                lines = self.output.GetSize().GetHeight() // self.outputLineHeight
                if lines > 4: lines -= 2
                self.output.ScrollLines(-lines // self.kOutputLineFudge)
                return
        elif key == wx.WXK_PAGEDOWN:
            if ev.ShiftDown():
                lines = self.output.GetSize().GetHeight() // self.outputLineHeight
                if lines > 4: lines -= 2
                self.output.ScrollLines(lines // self.kOutputLineFudge)
                return
        elif ev.ControlDown() and ev.AltDown():
            if key == 4:  # CTRL-D
                pdb.set_trace()
                return

        ev.Skip()


    def OnOutputFocus(self, ev):
        self.SetFocus()


    def Output(self, text, style=''):
        self.logfile.write(text + '\n')

        attr = wx.TextAttr()
        if style and style[0] != '@':
            print 'error: style "%s" missing leading "@"' % style
            style = ''
        else:
            style = style[1:]
        for s in style.split(','):
            if s == '':
                pass
            elif s == 'error' or s == 'red':
                attr.SetTextColour(wx.RED)
            elif s == 'important' or s == 'blue':
                attr.SetTextColour(wx.BLUE)
            elif s == 'success' or s == 'green':
                attr.SetTextColour(wx.Colour(0,128,0))
            elif s == 'command' or s == 'bold':
                font = self.output.GetFont()
                font.SetWeight(wx.FONTWEIGHT_BOLD)
                attr.SetFont(font)
            else:
                print 'error: unknown output style "%s"' % style
        self.output.SetDefaultStyle(attr)
        #print "Style:",self.output.GetDefaultStyle().GetTextColour()
        self.output.AppendText(text)
        self.output.AppendText('\n')
        self.output.SetDefaultStyle(wx.TextAttr())
        #print "Output:",style,text,"Default:",self.output.GetDefaultStyle().GetTextColour()
        self.output.ScrollLines(-1)
        self.output.ScrollLines(1)


    def SetInputData(self, text, clear=True):
        if clear:
            self.input.Clear()
        self.input.AppendText(text)


    def SetPrompt(self, text):
        self.SetInputData(text)
        self.promptLen = len(text)


    def GetInputData(self):
        return self.input.GetValue()


###############################################################################


class Main(wx.Frame):
    """This is the Deity main window."""

    kHelpSingleLfRe = re.compile("[ 	]*\n[ 	]*(.)")


    def __init__(self, testmode=False):
        # frame settings
        wx.Frame.__init__(self, None, wx.ID_ANY, _('deity'), style=wx.DEFAULT_FRAME_STYLE|wx.MAXIMIZE)
        self.Centre()

        # system objects
        self.gs = deity.gamesystem.harnmaster.HarnMaster3()
        self.gw = deity.world.kelestia.kethira.harn.Harn()
        self.gsc = self.gs.Character # class shortcut
        self.testmode = testmode

        # user interface
        self.commandDispatch = deity.commands.CommandDispatch((_('only enough characters to uniquely identify the command are required'), _('type "help <command>" for more information on a specific command')))
        self.commandLast     = ''
        self.commandPrompt   = ''
        self.commandDefault  = ''
        self.commandRunning  = False

        # application variables
        self.modeCommand = True
        self.curchar = Watched(None)
        self.currentMode = None
        self.exiting = False

        # menu: file
        self.menuFile = wx.Menu()
        self.menuFile.Append(wx.ID_EXIT, P_('menuitem:e&xit'), P_('menuhelp:terminate this program'))
        wx.EVT_MENU(self, wx.ID_EXIT, self.OnExit)
        # menu: actions
        self.menuActions = wx.Menu()
        self.menuActionsGroup = None
        self.menuActionsFuncs = {}
        # menu: game
        self.menuGame = wx.Menu()
        menuitem = self.menuGame.Append(wx.ID_ANY, P_('menuitem:&options...'), P_('menuhelp:set game options'))
        wx.EVT_MENU(self, menuitem.GetId(), self.OnGameOptions)
        # menu: help
        self.menuHelp = wx.Menu()
        self.menuHelp.Append(wx.ID_HELP_COMMANDS, P_('menuitem:list &commands'), P_('menuhelp:list available commands'))
        wx.EVT_MENU(self, wx.ID_HELP_COMMANDS, self.OnListCommands)
        self.menuHelp.Append(wx.ID_ABOUT, P_('menuitem:&about'), P_('menuhelp:information about this program'))
        wx.EVT_MENU(self, wx.ID_ABOUT, self.OnAbout)
        # menu bar
        self.menu = wx.MenuBar()
        self.menu.Append(self.menuFile, P_('menu:&file'))
        self.menu.Append(self.menuActions, P_('menu:&actions'))
        self.menu.Append(self.menuGame, P_('menu:&game'))
        self.menu.Append(self.menuHelp, P_('menu:&help'))
        self.SetMenuBar(self.menu)
        # I/O section
        self.io = UserIo(self)
        # mode book
        self.modes = wx.Notebook(self, style=wx.NB_NOPAGETHEME)
        self.modeExplore = deity.modes.Explore(self.modes, self)
        self.modeExplore.SetClientData(self.modes.GetPageCount())
        self.modes.AddPage(self.modeExplore, _('explore'), select=True)
        self.modeCharedit = deity.modes.Charedit(self.modes, self)
        self.modeCharedit.SetClientData(self.modes.GetPageCount())
        self.modes.AddPage(self.modeCharedit, _('charedit'))
        self.modeCombat = deity.modes.Combat(self.modes, self)
        self.modeCombat.SetClientData(self.modes.GetPageCount())
        self.modes.AddPage(self.modeCombat, _('combat'))
        self.modes.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnNotebookChange)
        self.modes.SetMinSize((0, self.modeCombat.GetMinSize().GetHeight()+35))  # FIXME: GTK hack so everything fits
        # screen layout
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.modes, 2, wx.EXPAND)
        self.sizer.Add(self.io, 1, wx.EXPAND)
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)
        # make it happen
        self.CommandReset()
        self.DisplayRefresh()
        self.Show()

        # events
        self.Bind(wx.EVT_TEXT_ENTER, self.OnInputEnter)

        # output
        self.Output('This is normal text.')
        self.Output('This is error text.','@error')
        self.Output('This is command text.','@command')
        self.Output('This is important text.','@important')

        # startup
        if not testmode:
            GAME.OptionsFill()

        # test
        #choices = xlist(('a', 'bb', 'ccc', 'dddd'))
        #print self.Select('select', choices, 'select-box')
        #print self.Choose('choose', choices, 'choose-box')
        #print self.Input('input', None, r'^[0-9]+$', 'input-box')


    def OnExit(self, ev):
        self.exiting = True
        self.Close()


    def OnAbout(self, ev):
        about = wx.AboutDialogInfo()
        about.SetName(kDeityName)
        about.SetVersion(kDeityVersion)
        about.SetCopyright(P_('about:',kDeityCopyright))
        #about.SetWebSite('http://deity.sourceforge.net/') # no native support
        about.SetDevelopers(kDeityDevelopers)
        about.SetDescription(P_('about:',kDeityDescription))
        wx.AboutBox(about)


    def OnListCommands(self, ev):
        self.CommandExecute('?')


    def OnGameOptions(self, ev):
        GAME.OptionsSelect()


    def OnMenuActions(self, ev):
        evid = ev.GetId()
        if evid not in self.menuActionsFuncs:
            return
        func = self.menuActionsFuncs[evid]
        if isinstance(func, (str,unicode)):
            self.CommandInject(func, execute=True)
        else:
            func()


    def Prompt(self):
        default = ''
        if self.commandDefault:
            default = ' ['+self.commandDefault+']'
        self.io.SetPrompt(self.commandPrompt+default+'> ')


    def OnInputEnter(self, ev):
        self.CommandExecute(self.io.input.GetValue())
        self.Prompt()


    def OnNotebookChange(self, ev):
        #print 'NotebookChange:',ev.GetSelection()
        if not self.modeCommand and not self.exiting:
            self.CommandExecute(P_('cmd:',self.modes.GetPage(ev.GetSelection()).__class__.__name__.lower()))
        ev.Skip()


    def Output(self, text, style=''):
        self.io.Output(text, style)


    def Query(self, msg, title, test=None):
        qw = deity.widgets.QueryBox(msg, title=title)
        return qw.Run()


    def Choose(self, msg, choices, title, test=None):
        qw = deity.widgets.ChooseBox(msg, choices, title=title)
        return qw.Run()


    def Input(self, msg, validator, validre, title, test=None):
        qw = deity.widgets.InputBox(msg, validator, validre, title=title)
        return qw.Run()


    def Select(self, msg, choices, title, test=None):
        qw = deity.widgets.SelectBox(msg, choices, title=title)
        return qw.Run()


    def CommandLast(self):
        return self.commandDispatch.Last()


    def CommandLastClear(self):
        self.commandDispatch.Clear()


    def CommandRegister(self, cmd, func, menugroup=None, menuitem=None, menufunc=None, menuhelp=None):
        if cmd.find(':') >= 0:
            raise Exception, 'command "%s" should have no translation prefix' % cmd
        self.commandDispatch.Register(cmd, func)

        if menuitem:
            oldcount = self.menuActions.GetMenuItemCount()
            if self.menuActionsGroup is not None and self.menuActionsGroup != menugroup:
                self.menuActions.AppendSeparator()
            self.menuActionsGroup = menugroup
            id = 4000 + self.menuActions.GetMenuItemCount()
            if menuhelp:
                self.menuActions.Append(id, P_('menuitem:',menuitem), P_('menuhelp:',menuhelp))
            else:
                self.menuActions.Append(id, P_('menuitem:',menuitem))
            wx.EVT_MENU(self, id, self.OnMenuActions)
            self.menuActionsFuncs[id] = menufunc or cmd


    def CommandDispatch(self, cmd, *opts):
        self.commandDefault = ''
        self.commandRunning = True
        try:
            retval = self.commandDispatch.Dispatch(cmd, *opts)
            self.commandRunning = False
            return retval
        except Exception, e:
            self.commandRunning = False
            raise


    def CommandExecute(self, cmd, *opts):
        #print 'CommandExecute:',cmd,opts
        # remove prompt
        sep = cmd.find('>')
        if sep >= 0: cmd = cmd[sep+1:]
        cmd = cmd.strip()

        # handle default action
        if not cmd:
            cmd = self.commandDefault

        # ignore empty command
        if not cmd:
            return True

        # append any options
        if opts:
            cmd += ' ' + ' '.join(opts)

        # record current output position
        outputpos = self.io.output.GetLastPosition()

        # display command in output
        Output('')
        Output(self.commandPrompt+'> '+cmd, '@command')

        # execute the command
        try:
            msg = self.commandDispatch.Execute(cmd)
            if msg:
                Output(msg)
        except DeityException as e:
            style = '@error'
            for msg in e:
                Output(str(msg), style)
                style = None
            if isinstance(e, BadParameter):
                cmd = cmd.split()[0]
                func = self.commandDispatch.Function(cmd)
                if func:
                    doc = func.__doc__
                    if doc:
                        use = doc.find('use:')
                        if use >= 0:
                            Output(doc[use:] % {'cmd':cmd})
            if self.testmode:
                print ""
                print "==============================================================================="
                print self.commandPrompt+'> '+cmd
                print "-------------------------------------------------------------------------------"
                print self.io.output.GetValue()[outputpos:].strip()
                print "-------------------------------------------------------------------------------"
                self.DisplayRefresh()
                raise
        except Exception as e:
            Output(_('internal error -- could not execute command'), '@error')
            print ""
            print "==============================================================================="
            print self.commandPrompt+'> '+cmd
            print "-------------------------------------------------------------------------------"
            print self.io.output.GetValue()[outputpos:].strip()
            print "-------------------------------------------------------------------------------"
            self.DisplayRefresh()
            raise

        # Refresh Display
        self.DisplayRefresh()


    def CommandReset(self):
        self.commandDispatch.Reset()
        self.commandDispatch.Match(deity.kDieRollRe, self.DoDieRoll)
        self.commandDispatch.Register(P_('cmd:help'), self.DoHelp)
        self.commandDispatch.Register(P_('cmd:explore'), self.DoExplore)
        self.commandDispatch.Register(P_('cmd:charedit'), self.DoCharedit)
        self.commandDispatch.Register(P_('cmd:combat'), self.DoCombat)
        self.menuActionsFuncs = {}
        self.menuActionsGroup = None
        for item in self.menuActions.GetMenuItems():
            self.menuActions.DeleteItem(item)


    def CommandInject(self, text, execute=False):
        self.io.SetInputData(text, clear=False)
        if execute:
            self.CommandExecute(self.io.GetInputData())
            self.Prompt()


    def CommandPrompt(self, prompt):
        if prompt is None: raise Exception, 'prompt must be a (possibly empty) string'
        self.commandPrompt = prompt
        self.Prompt()


    def CommandDefault(self, cmd):
        self.commandDefault = cmd
        self.Prompt()


    def DoHelp(self, cmd, *opts):
        '''Return help about a given command.
        Type a lone question-mark (?) to get a list of available commands.
        use: %(cmd)s <command>'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        cmd = self.commandDispatch.Lookup(opts[0])

        text = P_('help:no help is available for this command')
        func = self.commandDispatch.Function(cmd)
        if func:
            doc = func.__doc__
            if doc:
                use = doc.find('use:')
                if use >= 0:
                    Output(P_('help:use:') + doc[use+4:] % {'cmd':cmd})
                    Output('')
                if help >= 0:
                    text = P_('help:',doc[:use])

            def ValSub(match):
                char = match.group(1)
                if char.isalnum():
                    return " "+char
                return "\n"+char
            text = self.kHelpSingleLfRe.sub(ValSub, text)
            Output(text)

        else:
            raise BadParameter(_('unknown command "%(cmd)s"') % {'cmd':cmd})


    def DoDieRoll(self, cmd, match):
        dcnt = match.group(1)
        if not dcnt: dcnt = 1
        dice = list()
        roll = deity.Roll(int(dcnt), int(match.group(2)), dice)
        dice.sort(reverse=True)
        dstr = str(dice[0])
        for d in dice[1:]:
            dstr += ' + '
            dstr += str(d)
        return '%s = %d' % (dstr, roll)


    def DoExplore(self, cmd, *opts):
        '''Switch to exploration mode.
        use: %(cmd)s'''

        #print "DoExplore:",cmd,opts,self.modeExplore.GetClientData()
        self.modeCommand = True
        self.modes.SetSelection(self.modeExplore.GetClientData())
        Output(_('now in exploration mode'))


    def DoCharedit(self, cmd, *opts):
        '''Switch to character editing mode.
        use: %(cmd)s'''

        #print "DoCharedit:",cmd,opts,self.modeCharedit.GetClientData()
        self.modeCommand = True
        self.modes.SetSelection(self.modeCharedit.GetClientData())
        Output(_('now in char-edit mode'))


    def DoCombat(self, cmd, *opts):
        '''Switch to combat mode.
        use: %(cmd)s'''

        #print "DoCombat:",cmd,opts,self.modeCombat.GetClientData()
        self.modeCommand = True
        self.modes.SetSelection(self.modeCombat.GetClientData())
        Output(_('now in combat mode'))


    def DisplayRefresh(self, rpage=None):
        changed = False
        if self.modeCommand:
            if self.currentMode is not None:
                self.currentMode.Deactivate()
            self.currentMode = self.modes.GetCurrentPage()
            self.currentMode.Activate()
            self.modeCommand = False

        if rpage:
            changed |= rpage.DisplayRefresh()
        else:
            changed |= self.modes.GetCurrentPage().DisplayRefresh()

        #if changed:
        #    self.sizer.SetMinSize(self.modes.GetBestSize())
        #    self.Fit()

        wx.CallAfter(self.io.SetFocus)
        return changed


###############################################################################
