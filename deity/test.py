###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program ("Test" Package)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import __builtin__
import bsddb
import re
import sys
import unittest
import wx

import deity
import deity.gettext
import deity.watcher
import deity.commands


###############################################################################


# constants
kTestWorldName = 'TestWorld'

# test databases
myBDB = bsddb.btopen(deity.kBeastiaryFilename, 'r')
myGDB = dict()
__builtin__.__dict__['BDB'] = myBDB
__builtin__.__dict__['GDB'] = myGDB

GDB['/HarnMaster3/Rules/Options'] = \
'CombatWeaponDamage;CombatWeaponWear;CombatBodyBlock;' \
'CombatUnhorse;CombatKnockback;CombatFumble;CombatStumble;CombatMinorWoundShock;' \
'CombatAmputate;CombatBleeders;WoundInfections;'
GDB['/World/%s/CurTime'%kTestWorldName] = '1000-01-01 08:00:00.0'
GDB['/World/%s/TimeZone'%kTestWorldName] = '+0000'


###############################################################################


# output capture
TestOutputBuf = None
TestOutputHistory = ''

def TestOutput(msg, style=None):
    global TestOutputBuf
    global TestOutputHistory
    TestOutputBuf = msg
    #print msg
    TestOutputHistory += msg + '\n'

def TestOutputClear():
    global TestOutputHistory
    TestOutputHistory = ''

def TestOutputGet():
    global TestOutputHistory
    output = TestOutputHistory
    TestOutputClear()
    return output

__builtin__.__dict__['Output'] = TestOutput


###############################################################################


# inputcapture
TestUserResponses = dict()

def TestUserResponse(test, response):
    TestUserResponses[test] = response

def TestGetResponse(test):
    if test not in TestUserResponses:  # pragma: no cover
        raise KeyError('user input "%s" is unknown' % test)
    res = TestUserResponses[test]
    del TestUserResponses[test]
    return res

def TestQuery(msg, title, test):
    res = TestGetResponse(test)
    return res

def TestChoose(msg, choices, title, test):
    res = TestGetResponse(test)
    if not res in choices:  # pragma: no cover
        raise KeyError('test response "%s" not valid: %s' % (res, ', '.join(choices)))
    return res

def TestInput(msg, validator, validre, title, test):
    res = TestGetResponse(test)
    if res is None: return None
    if isinstance(validre, (str,unicode)):
        validre = re.compile(validre)
    if validre is not None and not validre.match(res):  # pragma: no cover
        raise KeyError('test response "%s" does not match pattern: %s' % (res, validre.pattern))
    return res

def TestSelect(msg, choices, title, test):
    res = TestGetResponse(test)
    if res is None: return None
    match = None
    for choice in choices:
        if choice.startswith(res):
            if match is not None:
                raise KeyError('multiple matches for "%s": %s %s' % (res, match, choice))
            match = choice
    if match is None:  # pragma: no cover
        raise KeyError('test response "%s" not valid: %s' % (res, ', '.join(choices)))
    return match

__builtin__.__dict__['Query']  = TestQuery
__builtin__.__dict__['Choose'] = TestChoose
__builtin__.__dict__['Input'] = TestInput
__builtin__.__dict__['Select'] = TestSelect


###############################################################################


class TestCase(unittest.TestCase):

    def setUp(self):
        TestOutputClear()

    def tearDown(self):
        exc = sys.exc_info()
        if exc != (None, None, None):  # pragma: no cover
            output = TestOutputGet()
            if output:
                print "\n==============================================================================="
                print output,
                print "-------------------------------------------------------------------------------"
                print exc[1]
                print "===============================================================================\n\n"


###############################################################################


class App(object):

    def __init__(self):
        self.curchar = deity.watcher.Watched(None)

        self.commandDispatch = deity.commands.CommandDispatch()
        self.commandPrompt = None
        self.commandInject = ''

    def CommandPrompt(self, prompt):
        self.commandPrompt = prompt

    def CommandDefault(self, cmd):
        pass

    def CommandLast(self):
        return self.commandDispatch.Last()

    def CommandLastClear(self):
        self.commandDispatch.Clear()

    def CommandRegister(self, cmd, func, *extra):
        if cmd.find(':') >= 0:  # pragma: no cover
            raise Exception, 'command "%s" should have no translation prefix' % cmd
        self.commandDispatch.Register(cmd, func)

    def CommandDispatch(self, cmd, *opts):
        return self.commandDispatch.Dispatch(cmd, *opts)

    def CommandInject(self, text, execute=False):
        self.commandInject += text
        if execute:
            self.CommandExecute(self.commandInject)

    def CommandExecute(self, cmd, *opts):
        #print 'CommandExecute:',cmd,opts
        self.commandDispatch.Execute(cmd, *opts)
        self.commandInject = ''
        self.DisplayRefresh()

    def CommandReset(self):
        self.commandDispatch.Reset()

    def DisplayRefresh(self):
        pass


class Mode(wx.Frame):

    def __init__(self, parent, app, **kwds):
        wx.Frame.__init__(self, parent, wx.ID_ANY, 'deity test', **kwds)
        self.app = app


###############################################################################
