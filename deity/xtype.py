###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common (translatable lists)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################
#
# Note:  An "X" at the beginning of a type means "translateABLE".  An "X" at
# the end of a type means "translatED".
#
###############################################################################


import re

from deity.exceptions import *


###############################################################################


class xlatebase(object):
    kMultiItemRe = re.compile(r'^(.*)\((.*?\|.*?)\)(.*)$')


    def _xlateval(self, v, xlate, xpart):
        if xpart:
            m = xpart(v)
            if m is None or m[0] == '':
                return ''
            else:
                v = P_(xlate, m[0])
                return xpart(v, m)
        else:
            if xlate is not None:
                return P_(xlate, v)
        return v # pragma: no cover # handled by _xlate


    def _xsplit(self, v, xlate=None, xpart=None, xsep=None):
        if xsep is None: xsep = self.xsep
        if xsep is None: return [v]
        
        if xlate is None: xlate = self.xlate
        if xpart is None: xpart = self.xpart

        parts = v.split(self.xsep)
        #print 'xsplit:',v,xlate,xpart,xsep
        for i in xrange(0, len(parts)):
            xl = None
            xp = None
            if xlate is not None and i < len(xlate): xl=self.xlate[i]
            if xpart is not None and i < len(xpart): xp=self.xpart[i]
            parts[i] = xstr(parts[i], xlate=xl, xpart=xp)
        return parts


    def _xlate(self, v, xlate=None, xpart=None, xsep=None):
        if xlate is None: xlate = self.xlate
        if xpart is None: xpart = self.xpart
        if xsep  is None: xsep  = self.xsep

        if xlate is None:
            return v
        if xsep: # is not None and xsep != '':
            parts = self._xsplit(v, xlate, xpart, xsep)
            for i in xrange(0, len(parts)):
                parts[i] = parts[i].xval()
            return xsep.join(parts)
        return self._xlateval(v, xlate, xpart)
        

###############################################################################


class xval(object):

    def xprefix(self, prefix):
        p = prefix.lower()
        v = self.xval().lower()
        return v.find(p) == 0


class strx(xval):
    """A Translated String"""
    

    def __init__(self, value='', xlate=None, xpart=None, xsep=None):
        self.value = value
        self.xlate = xlate
        self.xpart = xpart
        self.xsep  = xsep
        if isinstance(value, xstr):
            self.value = value.xval()


    def __nonzero__(self):
        return self.value is not None and self.value != ''


    def __str__(self):
        return '<<%s>>' % self.value


    def xval(self):
        return self.value


class xstr(str, xlatebase, xval):
    """A Translateable String"""


    def __new__(cls, value=None, *opts, **kwds):
        if value is None:
            new = super(xstr, cls).__new__(cls)
        else:
            new = super(xstr, cls).__new__(cls, value)
        return new
    

    def __init__(self, value=None, xlate=None, xpart=None, xsep=None):
        self.xlate = xlate
        self.xpart = xpart
        self.xsep  = xsep


    def __nonzero__(self):
        return self is not None and self != ''


    def xstr(self, val=None):
        return xstr(val, xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)


    def xval(self):
        return self._xlate(self)


    def xsplit(self):
        return self._xsplit(self)


###############################################################################


class xlist(list, xlatebase):
    """A Translateable List"""


    def __init__(self, value=None, xlate=None, xpart=None, xsep=None):
        self.xlate = xlate
        self.xpart = xpart
        self.xsep  = xsep
        self.cmp   = None
        if value is not None:
            list.__init__(self, value)
        else:
            list.__init__(self)


    def _xcmp(self, a, b):
        x = self._xlate(a)
        y = self._xlate(b)
        #if x is None or y is None:
        #    return False
        if self.sortkey is not None:
            x = self.sortkey(x)
            y = self.sortkey(y)
        if self.cmp is not None:
            return self.cmp(x, y)
        return cmp(x, y)


    #-------------------------------------------------------------------------#


    def __getitem__(self, key):
        item = list.__getitem__(self, key)
        if item is None: return None
        return xstr(item, xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)


    def __iter__(self):
        for n in xrange(0, len(self)):
            i = self[n]
            m = self.kMultiItemRe.match(i)
            if m:
                for o in str(m.group(2)).split('|'):
                    if o == "": continue
                    x = self.xstr(str(m.group(1))+o+str(m.group(3)))
                    yield x
            else:
                yield i


    def __contains__(self, key):
        for i in self:
            if key == i: return True
        return False


    #-------------------------------------------------------------------------#


    def xstr(self, val=None):
        return xstr(val, xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)


    def xlist(self):
        return xlist(xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)


    def xenumerate(self, index=False):
        for n in xrange(0, len(self)):
            i = self[n]
            m = self.kMultiItemRe.match(i)
            if m:
                for o in str(m.group(2)).split('|'):
                    if o == "": continue
                    i = xstr(str(m.group(1))+o+str(m.group(3)), xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)
                    x = self._xlate(i)
                    if index:
                        yield (n, x)
                    else:
                        yield (i, x)
            else:
                x = self._xlate(i)
                #if x is None:
                #    continue
                if index:
                    yield (n, x)
                else:
                    yield (i, x)


    def xitems(self, index=False):
        l = list()
        for ix in self.xenumerate(index):
            l.append(ix[1])
        return l


    def xsplit(self):
        items = []
        for i in self:
           parts = self._xsplit(i)
           items.append(parts)
        return items


    def xsearch_re(self, regex, index=False):
        for ix in self.xenumerate(index):
            m = regex.search(ix[1])
            if m: return (ix[0], m)
        return None


    def xsearch(self, string, reopt=None, index=False):
        if reopt is None: reopt = re.I
        r = re.compile('^('+string+')$', reopt)
        m = self.xsearch_re(r, index)
        if m: return m[0]
        return None


    def xfind(self, item, index=False):
        for i,x in self.xenumerate(index):
            if x == item:
                return i


    def xprefix(self, prefix, index=False):
        p = prefix.lower()
        if index:
            m = list()
        else:
            m = xlist(xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)
        for ix in self.xenumerate(index):
            (i,x) = (ix[0],ix[1].lower())
            if x.find(p) == 0:
                m.append(i)
        if len(m) > 1: m.sort()
        return m


    def xsort(self, cmp=None, key=None, reverse=False):
        self.sortkey = key
        self.cmp = cmp
        retval = self.sort(self._xcmp, key=None, reverse=reverse)
        self.cmp = None
        return retval


    def xjoin(self, sep):
        s = ''
        for i,x in self.xenumerate():
            if i == '' or x == '': continue
            if s == '':
                s = x
            else:
                s += sep + x
        return s


    #-------------------------------------------------------------------------#


    def find(self, item, index=False):
        for i in xrange(0, len(self)):
            if self[i] == item:
                if index:
                    return i
                else:
                    return self[i]


    def prefix(self, prefix, index=False):
        p = prefix.lower()
        if index:
            m = list()
        else:
            m = xlist(xlate=self.xlate, xpart=self.xpart, xsep=self.xsep)
        for i in xrange(0,len(self)):
            x = self[i]
            if x.lower().find(p) == 0:
                if index:
                    m.append(i)
                else:
                    m.append(x)
        if len(m) > 1: m.sort()
        return m


###############################################################################


class xdict(dict, xlatebase):
    """A Translateable Dictionary"""


    def __init__(self, value=None, xlatekey=None, xlateval=None, xpartkey=None, xpartval=None, xsepkey=None, xsepval=None):
        self.xlatekey = xlatekey
        self.xlateval = xlateval
        self.xpartkey = xpartkey
        self.xpartval = xpartval
        self.xsepkey  = xsepkey
        self.xsepval  = xsepval
        if value is not None:
            dict.__init__(self, value)
        else:
            dict.__init__(self)


    def xkeydict(self, val=None):
        return xdict(val, xlatekey=self.xlatekey, xpartkey=self.xpartkey, xsepkey=self.xsepkey)


    def xkeystr(self, val=None):
        return xstr(val, xlate=self.xlatekey, xpart=self.xpartkey, xsep=self.xsepkey)


    def xkeylist(self):
        return xlist(xlate=self.xlatekey, xpart=self.xpartkey, xsep=self.xsepkey)


    def xvalstr(self, val=None):
        return xstr(val, xlate=self.xlateval, xpart=self.xpartval, xsep=self.xsepval)


    def xvallist(self):
        return xlist(xlate=self.xlateval, xpart=self.xpartval, xsep=self.xsepval)


    def iterkeys(self):
        for k in dict.iterkeys(self):
            if isinstance(k, str): k = self.xkeystr(k)
            yield k


    def keys(self):
        keys = xlist(xlate=self.xlatekey, xpart=self.xpartkey, xsep=self.xsepkey)
        keys.extend(dict.keys(self))
        return keys


    def itervalues(self):
        for v in dict.itervalues(self):
            if isinstance(v, str): v = self.xvalstr(v)
            yield v


    def values(self):
        if self.xlateval is not None:
            vals = xlist(xlate=self.xlateval, xpart=self.xpartval, xsep=self.xsepval)
            vals.extend(dict.values(self))
            return vals
        else:
            return dict.values(self)


    def iteritems(self):
        for k,v in dict.iteritems(self):
            if isinstance(k, str): k = self.xkeystr(k)
            if isinstance(v, str): v = self.xvalstr(v)
            yield(k, v)


    def items(self):
        return list(self.iteritems())


###############################################################################


def XLookup(vallist, valx, typename="", defindex=None, multiple=False, index=False):
    if typename:
        typename = " " + _(typename)
    if vallist is None:
        raise BadParameter(_('no%(type)s values') % {'type':typename})
    if not valx:
        if defindex is None:
            raise BadParameter(_('no%(type)s value specified -- %(opts)s') % {'type':typename, 'opts':', '.join(vallist.xitems())})
        else:
            if defindex >= len(vallist):
                raise BadParameter(_('no%(type)s value #%(index)d -- %(opts)s') % {'type':typename, 'index':defindex, 'opts':', '.join(vallist.xitems())})
            if index:
                return defindex
            return vallist[defindex];
    if isinstance(valx, strx) or isinstance(valx, xstr):
        vald = valx.xval()
        if not multiple:
            match = vallist.xfind(vald, index=index)
            if match:
                return match
        opts = vallist.xprefix(vald, index=index)
    else:
        vald = str(valx)
        if not multiple:
            match = vallist.find(vald, index=index)
            if match:
                return match
        opts = vallist.prefix(vald, index=index)
    if index and opts and not isinstance(opts[0], int):  # pragma: no cover
        raise Exception('request for indecies did not return integer')
    if multiple:
        return opts
    if len(opts) == 0:
        items = vallist.xitems()
        items.sort()
        raise BadParameter(_('no%(type)s "%(val)s" -- %(opts)s') % {'type':typename, 'val':vald, 'opts':', '.join(items)})
    if len(opts) > 1:
        items = vallist.xitems()
        items.sort()
        raise BadParameter(_('ambiguous%(type)s "%(val)s" -- %(opts)s') % {'type':typename, 'val':vald, 'opts':', '.join(items)})
    return opts[0]


###############################################################################
