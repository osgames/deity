###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common Widgets
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


###############################################################################


class DeityException(Exception):
    pass


#    def __init__(self, xtext, **kwds):
#        super(DeityException, self).__init__(xtext, **kwds)


#    def __str__(self):
#        return self.message
    

###############################################################################


class NotPossible(DeityException):
    pass


###############################################################################


class DatafillError(DeityException):
    pass


###############################################################################


class BadParameter(DeityException):
    pass


###############################################################################
