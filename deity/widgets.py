###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common Widgets
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import re
import wx

from deity.xtype import *
from deity.exceptions import *

kScrollbarWidth = 25
kTextCtrlWidth  = 12


###############################################################################


class DWidget(object):


    def SizeWidth(self, size, prefix='', postfix='', font=None):
        if isinstance(size, str):
            (width, height, descent, externalLeading) = self.GetFullTextExtent(prefix+size+postfix, font)
            return width

        if isinstance(size, (list,tuple)):
            width = list[0]

        if isinstance(size, int):
            if prefix != '' or postfix != '':
                (width, height, descent, externalLeading) = self.GetFullTextExtent(prefix+postfix, font)
            else:
                width = 0
            return self.GetCharWidth() * size + width


    def SizeHeight(self, size=0, font=None):
        if isinstance(size, str):
            (width, height, descent, externalLeading) = self.GetFullTextExtent(size, font)
            return height

        if isinstance(size, (list,tuple)):
            width = list[1]

        if isinstance(size, int):
            (width, height, descent, externalLeading) = self.GetFullTextExtent('X', font)
            return height


    def SizeTitleBar(self, title, framing=True, closebutton=True):
        font = self.GetFont()
        font.SetWeight(wx.FONTWEIGHT_BOLD)
        (width, height, descent, externalLeading) = self.GetFullTextExtent(title, font)
        if framing:
            width += 25
        if closebutton:
            width += 30
        return width


    def Format(self, v, template=None):
        if v is None:
            return '-'
        if template is None:
            template = self.template
        if self.template:
            v = self.template % v
        elif isinstance(v, xstr):
            v = v.xval()
        else:
            v = str(v)
        return v


    def CancelOnEscape(self, ev):
        key = ev.GetKeyCode()
        if key == wx.WXK_ESCAPE:
            self.EndModal(wx.ID_CANCEL)
        else:
            ev.Skip()


###############################################################################


class CopyValidator(wx.PyValidator):

    def __init__(self, container, element, **kwds):
        wx.PyValidator.__init__(self, **kwds)
        self.container = container
        self.element = element

    def TranferFromWindow(self):
        print 'CopyValidator::TransferFromWindow:',self.element,self.container
        return True

    def TranferToWindow(self):
        print 'CopyValidator::TransferToWindow:',self.element,self.container
        return True


###############################################################################


class QueryBox(wx.Dialog, DWidget):
    kPadding = 10


    def __init__(self, text, yes=None, no=None, **kwds):
        wx.Dialog.__init__(self, None, **kwds)
        minwidth = 0
        if "title" in kwds:
            minwidth = self.SizeTitleBar(kwds["title"])

        if yes is None: yes = P_('btn:yes')
        if no  is None: no  = P_('btn:no')
        self.sizer0 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1 = wx.BoxSizer(wx.VERTICAL)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1.AddSpacer(self.kPadding)
        self.message = wx.StaticText(self, wx.ID_ANY, text, style=wx.ALIGN_CENTER)
        self.sizer1.Add(self.message, 1, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer2.AddSpacer(self.kPadding)
        self.ybutton = wx.Button(self, wx.ID_OK, yes)
        self.sizer2.Add(self.ybutton, 1, wx.EXPAND)
        self.sizer2.AddSpacer(self.kPadding)
        self.nbutton = wx.Button(self, wx.ID_CANCEL, no)
        self.sizer2.Add(self.nbutton, 1, wx.EXPAND)
        self.sizer2.AddSpacer(self.kPadding)
        self.sizer1.Add(self.sizer2, 0, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer0.AddSpacer(self.kPadding)
        self.sizer0.Add(self.sizer1, 1, wx.EXPAND)
        self.sizer0.AddSpacer(self.kPadding)

        self.SetSizerAndFit(self.sizer0)
        self.SetMinSize((minwidth, 0))
        #self.Bind(wx.EVT_CHAR_HOOK, self.CancelOnEscape)  # ESC probably shouldn't be allowed
        #self.SetFocus()


    def Run(self):
        result = self.ShowModal()
        self.Destroy()
        if result == wx.ID_OK:
            return True
        return False


###############################################################################


class ChooseBox(wx.Dialog, DWidget):
    kPadding = 10


    def __init__(self, text, choices, **kwds):
        wx.Dialog.__init__(self, None, **kwds)
        minwidth = 0
        if "title" in kwds:
            minwidth = self.SizeTitleBar(kwds["title"])

        self.choices = choices
        self.cbase = 10000
        self.sizer0 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1 = wx.BoxSizer(wx.VERTICAL)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1.AddSpacer(self.kPadding)
        self.message= wx.StaticText(self, wx.ID_ANY, text, style=wx.ALIGN_CENTER)
        self.sizer1.Add(self.message, 1, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer2.AddSpacer(self.kPadding)
        for i in xrange(len(choices)):
            c = choices[i]
            b = wx.Button(self, i+self.cbase, c.xval())
            self.sizer2.Add(b, 1, wx.EXPAND)
            self.sizer2.AddSpacer(self.kPadding)
        self.sizer1.Add(self.sizer2, 0, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer0.AddSpacer(self.kPadding)
        self.sizer0.Add(self.sizer1, 1, wx.EXPAND)
        self.sizer0.AddSpacer(self.kPadding)

        self.SetSizerAndFit(self.sizer0)
        self.SetMinSize((minwidth, 0))
        self.Bind(wx.EVT_BUTTON, self.OnButton)
        self.Bind(wx.EVT_CHAR_HOOK, self.CancelOnEscape)
        self.SetFocus()


    def OnButton(self, ev):
        self.EndModal(ev.GetId())


    def Run(self):
        result = self.ShowModal() - self.cbase
        self.Destroy()
        if 0 <= result < len(self.choices):
            return self.choices[result]
        return None


###############################################################################


class InputBox(wx.Dialog, DWidget):
    kPadding = 10


    def __init__(self, text, validator=None, validre=None, **kwds):
        wx.Dialog.__init__(self, None, **kwds)
        minwidth = 0
        if "title" in kwds:
            minwidth = self.SizeTitleBar(kwds["title"])
        if isinstance(validre, (str,unicode)):
            validre = re.compile(validre)
        if validator is None:
            validator = wx.DefaultValidator
        self.validre = validre

        self.sizer0 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1 = wx.BoxSizer(wx.VERTICAL)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1.AddSpacer(self.kPadding)
        self.message = wx.StaticText(self, wx.ID_ANY, text, style=wx.ALIGN_CENTER)
        self.message.Wrap(300)
        self.sizer1.Add(self.message, 1, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer2.AddSpacer(self.kPadding)
        self.input = wx.TextCtrl(self, wx.ID_ANY, validator=validator,
                                 style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnInput)
        self.sizer2.Add(self.input, 1, wx.EXPAND)
        self.sizer2.AddSpacer(self.kPadding)
        self.sizer1.Add(self.sizer2, 0, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer0.AddSpacer(self.kPadding)
        self.sizer0.Add(self.sizer1, 1, wx.EXPAND)
        self.sizer0.AddSpacer(self.kPadding)
        self.SetSizerAndFit(self.sizer0)
        self.SetMinSize((minwidth, 0))
        self.Bind(wx.EVT_CHAR_HOOK, self.CancelOnEscape)
        self.input.SetFocus()


    def OnInput(self, ev):
        self.invalue = self.input.GetValue()
        if self.validre is not None and not self.validre.match(self.invalue):
            wx.Bell()
        else:
            self.EndModal(wx.ID_OK)


    def Run(self):
        result = self.ShowModal()
        self.Destroy()
        if result == wx.ID_OK:
            return self.invalue
        return None


###############################################################################


class SelectBox(wx.Dialog, DWidget):
    kPadding = 10


    def __init__(self, text, choices, **kwds):
        wx.Dialog.__init__(self, None, **kwds)
        self.choices = choices
        minwidth = 0
        if "title" in kwds:
            minwidth = self.SizeTitleBar(kwds["title"])

        self.sizer0 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1 = wx.BoxSizer(wx.VERTICAL)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer1.AddSpacer(self.kPadding)
        self.message = wx.StaticText(self, wx.ID_ANY, text, style=wx.ALIGN_CENTER)
        self.message.SetMinSize((200,-1))
        self.sizer1.Add(self.message, 1, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer2.AddSpacer(self.kPadding)
        self.input = wx.ListBox(self, wx.ID_ANY)
        for c in choices:
            self.input.Append(c.xval())
        self.input.SetMinSize((-1, len(choices)*(self.SizeHeight()+1)))
        self.Bind(wx.EVT_LISTBOX, self.OnInput)
        self.sizer2.Add(self.input, 1, wx.EXPAND)
        self.sizer2.AddSpacer(self.kPadding)
        self.sizer1.Add(self.sizer2, 0, wx.EXPAND)
        self.sizer1.AddSpacer(self.kPadding)
        self.sizer0.AddSpacer(self.kPadding)
        self.sizer0.Add(self.sizer1, 1, wx.EXPAND)
        self.sizer0.AddSpacer(self.kPadding)
        self.SetSizerAndFit(self.sizer0)
        self.SetMinSize((minwidth, 0))
        self.Bind(wx.EVT_CHAR_HOOK, self.CancelOnEscape)
        self.SetFocus()


    def OnInput(self, ev):
        self.invalue = ev.GetInt()
        self.EndModal(wx.ID_OK)


    def Run(self):
        result = self.ShowModal()
        self.Destroy()
        if result == wx.ID_OK:
            return self.choices[self.invalue]
        return None


###############################################################################


class SearchableListBox(wx.Panel, DWidget):

    def __init__(self, parent, id, **kwds):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        self.vsizer = wx.BoxSizer(wx.VERTICAL)
        self.hsizer = wx.BoxSizer(wx.HORIZONTAL)
        self.search = wx.TextCtrl(self, wx.ID_ANY)
        self.hsizer.Add(self.search, 1, wx.EXPAND)
        self.clearb = wx.Button(self, wx.ID_ANY, label='X', style=wx.BU_EXACTFIT)
        self.hsizer.Add(self.clearb)
        self.vsizer.Add(self.hsizer, 0, wx.EXPAND)
        self.lstbox = wx.ListBox(self, id, **kwds)
        self.vsizer.Add(self.lstbox, 1, wx.EXPAND)
        self.SetSizerAndFit(self.vsizer)
        self.SetMinSize(self.lstbox.GetMinSize())  # needed in GTK or appears very narrow

        self.clearb.Bind(wx.EVT_BUTTON, self.OnClearButton)
        self.search.Bind(wx.EVT_TEXT, self.OnSearchChange)

        self.items = []


    def OnClearButton(self, ev):
        self.search.Clear()
        self.search.SetFocus()


    def OnSearchChange(self, ev):
        self.UpdateListItems()


    def UpdateListItems(self):
        match = self.search.GetValue().lower()
        self.lstbox.Clear()
        for item in self.items:
            if match == '' or match in item.lower():
                self.lstbox.Append(item)
        if self.lstbox.GetCount() == 1:
            self.lstbox.SetSelection(0)


    def Append(self, item):
        self.items.append(item)
        match = self.search.GetValue().lower()
        if match == '' or match in item.lower():
            self.lstbox.Append(item)
        #self.Fit()


    def Clear(self):
        self.items = []
        self.lstbox.Clear()


    def GetStringSelection(self):
        return self.lstbox.GetStringSelection()


    def SetStringSelection(self, item):
        return self.lstbox.SetStringSelection(item)


###############################################################################


class SelectableItemList(wx.ListBox, DWidget):
    kDisplayExtractRe = re.compile(r'^\s*(\S+)\b')
    kDisplayIndent    = '     '


    def __init__(self, parent, width, id=wx.ID_ANY, style=0, collapse=None, **kwds):
        style |= wx.LB_NEEDED_SB
        wx.ListBox.__init__(self, parent, id, style=style, **kwds)
        self.Bind(wx.EVT_LISTBOX, self.OnListBox)
        self.selection  = ''
        self.selectnum  = None
        self.selections = set()
        self.display    = list()
        self.names      = list()
        self.keyindex   = dict()
        self.aliases    = dict()
        self.infos      = dict()
        self.multi      = (style & wx.LB_MULTIPLE)
        self.collapse	= collapse
        width = self.SizeWidth(width, prefix=self.kDisplayIndent)
        self.SetMinSize(wx.Size(width,-1))


    def DisplayFromIndex(self, index):
        #print 'DisplayFromIndex:',index,self.names
        name = self.names[index]
        string = self.kDisplayIndent + name.xval()
        if name in self.aliases and self.aliases[name] != '':
            string += ' (%s)' % self.aliases[name]
        if name in self.infos and self.infos[name] != '':
            string += ' [%s]' % self.infos[name]
        return string


    def NamexFromDisplay(self, display):
        match = self.kDisplayExtractRe.match(display)
        if not match: return None
        return strx(match.group(1))


    def IndexFromNamex(self, namex):
        if isinstance(namex, strx):
            namex = namex.xval()
            #print 'IndexFromNamex:',namex,self.display
            for i in xrange(len(self.names)):
                #print ' -',self.display[i], self.names[i]
                if self.names[i] is not None and self.names[i].xval() == namex:
                    return i
        else:
            #print 'IndexFromNamex:',namex,self.display
            for i in xrange(len(self.names)):
                #print ' -',self.display[i], NamexFromDisplay(self.display[i])
                if self.names[i] == namex:
                    return i
        #raise BadParameter(_('"%(item)s" is unknown') % {'item':namex})
        return None


#    def IndexFromKey(self, key):
#        if key not in self.keyindex:
#            raise BadParameter(_('"%(thing)s" is unknown') % {'thing':key})
#        return self.keyindex[key]


    def IndexFromValue(self, value):
        for i in xrange(0, len(self.values)):
            if self.values[i] == value:
                return i
        raise BadParameter(_('"%(thing)s" is unknown') % {'thing':value})


    def SetItems(self, items, order=None):
        self.selectnum  = None
        self.selections = set()
        self.keys       = items.xkeylist()
        self.values     = list()
        self.names      = list()
        self.display    = list()
        self.keyindex   = dict()
        lastheading     = heading = ''
        oldaliases      = self.aliases
        oldinfos        = self.infos
        self.aliases    = dict()
        self.infos      = dict()

        cinfo = None
        if self.collapse is not None:
            cinfo = self.collapse.Get()

        if order is None:
            order = items.keys()
            order.xsort()

        #print "SetItems:",items
        collapsed = False
        for i in order:
            l = i.xsplit()
            name = l[-1]
            if len(l) > 1:
                heading = l[-2]
            else:
                name = i

            #print 'SetItems:',i,items[i],type(items[i])
            if heading != lastheading:
                self.keys.append('*'+heading)
                self.values.append(None)
                self.names.append(None)
                cmark = ''
                if cinfo is not None:
                    if heading not in cinfo:
                        cinfo[heading] = False
                    if cinfo[heading]:
                        cmark = '+ '
                        collapsed = True
                    else:
                        cmark = '- '
                        collapsed = False
                self.display.append(cmark+heading.xval()+':')
                lastheading = heading
            if name in oldaliases:
                self.aliases[name] = oldaliases[name]
            if name in oldinfos:
                self.infos[name] = oldinfos[name]
            if not collapsed:
                if name == self.selection:
                    self.selectnum = len(self.display)
                self.keyindex[i] = len(self.display)
                self.keys.append(i)
                self.values.append(items[i])
                self.names.append(name)
                self.display.append(self.DisplayFromIndex(len(self.keys)-1))
        #print 'SetItems:',self.keys, self.values, self.display
        self.Set(self.display)
        if self.selectnum is not None:
            self.Select(self.selectnum)


    def SetAlias(self, namex, alias):
        dindex = self.IndexFromNamex(namex)
        if dindex is None:
            return
        name = self.names[dindex]
        if alias is None:
            if name in self.aliases:
                del self.aliases[name]
        else:
            self.aliases[name] = alias
        #print 'SetAlias:',name,dindex,self.aliases
        self.display[dindex] = self.DisplayFromIndex(dindex)
        self.SetString(dindex, self.display[dindex])


    def SetInfo(self, namex, info):
        dindex = self.IndexFromNamex(namex)
        if dindex is None:
            return
        name = self.names[dindex]
        if info is None:
            if name in self.infos:
                del self.infos[name]
        else:
            self.infos[name] = info
        #print 'SetInfo:',key,dindex,self.infos
        self.display[dindex] = self.DisplayFromIndex(dindex)
        self.SetString(dindex, self.display[dindex])


    def GetEntries(self):
        return self.values


    def GetStringEntries(self):
        return self.names


    def GetString(self, index):
        return self.keys[index];


    def GetSelectionIndex(self):
        if self.multi:
            dindex = self.selectnum  # return last selected item
        else:
            dindex = wx.ListBox.GetSelection(self)
        if dindex is None or dindex < 0: return None
        return dindex


    def GetSelection(self):
        dindex = self.GetSelectionIndex()
        if dindex is None: return None
        return self.values[dindex]


    def GetStringSelection(self):
        dindex = self.GetSelectionIndex()
        if dindex is None: return None
        return self.names[dindex]


    def GetSelections(self):
        sel = list()
        for i in wx.ListBox.GetSelections(self):
            if i < len(self.values):  # can return 0 when no items in list
                sel.append(self.values[i])
        return sel


    def GetStringSelections(self):
        sel = list()
        for i in wx.ListBox.GetSelections(self):
            sel.append(self.names[i])
        return sel


    def SetSelection(self, value, select=True):
        if value is None: return
        dindex = self.IndexFromValue(value)
        wx.ListBox.SetSelection(self, dindex, select)


    def SetStringSelection(self, namex, select=True):
        if namex is None or namex == '': return
        dindex = self.IndexFromNamex(namex)
        if dindex is None:
            return
        wx.ListBox.SetSelection(self, dindex, select)


    def ExpandHeading(self, heading):
        if self.collapse is not None:
            cinfo = self.collapse.Alter()
            cinfo[heading] = not cinfo[heading]
            return True
        return False


    def OnListBox(self, ev):
        #print "SelectableItemList::OnListBox:",self.GetStringSelections(),ev.IsSelection(),self.keys,self.selections
        oldselections = self.selections
        self.selections = set(wx.ListBox.GetSelections(self))
        if not self.multi and not ev.IsSelection():
            ev.Skip()
            return

        newselections = self.selections - oldselections
        if len(newselections) >= 1:
            newsel = list(newselections)[0]
        else:
            newsel = self.selectnum  # selecting same item

        #print "select:", oldselections, "-", self.selections, "=>", newsel, "of", self.keys
        if newsel >= len(self.keys):  # pragma: no cover
            #print 'error: new selection', newsel, 'out of range of keys:', self.keys
            #print '- listbox.GetSelections:', wx.ListBox.GetSelections(self)
            #print '- listbox.GetItems:', wx.ListBox.GetItems(self)
            # listbox can return selection==0 when no items
            ev.Skip()
            return
        if newsel is not None and self.keys[newsel][0] == '*':
            # heading was selected: don't select and notify
            if self.selectnum is not None:
                wx.ListBox.SetSelection(self, self.selectnum, True)
            else:
                wx.ListBox.SetSelection(self, newsel, False)
            self.selections.remove(newsel)
            self.ExpandHeading(self.keys[newsel][1:])
            return

        self.selectnum = newsel
        ev.Skip()


###############################################################################


class OverlayableImage(wx.StaticBitmap, DWidget):
    def __init__(self, parent, **kwds):
        wx.StaticBitmap.__init__(self, parent, **kwds)
        self.SetBackgroundColour(wx.WHITE)


###############################################################################


class InfoTable(wx.Panel, DWidget):

    def __init__(self, parent, items=None, order=None, cols=1, padding=1, spacing='',
                 width=None, template=None, font=None, flex=True, expand=False, **kwds):
        wx.Panel.__init__(self, parent, **kwds)
        self.cols    = cols
        self.template= template
        self.padding = padding
        self.spacing = spacing
        self.expand  = expand
        self.widgets = None
        self.oldorder= list()
        if flex:
            self.sizer = wx.FlexGridSizer(0, self.cols*3-1, self.padding, self.padding)
            self.sizer.SetFlexibleDirection(wx.HORIZONTAL)
            if expand:
                self.sizer.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)
        else:
            self.sizer = wx.GridSizer(0, self.cols*3-1, self.padding, self.padding)

        # set font
        if font is not None:
            self.SetFont(font)

        # create full structure if we have the info
        if items is not None:
            self.SetItems(items, order)

        # set window size
        self.SetSizerAndFit(self.sizer)
        if width:
            width = self.SizeWidth(width)
            self.sizer.SetMinSize(wx.Size(width,-1))


    def SetItems(self, items, order=None):
        # set display order
        if not order:
            if items is not None:
                order = items.keys()
                order.sort()

        # if item list is identical: update existing structure
        if self.widgets is not None and self.oldorder == order:
            # update values
            for k,w in self.widgets.iteritems():
                if k in items:
                    w.SetLabel(self.Format(items[k]))
                else:
                    w.SetLabel(' ')
            return

        # build widget from scratch
        self.sizer.Clear(True)
        self.widgets = None

        # stop if nothing to do
        if order is None or len(order) == 0:
            return

        self.widgets = dict()
        self.oldorder = order
        ocount = len(order)
        rows   = int((ocount + self.cols - 1) / self.cols)
        cols   = self.cols * 3 - 1
        #print "Info Rows:",rows,"Cols:",cols

        for r in xrange(rows):
            for c in xrange(self.cols):
                i = r * self.cols + c
                if i >= len(items): break

                k = h = order[i]
                if not isinstance(k, xstr):
                    k = items.xkeystr(k)
                #print ' -',r,c,i,h,items[h],'->',xstr(h, xlate=items.xlatekey, xpart=items.xpartkey).xval(),self.Format(items[h])

                w = wx.StaticText(self, wx.ID_ANY, label=k.xval()+': ', style=wx.ALIGN_LEFT|wx.ST_NO_AUTORESIZE)
                self.sizer.Add(w, 1, wx.EXPAND|wx.LEFT, border=self.padding)

                w = wx.StaticText(self, wx.ID_ANY, label=self.Format(items[h]), style=wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE)
                self.sizer.Add(w, 1, wx.EXPAND|wx.RIGHT, border=self.padding)
                self.widgets[h] = w

                if c != self.cols - 1:
                    w = wx.StaticText(self, wx.ID_ANY, label=self.spacing)
                    self.sizer.Add(w, 1, wx.EXPAND)

        if cols == 2:
            self.sizer.AddGrowableCol(1,1)
        else:
            for c in xrange(2,cols,3):
                #print "Growable:",c
                self.sizer.AddGrowableCol(c,1)

        self.Fit()


###############################################################################


class InfoList(wx.ScrolledWindow, DWidget):
    kIndent = '   '


    def __init__(self, parent, padding=1, labels=None, template=None,
                 font=None, scrollable=True, **kwds):
        wx.ScrolledWindow.__init__(self, parent, **kwds)
        self.template   = template
        self.scrollable = scrollable
        self.padding    = padding
        #self.widgets = dict()

        # set font
        if font is not None:
            self.SetFont(font)

        maxwidth = -1
        #print 'InfoList:',labels
        if labels is not None:
            for l in labels:
                width = self.SizeWidth(l.xval()+':'+self.Format(88), self.kIndent) + padding*4
                if width > maxwidth: maxwidth = width
            maxwidth += kScrollbarWidth

        self.sizer = wx.FlexGridSizer(0, 2, self.padding, self.padding)
        self.sizer.SetFlexibleDirection(wx.HORIZONTAL)
        self.sizer.AddGrowableCol(0, 1)
        self.SetSizerAndFit(self.sizer)
        self.SetMinSize((maxwidth,-1))


    def SetItems(self, items, order=None):
        #print 'InfoList:',items,order
        self.sizer.Clear(True)
        #self.widgets = dict()
        self.SetScrollbars(0,0,0,0)

        if not order:
            order = items.keys()
            order.sort()

        space = ''
        for i in order:
            k = i
            if not isinstance(k, xstr):
                k = items.xkeystr(k)

            if i not in items:
                w = wx.StaticText(self, wx.ID_ANY, label=k.xval()+':')
                self.sizer.Add(w, 1, wx.EXPAND|wx.LEFT, border=self.padding)
                w = wx.StaticText(self, wx.ID_ANY, label=' ')
                self.sizer.Add(w, 0, wx.EXPAND|wx.RIGHT)
                space = self.kIndent
            else:
                w = wx.StaticText(self, wx.ID_ANY, label=space+k.xval()+':', style=wx.ALIGN_LEFT)
                self.sizer.Add(w, 1, wx.EXPAND|wx.LEFT, border=self.padding)
                w = wx.StaticText(self, wx.ID_ANY, label=self.Format(items[i]), style=wx.ALIGN_RIGHT)
                self.sizer.Add(w, 1, wx.EXPAND|wx.RIGHT, border=self.padding)

        if order:
            (width,height) = w.GetClientSizeTuple()
            self.SetScrollbars(0, height, 0, len(order)*2)


###############################################################################


class EditList(wx.ScrolledWindow, DWidget):
    kIndent = '   '


    def __init__(self, parent, valwidth, valmgr=None, padding=1, labels=None,
                 font=None, scrollable=False, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.TAB_TRAVERSAL
        wx.ScrolledWindow.__init__(self, parent, **kwds)

        self.valuemgr   = valmgr
        self.scrollable = scrollable
        self.padding    = padding
        self.curchange  = None
        self.editwidth  = self.SizeWidth(valwidth) + kTextCtrlWidth

        # set font
        if font is not None:
            self.SetFont(font)

        maxwidth = -1
        #print 'EditList:',labels
        if labels is not None:
            for l in labels:
                width = self.SizeWidth(self.kIndent+l.xval()+':') + self.editwidth + padding*4
                if width > maxwidth: maxwidth = width
            maxwidth += kScrollbarWidth

        self.sizer = wx.FlexGridSizer(0, 2, self.padding, self.padding)
        self.sizer.SetFlexibleDirection(wx.HORIZONTAL)
        self.sizer.AddGrowableCol(0, 1)
        self.SetSizerAndFit(self.sizer)
        self.SetMinSize((maxwidth, -1))


    def SetItems(self, items, order=None, validator=None):
        #print 'EditList::SetItems:',items,order
        self.sizer.Clear(True)
        self.keywidgetmap = dict()
        self.widgetkeymap = dict()
        self.valuekeymap  = dict()
        self.colored = list()
        self.validator = validator
        self.items = items
        self.SetScrollbars(0,0,0,0)

        if order is None:
            order = items.keys()
            order.sort()

        space = ''
        for i in order:
            k = i
            if not isinstance(k, xstr):
                k = items.xkeystr(k)

            if i not in items:
                # heading
                w = wx.StaticText(self, wx.ID_ANY, label=k.xval()+':')
                self.sizer.Add(w, 1, wx.EXPAND|wx.LEFT, border=self.padding)
                w = wx.StaticText(self, wx.ID_ANY, label=' ')
                self.sizer.Add(w, 0, wx.EXPAND|wx.RIGHT)
                space = self.kIndent
            else:
                # value entry
                value = items[i]
                if not isinstance(value, str):
                    raise Exception('%s: can only edit string values (was %s:%s) -- use validator to limit otherwise'
                                    % (str(i), type(value), str(value)))
                if self.valuemgr is not None:
                    self.valuekeymap[value] = i
                    value = self.valuemgr.GetValue(value, notify=self.ValueUpdated)
                    if value is None:
                        value = ''
                validator = None
                if self.validator is not None:
                    if isinstance(self.validator, dict):
                        if i in self.validator:
                            validator = self.validator[i]
                    else:
                        validator = self.validator
                w = wx.StaticText(self, wx.ID_ANY, label=space+k.xval()+':', style=wx.ALIGN_LEFT)
                self.sizer.Add(w, 1, wx.EXPAND|wx.LEFT, border=self.padding)
                if validator is not None:
                    w = wx.TextCtrl(self, wx.ID_ANY, value=value, style=wx.ALIGN_RIGHT|wx.TE_RIGHT, size=(self.editwidth,-1), validator=validator)
                else:
                    w = wx.TextCtrl(self, wx.ID_ANY, value=value, style=wx.ALIGN_RIGHT|wx.TE_RIGHT, size=(self.editwidth,-1))
                #w.Bind(wx.EVT_SET_FOCUS, self.OnSetFocus)
                w.Bind(wx.EVT_KILL_FOCUS, self.OnKillFocus)
                self.sizer.Add(w, 1, wx.EXPAND|wx.RIGHT, border=self.padding)
                self.keywidgetmap[i] = w
                self.widgetkeymap[w] = i

        if order:
            (width,height) = w.GetClientSizeTuple()
            self.SetScrollbars(0, height, 0, len(order)*2)


    #def OnSetFocus(self, ev):
    #    pass


    def OnKillFocus(self, ev):
        #print 'EditList::OnKillFocus:', ev
        widget = ev.GetEventObject()
        if widget is None:
            print '- no associated window'
            return
        itemkey = self.widgetkeymap[widget]
        newval  = widget.GetValue()
        #print '- values: key:',itemkey,'new:',newval,type(newval),'old:',self.items[itemkey],type(self.items[itemkey])
        if newval == self.items[itemkey]:
            #print '- value "%s" unchanged from "%s"' % (itemkey,newval)
            return
        self.curchange = itemkey
        self.Change(itemkey, newval)
        self.curchange = None


    def Change(self, itemkey, newval):
        if not self.valuemgr:
            #print 'EditList::Change: value "%s" changed from "%s" to "%s"' % (itemkey,self.items[itemkey],newval)
            self.items[itemkey] = newval
        else:
            #print 'EditList::Change: value "%s" changed from "%s" to "%s"' % (itemkey,self.valuemgr.GetValue(itemkey),newval)
            self.valuemgr.ChangeValue(self.items[itemkey], newval)


    def ValueUpdated(self, changes):
        #print 'EditList::ValueUpdated:',changes,self.colored
        for c in self.colored:
            self.keywidgetmap[c].SetBackgroundColour(wx.WHITE)
            self.keywidgetmap[c].Refresh()
        self.colored = list()
        for c in changes:
            val = c[0]
            newval = c[1]
            oldval = c[2]
            itemkey = self.valuekeymap[val]
            #print 'EditList::ValueUpdated: value "%s" changed externally from "%s" to "%s" while changing "%s"' % (itemkey,oldval,newval,self.curchange)
            self.keywidgetmap[itemkey].SetValue(newval)
            if itemkey == self.curchange:
                continue
            self.keywidgetmap[itemkey].SetBackgroundColour(wx.CYAN)
            self.colored.append(itemkey)
        #print 'EditList::ValueUpdated: exit',self.colored


###############################################################################
