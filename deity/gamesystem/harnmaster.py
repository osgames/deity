###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameSystem: HarnMaster
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import math
import random
import re
import wx

import deity
import deity.gamesystem

from deity.watcher import *
from deity.xtype import *
from deity.exceptions import *


###############################################################################


class HarnMaster(deity.gamesystem.GameSystem):
    kFlagsDefendRe = re.compile(r'(^|;)defend=([\w\|]+)($|;)')

    kMonths	= ('Nuzyael','Peonu','Kelen','Nolus','Larane','Agrazhar',
                   'Azura','Halane','Savor','Ilvin','Navek','Morgat')
    kSunsigns	= ('ULA','ARA','FEN','AHN','ANG','NAD',
                   'HIR','TAR','TAI','SKO','MAS','LAD')
    kSunsignDays= (4,4,3, 4,5,7, 6,5,4, 3,3,2)
#    kArmorProtectionTemplate	= '0-0-0-0-0-0'
#    kHitLocationRoll	= '1d100'

    kCombatRoundTime = ('10', 's')
    kAttackAreas = xlist((NP_('loc:anywhere'), NP_('loc:high'), NP_('loc:middle'), NP_('loc:low')),
                         xlate='loc:')

    kDefendOptions = xlist((NP_('cmd:dodge'), NP_('cmd:block'), NP_('cmd:counterstrike'),
                            NP_('cmd:grapple'), NP_('cmd:ignore')),
                           xlate='cmd:')

    kAttackOptions = xlist((NP_('cmd:attack'), NP_('cmd:grapple'), NP_('cmd:missile'), NP_('cmd:wound'),
                            NP_('cmd:trample')),
                           xlate='cmd:')

    kAttackRanges = xlist((NP_('range:short'), NP_('range:medium'), NP_('range:long'), NP_('range:extreme')),
                           xlate='range:')

    kAttackRangeModifiers = {'short':0, 'medium':-20, 'long':-40, 'extreme':-80}

    kMakeActions = xlist((NP_('cmd:fumble'), NP_('cmd:stumble'), NP_('cmd:unhorse')),
                         xlate='cmd:')

    kSkillCheckResults = xlist((NP_('result:cs'), NP_('result:ms'), NP_('result:mf'), NP_('result:cf')),
                               xlate='result:')

    kCombatCheckIndex = { 'cf':0, 'mf':1, 'ms':2, 'cs':3 }
    kCombatAttackTable = {
        'attack': {
            # def:  CF      MF      MS      CS     # att
            'block': (
                (  'BF' ,  'AF' , 'DTA' , 'DTA' ), # CF
                (  'DF' ,'block', 'DTA' , 'DTA' ), # MF
                ( 'A*2' , 'A*1' ,'block', 'DTA' ), # MS
                ( 'A*3' , 'A*2' , 'A*1' ,'block'), # CS
            ),
            'counterstrike': (
                (  'BF' ,  'AF' , 'D*2' , 'D*3' ), # CF
                (  'DF' ,'block', 'D*1' , 'D*2' ), # MF
                ( 'A*3' , 'A*2' , 'B*1' , 'D*1' ), # MS
                ( 'A*4' , 'A*3' , 'A*1' , 'B*2' ), # CS
            ),
            'grapple': (
                ( 'BS'  ,  'AS' ,'hold' , 'hold'), # CF
                ( 'DS'  ,'block','hold' , 'hold'), # MF
                ( 'A*3' , 'A*2' ,'block', 'hold'), # MS
                ( 'A*4' , 'A*3' , 'A*1' ,'block'), # CS
            ),
            'dodge': (
                (  'BS' ,  'AS' , 'DTA' , 'DTA' ), # CF
                (  'DS' ,'dodge','dodge', 'DTA' ), # MF
                ( 'A*2' , 'A*1' ,'dodge','dodge'), # MS
                ( 'A*3' , 'A*2' , 'A*1' ,'dodge'), # CS
            ),
            'ignore': (
                ( 'DTA' , 'DTA' , 'DTA' , 'DTA' ), # CF
                ( 'A*1' , 'A*1' , 'A*1' , 'A*1' ), # MF
                ( 'A*3' , 'A*3' , 'A*3' , 'A*3' ), # MS
                ( 'A*4' , 'A*4' , 'A*4' , 'A*4' ), # CS
            ),
        },
        'grapple': {
            # def:  CF      MF      MS      CS     # att
            'block': (
                (  'BS' ,  'AS' , 'DTA' , 'DTA' ), # CF
                (  'DS' ,'block', 'DTA' , 'DTA' ), # MF
                ( 'hold', 'hold','block', 'DTA' ), # MS
                ( 'hold', 'hold', 'hold','block'), # CS
            ),
            'counterstrike': (
                (  'BS' ,  'AS' , 'D*2' , 'D*3' ), # CF
                (  'DS' ,'block', 'D*1' , 'D*2' ), # MF
                ( 'hold', 'hold', 'hold', 'D*1' ), # MS
                ( 'hold', 'hold', 'hold', 'hold'), # CS
            ),
            'grapple': (
                ( 'hold', 'hold', 'hold', 'hold'), # CF
                ( 'hold', 'hold', 'hold', 'hold'), # MF
                ( 'hold', 'hold', 'hold', 'hold'), # MS
                ( 'hold', 'hold', 'hold', 'hold'), # CS
            ),
            'dodge': (
                (  'BS' ,  'AS' , 'DTA' , 'DTA' ), # CF
                (  'DS' ,'dodge','dodge', 'DTA' ), # MF
                ( 'hold', 'hold','dodge','dodge'), # MS
                ( 'hold', 'hold', 'hold','dodge'), # CS
            ),
            'ignore': (
                ( 'DTA' , 'DTA' , 'DTA' , 'DTA' ), # CF
                ( 'hold', 'hold', 'hold', 'hold'), # MF
                ( 'hold', 'hold', 'hold', 'hold'), # MS
                ( 'hold', 'hold', 'hold', 'hold'), # CS
            ),
        },
        'missile': {
            # def:  CF      MF      MS      CS     # att
            'block': (
                ( 'wild', 'wild', 'wild', 'wild'), # CF
                ( 'miss', 'miss', 'miss', 'miss'), # MF
                ( 'M*2' , 'M*1' ,'block','block'), # MS
                ( 'M*3' , 'M*2' , 'M*1' ,'block'), # CS
            ),
            'dodge': (
                ( 'wild', 'wild', 'wild', 'wild'), # CF
                ( 'miss', 'miss','dodge','dodge'), # MF
                ( 'M*2' , 'M*1' ,'dodge','dodge'), # MS
                ( 'M*3' , 'M*2' , 'M*1' ,'dodge'), # CS
            ),
            'ignore': (
                ( 'wild', 'wild', 'wild', 'wild'), # CF
                ( 'miss', 'miss', 'miss', 'miss'), # MF
                ( 'M*2' , 'M*2' , 'M*2' , 'M*2' ), # MS
                ( 'M*3' , 'M*3' , 'M*3' , 'M*3' ), # CS
            ),
        },
    }

    kInjuryDescRe = re.compile(r'^(.*?)-([MSGK])(\d)(\w+)/?([HE\d\*]*)/?(INF-H\d)?/?([\d\.]*)$')
    kInjuryAspectDescription = {
        'Mb':P_('injury:bruised'),   'Me':P_('injury:cut'),     'Mp':P_('injury:nicked'),
        'Sb':P_('injury:fractured'), 'Se':P_('injury:sliced'),  'Sp':P_('injury:stabbed'),
        'Gb':P_('injury:broken'),    'Ge':P_('injury:gashed'),  'Gp':P_('injury:punctured'),
        'Kb':P_('injury:shattered'), 'Ke':P_('injury:severed'), 'Kp':P_('injury:impaled'),
    }
    kInjuryHealingRate = {
        'nt': { # no test
            'Mb': 'H5',  'Sb': 'H4*', 'Gb': 'H3*',
            'Me': 'H5',  'Se': 'H4',  'Ge': 'H3*',
            'Mp': 'H5',  'Sp': 'H4',  'Gp': 'H3*',
            'Mf': 'H5',  'Sf': 'H3',  'Gf': 'H2',
        },
        'cf': { # critical failure
            'Mb': 'H4',  'Sb': 'H3*', 'Gb': 'H2*',
            'Me': 'H4',  'Se': 'H3',  'Ge': 'H2*',
            'Mp': 'H4',  'Sp': 'H3',  'Gp': 'H2*',
            'Mf': 'H4',  'Sf': 'H2',  'Gf': 'H1',
        },
        'mf': { # marginal failure
            'Mb': 'H5',  'Sb': 'H4*', 'Gb': 'H3*',
            'Me': 'H5',  'Se': 'H4',  'Ge': 'H3*',
            'Mp': 'H5',  'Sp': 'H4',  'Gp': 'H3*',
            'Mf': 'H5',  'Sf': 'H3',  'Gf': 'H2',
        },
        'ms': { # marginal success
            'Mb': 'H6',  'Sb': 'H5',  'Gb': 'H4',
            'Me': 'H6',  'Se': 'H5',  'Ge': 'H4*',
            'Mp': 'H6',  'Sp': 'H5',  'Gp': 'H4*',
            'Mf': 'H6',  'Sf': 'H4',  'Gf': 'H3',
        },
        'cs': { # critical success
            'Mb': 'EE',  'Sb': 'H6',  'Gb': 'H5',
            'Me': 'EE',  'Se': 'H6',  'Ge': 'H5',
            'Mp': 'EE',  'Sp': 'H6',  'Gp': 'H5',
            'Mf': 'EE',  'Sf': 'H5',  'Gf': 'H4',
        },
    }
    kInjuryHealingModifier = {
        'Mb': +30, 'Sb': +20, 'Gb': +10,
        'Me': +30, 'Se': +20, 'Ge': +10,
        'Mp': +25, 'Sp': +15, 'Gp': +5,
        'Mf': +30, 'Sf': +15, 'Gf': +5,
    }


    kRuleOptions = [
        {
            'category': NP_("heading:combat"),
            'order':	10,
            'rule':	'CombatFumble',
            'default':	False,
            'short':	NP_("shortdesc:fumble items"),
            'long':	NP_("longdesc:can a bad roll or reception of an attack cause the character to fumble what is being held in that location?"),
        },
        {
            'category': NP_("heading:combat"),
            'order':	12,
            'rule':	'CombatStumble',
            'default':	False,
            'short':	NP_("shortdesc:stumble"),
            'long':	NP_("longdesc:can a bad roll or reception of an attack cause the character to stumble and fall?"),
        },
        {
            'category': NP_("heading:combat"),
            'order':	15,
            'rule':	'CombatKnockback',
            'default':	False,
            'short':	NP_("shortdesc:knockback"),
            'long':	NP_("longdesc:can a good hit knock the target backwards out of the current hex?"),
        },
        {
            'category': NP_("heading:combat"),
            'order':	18,
            'rule':	'CombatUnhorse',
            'default':	False,
            'short':	NP_("shortdesc:unhorse checks"),
            'long':	NP_("longdesc:are checks made to remain mounted during combat?"),
        },
        {
            'category': NP_("heading:combat"),
            'order':	20,
            'rule':	'CombatWeaponDamage',
            'default':	False,
            'short':	NP_("shortdesc:weapon damage"),
            'long':	NP_("longdesc:can weapons break when striking or being struck by another weapon?"),
        },
        {
            'category': NP_("heading:combat"),
            'order':	22,
            'rule':	'CombatWeaponPartialDamage',
            'requires':	[ 'CombatWeaponDamage' ],
            'default':	False,
            'short':	NP_("shortdesc:partial weapon damage"),
            'long':	NP_("longdesc:can weapons be weakened by striking or being struck by another weapon?"),
        },
        {
            'category': NP_("heading:combat"),
            'order':	30,
            'rule':	'CombatMinorWoundShock',
            'default':	True,
            'short':	NP_("shortdesc:minor wound shock"),
            'long':	NP_("longdesc:does a shock check have to be performed for minor wounds?  turn this off to ensure that a small bump doesn't knock a character out."),
        },
        {
            'category': NP_("heading:combat"),
            'order':	32,
            'rule':	'CombatBleeders',
            'default':	False,
            'short':	NP_("shortdesc:bleeding wounds"),
            'long':	NP_("longdesc:do serious wounds bleed?  this will weaken the character and result in death if not stopped."),
        },
        {
            'category': NP_("heading:combat"),
            'order':	35,
            'rule':	'CombatAmputate',
            'default':	False,
            'short':	NP_("shortdesc:single-hit amputations"),
            'long':	NP_("longdesc:can a critical hit sever a limb from the body?  this is a debilitating wound that can cause instant death."),
        },
        {
            'category': NP_("heading:combat"),
            'order':	40,
            'rule':	'WoundInfections',
            'default':	False,
            'short':	NP_("shortdesc:wound infections"),
            'long':	NP_("longdesc:can open wounds become infected?  infected wounds reduce a characters effectiveness and can even result in death."),
        },
    ]


    def __init__(self, **kwds):
        deity.gamesystem.GameSystem.__init__(self, **kwds)


    def UserTest(self, char, roll, point, default=None, passfail=False, msg=None, title=None, test=None):
        #print 'UserTest:',roll,point,default,test
        if passfail:
            if self.UserCheck(char, roll, '<=', point, default=default, msg=msg, title=title, test=test):
                return 's'
            else:
                return 'f'

        if roll is None or roll == '':
            roll = default
        res = None
        try:
            if isinstance(roll, (str,unicode)):
                # dice roll substitutions
                match = deity.kDieRollRe.match(roll)
                if match:
                    if char is not None and char.GetPrompt():
                        if msg is None:
                            msg = ''
                        title = '%s: %s (%s %s %s)' % (char.Name(), title or P_('title:check'), roll, '<', str(point))
                        res = Choose(msg, self.kSkillCheckResults, title=title, test=test)
                        if res is not None:
                            return res
                    if res is None:
                        res = eval(str(deity.Roll(match.string[match.start():match.end()], rand=self.rand, test=test))
                                   + roll[match.end():])
                if res is None:
                    res = eval(roll)
            else:
                res = roll
            # calculate result
            if res <= int(point):
                # success
                if res % 5 == 0:
                    return 'cs'
                else:
                    return 'ms'
            else:
                # failure
                if res % 5 == 0:
                    return 'cf'
                else:
                    return 'mf'

        except NameError, e:
            raise BadParameter(_('invalid test result "%(val)s" -- must be number or die roll +/- modifier')
                               % {'val':roll})


    #--------------------------------------------------------------------------#


    def AttributeCheck(self, char, attr, roll=None, modifier=None, penalties=True, msg=None, test=None):
        if roll is not None and roll == -1: return True
        if test is None:
            test = 'attrcheck-%s'%attr
        # get attribute value
        aval = char.GetAttribute(attr, 0)
        pointstr = '%s=%d' % (P_('heading:ability'), aval)
        # include modifier
        if modifier is None:
            modifier = 0
        elif modifier != 0:
            pointstr += ', user-mod=%+d' % modifier
        # get penalties
        ipenalty = 0
        fpenalty = 0
        epenalty = 0
        if penalties:
            ipenalty = char.GetPenalty('ip')
            if ipenalty != 0:
                pointstr += ', %s=%+d' % (P_('heading:injuries'), -ipenalty)
            fpenalty = char.GetPenalty('fp')
            if fpenalty != 0:
                pointstr += ', %s=%+d' % (P_('heading:fatigue'), -fpenalty)
            if attr == 'str' or attr == 'sta' or attr == 'dex' or attr == 'agl':
                epenalty = char.GetPenalty('ep')
                if epenalty != 0:
                    pointstr += ', %s=%+d' % (P_('heading:encumbrance'), -epenalty)
        # determine ability
        amod = aval + modifier - ipenalty - fpenalty - epenalty
        # limit success/fail values on standard checks
        if roll is None or str(roll).find('d') < 0 or roll == '3d6':
            if amod <  3: amod = 3
            if amod > 17: amod = 17
        pointstr += ', %s=%d' % (P_('heading:point'), amod)

        # basic 3d6 roll (if no value provided)
        if msg is None:
            msg = _('%(char)s is checking attribute "%(attr)s" (%(mods)s)') % {'char':char.Name(), 'attr':P_('attr:',attr), 'mods':pointstr}
        else:
            msg += ' (%s)' % pointstr
        title = P_('title:check %(attr)s') % {'attr':P_('attr:',attr)}
        okay = self.UserCheck(char, roll, '<=', amod, default='3d6', msg=msg, title=title, test=test)
        if okay:
            ostr = P_('result:s')
        else:
            ostr = P_('result:f')
        Output(_('%(name)s checks %(attr)s (%(pointstr)s): %(result)s')
               % {'name':char.Name(), 'attr':P_('attr:',attr), 'pointstr':pointstr, 'result':ostr})
        #print '%(name)s checks %(attr)s (%(pointstr)s): %(result)s' \
        #       % {'name':char.Name(), 'attr':P_('attr:',attr), 'pointstr':pointstr, 'result':ostr}
        return okay


    def SkillLevel(self, char, skill, open=False, info=None):
        #print 'SkillLevel: char:', char, 'skill:', skill, 'open:', open, 'info:', info, 'known:',char.GetSkills()
        base = None
        skill,sep,sbase = skill.partition('/')
        if skill not in self.kSkillInfo:
            base = char.GetValue('x:base-s:%s' % skill)
            if not base and sbase:
                char.SetValue('x:base-s:%s' % skill, sbase)
                base = sbase
            if not base:
                raise BadParameter(_('unknown skill "%(skill)s"') % {'skill':P_('skill:',skill)})
            if base not in self.kSkillInfo:
                raise BadParameter(_('unknown base "%(base)s" for skill "%(skill)s"')
                                   % {'base':P_('skill:',base), 'skill':P_('skill:',skill)})
            if info is not None:
                info.append(self.kSkillInfo[base])
        else:
            if info is not None:
                info.append(self.kSkillInfo[skill])
        sml = char.GetSkill(skill)
        # open it if necessary
        if sml is None:
            if not open:
                raise NotPossible(_('%(char)s does not know skill "%(skill)s".') %
                                  {'char':char.Name(), 'skill':P_('skill:',skill)})
            if base is not None:
                sml = char.GetSkill(base)
                if sml is None:
                    sml = char.GetSkillStart(base)
                    char.SetSkill(base, sml)
            if sml is None:
                sml = char.GetSkillStart(skill)
            Output(_('%(char)s opened skill "%(skill)s" at ml %(level)d.') %
                   {'char':char.Name(), 'skill':P_('skill:',skill), 'level':sml}, style="@blue")
            char.SetSkill(skill, sml)
        return sml


    def SkillCheck(self, char, skill, roll=None, locmod=None, itemmod=None, wearmod=None, rangemod=None, modifier=None, mult=None,
                   sml=None, passfail=False, open=False, msg=None, test=None):
        #print 'SkillCheck: char:',char,'skill:',skill,'roll:',roll,'locmod:',locmod,'itemmod:',itemmod,'wearmod:',wearmod,'rangemod:',rangemod,'modifier:',modifier,'mult:',mult
        if skill == 's' or skill == 'f': return skill
        if skill == 'cs' or skill == 'ms' or skill == 'mf' or skill == 'cf': return skill
        if test is None:
            test = 'skillcheck-%s'%skill
        open |= self.commandForce
        info = list()
        if sml is None:
            sml = self.SkillLevel(char, skill, open=open, info=info)
        info = info[0]
        pointstr = '%s=%d' % (P_('heading:mastery-level'), sml)
        # include range
        if rangemod is None:
            rangemod= 0
        elif rangemod != 0:
            pointstr += ', range-mod=%+d' % rangemod
        # include modifier
        if modifier is None:
            modifier = 0
        elif modifier != 0:
            pointstr += ', user-mod=%+d' % modifier
        # include modifiers from worn items
        if wearmod is None:
            wearmod = 0
            worn = char.GetWorn()
            for item in worn.iterkeys():
                wearmod += int(float(item.GetValue(skill, type="s:", default=0)))
        # get penalties
        ipenalty = char.GetPenalty('ip')
        fpenalty = char.GetPenalty('fp')
        if ipenalty != 0:
            pointstr += ', %s=%+d' % (P_('heading:injuries'), -ipenalty*5)
        if fpenalty != 0:
            pointstr += ', %s=%+d' % (P_('heading:fatigue'), -fpenalty*5)
        # subtract encumbrance penalty
        epenalty = 0
        if info.find('physical') == 2 or info.find('combat') == 2:
            epenalty = char.GetPenalty('ep')
            if epenalty != 0:
                pointstr += ', %s=%+d' % (P_('heading:encumbrance'), -epenalty*5)
        # determine ability
        eml = sml + rangemod + modifier - ipenalty*5 - fpenalty*5 - epenalty*5
        # location/item modifiers
        lstr = ''
        if locmod is not None:
            eml += locmod
            pointstr += ', %s=%+d' % (P_('heading:hit-location'), locmod)
        istr = ''
        if itemmod is not None:
            eml += itemmod
            pointstr += ', %s=%+d' % (P_('heading:item-mod'), itemmod)
        if wearmod:  # don't display if specified as zero
            eml += wearmod
            pointstr += ', %s=%+d' % (P_('heading:wear-mod'), wearmod)
        # include final multiplier
        if mult and mult != 1:
            eml *= mult
            pointstr += ', %s=*%0.1f' % (P_('heading:multiplier'), mult)
        # limit values
        if eml <  5: eml =  5
        if eml > 95: eml = 95
        pointstr += ', %s=%02d' % (P_('heading:point'), eml)

        # roll (if no value provided)
        if msg is None:
            msg = _('%(char)s is testing skill "%(skill)s" (%(mods)s)') % {'char':char.Name(), 'skill':P_('skill:',skill), 'mods':pointstr}
        else:
            msg += ' (%s)' % pointstr
        title = P_('title:test %(skill)s') % {'skill':P_('skill:',skill)}
        result = self.UserTest(char, roll, eml, default='1d100', passfail=passfail,
                               msg=msg, title=title, test=test)

        rstr = P_('result:',result)
        Output(_('%(name)s tests %(skill)s (%(pointstr)s): %(result)s')
               % {'name':char.Name(), 'skill':P_('skill:',skill), 'pointstr':pointstr, 'result':rstr})
        #print '%(name)s tests %(skill)s (%(pointstr)s): %(result)s' \
        #       % {'name':char.Name(), 'skill':P_('skill:',skill), 'pointstr':pointstr, 'result':rstr}
        if passfail:
            if result == 's':
                return True
            else:
                return False
        return result


    def SkillImprove(self, char, skill, roll=None, modifier=None, open=False, msg=None, test=None):
        #print 'SkillImprove: char:',char,'skill:',skill,'roll:',roll,'modifier:',modifier,'mult:',mult
        if test is None:
            test = 'skillimprove-%s'%skill
        sml = GAME.SkillLevel(char, skill, open=open)
        sb  = char.GetSkillBase(skill)
        pointstr = 'base=%d' % sb
        if modifier is not None:
            pointstr = ',modifier=%d' % modifier
        if pointstr:
            pointstr = ' (%s)' % pointstr
        if msg is None:
            msg = P_('%(char)s is trying to improve skill "%(skill)s" from %(current)d.') \
                  % {'char':char.Name(), 'skill':P_('skill:',skill), 'current':sml}
        title = P_('title:improve %(skill)s') % {'skill':P_('skill:',skill)}
        result = self.UserCheck(char, roll, '>', sml-sb, default='1d100', msg=msg+pointstr, title=title, test=test)
        sbase = char.GetValue('x:base-s:%s' % skill)
        if result:
            incr = sbase and 2 or 1  # specialized skills increment by 2's
            sml += incr
            char.SetSkill(skill, sml)
            Output(ngettext('%(char)s has improved base skill "%(skill)s" by 1 point (now %(sml)d).',
                            '%(char)s has improved specialized skill "%(skill)s" by 2 points (now %(sml)d).',
                            incr) % {'char':char.Name(), 'skill':P_('skill:',skill), 'sml':sml},
                   '@green')
            skills = char.GetSkills()
            for s in skills.iterkeys():
                b = char.GetValue('x:base-s:%s' % s)
                if b == skill:
                    if sbase:  # pragma: no cover
                        raise DatafillError(_('skill "%(skill)s" has base skill "%(base1)s" which itself has base skill "%(base2)s"')
                                              % {'skill':s, 'base1':b, 'base2':sbase})
                    dml = char.GetSkill(s)
                    if dml < sml:
                        char.SetSkill(s, sml)
                        Output(_('%(char)s has also improved specialized skill "%(skill)s" to %(current)d')
                               % {'char':char.Name(), 'skill':P_('skill:', s), 'current':sml})
                    else:
                        msg = P_('%(char)s is also trying to improve specialized skill "%(skill)s" from %(current)d.') \
                              % {'char':char.Name(), 'skill':P_('skill:',s), 'current':dml}
                        title = P_('title:improve %(skill)s') % {'skill':P_('skill:',s)}
                        dresult = self.UserCheck(char, roll, '>', dml-sb, default='1d100', msg=msg+pointstr, title=title, test=test)
                        if dresult:
                            dml += 1
                            char.SetSkill(s, dml)
                            Output(_('%(char)s has also improved specialized skill "%(skill)s" by 1 point.') % {'char':char.Name(), 'skill':P_('skill:',s)}, '@green')
                        else:
                            Output(_('%(char)s has not improved specialized skill "%(skill)s".') % {'char':char.Name(), 'skill':P_('skill:',s)}, '@red')
        else:
            Output(_('%(char)s has not improved skill "%(skill)s".') % {'char':char.Name(), 'skill':P_('skill:',skill)}, '@red')
        return result


    def WoundBreakdown(self, wound):
        match = self.kInjuryDescRe.match(wound)
        #print 'WoundBreakdown:',wound,match and match.groups()
        if match:
            (loc, sev, num, asp, rat, inf, tim) = match.groups()
        else:  # pragma: no cover
            raise Exception('illegal wound "%s"' % wound)
        return {
            'location': loc,
            'severity': sev,
            'damage': int(num),
            'aspect': asp,
            'sevasp': sev+asp[0],
            'lastupdate': tim and int(tim) or None,
            'healrate': rat or None,
            'infection': inf or None,
        }


    def WoundCompose(self, winfo):
        #print 'WoundCompose:',winfo
        lastupdate = winfo['lastupdate'] and str(winfo['lastupdate']) or ''
        healrate   = winfo['healrate'] or ''
        infection  = winfo['infection'] or ''

        return '%s-%s%d%s/%s/%s/%s' % (
            winfo['location'],
            winfo['severity'],
            winfo['damage'],
            winfo['aspect'],
            healrate,
            infection,
            lastupdate,
        )


    def WoundDescription(self, wound, char):
        winfo = self.WoundBreakdown(wound)
        if winfo['infection']:
            desc = _('infected %(wound)s %(location)s') % {
                'wound':P_('bdb:',self.kInjuryAspectDescription[winfo['sevasp']]),
                'location':P_('bdb:',winfo['location'])
            }
        else:
            desc = _('%(wound)s %(location)s') % {
                'wound':P_('bdb:',self.kInjuryAspectDescription[winfo['sevasp']]),
                'location':P_('bdb:',winfo['location'])
            }
        return desc


    def WoundTreatment(self, tochar, fromchar, wound=None, modifier=None, roll=None, time=None):
        #print 'WoundTreatment: tochar:',tochar,'fromchar:',fromchar,'wound:',wound
        wounds = tochar.GetList('wounds', [])
        if not wounds:
            raise NotPossible(_('%(name)s has no wounds.') % {'name':tochar.Name()})
        if wound is None:
            wound = ''
        wlen = len(wound)
        found = False
        for i in xrange(len(wounds)):
            w = wounds[i]
            if wound == w[:wlen]:
                winfo = self.WoundBreakdown(w)
                if winfo['healrate'] is not None and not self.commandForce:
                    Output(_('wound "%(wound)s" has already been treated (can "force" but use full location to avoid mismatch).') % {'wound':w})
                    found = True
                    continue
                sevmod = self.kInjuryHealingModifier[winfo['sevasp']]
                timmod = 0
                if winfo['lastupdate'] is not None and time:
                    timmod = -(int(time) - winfo['lastupdate']) * 5
                result = self.SkillCheck(fromchar, 'physician', open=True, roll=roll,
                                         modifier=modifier, locmod=sevmod, rangemod=timmod)
                winfo['healrate'] = self.kInjuryHealingRate[result][winfo['sevasp']]
                wounds[i] = self.WoundCompose(winfo)
                tochar.SetList('wounds', wounds)
                Output(_('wound "%(wound)s" will heal at rate "%(rate)s".') % {'wound':w, 'rate':winfo['healrate']})
                return
        if not found:
            raise BadParameter(_('unknown wound "%(wound)s": %(list)s')
                               % {'wound':wound, 'list':', '.join(wounds)})


    def Update(self, char, curtime, attending=None):
        lastupdate = char.GetValue('x:lastupdate')
        deity.gamesystem.GameSystem.Update(self, char, curtime)

        def AskAttendingSkill(char):
            title = _('%(name)s: attending physician') % {'name':char.Name()}
            msg = _('what is the physician skill of the character attending to the wounds of %(name)s? (leave blank if none)') \
                  % {'name':char.Name()}
            physkill = Input(msg, None, '^\d*$', title=title, test='physkill')
            if not physkill:
                return 0
            else:
                return int(physkill)

        abstime = curtime['abs']
        if lastupdate is None:
            lastupdate = abstime
        lastupdate = float(lastupdate)
        char.SetValue('x:lastupdate', str(abstime));
        physkill = None
        if attending:
            physkill = self.SkillLevel(attending, 'physician', open=True)
        cname = char.Name()
        ctype = char.GetValue('x:realtype');
        cheal = char.GetInfo('i:healrate');
        if cheal is None:
            cheal = 1
        else:
            cheal = int(cheal)

        # status
        status = char.GetStatus()
        if status == 'dead':
            return

        # bloodloss
        bloodloss = int(char.GetValue('x:bloodloss', '0'))
        if bloodloss:
            end  = char.GetAttribute('end')
            point= end * 6
            bltime = int(float(char.GetValue('x:bloodloss-time', '0')))
            if bltime == 0:
                bltime = abstime - 1
            while bloodloss > 0 and abstime - bltime >= 5:
                roll = deity.Roll('1d100', rand=self.rand, test='bloodlosscheck')
                if roll <= point:
                    if roll % 5 == 0:
                        bloodloss -= 2
                    else:
                        bloodloss -= 1
                bltime += 5
            if bloodloss <= 0:
                bloodloss = 0
                bltime = 0
                Output(_('%(name)s has recovered from all bloodloss.') % {'name':cname}, '@green')
            char.SetValue('x:bloodloss', str(bloodloss))
            char.SetValue('x:bloodloss-time', str(int(bltime)))

        # wounds
        wounds = char.GetList('wounds', [])
        newwounds = [];
        for w in wounds:
            wdesc = self.WoundDescription(w, char)
            winfo = self.WoundBreakdown(w)
            wmod  = 0;
            #print w,wdesc,winfo
            if winfo['lastupdate'] is None:
                winfo['lastupdate'] = int(abstime)
            hrate = winfo['healrate']
            if hrate is None:
                if physkill is None:
                    physkill = AskAttendingSkill(char)
                if physkill == 0:
                    result = 'nt'
                else:
                    result = self.UserTest(None, '1d100', physkill, test='treatcheck')
                winfo['healrate'] = hrate = self.kInjuryHealingRate[result][winfo['sevasp']]
                Output(_('%(char)s\'s %(wound)s wound will heal at a rate of "%(hrate)s"')
                       % {'char':cname, 'wound':wdesc, 'hrate':hrate})
            if not self.RuleCheck('WoundInfections'):
                winfo['infection'] = None
            tim = winfo['lastupdate']
            oldtim = tim - 1
            while tim != oldtim and abstime - tim >= 1:
                oldtim = tim

                if winfo['damage'] <= 0 or hrate == 'EE':
                    winfo['damage'] = 0
                    break

                # getting help from a physician
                if (abstime - tim >= 5) or (winfo['infection'] is not None and abstime - tim >= 1):
                    if physkill is None:
                        physkill = AskAttendingSkill(char)

                # infection
                if winfo['infection'] is not None and abstime - tim >= 1:
                    irate= int(winfo['infection'][-1])
                    roll = deity.Roll('1d100', rand=self.rand, test='infectioncheck')
                    end  = char.GetAttribute('end')
                    point= end * irate + physkill
                    if point < 5:  point = 5
                    if point > 95: point = 95
                    if roll <= point:
                        if roll % 5 == 0:
                            irate += 2
                            if irate < 6:
                                Output(_('%(name)s\'s %(wound)s\'s infection is greatly reduced.')
                                       % {'name':cname, 'wound':wdesc}, '@green')
                        else:
                            irate += 1
                            if irate < 6:
                                Output(_('%(name)s\'s %(wound)s\'s infection is somewhat reduced.')
                                       % {'name':cname, 'wound':wdesc}, '@green')
                    else:
                        if roll % 5 == 0:
                            irate -= 2
                            if irate > 0:
                                Output(_('%(name)s\'s %(wound)s\'s infection is greatly increased.')
                                       % {'name':cname, 'wound':wdesc}, '@red')
                        else:
                            irate -= 1
                            if irate > 0:
                                Output(_('%(name)s\'s %(wound)s\'s infection is somewhat increased.')
                                       % {'name':cname, 'wound':wdesc}, '@red')
                    winfo['infection'] = 'INF-H%d' % irate
                    if irate >= 6:
                        Output(_('%(name)s\'s %(wound)s\'s infection is gone; healing will resume.')
                               % {'name':cname, 'wound':wdesc}, '@green')
                        winfo['infection'] = None
                    elif irate <= 0:
                        Output(_('%(name)s is dead from %(wound)s infection.')
                               % {'name':cname, 'wound':wdesc}, '@red')
                        char.SetStatus('dead')
                        tim = abstime - 1
                    tim += 1;

                # healing
                elif abstime - tim >= 5:
                    for i in xrange(cheal):
                        roll = deity.Roll('1d100', rand=self.rand, test='healcheck')
                        end  = char.GetAttribute('end')
                        point= end * int(hrate[1]) + physkill/2
                        if point < 5:  point = 5
                        if point > 95: point = 95
                        if roll <= point:
                            if roll % 5 == 0:
                                wmod -= 2
                                if winfo['damage'] > 0:
                                    Output(_('%(name)s\'s %(wound)s is healing wonderfully.')
                                           % {'name':cname, 'wound':wdesc}, '@green')
                            else:
                                wmod -= 1
                                if winfo['damage'] > 0:
                                    Output(_('%(name)s\'s %(wound)s is healing nicely.')
                                           % {'name':cname, 'wound':wdesc}, '@green')
                        else:
                            if roll % 5 == 0 and ctype != 'sindarin' and self.RuleCheck('WoundInfections'):
                                sev = winfo['severity']
                                asp = winfo['aspect']
                                if ((asp[0] == 'b' and sev == 'G') or
                                    (asp[0] == 'f' and sev != 'M') or
                                    (asp[0] != 'b' and asp[0] != 'f')):
                                    Output(_('%(name)s\'s %(wound)s has become infected.')
                                           % {'name':cname, 'wound':wdesc}, '@red')
                                    winfo['infection'] = 'INF-%s' % hrate[0:2]
                            else:
                                Output(_('%(name)s\'s %(wound)s has not improved.')
                                       % {'name':cname, 'wound':wdesc})
                    tim += 5;

            # check final wound status
            wmod = max(wmod, -winfo['damage'])  # -wmod <= winfo[damage]
            winfo['damage'] += wmod
            ipenalty = int(char.GetValue('x:ipenalty', '0'));
            ipenalty += wmod;
            if ipenalty < 0:
                ipenalty = 0
            char.SetValue('x:ipenalty', str(ipenalty));
            if winfo['damage'] <= 0:
                if hrate.find('*') >= 0:
                    Output(_('%(name)s\'s %(wound)s has healed but left an impairment!') % {'name':cname, 'wound':wdesc}, '@red,bold')
                else:
                    Output(_('%(name)s\'s %(wound)s has healed.') % {'name':cname, 'wound':wdesc}, '@green')
            else:
                winfo['lastupdate'] = tim
                newwounds.append(self.WoundCompose(winfo))

        # store updated wound list
        char.SetList('wounds', newwounds)


    #-------------------------------------------------------------------------#


    def CombatNewRound(self):
        deity.gamesystem.GameSystem.CombatNewRound(self)
        self.tacticalAdvantages = 0


    def CombatNewTurn(self, char):
        deity.gamesystem.GameSystem.CombatNewTurn(self, char)
        self.CombatUpdateBloodloss(char)
        self.CombatUpdateStatus(char)
        self.tacticalAdvantages = 0


    def CombatUpdateBloodloss(self, char):
        bleeders = char.GetDict('bleeders')
        if bleeders:
            bloodloss = int(char.GetValue('x:bloodloss', '0'))
            for wound,ticker in bleeders.iteritems():
                ticker = int(ticker) + 1
                if ticker == 6:
                    bloodloss += 1
                    ticker = 0
                bleeders[wound] = str(ticker)
            char.SetValue('x:bloodloss', str(bloodloss))
            char.SetDict('bleeders', bleeders)
            end = char.GetAttribute('end')
            if bloodloss > end:
                Output(_('%(char)s has died from bloodloss.') % {'char':char.Name()}, '@red,bold')
                char.SetStatus('dead')
            else:
                Output(ngettext('%(char)s is bleeding from %(count)d wound. (%(loss)d/%(endurance)d)',
                                '%(char)s is bleeding from %(count)d wounds. (%(loss)d/%(endurance)d)',
                                len(bleeders)) % {'char':char.Name(), 'count':len(bleeders),
                                                  'loss':bloodloss, 'endurance':end},
                       '@red')


    def CombatUpdateStatus(self, char):
        status = char.GetStatus()
        if status == 'unconscious':
            end = char.GetAttribute('end')
            upenalty = char.GetPenalty('up')
            # wake-up test
            msg = _('%(char)s checks endurance to regain consciousness.') % {'char':char.Name()}
            if not self.AttributeCheck(char, 'end', roll='%dd6'%upenalty, msg=msg, penalties=False, test='waketest'):
                Output(_('%(char)s remains unconscious. (%(penalty)dd6 > %(endurance)d)') %
                       {'char':char.Name(), 'penalty':upenalty, 'endurance':end}, '@red')
                return
            # shock test
            msg = _('%(char)s checks endurance to avoid going into shock.') % {'char':char.Name()}
            if self.AttributeCheck(char, 'end', roll='%dd6'%upenalty, msg=msg, penalties=False, test='standtest'):
                char.SetStatus('prone')
                Output(_('%(char)s awakes and can stand up.') % {'char':char.Name()}, '@blue')
            else:
                char.SetStatus('shock')
                if self.SkillCheck(char, 'initiative', passfail=True, open=True, test='shockmovetest'):
                    Output(_('%(char)s awakes in shock but can move.') % {'char':char.Name()}, '@blue')
                else:
                    Output(_('%(char)s awakes in shock and cannot act.') % {'char':char.Name()}, '@red')
        elif status == 'shock':
            if self.SkillCheck(char, 'initiative', passfail=True, open=True, test='shockmovetest'):
                Output(_('%(char)s is in shock but can move.') % {'char':char.Name()}, '@blue')
            else:
                Output(_('%(char)s is in shock and cannot act.') % {'char':char.Name()}, '@red')


    def GetCombatSummary(self, char):
        summary = P_('i:', char.GetStatus())
        wounds = char.GetList('wounds', [])
        if len(wounds) > 0:
            summary += ', '
            summary += ngettext('%d wound', '%d wounds', len(wounds)) % len(wounds)
        bleeders = char.GetDict('bleeders', {})
        if len(bleeders) > 0:
            summary += ', '
            summary += ngettext('%d bleeder', '%d bleeders', len(bleeders)) % len(bleeders)
        bloodloss = int(char.GetValue('x:bloodloss', '0'))
        if bloodloss > 0:
            summary += ', '
            summary += _('bloodloss=%d') % bloodloss
        return summary


    def CombatEnter(self, char):
        pass


    def CombatExit(self, char):
        if char.GetStatus() != 'dead':
            bleeders = char.GetDict('bleeders')
            if bleeders:
                Output(ngettext('%(char)s is still bleeding from %(count)d wound.',
                                '%(char)s is still bleeding from %(count)d wounds.',
                                len(bleeders)) % {'char':char.Name(), 'count':len(bleeders)},
                       '@red')


    def _CombatTA(self, who):
        if self.tacticalAdvantages >= 1:
            return 0
        if who == 'attacker' or who == 'me':
            return +1
        if who == 'defender' or who == 'other':
            return -1
        raise Exception('unknown entity "%s" for TA' % who)  # pragma: no cover


    def CombatActionTime(self, char, action, *opts):
        if action == 'stand':
            return 1
        else:
            raise Exception, 'unknown combat action "%s"' % action


    def CombatFirstAid(self, char, other, locx=None, roll=None, mod=None):
        bleeders = other.GetDict('bleeders', {})
        if not bleeders:
            raise NotPossible(_('%(other)s is not bleeding anywhere') % { 'other':other.Name() })
        bleedlocs = xlist(bleeders.keys(), xlate=GAME.xlate, xpart=GAME.DashPart)
        match = XLookup(bleedlocs, locx, typename=_("bleeder"), defindex=0)
        if mod is None:
            mod = 0
        mod = mod + 50 # all checks are +50
        if self.SkillCheck(char, 'physician', roll=roll, modifier=mod, passfail=True, open=True):
            Output(_('%(char)s stops bleeder "%(wound)s" on %(other)s.')
                   % { 'char':char.Name(), 'wound':match.xval(), 'other':other.Name() }, '@green')
            del bleeders[match]
        else:
            Output(_('%(char)s could not stop bleeder "%(wound)s" on %(other)s.')
                   % { 'char':char.Name(), 'wound':match.xval(), 'other':other.Name() }, '@red')
            bleeders[match] = '0'
            bloodloss = int(other.GetValue('x:bloodloss', '0'))
            other.SetValue('x:bloodloss', str(bloodloss+1))
        other.SetDict('bleeders', bleeders)


    def CombatDraw(self, char, itemx, locx, roll=None, mod=None):
        onground = False
        # find item in inventory
        try:
            item = char.SearchInventory(itemx)
        except BadParameter as e:
            ground = self.GetId(self.kCombatGroundTid)
            try:
                item = ground.SearchInventory(itemx)
                onground = True
            except BadParameter:
                pass
            if not onground:
                raise e  # return the original failure, not the ground failure
        # requires skill roll
        if not self.commandForce and not self.AttributeCheck(char, 'dex', roll, mod):
            Output(_('failed %(attr)s check to wield %(item)s.')
                   % {'attr':P_('attr:dex'), 'item':item.Name()},
                   '@red')
            return
        # take it from the ground
        if onground:
            ground.SubInventory(item)
            char.AddInventory(item)
            Output(_('%(item)s taken from the ground.') % {'item':item.Name()})
        # place it
        locx = char.WieldItem(item, locx, defindex=0, replace=True)
        Output(_('%(item)s now in %(place)s.') % {'item':item.Name(), 'place':locx.xval()}, '@green')


    def CombatMount(self, char, mount, roll=None, modifier=None):
        #print 'CombatMount:',char,mount,roll,modifier
        # make sure we're not already mounted
        curmount = char.GetMount()
        if curmount is not None:
            raise NotPossible(_('%(char)s is aready mounted on %(mount)s')
                              % {'char':char.Name(), 'mount':curmount.Name()})
        # requires skill roll
        result = ''
        if not self.commandForce:
          skill = '%s/riding' % mount.Name()
          result = self.SkillCheck(char, skill, roll=roll, modifier=modifier, open=True, test='mountcheck')
          if result == 'mf' or result == 'cf':
              Output(_('failed %(skill)s check to mount %(mount)s.')
                     % {'skill':'%s/%s'%(mount.Name(),P_('skill:riding')), 'mount':mount.Name()})
              ucresult = self._CombatUnhorseCheck4(char, mount)
              if ucresult == 'cs':  # nice recovery!
                  return char
              return None
        # mount it
        char.MountCharacter(mount)
        Output(_('%(char)s has mounted %(mount)s.') % {'char':char.Name(), 'mount':mount.Name()})
        if result == 'cs':
            Output(_('%(char)s has gained a tactical advantage.') % {'char':char.Name()}, '@green')
            return char
        return None
        

    def CombatDismount(self, char, roll=None, modifier=None):
        #print 'CombatDismount:',mount,roll,modifier
        # make sure we're mounted
        mount = char.GetMount()
        if mount is None:
            raise NotPossible(_('%(char)s is not mounted') % {'char':char.Name()})
        char.Dismount()
        # requires skill roll to avoid inuries/penalties
        result = ''
        if not self.commandForce:
          skill = '%s/riding' % mount.Name()
          result = self.SkillCheck(char, skill, roll=roll, modifier=modifier, open=True, test='dismountcheck')
          if result == 'mf' or result == 'cf':
              Output(_('failed %(skill)s check to dismount %(mount)s.')
                     % {'skill':'%s/%s'%(mount.Name(),P_('skill:riding')), 'mount':mount.Name()})
              ucresult = self._CombatUnhorseCheck4(char, mount)
              if ucresult == 'cs':  # nice recovery!
                  return char
              return None
        # dismount
        Output(_('%(char)s has dismounted %(mount)s.') % {'char':char.Name(), 'mount':mount.Name()})
        if result == 'cs':
            Output(_('%(char)s has gained a tactical advantage.') % {'char':char.Name()}, '@green')
            return char
        return None
        

    def _CombatSeverityAdjustment(self, char, severity, numdice):
        if severity == 'K':
            killok = Query(_('this could be a killing blow.  allow?'), title=_('kill %(name)s')%{'name':char.Name()}, test='kill?')
            limit  = char.GetAttribute('end')
            if not killok or deity.Roll(numdice, 6, rand=self.rand, test='killendcheck') <= limit:
                return 'G'
        return severity


    def _CombatAmputateCheck(self, char, amputate, numdice, aspect):
        #print 'AmputateCheck:',char,amputate,numdice,aspect,':',self.RuleCheck('CombatAmputate')
        if amputate == '': return False
        if aspect != 'edge': return False
        if not self.RuleCheck('CombatAmputate'): return False
        ampok = Query(_('this could amputate.  allow?'), title=_('amputate %(name)s')%{'name':char.Name()}, test='amputate?')
        if not ampok: return False
        weight = float(char.GetInfo('weight', '99999')) * deity.kLbsPerKg
        return deity.Roll(numdice, 6, rand=self.rand, test='amputateweightcheck') * 10 > weight


    def _CombatUnhorseCheck4(self, char, mount=None, shockroll=True, result=None, modifier=None, mult=None, msmsg=None, csmsg=None):
        if mount is None:
            if char.GetStatus() != 'mounted': return None
            mount = char.GetMount()
            if mount is None:  # pragma: no cover
                print 'Error: status is "mounted" but have no mount'
                return None
        skill = "%s/riding" % mount.Name()
        if result is None:
            result = self.SkillCheck(char, skill, modifier=modifier, mult=mult, open=True, test='unhorsecheck')
        if self.commandForce:
            result = result[0] + 'f'  # force a failure
        if result == 'mf' or result == 'cf':
            char.SetMount(None)
            char.SetStatus('prone')
            if result == 'mf':
                Output(_('%(char)s falls from mount and takes damage according to mount\'s speed: walk=none, canter=2d6, gallop=3d6') % {'char':char.Name()}, '@red');
                dice = 1
            else:
                Output(_('%(char)s falls from mount HARD and takes damage according to mount\'s speed: walk=4d6, canter=5d6, gallop=6d6') % {'char':char.Name()}, '@red');
                dice = 4
            self._CombatDoWound(char, '%dd6'%dice, xstr('blunt'), None, shockroll=shockroll, attacker=self.charPrompter)
        if result == 'cs':
            if not self._CombatTA('me'):
                result = 'ms'
            else:
                if csmsg is not None:
                    if csmsg != '':
                        Output(csmsg, '@green')
                else:
                    Output(_('%(char)s remains mounted and gains a tactical advantage!') % {'char':char.Name()}, '@green')
        if result == 'ms':
            if msmsg is not None:
                if msmsg != '':
                    Output(msmsg)
            else:
                Output(_('%(char)s remains mounted.') % {'char':char.Name()})
        return result


    def _CombatSteedCommandCheck(self, char, mount=None, result=None, modifier=None, mult=None, csmsg=None, msmsg=None, mfmsg=None, cfmsg=None):
        if mount is None:
            if char.GetStatus() != 'mounted': return None
            mount = char.GetMount()
            if mount is None:  # pragma: no cover
                print 'Error: status is "mounted" but have no mount'
                return None
        skill = "%s/riding" % mount.Name()
        if result is None:
            result = self.SkillCheck(char, skill, modifier=modifier, mult=mult, open=True, test='steedcmdcheck')
        msg = ''
        if result == 'cs' and csmsg is not None:
            msg = '(' + csmsg + ')'
        elif result == 'ms' and msmsg is not None:
            msg = '(' + msmsg + ')'
        elif result == 'mf' and mfmsg is not None:
            msg = '(' + mfmsg + ')'
        elif result == 'cf' and cfmsg is not None:
            msg = '(' + cfmsg + ')'
        Output(_('%(name)s performs a steed command check: %(result)s %(msg)s')
                 % {'name':char.Name(), 'result':P_('result:'+result), 'msg':msg})
        return result


    def _CombatSteedCommandModifier(self, char, mount=None, result=None, modifier=None, mult=None):
        result = self._CombatSteedCommandCheck(char, mount=mount, result=result, modifier=modifier, mult=mult,
                                               csmsg=_('+10 to Attack-ML'), mfmsg=_('-10 to Attack-ML'),
                                               cfmsg=_('steed balks -- unhorsed'))
        if result == 'cs':
            return +10
        if result == 'ms':
            return 0
        if result == 'mf':
            return -10
        if result == 'cf':
            self._CombatUnhorseCheck4(char, mount=mount, shockroll=True, result='cf')
        return None
        

    def _CombatKnockbackAmount(self, char, flags, damage):
        #print 'CombatKnockback:',char,flags,damage,self.RuleCheck('CombatKnockback'),char.GetStatus()
        if not self.RuleCheck('CombatKnockback'): return 0
        if char.GetStatus() != 'standing': return 0
        match = re.search(r'(^|;)knockback=(\d+)($|;)', flags)
        if not match:
            return 0
        if damage <= char.GetAttribute('str'):
            return 0
        kbamount = int(match.group(2))
        if kbamount:
            Output(ngettext('%(name)s is knocked-back %(amount)d hex',
                            '%(name)s is knocked-back %(amount)d hexes',
                            kbamount) % {'name':char.Name(), 'amount':kbamount}, '@blue')
        return kbamount


    def _CombatDoFumble(self, char, flags, severity, location):
        #print 'CombatFumble:',char,flags,severity,location
        if not self.RuleCheck('CombatFumble'): return None
        if severity != 'S' and severity != 'G' and severity != 'K': return None
        match = re.search(r'(^|;)fumble=(\w+)($|;)', flags)
        if match:
            fumbleloc = match.group(2)
            prefix = list()
            self.BaseLocation(location, prefix)
            if prefix:
                fumbleloc = prefix[0] + fumbleloc
        else:
            fumbleloc = location
        wielded = char.GetWielded()
        if fumbleloc not in wielded or wielded[fumbleloc] is None:
            return None
        if not self.commandForce and self.AttributeCheck(char, 'dex'): return None
        fumbleitem = wielded[fumbleloc];
        ta = self._CombatTA('other')
        Output((ta
                and _('%(name)s fumbled %(item)s in %(location)s and gives attacker a tactical advantage.')
                or  _('%(name)s fumbled %(item)s in %(location)s.'))
               % {'name':char.Name(), 'item':fumbleitem.Name(), 'location':P_('bdbloc:',fumbleloc)},
               '@red')
        char.UnwieldItem(fumbleitem)
        char.SubInventory(fumbleitem)
        # put it on the ground
        ground = self.GetId(self.kCombatGroundTid)
        ground.AddInventory(fumbleitem)
        return ta


    def _CombatDoStumble(self, char, flags, severity, knockback):
        #print 'Stumble:',char,flags,severity,knockback,self.RuleCheck('CombatStumble'),char.GetStatus()
        if not self.RuleCheck('CombatStumble'): return None
        match = re.search(r'(^|;)stumble=(\w+)($|;)', flags)
        if not match and severity != 'K' and not knockback: return None
        if char.GetStatus() != 'standing': return None
        if not self.commandForce and severity == 'M' and not knockback: return None
        if char.IsA('_humanoid'):
            if not self.commandForce and severity != 'K' and self.AttributeCheck(char, 'dex'):
                Output(_('%(name)s stumbles but remains standing.') % {'name':char.Name()}, '@blue')
                self._CombatDoUnrider(char, '', severity, 0, stumble=True)
                return None
        elif char.IsA('_quadruped'):
            if not self.commandForce and severity != 'K' and self.SkillCheck(char, 'dodge', open=True, passfail=True):
                Output(_('%(name)s stumbles but remains standing.') % {'name':char.Name()}, '@blue')
                self._CombatDoUnrider(char, '', severity, 0, stumble=True)
                return None
        else:
            return None  # things with more than 4 legs never stumble
        if knockback:
            status = 'prone'
        else:
            status = match.group(2)
        ta = self._CombatTA('other')
        Output((ta
                and _('%(name)s stumbles and is now %(status)s, giving attacker a tactical advantage.')
                or  _('%(name)s stumbles and is now %(status)s.'))
               % {'name':char.Name(), 'status':P_('status:',status)}, '@red')
        char.SetStatus(status)
        for tid in char.GetList('riders', []):
            rider = self.Character(tid)
            self._CombatDoUnhorse(rider, '', '', 0, stumble=True, result='cf')
            if status != 'standing' and rider.GetStatus() == 'mounted':
                rider.Dismount()
        return ta


    def _CombatDoUnhorse(self, char, flags, severity, damage, stumble=False, shockroll=True, result=None, msmsg=None, csmsg=None):
        #print 'Unhorse:',char,flags,severity,damage,stumble,shockroll,result,self.RuleCheck('CombatUnhorse'),char.GetStatus()
        if char.GetStatus() != 'mounted': return None
        if result is None:
            if not self.RuleCheck('CombatUnhorse'): return None
            if not self.commandForce and not stumble and damage <= char.GetAttribute('str'): return None
        if csmsg is None:
            csmsg = _('%(char)s recovers beautifully and gains a tactical advantage.') % {'char':char.Name()}
        result = self._CombatUnhorseCheck4(char, shockroll=shockroll, result=result, msmsg=msmsg, csmsg=csmsg)
        if result == 'cs':  # nice recovery!
            return self._CombatTA('me')
        #if result[1] == 'f':
        #    return self._CombatTA('other')
        return 0


    def _CombatDoUnrider(self, char, flags, severity, damage, stumble=False, shockroll=True, result=None):
        # Note: There is nothing in the official HM3 rules about this.  It's made up.
        #print 'Unrider:',char,flags,severity,damage,result,char.GetStatus()
        riders = char.GetList('riders')
        if riders is None: return None
        if "stumble=" in flags:
            return None  # unhorse already accounted for by stumble roll
        status = char.GetStatus()
        if status == 'standing':
            if stumble:
                pass
            elif result is None:
                if self.SkillCheck(char, 'initiative', passfail=True, open=True, test='unriderinitiative'):
                    Output(_('%(steed)s is unperterbed by this') % {'steed':char.Name()})
                    return None
                else:
                    Output(_('%(steed)s reacts violently to the wound') % {'steed':char.Name()}, '@red')
        else:
            if result is None:
                result = 'cf'
        for tid in riders:
            rider = self.Character(tid)
            self._CombatDoUnhorse(rider, '', '', 0, stumble=True, shockroll=shockroll, result=result)
            if status != 'standing' and rider.GetStatus() == 'mounted':
                rider.Dismount()
        return None


    def _CombatDoWeaponDamage(self, achar, aweaponloc, dchar, dweaponloc):
        if not self.RuleCheck('CombatWeaponDamage'): return None
        partial = self.RuleCheck('CombatWeaponPartialDamage')
        awielded = achar.GetWielded()
        dwielded = dchar.GetWielded()
        aweapon  = awielded[aweaponloc]
        dweapon  = dwielded[dweaponloc]
        tactadv  = 0
        awq = awn = aweapon.GetAttribute('quality', 10)
        dwq = dwn = dweapon.GetAttribute('quality', 10)
        awr = deity.Roll(3, 6, rand=self.rand, test='attackerweapondamage')
        dwr = deity.Roll(3, 6, rand=self.rand, test='defenderweapondamage')
        #print 'WeaponDamage:',aweapon.Name(),awq,awr,';',dweapon.Name(),dwq,dwr

        if dwq < awq:
            if dwr > dwq:
                dwn = 0
            elif awr > awq:
                awn = 0
            elif partial:
                awn -= 1
                dwn -= 1
        else:
            if awr > awq:
                awn = 0
            elif dwr > dwq:
                dwn = 0
            elif partial:
                awn -= 1
                dwn -= 1

        if awn != awq:
            aweapon.SetAttribute('quality', awn)
            if awn == 0:
                tactadv = self._CombatTA('defender')
                Output((tactadv
                        and _('%(name)s\'s %(weapon)s has broken and given %(other)s a tactical advantage.')
                        or  _('%(name)s\'s %(weapon)s has broken.'))
                       % {'name':achar.Name(), 'weapon':aweapon.Name(), 'other':dchar.Name()}, '@red')
                aweapon.Break()
            else:
                Output(_('%(name)s\'s %(weapon)s has been damaged.')
                       % {'name':achar.Name(), 'weapon':aweapon.Name()}, '@blue')
        if dwn != dwq:
            dweapon.SetAttribute('quality', dwn)
            if dwn == 0:
                tactadv = self._CombatTA('attacker')
                Output((tactadv
                        and _('%(name)s\'s %(weapon)s has broken and given %(other)s a tactical advantage.')
                        or  _('%(name)s\'s %(weapon)s has broken.'))
                       % {'name':dchar.Name(), 'weapon':dweapon.Name(), 'other':achar.Name()}, '@red')
                dweapon.Break()
            else:
                Output(_('%(name)s\'s %(weapon)s has been damaged.')
                       % {'name':dchar.Name(), 'weapon':dweapon.Name()}, '@blue')

        return tactadv


    def _CombatDoHold(self, achar, dchar):
        #print 'CombatDoHold:',achar,dchar
        msg = _('test strength for hold.')
        ta = 0
        apen = achar.GetPenalty('pp')
        astr = achar.GetAttribute('str')
        dpen = dchar.GetPenalty('pp')
        dstr = dchar.GetAttribute('str')
        roll1 = '3d6+%d-%d' % (astr, apen)
        roll2 = '3d6+%d-%d' % (dstr, dpen)
        while True:
            Output(_('%(char1)s and %(char2)s check strength to determine the victor. (%(str1)d vs %(str2)d, penalty is %(pen1)d vs %(pen2)d)')
                     % { 'char1':achar.Name(), 'str1':astr, 'pen1':apen, 'char2':dchar.Name(), 'str2':dstr, 'pen2':dpen })
            str1 = self.UserRoll(achar, roll1, msg=msg, test='holdstrength')
            str2 = self.UserRoll(dchar, roll2, msg=msg, test='holdstrength')
            Output(_('%(char1)s gets %(val1)d vs %(val2)d for %(char2)s.')
                   % { 'char1':achar.Name(), 'val1':str1, 'char2':dchar.Name(), 'val2':str2 })
            if str1 > str2:
                up = achar
                down = dchar
                ta = self._CombatTA('attacker')
            elif str1 < str2:
                up = dchar
                down = achar
                ta = self._CombatTA('defender')
            else:
                Output(_('tie -- roll again'))
                continue

            # no official rule on mounted-to-mounted grapples
            csmsg = _('%(char)s recovers beautifully and gains a tactical advantage.') % {'char':down.Name()}
            ucresult = self._CombatUnhorseCheck4(down, mult=0.5, csmsg=csmsg, result='cf')  # force CF for now
            if ucresult is not None:
                if ucresult == 'cs':  # nice recovery!
                    return -ta
                return ta

            down.SetStatus('prone')
            Output((ta
                    and _('%(char1)s sends %(char2)s to the ground and gains a tactical advantage.')
                    or  _('%(char1)s sends %(char2)s to the ground.'))
                   % { 'char1':up.Name(), 'char2':down.Name() })
            return ta


    def CombatMove(self, char):
        #print 'CombatMove:',char
        penalty = char.GetPenalty('pp')
        agility = char.GetAttribute('agl')
        move = agility - penalty
        Output(_('%(char)s can move half=%(half)d, full=%(full)d, or double=%(double)d')
               % {'char':char.Name(), 'half':max(move//2, 1), 'full':max(move, 1), 'double':max(move*2, 1)})


    def _CombatDoWound(self, char, damage, aspect, location=None, attacker=None, shockroll=True):
        #print 'CombatDoWound:',char.Name(),damage,aspect,location,attacker
        aspect.xval()  # assert translateable
        name = char.Name()

        # choose hit location
        location = self.HitLocation(char, location, attacker=attacker)
        Output(_('%(name)s was hit in the %(location)s.') %
               {'name':name, 'location':location.xval()})

        # no longer busy
        busy = char.GetInfo('busy')
        if busy is not None:
            char.SetInfo('busy', None)
            Output(_('%(name)s was busy but has been interrupted.') % {'name':name}, '@red')

        # verify damage
        msg = _('how much damage is done?')
        damage = self.UserRoll(attacker, damage, msg=msg, test='damage')
        if damage <= 0:
            Output(_('%(name)s takes no damage.') % {'name':name}, '@blue')
            return 0

        # translate/verify aspect
        aspect = XLookup(self.kWeaponAspects, aspect, N_('aspect'))

        # determine armor at that location
        armor = char.GetValue('x:armor-%s'%location)
        if armor is None:
            armor = '0;'
        armor = armor.split(';')
        items = self.xlist()
        items.extend(armor[1:])
        items = items.xjoin(', ')
        armor = armor[0]
        if items == '':
            Output(_('%(name)s has no protection at that location.') % {'name':name})
        else:
            Output(_('%(name)s has protection %(protection)s: %(itemlist)s') %
                   {'name':name, 'protection':armor, 'itemlist':items})

        # calculate damage
        armorvals = armor.split(',')
        armor = 0
        for a in xrange(max(len(armorvals),len(self.kWeaponAspects))):
            if self.kWeaponAspects[a] == aspect:
                if a < len(armorvals):
                    armor = int(armorvals[a])
                break
        effective = damage - armor
        Output(_('%(name)s takes impact of %(impact)d to %(location)s minus %(armor)d of armor for effective impact of %(effective)d.')
               % {'name':name, 'impact':damage, 'location':location.xval(), 'armor':armor, 'effective':effective})
        if effective <= 0:
            Output(_('%(name)s takes no damage.') % {'name':name}, '@blue')
            return self._CombatDoUnhorse(char, '', '', damage)

        # damage was taken; determine wound
        (injury, flags) = self.WoundInfo(location, effective)
        match = self.kInjuryWoundRe.match(injury)
        (fumble, severity, numdice, amputate) = match.groups()
        numdice = int(numdice)
        severity = self._CombatSeverityAdjustment(char, severity, numdice)
        amputate = self._CombatAmputateCheck(char, amputate, numdice, aspect)
        if amputate: numdice = 5
        wound = '%s-%s%d%s' % (location, severity, numdice, aspect)
        wdesc = self.WoundDescription(wound, char)
        Output(_('%(name)s received a %(wounddesc)s (%(woundcode)s)') %
               {'name':name, 'wounddesc':wdesc, 'woundcode':wound}, '@red')
        if amputate:
            Output(_('%(name)s\'s %(location)s has been severed!') %
                   {'name':name, 'location':location.xval()}, '@red,bold')
        wlist = char.GetList('wounds', [])
        wlist.append(wound)
        char.SetList('wounds', wlist)

        # bleeding
        if severity == 'G' and self.RuleCheck('CombatBleeders'):
            Output(_('%(name)s is bleeding from this wound!') % {'name':name}, '@red')
            bleeders = char.GetDict('bleeders', {})
            bleeders[wound] = '0'
            char.SetDict('bleeders', bleeders)

        # update penalties
        ipenalty  = int(char.GetValue('x:ipenalty', '0'))
        ipenalty += numdice
        char.SetValue('x:ipenalty',str(ipenalty))

        tactadv = 0

        # knockback
        kbamount = self._CombatKnockbackAmount(char, flags, effective)

        # fumble/stumble check
        tactadv += self._CombatDoFumble(char, flags, severity, location)  or 0
        tactadv += self._CombatDoStumble(char, flags, severity, kbamount) or 0

        # unhorse check
        tactadv += self._CombatDoUnhorse(char, flags, severity, damage, shockroll=False) or 0

        # death/shock check
        if severity == 'K':
            self._CombatDoUnhorse(char, flags, severity, damage, shockroll=False, result='cf')
            Output(_('%(name)s is dead') % {'name':name}, '@red,bold')
            char.SetStatus('dead')
            tactadv = min(tactadv, 0)
        elif severity != 'M' or self.RuleCheck('CombatMinorWoundShock'):
            end = char.GetAttribute('end')
            if char.GetStatus() != 'unconscious' and shockroll:
                msg = _('%(char)s checks endurance to remain conscious.') % {'char':char.Name()}
                upenalty = char.GetPenalty('up')
                if not self.AttributeCheck(char, 'end', roll='%dd6'%upenalty, msg=msg, penalties=False, test='shockroll'):
                    tactadv += self._CombatTA('other')
                    Output((tactadv
                            and _('%(name)s falls unconscious and gives attacker a tactical advantage. (%(penalty)dd6 > %(endurance)d)')
                            or  _('%(name)s falls unconscious. (%(penalty)dd6 > %(endurance)d)'))
                           % {'name':name, 'penalty':upenalty, 'endurance':end}, style='@red')
                    self._CombatDoUnhorse(char, flags, severity, damage, shockroll=False, result='cf')  # ignore TA result
                    char.SetStatus('unconscious')
                    tactadv = min(tactadv, 0)
                else:
                    Output(_('%(name)s manages to remain conscious despite the new wound. (%(penalty)dd6 <= %(endurance)d)') %
                           {'name':name, 'penalty':upenalty, 'endurance':end})

        # unrider check
        tactadv += self._CombatDoUnrider(char, flags, severity, damage) or 0

        # too many wounds
        if char.GetPenalty('ip') > char.GetAttribute('end') and char.GetStatus() != 'dead':
            Output(_('%(name)s has died from wounds') % {'name':name}, '@red,bold')
            char.SetStatus('dead')

        return tactadv


    def CombatWound(self, char, damage, aspect, location=None, attacker=None):
        tactadv = self._CombatDoWound(char, damage, aspect, location=location, attacker=attacker)

        if not tactadv: return None
        self.tacticalAdvantages += 1
        if tactadv < 0:
            return attacker
        else:
            return char


    def CombatAttack(self, attacker, defender,
                     attackx=None,
                     ausex=None, aaspectx=None, arangex=None, alocx=None, amod=None, aroll=None, adamage=None,
                     defensex=None,
                     dusex=None, daspectx=None, drangex=None, dlocx=None, dmod=None, droll=None, ddamage=None):
        if amod is not None: amod = int(amod)  # guaranteed int by matching regex
        if dmod is not None: dmod = int(dmod)

	tactadv = 0
	ahitmod = 0
	awepmod = 0
	aaspect = None
	asteed	= None
	arider	= None
	ahit	= False
	dhitmod = 0
	ddefmod = 0
	dwepmod = 0
	daspect = None
	dhit	= False

        #print ''
        #print 'CombatAttack: attacker:',attacker.Name(),'defender:',defender.Name(),'firstattack:',firstattack
        #print 'CombatAttack: attackx:',attackx
        #print 'CombatAttack: ausex:',ausex,'aaspectx:',aaspectx,'alocx:',alocx,'amodx:',amodx,'aroll:',aroll,'adamage:',adamage
        #print 'CombatAttack: defensex:',defensex
        #print 'CombatAttack: dusex:',dusex,'daspectx:',daspectx,'dlocx:',dlocx,'dmodx:',dmodx,'droll:',droll,'ddamage:',ddamage

        # verify attacker
        aname = attacker.Name()
        attack = XLookup(self.kAttackOptions, attackx, N_('attack action'), defindex=0)
        astatus = attacker.GetStatus()
        if astatus != 'standing' and astatus != 'mounted' and astatus != 'prone':
            raise NotPossible(_('%(name)s cannot attack when %(status)s.') % {'name':aname, 'status':P_('status:',astatus)})
        if attack == 'trample':
            asteed = attacker.GetMount()
            if not asteed:
                raise NotPossible(_('%(name)s cannot trample when not mounted') % {'name':aname})
            dsteed = defender.GetMount()
            if dsteed:
                raise NotPossible(_('%(name)s cannot trample %(other)s who is also mounted') % {'name':aname, 'other':defender.Name()})

        # verify opponent
        dname = defender.Name()
        if defender.tid == attacker.tid:
            raise NotPossible(_('%(name)s cannot attack itself.') % {'name':dname})
        defense = XLookup(self.kDefendOptions, defensex, N_('defense action'))
        dstatus = defender.GetStatus()
        if dstatus == 'dead':
            raise NotPossible(_('%(name)s is dead -- leave the poor soul alone.') % {'name':dname})
        if dstatus == 'shock' or dstatus == 'unconscious' or dstatus == 'dead':
            if defense != 'ignore':
                raise NotPossible(_('%(name)s can offer no defense but "%(ignore)s."') % {'name':dname, 'ignore':_('ignore')})

        # if steed action, switch attacker and method of attack
        if attack == 'trample':
            arider = attacker
            arattack = attack
            attacker = asteed
            attack = 'attack'
            aname = attacker.Name()
            astatus = attacker.GetStatus()
            ausex = None
            aaspectx = None
            alocx = None
            asteed.SetPrompt(arider.GetPrompt())

        # verify attack action and weapon
        aworder = self.xlist()
        awielded= attacker.AttackFromLocations(order=aworder)
        #print 'CombatAttack: Attacker:',ausex,aworder
        ause    = self.HitFromLocation(attacker, ausex, aworder, N_('attack location'), defindex=0)
        aweapon = awielded[ause]
        askill  = ''
        asmult  = 1
        #print '->',ause,aweapon
        # "None" case now covered by "HitFromLocation"
            #if aweapon is None:
            #    aweapon = attacker.UnarmedWeapon(ause)
            #    if aweapon is None:
            #        raise NotPossible(_('%(name)s cannot use %(loc)s for unarmed combat') % {'name':aname, 'loc':ause.xval()})
        askill  = aweapon.Skill(attacker, willopen=True)
        awepmod	= aweapon.GetAttribute('attack', 0)
        abaseskill = attacker.GetValue('x:base-s:%s' % askill, askill)
        if attack == 'grapple' and abaseskill != 'unarmed':
            raise NotPossible(_('%(name)s cannot grapple while holding a %(item)s.') % {'name':aname, 'item':aweapon.Name()})
        #print 'CombatAttack: Attacker: Skill:',askill

        # verify defense action and weapon
        if defense not in self.kCombatAttackTable[attack]:
            raise NotPossible(_('%(name)s cannot %(defense)s against %(attack)s.') %
                              {'name':dname, 'attack':attack.xval(), 'defense':defense.xval()})
        dworder = self.xlist()
        dwielded= defender.AttackFromLocations(dworder)
        if defense == 'counterstrike' or defense == 'grapple':
            dwmode = 'attack'
            dindex = 0
        else:
            dwmode = 'defend'
            dindex = 1
        #print 'CombatAttack: Defender: dworder:',dworder,'dusex:',dusex,'dindex:',dindex
        duse    = self.HitFromLocation(defender, dusex, dworder, N_('defend location'), defindex=dindex)
        dweapon = dwielded[duse]
        dskill  = ''
        dsmult  = 1
        #print '->',duse,dweapon
        # "None" case now covered by "HitFromLocation"
        # FIXME: test "defend with hand" becomes "defend with forearm"
            #if dweapon is None:
            #    # block location may not be the defending hand
            #    if defense == 'block':
            #        flags = None
            #        prefix= ''
            #        if duse in self.kLocationInjuryFlagsMap:
            #            flags = self.kLocationInjuryFlagsMap[duse]
            #        else:
            #            prefixlist = list()
            #            place = self.BaseLocation(duse, prefixlist)
            #            if place in self.kLocationInjuryFlagsMap:
            #                flags = self.kLocationInjuryFlagsMap[place]
            #                prefix = prefixlist[0]
            #        if flags is not None:
            #            match = re.search(r'(^|;)defend=(\w+)($|;)', flags)
            #            if match:
            #                duse = prefix + match.group(2)
            #    # get appropriate weapon
            #    dweapon = defender.UnarmedWeapon(duse)
            #    if dweapon is None:
            #        raise NotPossible(_('%(name)s cannot use %(loc)s for unarmed combat') % {'name':dname, 'loc':duse.xval()})
        # dodge uses that skill
        if defense == 'dodge':
            dskill = 'dodge'
            if dstatus == 'mounted':
                dsmult *= 0.5
        # ignore uses no skill
        elif defense == 'ignore':
            dskill = 'cf'
        # determine skill from weapon being used
        if dskill == '':
            dskill = dweapon.Skill(defender, willopen=True)
            dwepmod = dweapon.GetAttribute(dwmode)
            if dwepmod is None or dskill is None:
                if dwmode == 'attack':
                    raise NotPossible(_('%(item)s cannot be used for attack.') % {'item':dweapon.Name()})
                else:
                    raise NotPossible(_('%(item)s cannot be used for defense.') % {'item':dweapon.Name()})
        dbaseskill = defender.GetValue('x:base-s:%s' % dskill, dskill)
        if defense == 'grapple' and dskill != 'unarmed':
            raise NotPossible(_('%(name)s cannot grapple while holding a %(item)s.') % {'name':dname, 'item':dweapon.Name()})
        if defense == 'block' and dbaseskill == 'unarmed' and abaseskill != 'unarmed' and defender.GetValue('x:promptbodyblock', 'yes') == 'yes':
            bblockok = Query(_('%(name)s has no shield or weapon in %(location)s for defense against %(weapon)s.  perform a "body block"?')
                             % {'name':dname, 'location':duse.xval(), 'weapon':aweapon.Name()},
                             title=_('%(name)s body-block?')%{'name':dname}, test='bodyblock?')
            if bblockok:
                defender.SetValue('x:promptbodyblock', 'no')
            else:
                raise NotPossible(_('%(name)s does not want to do a body-block against %(weapon)s')
                                  % {'name':dname, 'weapon':aweapon.Name()})
        if defense == 'counterstrike' and dskill == 'unarmed' and defender.GetValue('x:promptunarmed', 'yes') == 'yes':
            unarmedok = Query(_('%(name)s has no weapon in %(location)s for attack.  attack "unarmed"?')
                              % {'name':dname, 'location':duse.xval()},
                              title=_('%(name)s unarmed attack?')%{'name':dname}, test='unarmed?')
            if unarmedok:
                defender.SetValue('x:promptunarmed', 'no')
            else:
                raise NotPossible(_('%(dname)s does not want to do an unarmed attack against %(aname)s')
                                  % {'dname':dname, 'aname':aname})
        #print attack,defense,dskill,dskill[-6:],aweapon,aweapon.GetInfo('velocity')
        if attack == 'attack' and askill == 'unarmed' and attacker.GetValue('x:promptunarmed', 'yes') == 'yes':
            unarmedok = Query(_('%(name)s has no weapon in %(location)s for attack.  attack "unarmed"?')
                              % {'name':aname, 'location':ause.xval()},
                              title=_('%(name)s unarmed attack?')%{'name':aname}, test='unarmed?')
            if unarmedok:
                attacker.SetValue('x:promptunarmed', 'no')
            else:
                raise NotPossible(_('%(aname)s does not want to do an unarmed attack against %(dname)s')
                                  % {'aname':aname, 'dname':dname})
        if attack == 'missile':
            if defense == 'block' and dskill[-7:] != 'shields' and aweapon.GetInfo('velocity') == 'high':
                raise NotPossible(_('%(name)s cannot block high-velocity missile without shield.') % {'name':dname, 'item':dweapon.Name()})
            if askill == 'unarmed':
                raise NotPossible(_('%(name)s cannot use %(item)s as missile.') % {'name':aname, 'item':aweapon.Name()})
        #print 'CombatAttack: Defender: Skill:',dskill

        # verify requested aspects
        aaspect = aweapon.Aspect(aaspectx)
        if defense == 'counterstrike':
            daspect = dweapon.Aspect(daspectx)

        # verify requested ranges
        arange = arangex and XLookup(self.kAttackRanges, arangex, typename="attack range")
        drange = drangex and XLookup(self.kAttackRanges, drangex, typename="defend range")
        arngmod = None
        drngmod = None
        if arange is not None and attack != 'missile':
            raise NotPossible(_('%(name)s cannot %(attack)s at range') %
                              {'name':aname, 'attack':attack.xval()})
        if arange is None and attack == 'missile':
            arange  = self.kAttackRanges[0]
        if drange is not None and defense != 'missile':
            raise NotPossible(_('%(name)s cannot %(attack)s at range') %
                              {'name':dname, 'attack':defense.xval()})
        if drange is None and defense == 'missile':
            drange = self.kAttackRanges[0]
        if attack == 'missile' and arange is not None:
            arngmod = self.kAttackRangeModifiers[arange]
        if defense == 'missile' and drange is not None:
            drngmod = self.kAttackRangeModifiers[arange]

        # verify hit locations
        ahitrange = list()
        anopen = False
        if attack != 'missile' and not alocx:
            if astatus == 'mounted' and dstatus == 'standing':
                alocx = '0high'
            elif astatus == 'standing' and dstatus == 'mounted':
                alocx = '0low'
        if alocx and alocx[0] == '0':
            alocx = alocx[1:]
            anopen = True
        self.HitLocation(defender, alocx, attacker=attacker, range_return=ahitrange, validate=True)
        if ahitrange and not anopen:
            ahitmod += -10
        if defense == 'counterstrike':
            dhitrange = list()
            dnopen = False
            if not dlocx:
                if dstatus == 'mounted' and astatus == 'standing':
                    dlocx = '0high'
                elif dstatus == 'standing' and astatus == 'mounted':
                    dlocx = '0low'
            if dlocx and dlocx[0] == '0':
                dlocx = dlocx[1:]
                dnopen = True
            self.HitLocation(attacker, dlocx, attacker=defender, range_return=dhitrange, validate=True)
            if dhitrange and not dnopen:
                ddefmod += -10

        # verify roll
        self.UserRoll(None, aroll, 0)
        self.UserRoll(None, droll, 0)

        # verify damage
        self.UserRoll(None, adamage, 0)
        self.UserRoll(None, ddamage, 0)

        # check standing/prone in meelee
        if attack != 'missile':
            if dstatus != 'standing' and dstatus != 'mounted':
                if astatus == 'mounted':
                    raise NotPossible(_('cannot attack downed characters while mounted'))
                hitok = Query(_('can only attack downed characters if not engaged\nwith any standing characters.  continue?'),
                              title=_('attack %(name)s')%{'name':dname},
                              test='attackprone?')
                if not hitok:
                    raise NotPossible(_('cannot attack prone %(name)s') % {'name':dname})
                ahitmod += 20
            if astatus == 'prone':
                ddefmod += 20
            if astatus == 'mounted' and dstatus != 'mounted':
                ahitmod += 20
            if dstatus == 'mounted' and astatus != 'mounted':
                ddefmod += 20

        # deal with steed commands
        if arider:
            acmdmod = self._CombatSteedCommandModifier(arider, mount=attacker, modifier=amod)
            if acmdmod is None:
                return defender
            if amod is None:
                amod = 0
            amod += acmdmod

        # skill checks
        Output(_('%(name)s attacks with %(weapon)s %(aspect)s.') % {'name':aname, 'weapon':aweapon.Name(), 'aspect':aaspect.xval()})
        aresult = self.SkillCheck(attacker, askill, roll=aroll, locmod=ahitmod, itemmod=awepmod, rangemod=arngmod, modifier=amod, mult=asmult, open=True)
        if defense == 'counterstrike':
            Output(_('%(name)s does a %(defense)s with %(weapon)s %(aspect)s.') % {'name':dname, 'defense':P_('cmd:',defense), 'weapon':dweapon.Name(), 'aspect':daspect.xval()})
        elif defense == 'block':
            Output(_('%(name)s does a %(defense)s with %(weapon)s.') % {'name':dname, 'defense':P_('cmd:',defense), 'weapon':dweapon.Name()})
        else:
            Output(_('%(name)s does a %(defense)s.') % {'name':dname, 'defense':P_('cmd:',defense)})
        if dskill == 'dodge':
            dacro = defender.GetSkill('acrobatics', 0)
            if dacro > defender.GetSkill('dodge', 0):
                dskill = 'acrobatics'
            if aweapon.GetInfo('velocity') == 'high':
                Output(_('%(name)s checks at 50%% because missile is high-velocity.') % {'name':dname})
                dsmult *= 0.5
        dresult = self.SkillCheck(defender, dskill, roll=droll, locmod=ddefmod, itemmod=dwepmod, rangemod=drngmod, modifier=dmod, mult=dsmult, open=True)

        # outcome
        aoindex = self.kCombatCheckIndex[aresult]
        doindex = self.kCombatCheckIndex[dresult]
        outcome = self.kCombatAttackTable[attack][defense][aoindex][doindex]
        #print 'Attack:',ause,'/',duse,':',attack,defense,aresult,dresult,'->',aoindex,doindex,'->',outcome
        if outcome == 'AF' and abaseskill == 'unarmed': outcome = 'AS'
        if outcome == 'DF' and dbaseskill == 'unarmed': outcome = 'DS'
        if outcome == 'AF':
            if defense == 'block':
                Output(_('%(name)s blocked a poor attack.') % {'name':dname}, '@blue')
            elif defense == 'counterstrike' or defense == 'grapple':
                Output(_('%(name)s countered a poor attack.') % {'name':dname}, '@blue')
            tactadv += self._CombatDoFumble(attacker, '', 'G', ause) or 0
        elif outcome == 'DF':
            if defense == 'block':
                Output(_('%(name)s blocked the attack, but poorly.') % {'name':dname}, '@blue')
            elif defense == 'counterstrike' or defense == 'grapple':
                Output(_('%(name)s countered the attack, but poorly.') % {'name':dname}, '@blue')
            tactadv -= self._CombatDoFumble(defender, '', 'G', duse) or 0
        elif outcome == 'BF':
            if defense == 'block':
                Output(_('%(name)s mis-blocked a poor attack.') % {'name':dname}, '@blue')
            elif defense == 'counterstrike' or defense == 'grapple':
                Output(_('%(name)s mis-countered a poor attack.') % {'name':dname}, '@blue')
            tactadv += self._CombatDoFumble(attacker, '', 'G', ause) or 0
            tactadv -= self._CombatDoFumble(defender, '', 'G', duse) or 0
        elif outcome == 'AS':
            Output(_('%(name)s dodged a poor attack.') % {'name':dname}, '@blue')
            tactadv += self._CombatDoStumble(attacker, 'stumble=prone', 'G', 0)  or 0
            tactadv += self._CombatDoUnhorse(attacker, '', 'G', 0, stumble=True) or 0
        elif outcome == 'DS':
            Output(_('%(name)s dodged the attack, but poorly.') % {'name':dname}, '@blue')
            tactadv -= self._CombatDoStumble(defender, 'stumble=prone', 'G', 0)  or 0
            tactadv -= self._CombatDoUnhorse(defender, '', 'G', 0, stumble=True) or 0
        elif outcome == 'BS':
            Output(_('%(name)s mis-dodged a poor attack.') % {'name':dname}, '@blue')
            tactadv += self._CombatDoStumble(attacker, 'stumble=prone', 'G', 0)  or 0
            tactadv -= self._CombatDoStumble(defender, 'stumble=prone', 'G', 0)  or 0
            tactadv += self._CombatDoUnhorse(attacker, '', 'G', 0, stumble=True) or 0
            tactadv -= self._CombatDoUnhorse(defender, '', 'G', 0, stumble=True) or 0
        elif outcome == 'dodge':
            Output(_('%(name)s dodged the attack.') % {'name':dname}, '@blue')
        elif outcome == 'miss':
            Output(_('%(name)s missed.') % {'name':aname}, '@blue')
        elif outcome == 'block':
            #print ' - askill',askill,'abaseskill:',abaseskill
            #print ' - dskill',dskill,'dbaseskill:',dbaseskill
            if dresult == 'cs':
                Output(_('%(name)s deflected the attack with %(loc)s.') % {'name':dname, 'loc':dweapon.Name()}, '@blue')
            elif dbaseskill == 'unarmed':  # body blocks
                Output(_('%(name)s blocked with %(loc)s.') % {'name':dname, 'loc':dweapon.Name()}, '@blue')
                # treat as A*2 on specific location
                if adamage is None:
                    adamage = '2d6'
                    adamage += '+%d' % aweapon.GetAttribute(aaspect)
                flags = None
                if duse in self.kLocationInjuryFlagsMap:
                    flags = self.kLocationInjuryFlagsMap[duse]
                else:
                    prefixlist = list()
                    place = self.BaseLocation(duse, prefixlist)
                    if place in self.kLocationInjuryFlagsMap:
                        flags = self.kLocationInjuryFlagsMap[place]
                        prefix = prefixlist[0]
                #print "Blocking: duse:", duse, "place:", place, "prefix:", prefix, "flags:", flags
                if flags is not None:
                    match = self.kFlagsDefendRe.search(flags)
                    if match:
                        locs = defender.GetTypeInfo('Locations', asdict=True)
                        for bplace in match.group(2).split('|'):
                            if prefix + bplace in locs:
                                duse = prefix + bplace
                                break
                tactadv -= self._CombatDoWound(defender, damage=adamage, aspect=aaspect, location=duse, attacker=attacker) or 0
            else:
                Output(_('%(name)s blocked the attack with %(item)s.') % {'name':dname, 'item':dweapon.Name()}, '@blue')
                if attack != 'missile':
                    if abaseskill != 'unarmed' and dbaseskill != 'unarmed':
                        tactadv += self._CombatDoWeaponDamage(attacker, ause, defender, duse) or 0
        elif outcome == 'DTA':
            if defense == 'block':
                Output(_('%(name)s blocked the attack easily.') % {'name':dname}, '@blue')
            else:
                Output(_('%(name)s missed.') % {'name':aname}, '@blue')
            if self._CombatTA('defender'):
                Output(_('%(name)s gains a tactical advantage.') % {'name':dname}, '@green')
                tactadv += self._CombatTA('defender')
        elif outcome == 'hold':
            if attack == 'grapple' and astatus == 'mounted' and dstatus != 'mounted':
                attacker.Dismount()
            elif defense == 'grapple' and dstatus == 'mounted' and astatus != 'mounted':
                defender.Dismount()
            if attack == defense:
                Output(_('%(name1)s and %(name2)s grab each other.') % {'name1':aname, 'name2':dname})
            elif attack == 'grapple':
                Output(_('%(name1)s grabs %(name2)s.') % {'name1':aname, 'name2':dname})
            else:
                Output(_('%(name1)s grabs %(name2)s.') % {'name1':dname, 'name2':aname})
            tactadv += self._CombatDoHold(attacker, defender) or 0
        elif outcome == 'wild':
                Output(_('%(name1)s\'s attack goes wild (perhaps hitting a different target).') % {'name1':aname}, '@red')
                self._CombatDoFumble(attacker, '', 'G', ause)  # ignore TAs in missile case
        elif len(outcome) == 3 and outcome[1] == '*':
            if adamage is None:
                adamage = '%sd6' % outcome[2]
                adamage += '+%d' % aweapon.GetAttribute(aaspect)
            if ddamage is None:
                ddamage = '%sd6' % outcome[2]
                if daspect:
                    ddamage += '+%d' % dweapon.GetAttribute(daspect)
            if outcome[0] == 'A':
                ahit = True
                Output(_('%(name)s hit! (*%(dice)s)') % {'name':aname, 'dice':outcome[2]}, '@green')
                if defense == 'counterstrike':
                    Output(_('%(name)s missed.') % {'name':dname}, '@red')
                aloc = self.HitLocation(defender, alocx, ahitrange, attacker=attacker)
                tactadv -= self._CombatDoWound(defender, damage=adamage, aspect=aaspect, location=aloc, attacker=attacker) or 0
            elif outcome[0] == 'D':
                dhit = True
                Output(_('%(name)s missed.') % {'name':aname}, '@red')
                Output(_('%(name)s hit! (*%(dice)s)') % {'name':dname, 'dice':outcome[2]}, '@green')
                dloc = self.HitLocation(attacker, dlocx, dhitrange, attacker=defender)
                tactadv += self._CombatDoWound(attacker, damage=ddamage, aspect=daspect, location=dloc, attacker=defender) or 0
            elif outcome[0] == 'B':
                dhit = ahit = True
                Output(_('%(name)s hit! (*%(dice)s)') % {'name':aname, 'dice':outcome[2]}, '@green')
                Output(_('%(name)s hit! (*%(dice)s)') % {'name':dname, 'dice':outcome[2]}, '@green')
                aloc = self.HitLocation(defender, alocx, ahitrange, attacker=attacker)
                dloc = self.HitLocation(attacker, dlocx, dhitrange, attacker=defender)
                tactadv -= self._CombatDoWound(defender, damage=adamage, aspect=aaspect, location=aloc, attacker=attacker) or 0
                tactadv += self._CombatDoWound(attacker, damage=ddamage, aspect=daspect, location=dloc, attacker=defender) or 0
            elif outcome[0] == 'M':
                ahit = True
                Output(_('%(name)s hit! (*%(dice)s)') % {'name':aname, 'dice':outcome[2]}, '@green')
                aloc = self.HitLocation(defender, alocx, ahitrange, attacker=attacker)
                self._CombatDoWound(defender, damage=adamage, aspect=aaspect, location=aloc, attacker=attacker)  # ignore TAs in this case
            else:  # pragma: no cover
                raise Exception('unsupported outcome "%s"' % outcome)
        else:  # pragma: no cover
            raise Exception('unsupported outcome "%s"' % outcome)

        # handle forced moves
        if arider and arattack == 'trample' and not ahit:
            Output(_('%(dname)s is forced aside 1 hex by %(aname)s') % {'dname':dname, 'aname':aname}, '@red')

        if not tactadv: return None
        self.tacticalAdvantages += 1
        if tactadv > 0:
            if arider: return arider
            return attacker
        else:
            return defender


    def CombatMake(self, attacker, defender, actionx, *opts):
        action = XLookup(self.kMakeActions, actionx, typename=_('action'))
        tactadv = 0

        if action == 'fumble':
            wielded = defender.GetWielded()
            if len(opts) > 1:
                raise BadParameter(_('make fumble requires single "location" option'))
            if len(opts):
                locx = opts[0]
                loc = defender.LookupWieldLocation(locx)
            else:
                try:
                    defindex = 0
                    loc = None
                    while loc is None or wielded[loc] is None:
                        loc = defender.LookupWieldLocation(None, defindex=defindex)
                        defindex += 1
                except BadParameter:
                    pass
            if loc is None or wielded[loc] is None:
                raise NotPossible(_('nothing to fumble'))
            tactadv -= self._CombatDoFumble(defender, '', 'G', loc) or 0

        elif action == 'stumble':
            if len(opts):  # FIXME: modifier
                raise BadParameter(_('make stumble requires no options'))
            status = defender.GetStatus()
            if status != 'standing':
                raise NotPossible(_('%(char)s is not standing') % {'char':defender.Name()})
            tactadv -= self._CombatDoStumble(defender, '', 'G', 1) or 0

        elif action == 'unhorse':
            if len(opts):  # FIXME: modifier
                raise BadParameter(_('make unhorse requires no options'))
            status = defender.GetStatus()
            if status != 'mounted':
                raise NotPossible(_('%(char)s is not mounted') % {'char':defender.Name()})
            tactadv -= self._CombatDoUnhorse(defender, '', 'G', 0, stumble=True) or 0

        else:  # pragma: no cover
            raise Exception('unknown action "%s" (should not reach here)' % action)
        
        if not tactadv: return None
        self.tacticalAdvantages += 1
        if tactadv > 0:
            return attacker
        else:
            return defender


    #=========================================================================#


    class Character(deity.gamesystem.GameSystem.Character):
        kGenerateFuncs	= dict(deity.gamesystem.GameSystem.Character.kGenerateFuncs)


        def __init__(self, tid, **kwds):
            deity.gamesystem.GameSystem.Character.__init__(self, tid, **kwds)


        def GetAttributeStart(self, attr, recalc=False):
            if attr == 'end':
                base = self.GetAttribute('str') + self.GetAttribute('sta') + self.GetAttribute('wil')
                return int(base / 3.0 + 0.5)
            elif attr == 'mov':
                return self.GetAttribute('agl')
            else:
                raise DatafillError(_('no default value for "%(type)s"') % {'type':attr})


        def GetSkillBase(self, skill, recalc=False):
            #print 'HarnMaster::GetSkillBase:',skill
            sbase = skill
            if skill not in GAME.kSkillInfo:
                sbase = self.GetValue('x:base-s:%s' % skill)
                if sbase is None and skill.find('/') > 0:
                    s,sbase = skill.split('/')
                if not sbase or sbase not in GAME.kSkillInfo:
                    raise DatafillError(_('unknown skill "%(skill)s"') % {'skill':skill})
            # see if it's been calculated before (never changes thereafter)
            base = self.GetValue('x:skillbase-%s' % skill)
            #print 'HarnMaster::GetSkillBase: oldbase:',base
            if not recalc and base is not None:
                return int(base)
            (category,attrs,mult,sunsigns) = GAME.kSkillInfo[sbase].split()
            # start with attributes
            base = 0.0
            divs = 0
            for a in attrs.lower().split('+'):
                aval = self.GetAttribute(a)
                if aval is not None:
                    base += aval
                    divs += 1
            base = int(base / divs + 0.5)
            # add sunsign
            charsun1 = self.GetInfo('sunsign')
            charsun2 = ''
            if charsun1 and sunsigns != "none":
                if charsun1.find('/'):
                    charsun2 = charsun1[4:]
                    charsun1 = charsun1[0:3]
                    sunbase  = 0
                for sg in sunsigns.split(';'):
                    for s in sg.split('/'):
                        if s == '': continue
                        if charsun1 == s[0:3]:
                            sunbase = max(sunbase, int(sg[-1]))
                        if charsun2 == s[0:3]:
                            sunbase = max(sunbase, int(sg[-1]))
                base += sunbase
            # return it
            #print 'HarnMaster::GetSkillBase:',skill,'=',base,'(',self.GetAttribute(a1),',',self.GetAttribute(a2),',',self.GetAttribute(a3),')'
            if not recalc:
                self.SetValue('x:skillbase-%s' % skill, str(base))
            return base


        def GetSkillStart(self, skill, recalc=False):
            #print 'HarnMaster::GetSkillStart:',skill
            base = self.GetSkillBase(skill, recalc=recalc)
            if not base: return base
            if skill in GAME.kSkillInfo:
                sbase = skill
            else:
                s,sbase = skill.split('/')
            (category,attrs,mult,sunsigns) = GAME.kSkillInfo[sbase].split()
            if mult.find('SBx') == 0:
                value = base * int(mult[-1])
            elif mult == 'spcl':
                if skill == 'language':
                    value = 70 + base
                Output(_('%(skill)s is special -- please check the generated value.')
                       % {'skill':P_('skill:',skill) })
            else:  # pragma: no cover
                print 'error: unknown skill generator "%s" for "%s"' % (mult,skill)
                value = base
            #print 'SkillStart:',skill,'=',value
            return value


        def GetDisplayInfo(self):
            info = deity.gamesystem.GameSystem.Character.GetDisplayInfo(self)
            item = self.GetValue('x:bloodloss')
            if item and int(item) > 0:
                info[_('bloodloss')] = item
            return info


        def GetDisplayStatus(self):
            status = deity.gamesystem.GameSystem.Character.GetDisplayStatus(self)
            wounds = self.GetList('wounds', [])
            wstats = ''
            for w in wounds:
                winfo = GAME.WoundBreakdown(w)
                severity = winfo['severity']
                if winfo['healrate']:
                    severity = severity.lower()
                wstats += severity
            if self.GetValue('d:bleeders'):
                wstats += '*'
            if wstats:
                return '%s/%s' % (wstats, status)
            else:
                return status


        #----------------------------------------------------------------------#


        def _GenerateHarnmasterBirthday(self):
            mon = GAME.kMonths[self.rand.randint(0,11,'birth-month')]
            dom = self.rand.randint(1,30,'birth-day')
            return '%s %d' % (mon,dom)
        kGenerateFuncs['harnmasterbirthday'] = _GenerateHarnmasterBirthday


        def _GenerateHarnmasterSunsign(self):
            bday = self.GetInfo('birthday')
            if bday is None:
                raise DatafillError(_('must have birthday to generate sunsign'))
            (mon,dom) = bday.split()
            dom = int(dom)
            mon = mon.lower()

            idx = 0
            for m in GAME.kMonths:
                if mon == m.lower():
                    break
                idx += 1
            else:  # pragma: no cover
                print 'error: could not look-up birthday month "%s"' % mon
                return ''

            if dom >= GAME.kSunsignDays[idx]:
                sun = GAME.kSunsigns[idx]
                if dom < GAME.kSunsignDays[idx]+2:
                    sun += '/' + GAME.kSunsigns[(idx-1+12)%12]
            else:
                sun = GAME.kSunsigns[idx-1]
                if dom >= GAME.kSunsignDays[idx]-2:
                    sun += '/' + GAME.kSunsigns[idx]

            return sun
        kGenerateFuncs['harnmastersunsign'] = _GenerateHarnmasterSunsign


        def _GenerateInitialValue(self, key):
            v = None
            if key[0] == 'a':
                v = self.GetAttributeStart(key[2:], recalc=True)
            if key[0] == 's':
                v = self.GetSkillStart(key[2:], recalc=True)
            if v is not None:
                return v
            return deity.gamesystem.GameSystem.Character._GenerateInitialValue(self, key)


        #---------------------------------------------------------------------#


        def SetWielded(self, vals):
            self.SetValue('x:promptbodyblock', None)
            return super(HarnMaster.Character, self).SetWielded(vals)


        def SetWorn(self, vals):
            armor = dict()
            litems= dict()
            locs  = self.GetTypeInfo('Locations', aslist=True, keysonly=True)
            if not locs:  # pragma: no cover
                raise DatafillError(_('no locations for character type "%(type)s"')
                                    % {'type':self.GetValue('x:realtype')})

            for l in locs:
                armor[l] = '0'
            #print 'SetWorn:',vals
            for item,locs in vals.iteritems():
                #print 'item',item,'->',locs
                typs = list()
                type = item.GetValue('x:type')
                prot = ''
                #sepa = type.find('(')
                #sepb = type.find(')')
                #if sepa > 0 and sepb > 0:
                #    prot = type[sepa+1:sepb].split(',')
                #    #print 'SetWorn: inline:',type,prot
                if not prot:
                    prot = item.GetValue('i:protection', '').split(',')
                    #print 'SetWorn: external:',item.GetValue('i:protection', ''),prot
                #print 'SetWorn: locs:',locs
                for l in xrange(0,len(locs)):
                    (loc,sep,inf) = locs[l].partition('=')
                    #print ' -',locs[l],':',loc,'->',inf,'(',prot,')'
                    if loc not in armor: continue
                    if loc not in litems:
                        litems[loc] = item.ShortDesc()
                    else:
                        litems[loc] += ';%s' % item.ShortDesc()
                    arm = armor[loc].split(',')
                    inf = inf.split(',')
                    if len(prot) == 1:  # if only one value, replicate for all types
                        val = prot[0]
                        prot = [val for x in inf]
                    #print '   -',arm
                    for i in xrange(0, max(len(inf),len(prot))):
                        if i < len(inf) and inf[i] != '':
                            try:
                                val = int(inf[i])
                            except:  # pragma: no cover
                                raise DatafillError(_('invalid armor value "%(val)s" for %(item)s')
                                                    % {'val':inf[i], 'item':item.GetValue('x:realtype')})
                        else:
                            val = 0
                        if i < len(prot) and prot[i] != '':
                            cur = prot[i]
                            try:
                                if cur[0] == '+' or cur[0] == '-':
                                    val += int(cur)
                                    if val < 1: val = 1
                                else:
                                    val  = int(cur)
                            except:  # pragma: no cover
                                raise DatafillError(_('invalid protection value/modifier "%(val)s" for %(item)s')
                                                    % {'val':cur, 'item':item.GetValue('x:type')})

                        if len(arm) > i:
                            arm[i] += '+%d' % val
                        else:
                            arm.append('%d' % val)
                    armor[loc] = ','.join(arm)

            #print armor
            for l,values in armor.iteritems():
                arm = values.split(',')
                for a in xrange(0,len(arm)):
                    arm[a] = str(eval(arm[a]))
                items = ''
                if l in litems:
                    ilist = litems[l].split(';')
                    # keep the order somewhat consistent but with "natural" at the end
                    try:
                        ilist.remove('natural')
                        ilist.sort()
                        ilist.append('natural')
                    except:
                        ilist.sort()
                    items = ';'.join(ilist)
                self.SetValue('x:armor-%s'%l, ','.join(arm)+';'+items)

            return super(HarnMaster.Character, self).SetWorn(vals)


        #---------------------------------------------------------------------#


        def CombatValue(self, stat):
            if stat == 'initiative':
                mount = self.GetMount()
                if mount is not None:
                    return GAME.SkillLevel(self, '%s/riding' % mount.Name(), open=True)
                return self.GetSkill('initiative')
            else:  # pragma: no cover
                raise KeyError('unknown stat "%s"' % stat)


        def GetPenalty(self, penalty):
            if   penalty == 'ip': # injury penalty
                amount = int(self.GetValue('x:ipenalty', '0')) + int(self.GetValue('x:bloodloss', '0'))
            elif penalty == 'fp': # fatigue penalty
                amount = int(self.GetValue('x:fpenalty', '0'))
                if self.GetStatus() == 'mounted':
                    amount = amount // 2
            elif penalty == 'up': # universal penalty
                amount = self.GetPenalty('ip') + self.GetPenalty('fp')
            elif penalty == 'ep': # encumbrance penalty
                amount = int(self.Load(lbs=True, groups=('worn','wielded')) / self.GetAttribute('end'))
                if self.GetStatus() == 'mounted':
                    amount = amount // 2
            elif penalty == 'pp': # physical penalty
                amount = self.GetPenalty('up') + self.GetPenalty('ep')
            else:
                raise KeyError, 'unknown penalty "%s"' % penalty
            #print 'GetPenalty:',self.Name(),penalty,'=>',amount
            return amount


        #---------------------------------------------------------------------#


        def Generate(self, type, name=None, allopts=False):
            deity.gamesystem.GameSystem.Character.Generate(self, type, name, allopts=allopts)
            attrs = list(GAME.kBaseAttributes)
            attrs.extend(GAME.kMoreAttributes)
            #for a in attrs:
            #    if not (0 <= self.GetAttribute(a) <= 100):
            #        raise NotPossible(_('incomplete generation information for %(type)s -- missing "%(attr)s"')
            #                          % {'type':type, 'attr':a});


    #=========================================================================#


    class CombatAttackWidget(wx.Panel, deity.widgets.DWidget):
        kListSeparator	= '----------------'


        def __init__(self, parent, watchedchar, **kwds):
            wx.Panel.__init__(self, parent, **kwds)
            self.parent = parent
            self.char = watchedchar
            self.last = dict()
            self.aweaponmap = None
            self.dweaponmap = None

            char = self.char.Get()
            boldfont = wx.Font(pointSize=wx.DEFAULT, family=wx.FONTFAMILY_DEFAULT,
                               style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

            self.sizer1 = wx.BoxSizer(wx.VERTICAL)
            self.apanel = wx.Panel(self, style=wx.SUNKEN_BORDER)
            self.sizer1.Add(self.apanel, 1, wx.EXPAND)
            self.sizer2 = wx.BoxSizer(wx.VERTICAL)

            # Attacker

            self.alabel = wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:attacker')+':', style=wx.ALIGN_CENTER_HORIZONTAL)
            self.alabel.SetFont(boldfont)
            self.sizer2.Add(self.alabel, 0, wx.EXPAND)
            self.aname = wx.TextCtrl(self.apanel, wx.ID_ANY, style=wx.TE_READONLY)
            self.sizer2.Add(self.aname, 0, wx.EXPAND)

            self.amlabel = wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:maneuver')+':', style=wx.ALIGN_LEFT)
            self.sizer2.Add(self.amlabel, 0, wx.EXPAND)
            self.acmd = wx.ComboBox(self.apanel, wx.ID_ANY, style=wx.CB_READONLY)
            for a in HarnMaster.kAttackOptions:
                self.acmd.Append(a.xval())
            self.Bind(wx.EVT_COMBOBOX, self.OnACmdChange, self.acmd)
            self.sizer2.Add(self.acmd, 0, wx.EXPAND)

            self.awlabel = wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:weapon')+':', style=wx.ALIGN_LEFT)
            self.sizer2.Add(self.awlabel, 0, wx.EXPAND)
            self.aweapon = wx.ComboBox(self.apanel, wx.ID_ANY, style=wx.CB_READONLY)
            self.Bind(wx.EVT_COMBOBOX, self.OnAWeaponChange, self.aweapon)
            self.sizer2.Add(self.aweapon, 0, wx.EXPAND)
            self.aaspect = wx.ComboBox(self.apanel, wx.ID_ANY, style=wx.CB_READONLY)
            for a in HarnMaster.kWeaponAspects:
                self.aaspect.Append(a.xval())
            self.sizer2.Add(self.aaspect, 0, wx.EXPAND)

            self.sizer2p = wx.BoxSizer(wx.HORIZONTAL)
            self.allabel = wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:aim')+':', style=wx.ALIGN_LEFT|wx.ALIGN_BOTTOM)
            self.sizer2p.Add(self.allabel, 0, wx.ALIGN_BOTTOM)
            self.sizer2p.AddStretchSpacer()
            self.apen = wx.CheckBox(self.apanel, wx.ID_ANY, label='  '+_('no penalty'), style=wx.ALIGN_BOTTOM)
            self.sizer2p.Add(self.apen, 0, wx.ALIGN_BOTTOM)
            self.sizer2.Add(self.sizer2p, 0, wx.EXPAND)
            self.aloc = wx.ComboBox(self.apanel, wx.ID_ANY, style=wx.CB_READONLY)
            for a in HarnMaster.kAttackAreas:
                self.aloc.Append(a.xval())
            self.sizer2.Add(self.aloc, 0, wx.EXPAND)

            self.sizer2.AddSpacer(8)

            self.sizer2v = wx.GridSizer(rows=2, cols=3, hgap=0, vgap=0)
            self.sizer2v.Add(wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:modifier'), style=wx.ALIGN_CENTER_HORIZONTAL),
                             0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_BOTTOM)
            self.sizer2v.Add(wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:roll'), style=wx.ALIGN_CENTER_HORIZONTAL),
                             0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_BOTTOM)
            self.sizer2v.Add(wx.StaticText(self.apanel, wx.ID_ANY, label=P_('heading:damage'), style=wx.ALIGN_CENTER_HORIZONTAL),
                             0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_BOTTOM)
            self.amodify = wx.TextCtrl(self.apanel, wx.ID_ANY, style=wx.TE_RIGHT|wx.TE_DONTWRAP,
                                       validator=GAME.ValidateModifier())
            self.amodify.SetMinSize((self.SizeWidth('+000'),-1))
            self.sizer2v.Add(self.amodify, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_TOP)
            self.aroll = wx.TextCtrl(self.apanel, wx.ID_ANY, style=wx.TE_RIGHT|wx.TE_DONTWRAP,
                                     validator=GAME.ValidateModifier())
            self.aroll.SetMinSize((self.SizeWidth('+000'),-1))
            self.sizer2v.Add(self.aroll, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_TOP)
            self.adamage = wx.TextCtrl(self.apanel, wx.ID_ANY, style=wx.TE_RIGHT|wx.TE_DONTWRAP,
                                       validator=GAME.ValidateModifier())
            self.adamage.SetMinSize((self.SizeWidth('+000'),-1))
            self.sizer2v.Add(self.adamage, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_TOP)
            self.sizer2.Add(self.sizer2v, 0, wx.EXPAND)

            self.apanel.SetSizerAndFit(self.sizer2)

            # Defender

            self.dpanel = wx.Panel(self, style=wx.SUNKEN_BORDER)
            self.sizer1.Add(self.dpanel, 1, wx.EXPAND)
            self.sizer3 = wx.BoxSizer(wx.VERTICAL)
            self.dlabel = wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:defender')+':', style=wx.ALIGN_CENTER_HORIZONTAL)
            self.dlabel.SetFont(boldfont)
            self.sizer3.Add(self.dlabel, 0, wx.EXPAND)
            self.dname = wx.ComboBox(self.dpanel, wx.ID_ANY, style=wx.CB_READONLY)
            self.Bind(wx.EVT_COMBOBOX, self.OnDNameChange, self.dname)
            self.sizer3.Add(self.dname, 0, wx.EXPAND)

            self.dmlabel = wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:maneuver')+':', style=wx.ALIGN_LEFT)
            self.sizer3.Add(self.dmlabel, 0, wx.EXPAND)
            self.dcmd = wx.ComboBox(self.dpanel, wx.ID_ANY, style=wx.CB_READONLY)
            for a in HarnMaster.kDefendOptions:
                self.dcmd.Append(a.xval())
            self.Bind(wx.EVT_COMBOBOX, self.OnDCmdChange, self.dcmd)
            self.sizer3.Add(self.dcmd, 0, wx.EXPAND)

            self.dwlabel = wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:weapon')+':', style=wx.ALIGN_LEFT)
            self.sizer3.Add(self.dwlabel, 0, wx.EXPAND)
            self.dweapon = wx.ComboBox(self.dpanel, wx.ID_ANY, style=wx.CB_READONLY)
            self.Bind(wx.EVT_COMBOBOX, self.OnDWeaponChange, self.dweapon)
            self.sizer3.Add(self.dweapon, 0, wx.EXPAND)
            self.daspect = wx.ComboBox(self.dpanel, wx.ID_ANY, style=wx.CB_READONLY)
            for a in HarnMaster.kWeaponAspects:
                self.daspect.Append(a.xval())
            self.sizer3.Add(self.daspect, 0, wx.EXPAND)

            self.sizer3p = wx.BoxSizer(wx.HORIZONTAL)
            self.dllabel = wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:aim')+':', style=wx.ALIGN_LEFT|wx.ALIGN_BOTTOM)
            self.sizer3p.Add(self.dllabel, 0, wx.ALIGN_BOTTOM)
            self.sizer3p.AddStretchSpacer()
            self.dpen = wx.CheckBox(self.dpanel, wx.ID_ANY, label='  '+_('no penalty'), style=wx.ALIGN_BOTTOM)
            self.sizer3p.Add(self.dpen, 0, wx.EXPAND)
            self.sizer3.Add(self.sizer3p, 0, wx.EXPAND)
            self.dloc = wx.ComboBox(self.dpanel, wx.ID_ANY, style=wx.CB_READONLY)
            for a in HarnMaster.kAttackAreas:
                self.dloc.Append(a.xval())
            self.sizer3.Add(self.dloc, 0, wx.EXPAND)

            self.sizer3.AddSpacer(8)

            self.sizer3v = wx.GridSizer(cols=3, hgap=0, vgap=0)
            self.sizer3v.Add(wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:modifier'), style=wx.ALIGN_CENTER_HORIZONTAL),
                             0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_BOTTOM)
            self.sizer3v.Add(wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:roll'), style=wx.ALIGN_CENTER_HORIZONTAL),
                             0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_BOTTOM)
            self.sizer3v.Add(wx.StaticText(self.dpanel, wx.ID_ANY, label=P_('heading:damage'), style=wx.ALIGN_CENTER_HORIZONTAL),
                             0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_BOTTOM)
            self.dmodify = wx.TextCtrl(self.dpanel, wx.ID_ANY, style=wx.TE_RIGHT|wx.TE_DONTWRAP,
                                       validator=GAME.ValidateModifier())
            self.dmodify.SetMinSize((self.SizeWidth('+000'),-1))
            self.sizer3v.Add(self.dmodify, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_TOP)
            self.droll = wx.TextCtrl(self.dpanel, wx.ID_ANY, style=wx.TE_RIGHT|wx.TE_DONTWRAP,
                                     validator=GAME.ValidateModifier())
            self.droll.SetMinSize((self.SizeWidth('+000'),-1))
            self.sizer3v.Add(self.droll, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_TOP)
            self.ddamage = wx.TextCtrl(self.dpanel, wx.ID_ANY, style=wx.TE_RIGHT|wx.TE_DONTWRAP,
                                       validator=GAME.ValidateModifier())
            self.ddamage.SetMinSize((self.SizeWidth('+000'),-1))
            self.sizer3v.Add(self.ddamage, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_TOP)
            self.sizer3.Add(self.sizer3v, 0, wx.EXPAND)

            self.dpanel.SetSizerAndFit(self.sizer3)

            self.button = wx.Button(self, wx.ID_ANY, label=P_('btn:attack'))
            self.button.Bind(wx.EVT_BUTTON, self.OnAttack)
            self.sizer1.Add(self.button, 0, wx.EXPAND)
            self.SetSizerAndFit(self.sizer1)


        def GetDefender(self):
            cindex = self.dname.GetSelection()
            if cindex < 0: return None
            return self.dname.GetClientData(cindex)


        def FieldRefresh(self, last, field, name, default=None, valid=''):
            if isinstance(field, wx.ComboBox):
                if not valid:
                    (set,val) = ('invalid', '')
                    field.SetSelection(wx.NOT_FOUND)
                elif name in last:
                    (set, val) = ('last', last[name])
                    field.SetStringSelection(last[name])
                elif type(default) == type(str()):
                    (set, val) = ('default', default)
                    field.SetStringSelection(default)
                elif type(default) == type(int()):
                    (set, val) = ('default-pos', str(default))
                    field.SetSelection(default)
                else:
                    (set, val) = ('empty', '')
                    field.SetSelection(wx.NOT_FOUND)
                ret = field.GetStringSelection()
            elif isinstance(field, wx.CheckBox):
                if not valid:
                    (set, val) = ('invalid', 'False')
                    field.SetValue(False)
                elif name in last:
                    (set, val) = ('last', str(last[name]))
                    field.SetValue(last[name])
                elif default:
                    (set, val) = ('default', 'True')
                    field.SetValue(True)
                else:
                    (set, val) = ('default', 'False')
                    field.SetValue(False)
                ret = str(field.GetValue())
            elif isinstance(field, wx.TextCtrl):
                if not valid:
                    (set, val) = ('invalid', '')
                    field.SetValue('')
                elif name in last:
                    (set, val) = ('last', last[name])
                    field.SetValue(last[name])
                elif default is not None:
                    (set, val) = ('default', default)
                    field.SetValue(str(default))
                else:
                    (set, val) = ('empty', '')
                    field.SetValue('')
                ret = str(field.GetValue())
            else:
                raise Exception, 'error: unknown field type %s' % type(field)
            #print 'FieldRefresh: set %s field "%s" to %s "%s": %s' % (valid,name,set,val,ret)


        def OnACmdChange(self, ev):
            acmd = self.acmd.GetValue()
            self.aweapon.SetSelection(0)
            self.OnAWeaponChange(None)

            if acmd == P_('cmd:attack'):
                self.aweapon.Enable(True)
                self.aaspect.Enable(True)
                self.aloc.Enable(True)
                self.apen.Enable(True)
                self.amodify.Enable(True)
                self.aroll.Enable(True)
                self.adamage.Enable(True)
            elif acmd == P_('cmd:grapple'):
                self.aweapon.Enable(True)
                self.aaspect.Enable(False)
                self.aloc.Enable(False)
                self.apen.Enable(False)
                self.amodify.Enable(True)
                self.aroll.Enable(True)
                self.adamage.Enable(False)
            elif acmd == P_('cmd:wound'):
                self.aweapon.Enable(False)
                self.aaspect.Enable(True)
                self.aloc.Enable(True)
                self.apen.Enable(False)
                self.amodify.Enable(False)
                self.aroll.Enable(False)
                self.adamage.Enable(True)


        def OnAWeaponChange(self, ev):
            weapon = self.aweapon.GetSelection()
            if weapon < 0:
                return
            weapon = self.aweaponmap[weapon]
            if not weapon:
                return
            aspect = weapon.Aspect()
            self.aaspect.SetStringSelection(aspect.xval())


        def OnDNameChange(self, ev):
            self.DefenderRefresh()


        def OnDWeaponChange(self, ev):
            windex = self.dweapon.GetSelection()
            if windex < 0:
                return
            weapon = self.dweaponmap[windex]
            if not weapon:
                return
            defender = self.GetDefender()
            if not defender: return
            aspect = weapon.Aspect()
            self.daspect.SetStringSelection(aspect.xval())


        def OnDCmdChange(self, ev):
            dcmd = self.dcmd.GetValue()
            if dcmd == P_('cmd:counterstrike'):
                self.dweapon.SetSelection(0)
            else:
                self.dweapon.SetSelection(1)
            self.OnDWeaponChange(None)

            if dcmd == P_('cmd:block'):
                self.dweapon.Enable(True)
                self.daspect.Enable(False)
                self.dloc.Enable(False)
                self.dpen.Enable(False)
                self.dmodify.Enable(True)
                self.droll.Enable(True)
                self.ddamage.Enable(False)
            elif dcmd == P_('cmd:counterstrike'):
                self.dweapon.Enable(True)
                self.daspect.Enable(True)
                self.dloc.Enable(True)
                self.dpen.Enable(True)
                self.dmodify.Enable(True)
                self.droll.Enable(True)
                self.ddamage.Enable(True)
            elif dcmd == P_('cmd:dodge'):
                self.dweapon.Enable(False)
                self.daspect.Enable(False)
                self.dloc.Enable(False)
                self.dpen.Enable(False)
                self.dmodify.Enable(True)
                self.droll.Enable(True)
                self.ddamage.Enable(False)
            elif dcmd == P_('cmd:ignore'):
                self.dweapon.Enable(False)
                self.daspect.Enable(False)
                self.dloc.Enable(False)
                self.dpen.Enable(False)
                self.dmodify.Enable(False)
                self.droll.Enable(False)
                self.ddamage.Enable(False)


        def OnAttack(self, ev):
            char = self.char.Get()
            if not char: return
            name = char.Name()
            last = self.last[name]
            acmd = self.acmd.GetStringSelection()
            aweapon = self.aweapon.GetStringSelection()
            aaspect = self.aaspect.GetStringSelection()
            aloc = self.aloc.GetStringSelection()
            apen = self.apen.GetValue()
            amodify = self.amodify.GetValue()
            aroll = self.aroll.GetValue()
            adamage = self.adamage.GetValue()
            dname = self.dname.GetStringSelection()
            if amodify != '':
                if int(amodify) != 0:
                    amodify = '%+d' % int(amodify)
                else:
                    amodify = ''
            last['acmd'] = acmd
            last['aweapon'] = aweapon
            last['aaspect'] = aaspect
            last['aloc'] = aloc
            last['apen'] = apen
            last['amodify'] = amodify
            last['dname'] = dname
            aweapon = '%s:%s' % (aweapon, aaspect)
            if aloc == P_('loc:anywhere'):
                aloc = ''
            elif apen:
                aloc = '0'+aloc
            ard = aroll
            if adamage != '':
                ard = '%s!%s' % (aroll,adamage)

            if dname == '': return
            char = self.GetDefender()
            last = self.last[dname]
            dcmd = self.dcmd.GetStringSelection()
            dweapon = self.dweapon.GetStringSelection()
            daspect = self.daspect.GetStringSelection()
            dloc = self.dloc.GetStringSelection()
            dpen = self.dpen.GetValue()
            dmodify = self.dmodify.GetValue()
            droll = self.droll.GetValue()
            ddamage = self.ddamage.GetValue()
            if dmodify != '':
                if int(dmodify) != 0:
                    dmodify = '%+d' % int(dmodify)
                else:
                    dmodify = ''
            last['dcmd'] = dcmd
            last['dweapon'] = dweapon
            last['daspect'] = daspect
            last['dloc'] = dloc
            last['dpen'] = dpen
            last['dmodify'] = dmodify
            if dcmd == P_('cmd:dodge'):
                dweapon = ''
                daspect = ''
                dloc = ''
                dpen = ''
                ddamage = ''
            elif dcmd == P_('cmd:block'):
                daspect = ''
                dloc = ''
                dpen = ''
                ddamage = ''
            elif dcmd == P_('cmd:counterstrike'):
                pass
            elif dcmd == P_('cmd:grapple'):
                dweapon = ''
                daspect = ''
                dloc = ''
                dpen = ''
                ddamage = ''
            elif dcmd == P_('cmd:ignore'):
                dweapon = ''
                daspect = ''
                dloc = ''
                dpen = ''
                dmodify = ''
                droll = ''
                ddamage = ''
            else:
                print 'OnAttack: error: unknown defence command:',dcmd
            if daspect != '':
                dweapon = '%s:%s' % (dweapon, daspect)
            if dloc == P_('loc:anywhere'):
                dloc = ''
            elif dpen:
                dloc = '0'+dloc
            drd = droll
            if ddamage != '':
                drd = '%s!%s' % (droll,ddamage)

            #print 'OnAttack: acmd:',acmd,'dname:',dname,'aweapon:',aweapon,'aloc:',aloc,'amodify:',amodify,'ard:',ard,'defend dcmd:',dcmd,'dloc:',dloc,'dmodify:',dmodify,'drd:',drd
            #print 'OnAttack: last:',last
            if acmd == P_('cmd:wound'):
                self.parent.app.CommandExecute(acmd, dname, aaspect, aloc, adamage)
            else:
                self.parent.app.CommandExecute(acmd, dname, aweapon, aloc, amodify, ard, 'defend', dcmd, dweapon, dloc, dmodify, drd)


        def SetParticipants(self, charlist):
            #self.participants = charlist
            self.dname.Clear()
            for c in charlist:
                i = self.dname.GetCount()
                self.dname.Append(c.Name())
                self.dname.SetClientData(i, c)


        def AttackerRefresh(self):
            char = self.char.Get()
            name = ''
            last = dict()
            if char:
                name = char.Name()
                if name not in self.last:
                    self.last[name] = dict()
                last = self.last[name]

            self.aweapon.Clear()
            self.aweaponmap = list()
            if char:
                lorder  = list()
                wielded = char.AttackFromLocations(order=lorder, separate=self.kListSeparator)
                for l in lorder:
                    self.aweapon.Append(l)
                    if l in wielded:
                        self.aweaponmap.append(wielded[l])
                    else:
                        self.aweaponmap.append(None)

            self.aname.ChangeValue(name)
            self.FieldRefresh(last, self.acmd, 'acmd', valid=name, default=0)
            self.FieldRefresh(last, self.aweapon, 'aweapon', valid=name, default=0)
            self.OnACmdChange(None)
            #self.OnAWeaponChange(None)
            self.FieldRefresh(last, self.aloc, 'aloc', valid=name, default=0)
            self.FieldRefresh(last, self.apen, 'apen', valid=name)
            self.FieldRefresh(last, self.amodify, 'amodify', valid=name)
            self.FieldRefresh(last, self.aroll, 'aroll', valid=name)
            self.FieldRefresh(last, self.adamage, 'adamage', valid=name)
            self.FieldRefresh(last, self.dname, 'dname', valid=name)
            self.DefenderRefresh()


        def DefenderRefresh(self):
            char = self.GetDefender()
            name = ''
            last = dict()
            if char:
                name = char.Name()
                if name not in self.last:
                    self.last[name] = dict()
                last = self.last[name]

            self.dweaponmap = list()
            self.dweapon.Clear()
            if char:
                lorder  = list()
                wielded = char.AttackFromLocations(order=lorder, separate=self.kListSeparator)
                for l in lorder:
                    self.dweapon.Append(l)
                    if l in wielded:
                        self.dweaponmap.append(wielded[l])
                    else:
                        self.dweaponmap.append(None)

            self.FieldRefresh(last, self.dcmd, 'dcmd', valid=name, default=0)
            self.OnDCmdChange(None)
            #self.OnDWeaponChange(None)
            self.FieldRefresh(last, self.dloc, 'dloc', valid=name, default=0)
            self.FieldRefresh(last, self.dpen, 'dpen', valid=name)
            self.FieldRefresh(last, self.dmodify, 'dmodify', valid=name)
            self.FieldRefresh(last, self.droll, 'droll', valid=name)
            self.FieldRefresh(last, self.ddamage, 'ddamage', valid=name)


        def DisplayRefresh(self):
            changed = False

            if self.char.IsChanged(self):
                self.AttackerRefresh()
                changed = True

            return changed


    #=========================================================================#


    class CharEditValueManager(deity.gamesystem.GameSystem.CharEditValueManager):
        kValueDependents = {
            'i:sunsign':	[],
            'i:birthday':	('i:sunsign',),
            'a:agl':		('a:mov',),
            'a:str':		('a:end',),
            'a:sta':		('a:end',),
            'a:wil':		('a:end',),
        };
        kValueDependentsUpdated = False
        kImperialHeightRe = re.compile('^(?:(\d+)\')?(\d+)\s*(?:"|in)$')
        kMetricHeightRe   = re.compile('^(\d+)\s*cm$')
        kImperialWeightRe = re.compile('^(\d+(\.\d)?)\s*lbs?$')
        kMetricWeightRe   = re.compile('^(\d+(\.\d)?)\s*kg$')


        def __init__(self, char):
            deity.gamesystem.GameSystem.CharEditValueManager.__init__(self, char)

            if not self.__class__.kValueDependentsUpdated:
                #print 'CharEditValueManager::ValueUpdated: adding skill dependencies'
                for s,i in GAME.kSkillInfo.iteritems():
                    s  = 's:'+s.lower()
                    si = i.split()
                    self.__class__.kValueDependents['i:sunsign'].append(s)
                    for a in si[1].lower().split('+'):
                        if a == '': continue
                        a = 'a:'+a
                        if a not in self.__class__.kValueDependents:
                            self.__class__.kValueDependents[a] = list()
                        if isinstance(self.__class__.kValueDependents[a], tuple):
                            self.__class__.kValueDependents[a] = list(self.__class__.kValueDependents[a])
                        if s not in self.__class__.kValueDependents[a]:
                            self.__class__.kValueDependents[a].append(s)
                self.__class__.kValueDependentsUpdated = True
                #print self.__class__.kValueDependents


        def ValueUpdated(self, var, new, old):
            #print 'CharEditValueManager::ValueUpdated: var:',var,'new:',new,'old:',old

            # This block is here just to catch development mistakes.  It
            # should not be necessary when completed as there should be
            # no divengence.
            #if old != self.char.Get().GetValue(var) and not (old == "" and self.char.Get().GetValue(var) == None):
            #    raise Exception('editor value %s="%s" has diverged from stored value "%s"' % (var,old,self.char.Get().GetValue(var)))

            update = list()
            if var in self.kValueDependents:
                update = self.kValueDependents[var]

            if var == 'i:height':
                match = self.kImperialHeightRe.match(new)
                if match:
                    feet = match.group(1)
                    inch = match.group(2)
                    if feet:
                        feet = int(feet)
                    else:
                        feet = 0
                    inch = int(inch)
                    new = str(int((feet * 12 + inch) / deity.kInchesPerCm + 0.5))
                else:
                    match = self.kMetricHeightRe.match(new)
                    if match:
                        new = match.group(1)
                    else:
                        return N_('"%(val)s" is not a valid height')
            elif var == 'i:weight':
                match = self.kImperialWeightRe.match(new)
                if match:
                    lbs = match.group(1)
                    new = str(float(lbs) / deity.kLbsPerKg)
                else:
                    match = self.kMetricWeightRe.match(new)
                    if match:
                        new = match.group(1)
                    else:
                        return N_('"%(val)s" is not a valid weight')
            self.char.Alter().SetValue(var, new)

            for v in update:
                vcur = self.GetValue(v)
                #print ' -',var,'->',v,'=',vcur,'/',self.char.Get().GetValue(v)
                if not vcur:
                    continue
                if v[0:2] == 'a:':
                    self.char.Get().SetValue(var, old)
                    vold = self.char.Get().GetAttributeStart(v[2:])
                    self.char.Get().SetValue(var, new)
                    vnew = self.char.Get().GetAttributeStart(v[2:])
                    vcur = deity.DefaultInt(vcur, 0)
                    vold = deity.DefaultInt(vold, 0)
                    vnew = deity.DefaultInt(vnew, 0)
                    vadj = vcur - vold + vnew
                    #print ' - vcur:',vcur,'vold:',vold,'vnew:',vnew,'vadj:',vadj
                    if vadj < 0: vadj = 0
                    self.ChangeValue(v, str(vadj))
                elif v[0:2] == 's:':
                    self.char.Get().SetValue(var, old)
                    vold = self.char.Get().GetSkillBase(v[2:], recalc=True)
                    self.char.Get().SetValue(var, new)
                    vnew = self.char.Get().GetSkillBase(v[2:], recalc=True)
                    #print ' - vcur:',vcur,'vold:',vold,'vnew:',vnew
                    vcur = deity.DefaultInt(vcur, 1)
                    vold = deity.DefaultInt(vold, 1)
                    vnew = deity.DefaultInt(vnew, 1)
                    vmul = vcur // vold
                    vdif = vcur - vold * vmul
                    vadj = vnew * vmul + vdif
                    #print ' - vcur:',vcur,'vold:',vold,'vnew:',vnew,'vmul:',vmul,'vdif:',vdif,'vadj:',vadj
                    if vadj < 0: vadj = 0
                    self.ChangeValue(v, str(vadj))
                elif v[0:2] == 'i:':
                    if v == 'i:sunsign':
                        sunsign = self.char.Get()._GenerateHarnmasterSunsign()
                        self.ChangeValue(v, sunsign)
                    else:  # pragma: no cover
                        raise Exception('unknown info value dependent "%s" of "%s" (newval="%s"->"%s")' % (v,var,old,new))
                else:  # pragma: no cover
                    raise Exception('unknown value type for dependent "%s" of "%s" (newval="%s"->"%s")' % (v,var,old,new))


    #-------------------------------------------------------------------------#


#    class CharEditWidget(wx.Panel, deity.widgets.DWidget):
#
#        def __init__(self, parent, **kwds):
#            wx.Panel.__init__(self, parent, **kwds)
#            self.parent = parent


###############################################################################


class HarnMaster3(HarnMaster):
    kInjuryWoundRe = re.compile(r'^(\*?)([MSGK])(\d)(-?)$')
    kLocationInjuryWoundMap = {
        'skull':	('4= M1', '8= S2', '12= S3', '16= K4', '999= K5' ),
        'face':		('4= M1', '8= S2', '12= S3', '16= G4', '999= K5' ),
        'neck':		('4= M1', '8= S2', '12= S3', '16= K4', '999= K5-'),
        'shoulder':	('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*K4' ),
        'upperarm':	('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'elbow':	('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*G5-'),
        'arm':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'forearm':	('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'hand':		('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*G5-'),
        'thorax':	('4= M1', '8= S2', '12= S3', '16= G4', '999= K5' ),
        'abdomen':	('4= M1', '8= S2', '12= S3', '16= K4', '999= K5' ),
        'groin':	('4= M1', '8= S2', '12= S3', '16= G4', '999= G5-'),
        'hip':		('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*K4' ),
        'thigh':	('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*K4-'),
        'knee':		('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*G5-'),
        'calf':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'foot':		('4=*M1', '8=*S2', '12=*S3', '16=*G4', '999=*G5-'),
        # other locations for non-humanoid
        'arm':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'chest':	('4= M1', '8= S2', '12= S3', '16= G4', '999= K5' ),
        'head':		('4= M1', '8= S2', '12= S3', '16= K4', '999= K5' ),
        'leg':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'limb':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'quarter':	('4= M1', '8= S2', '12= S3', '16= G4', '999= K5' ),
        'tail':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        'wing':		('4=*M1', '8=*M1', '12=*S2', '16=*S3', '999=*G4-'),
        # catch-all for anything not listed above
        '':		('4= M1', '8= S2', '12= S3', '16= G4', '999= K5' ),
    }
    kLocationInjuryFlagsMap = {
        'skull':	'knockback=1',
        'face':		'knockback=1',
        'neck':		'knockback=1',
        'shoulder':	'knockback=1;fumble=hand',
        'upperarm':	'fumble=hand',
        'elbow':	'fumble=hand',
        'forearm':	'fumble=hand',
        'hand':		'fumble=hand;defend=forearm|arm',
        'thorax':	'knockback=1',
        'abdomen':	'knockback=1',
        'groin':	'',
        'hip':		'stumble=prone',
        'thigh':	'stumble=prone',
        'knee':		'stumble=prone',
        'calf':		'stumble=prone',
        'foot':		'stumble=prone',
        # other locations for non-humanoid
        'arm':		'fumble=hand',
        'chest':	'knockback=1',
        'head':		'knockback=1',
        'leg':		'stumble=prone',
        'limb':		'stumble=prone',
        'quarter':	'stumble=prone',
        'tail':		'',
        'wing':		'stumble=prone',
        # catch-all for anything not listed above
        '':		'',
    }

    kGeneralInfos   = xlist((NP_('attr:sex'), NP_('attr:height'),NP_('attr:weight'),NP_('attr:frame'),NP_('attr:piety'),
                             NP_('attr:birthday'), NP_('attr:sunsign')),
                            xlate='attr:')
    kBaseAttributes = xlist((NP_('attr:str'),NP_('attr:sta'),NP_('attr:dex'),NP_('attr:agl'),NP_('attr:eye'),NP_('attr:hrg'),
                             NP_('attr:sml'),NP_('attr:int'),NP_('attr:aur'),NP_('attr:wil'),NP_('attr:voi'),NP_('attr:cml')),
                            xlate='attr:')
    kMoreAttributes = xlist((NP_('attr:end'),NP_('attr:mov')), xlate='attr:')
    kSkillSections  = xlist((NP_('skill:physical'),NP_('skill:combat'),NP_('skill:religion'),NP_('skill:communication'),NP_('skill:craft')),
                            xlate='skill:')
    kSkillInfo = xdict({
        # physical
        NP_('skill:acrobatics'):	'c/physical STR+AGL+AGL SBx2 NAD+2;HIR+1',
        NP_('skill:climbing'):		'c/physical STR+DEX+AGL SBx4 ULA/ARA+2',
        NP_('skill:condition'):		'c/physical STR+STA+WIL SBx5 ULA/LAD+1',
        NP_('skill:dancing'):		'c/physical DEX+AGL+AGL SBx2 TAR+2;HIR/TAI+1',
        NP_('skill:jumping'):		'c/physical STR+AGL+AGL SBx4 NAD/HIR+2',
        NP_('skill:legerdemain'):	'c/physical DEX+DEX+WIL SBx1 SKO/TAI/TAR+2',
        NP_('skill:skiing'):		'c/physical STR+DEX+AGL SBx1 MAS+2;SKO/LAD+1',
        NP_('skill:stealth'):		'c/physical AGL+HRG+WIL SBx3 HIR/TAR/TAI+2',
        NP_('skill:swimming'):		'c/physical STA+DEX+AGL SBx1 SKO+1;MAS/LAD+3',
        NP_('skill:throwing'):		'c/physical STR+DEX+EYE SBx4 HIR+2;TAR/NAD+1',

        # combat
        NP_('skill:initiative'):	'a/combat AGL+WIL+WIL SBx4 none',
        NP_('skill:dodge'):		'a/combat AGL+AGL+AGL SBx5 none',
        NP_('skill:unarmed'):		'a/combat STR+DEX+AGL SBx4 MAS/LAD/ULA+2',
        NP_('skill:riding'):		'a/combat DEX+AGL+WIL SBx1 ULA/ARA+1',
        NP_('skill:axes'):		'a/combat STR+STR+DEX SBx3 AHN/FEN/ANG+1',
        NP_('skill:blowguns'):		'a/combat STA+DEX+EYE SBx4 HIR+2;TAR/NAD+1',
        NP_('skill:bows'):		'a/combat STR+DEX+EYE SBx2 HIR/TAR/NAD+1',
        NP_('skill:clubs'):		'a/combat STR+STR+DEX SBx4 ULA/ARA+1',
        NP_('skill:daggers'):		'a/combat DEX+DEX+EYE SBx3 ANG/NAD+2',
        NP_('skill:flails'):		'a/combat DEX+DEX+DEX SBx1 HIR/TAR/NAD+1',
        NP_('skill:nets'):		'a/combat DEX+DEX+EYE SBx1 ULA/ARA+1',
        NP_('skill:polearms'):		'a/combat STR+STR+DEX SBx2 ANG/NAD+2',
        NP_('skill:shields'):		'a/combat STR+DEX+DEX SBx3 ULA/LAD/MAS+1',
        NP_('skill:slings'):		'a/combat DEX+DEX+EYE SBx1 HIR/TAR/NAD+1',
        NP_('skill:spears'):		'a/combat STR+STR+DEX SBx3 ARA/FEN/ULA+1',
        NP_('skill:swords'):		'a/combat STR+DEX+DEX SBx3 ANG+3;AHN/NAD+1',
        NP_('skill:whips'):		'a/combat DEX+DEX+EYE SBx1 HIR/NAD+1',

        # religion
        NP_('skill:agrik'):		'b/religion VOI+INT+STR SBx1 NAD+2;ANG/AHN+1',
        NP_('skill:halea'):		'b/religion VOI+INT+CML SBx1 TAR+2;HIR/MAS+1',
        NP_('skill:ilvir'):		'b/religion VOI+INT+AUR SBx1 SKO+2;TAI/ULA+1',
        NP_('skill:larani'):		'b/religion VOI+INT+WIL SBx1 ANG+2;AHN/FEN+1',
        NP_('skill:morgath'):		'b/religion VOI+INT+AUR SBx1 LAD+2;AHN/MAS+1',
        NP_('skill:naveh'):		'b/religion VOI+INT+WIL SBx1 MAS+2;SKO/TAR+1',
        NP_('skill:peoni'):		'b/religion VOI+INT+DEX SBx1 ARA+2;ANG/ULA+1',
        NP_('skill:sarajin'):		'b/religion VOI+INT+STR SBx1 FEN+2;ARA/LAD+1',
        NP_('skill:saveknor'):		'b/religion VOI+INT+INT SBx1 TAI+2;SKO/TAR+1',
        NP_('skill:siem'):		'b/religion VOI+INT+AUR SBx1 HIR+2;FEN/ULA+1',

        # Shek-Pvar
        NP_('skill:lyahvi'):            'b/shekpvar AUR+AUR+EYE SBx1 ULA-3;ARA-2;FEN-1;AHN+0;ANG+1;NAD+2;HIR+3;TAR+2;TAI+1;SKO+0;MAS-1;LAD-2',
        NP_('skill:peleahn'):           'b/shekpvar AUR+AUR+AGL SBx1 ULA-1;ARA+0;FEN+1;AHN+2;ANG+3;NAD+2;HIR+1;TAR+0;TAI-1;SKO-2;MAS-3;LAD-2',
        NP_('skill:jmorvi'):            'b/shekpvar AUR+AUR+STR SBx1 ULA+1;ARA+2;FEN+3;AHN+2;ANG+1;NAD+0;HIR-1;TAR-2;TAI-3;SKO-2;MAS-1;LAD+0',
        NP_('skill:fyvria'):            'b/shekpvar AUR+AUR+SML SBx1 ULA+3;ARA+2;FEN+1;AHN+0;ANG-1;NAD-2;HIR-3;TAR-2;TAI-1;SKO+0;MAS+1;LAD+2',
        NP_('skill:odivshe'):           'b/shekpvar AUR+AUR+DEX SBx1 ULA+1;ARA+0;FEN-1;AHN-2;ANG-3;NAD-2;HIR-1;TAR+0;TAI+1;SKO+2;MAS+3;LAD+2',
        NP_('skill:savorya'):           'b/shekpvar AUR+AUR+INT SBx1 ULA-1;ARA-2;FEN-3;AHN-2;ANG-1;NAD+0;HIR+1;TAR+2;TAI+3;SKO+2;MAS+1;LAD+0',
        NP_('skill:neutral'):           'b/shekpvar AUR+AUR+WIL SBx1 none',

        # communication
        NP_('skill:acting'):		'd/communication AGL+VOI+INT SBx2 TAR/TAI+1',
        NP_('skill:awareness'):		'd/communication EYE+HRG+SML SBx4 HIR/TAR+2',
        NP_('skill:intrigue'):		'd/communication INT+AUR+WIL SBx3 TAI/TAR/SKO+1',
        NP_('skill:lovecraft'):		'd/communication CML+AGL+VOI SBx3 MAS/ANG+1',
        NP_('skill:mentalconflict'):	'd/communication AUR+WIL+WIL SBx3 none',
        NP_('skill:musician'):		'd/communication DEX+HRG+HRG SBx1 MAS/ANG+1',
        NP_('skill:oratory'):		'd/communication CML+VOI+INT SBx2 TAR+1',
        NP_('skill:rhetoric'):		'd/communication VOI+INT+WIL SBx3 TAI/TAR/SKO+1',
        NP_('skill:singing'):		'd/communication HRG+VOI+VOI SBx3 MAS+1',
        NP_('skill:language'):		'd/communication VOI+INT+WIL spcl TAI+1',
        NP_('skill:script'):		'd/communication DEX+EYE+INT spcl TAR/TAI+1',

        # crafts & lore
        NP_('skill:agriculture'):	'e/craft STR+STA+WIL SBx2 ULA/ARA+2',
        NP_('skill:alchemy'):		'e/craft SML+INT+AUR SBx1 SKO+3;TAI/MAS+2',
        NP_('skill:animalcraft'):	'e/craft AGL+VOI+WIL SBx1 ULA/ARA+1',
        NP_('skill:astrology'):		'e/craft EYE+INT+AUR SBx1 TAR+1',
        NP_('skill:brewing'):		'e/craft DEX+SML+SML SBx2 SKO+3;TAI/MAS+2',
        NP_('skill:ceramics'):		'e/craft DEX+DEX+EYE SBx2 ULA/ARA+2',
        NP_('skill:cookery'):		'e/craft DEX+SML+SML SBx3 SKO+1',
        NP_('skill:drawing'):		'e/craft DEX+EYE+EYE SBx2 SKO/TAI+1',
        NP_('skill:embalming'):		'e/craft DEX+EYE+SML SBx1 SKO/ULA+1',
        NP_('skill:engineering'):	'e/craft DEX+INT+INT SBx1 ULA/ARA+2;FEN+1',
        NP_('skill:fishing'):		'e/craft DEX+EYE+WIL SBx3 MAS/LAD+2',
        NP_('skill:fletching'):		'e/craft DEX+DEX+EYE SBx1 HIR+2;TAR/NAD+1',
        NP_('skill:folklore'):		'e/craft VOI+INT+INT SBx3 TAI+2',
        NP_('skill:foraging'):		'e/craft DEX+SML+INT SBx3 ULA/ARA+2',
        NP_('skill:glasswork'):		'e/craft DEX+EYE+WIL SBx1 FEN+2',
        NP_('skill:heraldry'):		'e/craft DEX+EYE+WIL SBx1 SKO/TAI+1',
        NP_('skill:herblore'):		'e/craft EYE+SML+INT SBx1 ULA+3;ARA+2',
        NP_('skill:hidework'):		'e/craft DEX+SML+WIL SBx2 ULA/ARA+1',
        NP_('skill:jewelcraft'):	'e/craft DEX+EYE+WIL SBx1 FEN+3;TAR/ARA+1',
        NP_('skill:law'):		'e/craft VOI+INT+WIL SBx1 TAR/TAI+1',
        NP_('skill:lockcraft'):		'e/craft DEX+EYE+WIL SBx1 FEN+1',
        NP_('skill:masonry'):		'e/craft STR+DEX+INT SBx1 ULA/ARA+2',
        NP_('skill:mathematics'):	'e/craft INT+INT+WIL SBx1 TAI+3;TAR/SKO+1',
        NP_('skill:metalcraft'):	'e/craft STR+DEX+WIL SBx1 FEN+3;AHN/ANG+1',
        NP_('skill:milling'):		'e/craft STR+DEX+SML SBx2 ULA+1',
        NP_('skill:mining'):		'e/craft STR+EYE+INT SBx1 ULA/ARA+2;FEN+1',
        NP_('skill:perfumery'):		'e/craft SML+SML+INT SBx1 HIR/SKO/TAR+1',
        NP_('skill:physician'):		'e/craft DEX+EYE+INT SBx1 MAS+2;SKO/TAI+1',
        NP_('skill:piloting'):		'e/craft DEX+EYE+INT SBx1 LAD+3;MAS+1',
        NP_('skill:runecraft'):		'e/craft INT+AUR+AUR SBx1 TAI+2;SKO+1',
        NP_('skill:seamanship'):	'e/craft STR+DEX+AGL SBx2 LAD+3;MAS/SKO+1',
        NP_('skill:shipwright'):	'e/craft STR+DEX+INT SBx1 LAD+3;MAS+1',
        NP_('skill:survival'):		'e/craft STR+DEX+INT SBx3 ULA+2;ARA+1',
        NP_('skill:tarotry'):		'e/craft INT+AUR+WIL SBx1 TAR/TAI+2;SKO/HIR+1',
        NP_('skill:textilecraft'):	'e/craft DEX+DEX+EYE SBx2 ULA/ARA+1',
        NP_('skill:timbercraft'):	'e/craft STR+DEX+AGL SBx2 ULA+3;ARA+1',
        NP_('skill:tracking'):		'e/craft EYE+SML+WIL SBx2 ULA/ARA+3',
        NP_('skill:weaponcraft'):	'e/craft STR+DEX+WIL SBx1 FEN+3;AHN/ANG+1',
        NP_('skill:weatherlore'):	'e/craft INT+EYE+SML SBx3 HIR/TAR/MAS/LAD+1',
        NP_('skill:woodcraft'):		'e/craft DEX+DEX+WIL SBx2 ULA+2;ARA/LAD+1',

        # Special
        NP_('skill:special'):		'z/other INT+AUR+WIL SBx1 NON+1',
    }, xlatekey='skill:')


    def __init__(self, **kwds):
        HarnMaster.__init__(self, **kwds)


    #-------------------------------------------------------------------------#


    class Character(HarnMaster.Character):

        def __init__(self, tid, **kwds):
            HarnMaster.Character.__init__(self, tid, **kwds)


###############################################################################
