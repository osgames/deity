###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameSystem: Common
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import __builtin__
import math
import random
import re
import wx

import deity
import deity.beastiary
import deity.dialogs
import deity.editor
import deity.trandom
import deity.widgets
import deity.world

from deity.watcher import *
from deity.xtype import *
from deity.exceptions import *


###############################################################################


class GameSystem(object):
    kMultiItemRe        = re.compile(r'^(.*)\((.*\|.*)\)(.*)$')
    kModifierRe		= re.compile(r'^[\+\-]?(?:\d+|\d*\.\d+|\d+\.\d*)$')
    kRequirementValRe	= re.compile(r'^(<|<=|==|>=|>)\s*(.*?)$')
    kCombatRoundTime	= ('1', 'min')

    # some dummy values for testing
    kBaseAttributes	= xlist((NP_('attr:str'),NP_('attr:dex'),NP_('attr:cml')), xlate='attr:')
    kSkillInfo		= xdict({
        NP_('skill:dagger'):  'c/combat something',
        NP_('skill:unarmed'): 'c/combat something more',
    }, xlatekey='skill:')

    kSucceedFailChoices = xlist((NP_('result:succeed'), NP_('result:fail')),
                                xlate='result:')

    kCharCategoryPlayersX	= P_('category:players')
    kCharCategoryGeneratedX	= P_('category:generated')
    kCharCategoryEncounterX	= P_('category:encounter')

    kRuleCategories = ['combat']

    # IDs 1-99 are reserved for constant things.  Dynamic IDs start at 100.
    kCharacterGroupsTid = 'Thing-1'
    kCombatGroundTid	= 'Thing-2'
    kAlwaysPromptTid	= 'Char-99'
    kWeaponAspects = xlist(xlate='aspect:')
    kWeaponAspects.extend((NP_('aspect:blunt'), NP_('aspect:edge'), NP_('aspect:point')))

    kCharacterStates = xlist(xlate='status:')
    kCharacterStates.extend((NP_('status:standing'), NP_('status:mounted'), NP_('status:held'), NP_('status:prone'),
                             NP_('status:shock'), NP_('status:unconscious'), NP_('status:dead')))
    kCharacterStatesStringMax = 0
    for s in kCharacterStates:
        l = len(s.xval())
        if l > kCharacterStatesStringMax: kCharacterStatesStringMax = l


    def __init__(self):
        self.name = self.__class__.__name__
        self.rand = deity.trandom.Random()
        ruleslist = deity.DbGet(GDB, '/%s/Rules/Options' % self.name, '')
        self.rules= dict()
        for rule in ruleslist.split(';'):
            if rule == '': continue
            (r,s,v) = rule.partition('=')
            self.rules[r] = v

        self.xlate = 'bdb:'
        self.xpart = self.ModPart

        self.commandForce = False

        # Make instance easily available
        __builtin__.__dict__['GAME'] = self

        # Get list of all supported game options
        self.ruleoptions = dict()
        self.new_options = False
        c = self.__class__
        while c:
            if 'kRuleOptions' in c.__dict__:
                for ruleinfo in c.kRuleOptions:
                    if ruleinfo['rule'] in self.ruleoptions:  # pragma: no cover
                        raise Exception('fatal: rule option "%s" defined in multiple places' % ruleinfo['rule'])
                    self.ruleoptions[ruleinfo['rule']] = ruleinfo
                    if ruleinfo['rule'] not in self.rules:
                        self.new_options = True
            c = c.__base__

        # Get list of all available equipment types
        self.equipTypeDict = xdict(xlatekey=self.xlate)
        genkey = '/Generate/Equip/%s' % self.name.lower()
        if genkey in BDB:
            for t in BDB[genkey].split(';'):
                self.equipTypeDict[P_(self.xlate,t)] = t

        # Get list of all available character types
        self.charTypeDict = xdict(xlatekey=self.xlate)
        genkey = '/Generate/Beast/%s' % self.name.lower()
        if genkey in BDB:
            for t in BDB[genkey].split(';'):
                self.charTypeDict[t] = t

        # Get list of all current characters
        self.charGroupThing = self.Thing(self.kCharacterGroupsTid)
        if not self.charGroupThing.GetInventory():
            group = self.Thing(None)
            group.Rename(self.kCharCategoryPlayersX)
            group.SetValue('x:sort', 'a')
            self.charGroupThing.AddInventory(group)
            group = self.Thing(None)
            group.Rename(self.kCharCategoryGeneratedX)
            group.SetValue('x:sort', 'y')
            self.charGroupThing.AddInventory(group)
            group = self.Thing(None)
            group.Rename(self.kCharCategoryEncounterX)
            group.SetValue('x:sort', 'z')
            self.charGroupThing.AddInventory(group)
        self.gameCharGroups = xlist()
        self.gameCharDisplay = xdict()
        self.gameCharNames = Watched(None)
        self.UpdateGameChars()

        # Create dummy character that always prompts
        self.charPrompter = self.Character(self.kAlwaysPromptTid)
        self.charPrompter.SetPrompt(True)


    def xstr(self, val=None):
        return xstr(val, xlate=self.xlate, xpart=self.xpart)


    def xlist(self, val=None):
        return xlist(val, xlate=self.xlate, xpart=self.xpart)


    def ModPart(self, text, info=None):
        if info is None:
            (t,s,e) = text.partition('(')
            if s == '':
                return (t, None)
            else:
                return (t, s+e)
        elif info[1] is None:
            return text
        else:
            return '%s%s' % (text, info[1])


    def DashPart(self, text, info=None):
        if info is None:
            (t,s,e) = text.partition('-')
            if s == '':
                return (t, None)
            else:
                return (t, s+e)
        elif info[1] is None:
            return text
        else:
            return '%s%s' % (text, info[1])


    def SetForce(self, force):
        self.commandForce = force


    def RuleCheck(self, rule, value=None):
        #print 'RuleCheck:',rule,value,self.rules
        if rule not in self.rules:
            return False
        if value is None:
            return self.rules[rule] == '' or self.rules[rule] == 'yes'
        return value == self.rules[rule]


    def UserRoll(self, char, roll, default=None, msg=None, title=None, test=None):
        #print 'UserRoll:',char,roll,default,test
        if roll is None or roll == '':
            roll = default
        try:
            if isinstance(roll, (str,unicode)):
                # dice roll substitutions
                match = deity.kDieRollRe.match(roll)
                if match:
                    #print 'UserRoll:',match.string,match.start(),match.end(),match.string[match.start():match.end()],roll[match.end():]
                    res = None
                    if char is not None and char.GetPrompt():
                        if msg is None:
                            msg = ''
                        title = '%s: %s (%s)' % (char.Name(), title or P_('title:roll'), roll)
                        res = Input(msg, None, '^[\+\-]?\d+$', title=title, test=test)
                    if res is None:
                        res = str(deity.Roll(match.string[match.start():match.end()], rand=self.rand, test=test)) + roll[match.end():]
                else:
                    res = roll
                # mathematical evaluation
                res = eval(res)
            else:
                res = int(roll)
            return res

        except NameError as e:
            raise BadParameter(_('invalid roll "%(val)s" -- must be number or die roll +/- modifier')
                               % {'val':roll})


    def UserCheck(self, char, roll, condition, point, default=None, msg=None, title=None, test=None):
        #print 'UserCheck:',roll,condition,point,default,test
        if roll is None or roll == '':
            roll = default
        res = None
        try:
            if isinstance(roll, (str,unicode)):
                # dice roll substitutions
                match = deity.kDieRollRe.match(roll)
                if match:
                    if char is not None and char.GetPrompt():
                        if msg is None:
                            msg = ''
                        title = '%s: %s (%s %s %s)' % (char.Name(), title or P_('title:check'), roll, condition, str(point))
                        res = Choose(msg, self.kSucceedFailChoices, title=title, test=test)
                        if res is not None:
                            if res == self.kSucceedFailChoices[0]:
                                return True
                            else:
                                return False
                    if res is None:
                        res = str(deity.Roll(match.string[match.start():match.end()], rand=self.rand, test=test)) + roll[match.end():]
                        res = eval(res + condition + str(point))
            if res is None:
                res = eval(str(roll) + condition + str(point))
            return res

        except NameError as e:  # catch eval failures
            raise BadParameter(_('invalid check "%(val)s" -- must be number or die roll +/- modifier')
                               % {'val':roll})


    def UserSelect(self, char, choices, roll=None, default=None, msg=None, title=None, test=None):
        #print 'UserSelect:',char,roll,type(roll),choices,default,test
        if roll is None or roll == '':
            roll = default
        if roll is None:
            (n,s,l) = choices[-1].partition('=')
            roll = '1d%d' % int(n)
            #print ' - roll:',1,int(n),'=>',roll

        try:
            if isinstance(roll, (str,unicode)):
                # dice roll substitutions
                match = deity.kDieRollRe.match(roll)
                if match:
                    if char is not None and char.GetPrompt():
                        if msg is None:
                            msg = ''
                        title = '%s: %s (%s)' % (char.Name(), title or P_('title:check'), roll)
                        schoices = xlist(xlate=(choices.xlate,None), xsep=' <= ')
                        for c in choices:
                            (n,s,l) = c.partition('=')
                            schoices.append('%s <= %s' % (l,n))
                        res = Select(msg, schoices, title=title, test=test)
                        if res is not None:
                            (res,sep,num) = res.partition(' <= ')
                            return xstr(res, xlate=choices.xlate)
                    roll = str(deity.Roll(match.string[match.start():match.end()], rand=self.rand, test=test)) + roll[match.end():]
                roll = eval(roll)

            roll = int(roll)
            #print ' -',choices
            for i in xrange(len(choices)):
                (n,s,l) = choices[i].partition('=')
                #print ' -',i,choices[i],'->',n,s,l
                if roll <= int(n):
                    #print ' -',l
                    mtest = test
                    if mtest is not None:
                        mtest += '-multi'
                    return xstr(deity.MultipleChoiceResolution(l, rand=self.rand, test=mtest), xlate=choices.xlate)

        except NameError as e:  # catch eval failures
            raise BadParameter(_('invalid check "%(val)s" -- must be number or die roll +/- modifier')
                               % {'val':roll})


    #-------------------------------------------------------------------------#


    def OptionsSelect(self, allowcancel=True, shownew=False):
        dialog = deity.dialogs.ChooseOptionsDialog(self.rules, self.kRuleCategories, self.ruleoptions,
                                                   columns=3, allowcancel=allowcancel, shownew=shownew,
                                                   title=_('%(gamesys)s game options') % {'gamesys':self.name})
        if dialog.Run():
            rulelist = ''
            for k,v in self.rules.iteritems():
                if rulelist:
                    rulelist += ';'
                rulelist += '%s=%s' % (k,v)
            GDB['/%s/Rules/Options' % self.name] = rulelist


    def OptionsFill(self):
        if self.new_options:
            self.OptionsSelect(allowcancel=False, shownew=(not not self.rules))
            self.new_options = False


    #-------------------------------------------------------------------------#


    def GetId(self, tid):
        (type,sep,index) = tid.partition('-')
        if type == 'Thing':
            return self.Thing(tid)
        elif type == 'Item':
            return self.Item(tid)
        elif type == 'Char':
            return self.Character(tid)
        else:
            raise Exception('invalid type "%s" in id "%s"' % (type, tid))


    def UpdateGameChars(self):
        cdispdict = xdict(xsepkey='/', xlatekey=(None,None,'bdb:'))
        cnamedict = xdict()
        cgrouplist= xlist()
        for g in self.charGroupThing.GetInventory():
            gname = g.Name()
            gsort = g.GetValue('x:sort', 'z')
            cgrouplist.append(gname)
            for c in g.GetInventory():
                cname = c.Name()
                chkey = '%s/%s/%s' % (gsort, gname, cname)
                cdispdict[chkey] = c
                cnamedict[cname] = c
                c.SetValue('x:gamegroup',gname)
        self.gameCharGroups = cgrouplist
        self.gameCharDisplay = cdispdict
        self.gameCharNames.Set(cnamedict)


    def AddCharacter(self, char, group):
        #print 'AddCharacter:', char.Name(), group, self.charGroupThing.tid, self.charGroupThing.Name()
        found = None
        for g in self.charGroupThing.GetInventory():
            #print ' -', g.tid, g.Name()
            if g.Name().lower() == group.lower():
                found = g
                break
        if found is None:
            if not self.commandForce:
                raise BadParameter(_('unknown group "%(group)s" for %(name)s (use "force" to create new group)') % {'group':group, 'name':char.Name()})
            found = self.Thing(None)
            found.Rename(group)
            found.SetValue('x:sort', 'g')
            self.charGroupThing.AddInventory(found)
        (oldgroup, loc) = char.GetPossessor()
        if oldgroup is not None:
            (topgroup, loc) = oldgroup.GetPossessor()
            if topgroup is not None and topgroup.tid != self.charGroupThing.tid:  # pragma: no cover
                raise Exception('character "%s" is part of "%s" which is not a group' % (char.Name(), oldgroup.Name()))
            oldgroup.SubInventory(char)
        found.AddInventory(char)
        self.UpdateGameChars()


    def SubCharacter(self, char):
        (group,loc) = char.GetPossessor()
        if group is not None:
            group.SubInventory(char)
            self.UpdateGameChars()


    def MatchCharacterName(self, prefix):
        cname = XLookup(self.gameCharNames.Get().keys(), prefix, typename=N_('character name'))
        return self.gameCharNames.Get()[cname]


    def MatchCharacterGroup(self, prefix):
        gname = XLookup(self.gameCharGroups, prefix, typename=N_('group name'))
        return gname


#    def GetCharacter(self, name, watcher=None, cache=None):
#        key = 'Names/%s/%s' % (self.Character.kGdbPrefix, name)
#        if key not in GDB:
#            return None
#        return self.Character(GDB[key], watcher=watcher, cache=cache)


    #-------------------------------------------------------------------------#


#    def GetItem(self, name, watcher=None, cache=None):
#        key = '%s/%s/Id' % (self.Character.kGdbPrefix, name)
#        if key not in GDB:
#            return None
#        return self.Item(GDB[key], watcher=watcher, cache=cache)


    #-------------------------------------------------------------------------#


    def _HitRangeLocation(self, char, range, attacker=None):
        #print 'HitRangeLocation:',char,range,attacker
        lkey = 'Hit'
        if range is not None and range != '':
            lkey = '%s-%s'%(lkey,range)
        else:
            range = xstr('full', xlate='bdbloc:')
        locs = char.GetTypeInfo(lkey, aslist=True)
        if locs is None:
            raise BadParameter(_('character %(name)s has no hit-range "%(range)s"') %
                               {'name':char.Name(), 'range':range.xval()})
        locs = xlist(locs, xlate='bdbloc:')
        return self.UserSelect(attacker, locs, test='rangeloc')


    def HitLocation(self, char, locx, range_return=None, attacker=None, validate=False):
        #print 'HitLocation:',char,locx,range
        if locx is None or locx == '':
            if validate: return True
            return self._HitRangeLocation(char, None, attacker)
        hitlocs = char.GetTypeInfo('Locations', aslist=True, keysonly=True)
        hitranges = char.GetTypeInfo('Hitranges', aslist=True, keysonly=True)
        #print ' - hitlocs:',hitlocs
        #print ' - hitranges:',hitranges
        if hitranges:
            hitlocs.extend(hitranges)
        loc = XLookup(hitlocs, locx, N_('hit-location'))
        if hitranges:
            for r in hitranges:
                if r == loc:
                    if range_return is not None:
                        range_return.append(loc)
                    if validate: return True
                    return self._HitRangeLocation(char, loc, attacker)
        if validate:
            return bool(loc)
        return loc


    def HitFromLocation(self, char, locx, loclist=None, typename='', defindex=None):
        if not loclist:
            loclist = self.xlist()
            wielded = char.AttackFromLocations(order=loclist)

        try:
            #print 'HitFromLocation: xlookup:',locx,loclist,typename,defindex
            return XLookup(loclist, locx, typename=typename, defindex=defindex)

        # lookup failed: search for location without left/right front/back
        except BadParameter:
            for o in loclist:
                if self.BaseLocation(o).xval() == locx:
                    return o
            raise


    def BaseLocation(self, loc, prefixptr=None):
        prefix = ''
        newloc = str(loc)
        while True:
            oldloc = newloc
            if newloc.find('left')  == 0:
                prefix += 'left'
                newloc = newloc[4:]
            if newloc.find('right') == 0:
                prefix += 'right'
                newloc = newloc[5:]
            if newloc.find('front') == 0:
                prefix += 'front'
                newloc = newloc[5:]
            if newloc.find('back')  == 0:
                prefix += 'back'
                newloc = newloc[4:]
            if newloc.find('fore') == 0:
                prefix += 'fore'
                newloc = newloc[4:]
            if newloc.find('hind')  == 0:
                prefix += 'hind'
                newloc = newloc[4:]
            if newloc == oldloc: break
        if prefixptr is not None: prefixptr.append(prefix)
        if isinstance(loc, xstr):
            newloc = xstr(newloc, xlate=loc.xlate, xpart=loc.xpart)
        return newloc


    def WoundInfo(self, location, damage=0):
        location.xval() # assert translateable
        loc = location
        if loc not in self.kLocationInjuryWoundMap:
            loc = self.BaseLocation(loc)
        if loc not in self.kLocationInjuryWoundMap:
            loc = ''
        wound = deity.ListKeyLookup(self.kLocationInjuryWoundMap[loc], damage)
        if wound is None:
            raise BadParameter(_('damage of %(damage)d to %(location)s is not possible') %
                               {'damage':damage, 'location':location.xval()})
        wound = wound.strip()
        flags = self.kLocationInjuryFlagsMap[loc]
        return (wound, flags)


    def WoundDescription(self, wound, char):
        return wound


    def Update(self, char, curtime):
        pass


    #=========================================================================#


    def GetCombatSummary(self, char):
        return P_('i:', char.GetStatus())


    def CombatNewRound(self):
        pass


    def CombatNewTurn(self, char):
        cname = char.Name()
        busy = char.GetInfo('busy')
        if busy is not None:
            if busy == '':  # just until end of round
                char.SetInfo('busy', None)
                Output(_('%(name)s is no longer busy and can act normally.') % {'name':cname}, '@blue')
            else:
                busy = WORLD.TimeImport(busy)
                rtime = WORLD.TimeGetCurrent()
                if WORLD.TimeCompare(busy, rtime) <= 0:
                    char.SetInfo('busy', None)
                    Output(_('%(name)s is no longer busy and can act normally.') % {'name':cname}, '@blue')
                else:
                    WORLD.TimeAdjust(''.join(self.kCombatRoundTime), rtime)
                    if WORLD.TimeCompare(busy, rtime) < 0:
                        char.SetInfo('busy', '')
                        Output(_('%(name)s is busy but can act at end of round.') % {'name':cname}, '@blue')
                    else:
                        Output(_('%(name)s is busy; acting now will interrupt.') % {'name':cname}, '@blue')


    def CombatNewAction(self, char):
        self.turnActions[char] = self.turnActions.Get(char, 0) + 1


    #=========================================================================#


    class ValidateModifier(wx.PyValidator):

        def Validate(self, parent):
            value = parent.GetValue()
            if GameSystem.kModifierRe.match(value):
                return True
            else:
                return False


    #=========================================================================#


    class Thing(object):
        kGdbPrefix      = 'Thing'
        kListSeparator  = "\x1C" # ASCII FS (field seperator)
        kGenerateKeyRe  = re.compile(r'\b(([A-Z]):([A-Za-z0-9]+))\b', re.I)
        kGenerateFuncRe = re.compile(r'@([a-z][\w-]+)\(([^\(\)]*)\)', re.I)
        kGenerateIExpRe	= re.compile(r'\[\s*([0-9\.]+)\s*/\s*([0-9\.]+)\s*\]')
        kEvaluateRe	= re.compile(r'^[\s\d\.\*\/\+\-\(\)]+$')
        kConditionOpRe  = re.compile(r'(==|!=|<|<=|>=|>)')
        kGenerateFuncs  = dict()

        kInventoryKey   = 'l:inventory'
        kSkillSetKey	= 'x:dbitems-s'
        kNameStringMax  = 15
        kTypeStringMax  = deity.beastiary.kTypeStringMax


        def __init__(self, tid, watcher=None, cache=None, tempobj=False):
            if tid is not None and not isinstance(tid, str):
                raise Exception('created Thing with non-str TID "%s" (%s)' % (str(tid), str(type(tid))))
            self.tid = tid
            self.cache = cache
            self.watcher = watcher
            self.rand = deity.trandom.Random()
            self.temp = tempobj
            self.dbitems  = dict()

            self.xlate = GAME.xlate
            self.xpart = GAME.xpart

            if self.temp and not self.cache is not None:
                self.cache = dict()

            if tid is not None:
                if tid.find(self.kGdbPrefix) != 0:  # pragma: no cover
                    raise Exception('tid "%s" not loaded as type "%s"' % (tid,self.kGdbPrefix))


        def __hash__(self):
            return hash(self.tid or self.MakeUnique())


        def __eq__(self, rhs):
            if not isinstance(rhs, GameSystem.Thing): return False
            return self.tid == rhs.tid


        def __ne__(self, rhs):
            return not self.__eq__(rhs)


        def __repr__(self):
            return '<%s>' % self.Name()


        def xstr(self, val=None):
            return xstr(val, xlate=self.xlate, xpart=self.xpart)


        def xlist(self):
            return xlist(xlate=self.xlate, xpart=self.xpart)


        def _GdbInfoKey(self, info):
            return '/%s/_%s/%s' % (self.kGdbPrefix, self.__class__.__name__, info)


        def _GdbKey(self, item):
            if self.tid is None or item is None:  # pragma: no cover
                raise Exception('can\'t access database with None objects')
            #return '/%s/%s/%s' % (self.kGdbPrefix, self.tid, item)
            return '/%s/%s' % (self.tid, item)


        def MakeUnique(self, uid=None):
            if self.temp or self.tid is not None:
                return # already unique
            oldkey = self._GdbInfoKey('lastid')  # FIXME: remove this
            key = self._GdbInfoKey('nextid')
            if key in GDB:
                dbuid = int(GDB[key])
            elif oldkey in GDB:  # pragma: no cover
                dbuid = int(GDB[oldkey])
                del GDB[oldkey]
            else:
                dbuid = 100
            if uid is None:
                uid = dbuid
            elif uid < dbuid:  # pragma: no cover
                raise Exception('trying to make unique id #%d that is less than next id #%d' % (uid, dbuid))
            self.tid = '%s-%d' % (self.kGdbPrefix, uid)
            #print "MakeUnique:", self.Name(), "=>", self.tid
            uid += 1
            GDB[key] = '%d' % uid
            return self.tid


        def Delete(self):
            (thing,loc) = self.GetPossessor()
            if thing is not None:  # pragma: no cover
                raise Exception('cannot delete item possessed by another')
            # FIXME: deallocate from database


        def WatchedBy(self, watcher):
            self.watcher = watcher
            if watcher.Get() != self:
                watcher.Set(self)


        def Modified(self, *items):
            if self.watcher is not None:
                self.watcher.Alter()


        def Name(self):
            '''The string by which this item is referred to by the user.'''
            #print 'Name:', self.tid, self.GetValue('i:name'), self.GetValue('x:type')
            if self.tid is None and not self.temp:
                return ''
            name = self.GetValue('i:name')
            if name is None:
                name = self.GetValue('x:type')
            if name is not None:
                (n, m) = GAME.ModPart(name)
                n = self.xstr(n)
                return n.xval()
            return _('unknown(%s)'%self.tid)


        def ShortDesc(self):
            '''A brief description of what this item is.'''
            name = self.Name()
            type = self.GetValue('x:type').xval()
            (tnam, tinf)= GAME.ModPart(type)
            if tinf:
                xinf = list()
                for m in tinf[1:-1].split(';'):
                    k,e,v = m.partition('=')
                    if e == '=':
                        xinf.append('%s=%s' % (P_('attr:',k),v))
                    else:
                        xinf.append(m)
                tinf = tinf[0] + ';'.join(xinf) + tinf[-1]
            if tnam and tnam[0] == '_':
                tnam = tnam[1:]
            if tnam != name:
                if tinf:
                    tinf = tinf[0] + tnam + ':' + tinf[1:]
                else:
                    tinf = '[%s]' % tnam
            else:
                if not tinf:
                    tinf = ''
            return name + tinf


        def GetDisplayInfo(self):
            return {}


        def Rename(self, newname):
            #key = 'Names/%s/%s' % (self.kGdbPrefix, newname.lower())
            #if key in GDB:
            #    tid = GDB[key]
            #    if tid != self.tid:
            #        raise NotPossible(_('name "%(name)s" already exists') % {'name':newname})
            #GDB[key] = self.tid
            #oldname = self.Name()
            self.SetValue('i:name', newname)
            #print 'Rename:',self.tid,oldname,'->',newname


        #---------------------------------------------------------------------#


        def GetBdbInfo(self, type, record, default=None, aslist=False, asdict=False, keysonly=False):
            info = deity.beastiary.DbGet(BDB, self.kBdbPrefix, type, record, sections=(GAME.name,'all'))
            #print 'GetBdbInfo:',type,record,'->',info
            if info is None: return default

            if not aslist and not asdict and not keysonly: return info

            if info == '':
                info = list()
            else:
                info = info.split(deity.beastiary.kListSep)
            if not asdict and not keysonly:
                return info

            keys = xlist(xlate=self.xlate, xpart=self.xpart)
            vals = xdict(xlatekey=self.xlate, xpartkey=self.xpart, xlateval=self.xlate, xpartval=self.xpart)
            for i in info:
                if i == '': continue
                (k,s,v) = i.partition('=')
                if asdict: vals[k] = v
                else:      keys.append(k)
            if asdict:
                return vals
            elif aslist:
                return keys
            else:
                raise ValueError('unreachable') # pragma: no cover # return ';'.join(keys)


        #---------------------------------------------------------------------#


        def IsA(self, typename):
	    #print "IsA:",self.Name(),"of",typename,":",self.GetValue('x:types')
            types = self.GetValue('x:types').split(';')
            if typename.lower() in types:
                return True
            return False


        def GetTypeInfo(self, info, default=None, aslist=False, asdict=False, keysonly=False):
            #print "GetTypeInfo",info,"for",self.Name(),
            types = self.GetValue('x:types').split(';')
            defret = default
            for t in types:
                result = self.GetBdbInfo(t, info, None, aslist, asdict, keysonly)
                #print "->",t,result,
                if result:
                    #print "=>",result
                    return result
            if result is not None:
                #print "=> (empty)",result
                return result
            #print "=> (default)",default
            return default


        #---------------------------------------------------------------------#


        def SetValue(self, item, val, type=None, values=None):
            #print "SetValue:", self.Name(), self.tid, ":", item, "<=", val
            # add type prefix if not already present
            if type is not None:
                if len(item) < 2 or item[1] != ':':
                    item = type + item
                elif item[0] != type[0]:
                    raise KeyError('key "%s" has type different that expected "%s"' % (item,type))
            # must be categorized by type
            if len(item) < 2 or item[1] != ':':
                raise KeyError('key "%s" has no type/category prefix' % item)
            if len(item) < 3:
                raise KeyError('key "%s" is only type/category prefix' % item)
            if val is None:
                key = self._GdbKey(item)
                if key in GDB:
                    del GDB[key]
                    self.Modified(item)
                return
            # value must be a string at this point (or writes to real db will fail outside of unit-tests)
            if not isinstance(val, (str, unicode)):  # pragma: no cover
                raise Exception('value "%s" is not a string for value item "%s"' % (str(val), item))
            # can't write to dummy character
            if self.tid is None:
                self.MakeUnique()
            # keep lists of all types except X:
            if item[0] != 'x':
                category = item[0]
                dbitem   = item[2:]
                if category not in self.dbitems or dbitem not in self.dbitems[category]:
                    self.dbitems[category] = dict()
                    for i in self.GetValue('x:dbitems-%s' % category, '').split(';'):
                        if not i: continue
                        self.dbitems[category][i] = None
                    self.dbitems[category][dbitem] = None
                    self.SetValue('x:dbitems-%s' % category, ';'.join(self.dbitems[category].keys()))
            # update user values
            if values is not None:
                values[item] = val
            # update internal cache
            if self.cache is not None:
                self.cache[item] = val
            # update real database (if not temp object)
            if self.tid:
                #print 'SetValue:',item,self._GdbKey(item),'->',val
                GDB[self._GdbKey(item)] = val
            # mark as changed
            self.Modified(item)


        def GetValue(self, item, default=None, type=None, values=None):
            #print 'GetValue:',self.tid,item,default,type,values
            # add type prefix if not already present
            if type is not None:
                if len(item) < 2 or item[1] != ':':
                    item = type + item
                elif item[0] != type[0]:
                    raise KeyError, 'key "%s" has type different that expected "%s"' % (item,type)
            # must be categorized by type
            if len(item) < 2 or item[1] != ':':
                raise KeyError('key "%s" has no type/category prefix' % item)
            if len(item) < 3:
                raise KeyError('key "%s" is only type/category prefix' % item)
            # see if we're provided with one
            if values is not None and item in values:
                return values[item]
            # see if it's in our local cache
            if self.cache is not None and item in self.cache:
                return self.cache[item]
            # unsaved character always returns default
            if self.tid is None:
                #print 'GetValue:',default,'(tid is None)'
                return default
            # return default if doesn't exist in real database
            key = self._GdbKey(item)
            if key not in GDB:
                #print 'GetValue:',default,'(no "%s" in db)'%key
                if default is None: return default
                return xstr(default, xlate=self.xlate, xpart=self.xpart)
            # get the value from the database and copy it to local cache
            val = GDB[key]
            #print 'GetValue:',item,self._GdbKey(item),'->',val
            if self.cache is not None:
                self.cache[item] = val
            # give it back
            return xstr(val, xlate=self.xlate, xpart=self.xpart)


        def SetValueList(self, key, vals, type=None, values=None):
            val = self.kListSeparator.join(vals)
            return self.SetValue(key, val, type=type, values=values)


        def GetValueList(self, key, default=None, type=None, values=None):
            val = self.GetValue(key, type=type, values=values)
            vals = xlist(xlate=self.xlate, xpart=self.xpart)
            if val is None:
                if default is None:
                    return default
                vals.extend(default)
                return vals
            if val == '': return vals
            vals.extend(val.split(self.kListSeparator))
            return vals


        def SetValueMap(self, key, vals, type=None, values=None):
            l = list()
            for k,v in vals.iteritems():
                l.append('%s=%s' % (k,v))
            val = self.kListSeparator.join(l)
            return self.SetValue(key, val, type=type, values=values)


        def GetValueMap(self, key, default=None, type=None, values=None):
            val = self.GetValue(key, type=type, values=values)
            if val is None: return default
            vals = xdict(xlatekey=self.xlate, xpartkey=self.xpart, xlateval=self.xlate, xpartval=self.xpart)
            if val == '': return vals
            for i in val.split(self.kListSeparator):
                (k,s,v) = i.partition('=')
                vals[k] = v
            return vals


        #---------------------------------------------------------------------#


        def SetAttribute(self, attr, val, values=None):
            return self.SetValue(attr, str(val), 'a:', values)


        def GetAttribute(self, attr, default=None, values=None):
            val = self.GetValue(attr, None, 'a:', values)
            #print "GetAttribute:", self.Name(), self.tid, ":", attr, "=>", val
            if val is None:
                return default
            try:
                return int(float(val))
            except ValueError:
                return default


        def GetAttributes(self, order=None, values=None):
            attrs = dict()
            for a in GAME.kBaseAttributes+GAME.kMoreAttributes:
                attrs[a] = self.GetAttribute(a, values=values)
                if order is not None: order.append(a)
            return attrs


        #---------------------------------------------------------------------#


        def SetInfo(self, info, val, values=None):
            return self.SetValue(info, val, 'i:', values)


        def GetInfo(self, info, default=None, values=None):
            return self.GetValue(info, default, 'i:', values)

        def GetInfos(self, order=None, values=None):
            infos = dict()
            for i in GAME.kGeneralInfos:
                infos[i] = self.GetInfo(i, '- - -')
                if infos[i].isalpha():
                    infos[i] = P_('bdb:',infos[i])
                if order is not None: order.append(i)
            if GAME.RuleCheck('ImperialMeasurements'):
                if 'height' in infos:
                    inches = int(float(infos['height']) * deity.kInchesPerCm + 0.5)
                    feet   = inches // deity.kInchesPerFoot
                    inches = inches - int(deity.kInchesPerFoot * feet)
                    infos['height'] = '%0d\'%0d"' % (feet, inches)
                if 'weight' in infos:
                    pounds = float(infos['weight']) * deity.kLbsPerKg
                    infos['weight'] = '%0.1f lbs' % pounds
            else:
                if 'height' in infos and infos['height'][0].isdigit():
                    infos['height'] = '%0d cm' % int(float(infos['height']))
                if 'weight' in infos and infos['weight'][0].isdigit():
                    infos['weight'] = '%0.1f kg' % float(infos['weight'])
            return infos


        def GetStatus(self):
            return self.GetInfo('status', default='standing')


        def SetStatus(self, newstatus):
            if newstatus not in GAME.kCharacterStates:
                raise Exception('new status "%s" for %s in not valid' % (newstatus, self.Name()))
            curstatus = self.GetStatus()
            if curstatus == 'mounted' and newstatus != 'mounted':
                mount = self.GetMount()
                if mount is not None:
                    raise Exception('status for %s changing from "%s" to "%s" but still mounted on %s'
                                    % (self.Name(), curstatus, newstatus, mount.Name()))
            return self.SetInfo('status', newstatus)


        #---------------------------------------------------------------------#


        def AddInventory(self, item):
            pos,loc = item.GetPossessor()
            if pos is not None and (pos.tid != self.tid or loc == ''):
                if loc == '=':
                    msg = _('already worn by %(name)s') % {'name':pos.Name()}
                elif loc != '':
                    msg = _('already wielded by %(name)s in %(loc)s') % {'name':pos.Name(), 'loc':loc.xval()}
                else:
                    msg = _('already possessed by %(name)s') % {'name':pos.Name()}
                raise NotPossible(_('%(name)s cannot add %(item)s to inventory -- %(reason)s') %
                                  {'name':self.Name(), 'item':item.Name(), 'reason':msg})
            inv = self.GetValueList(self.kInventoryKey, [])
            inv.append(item.tid)
            self.SetValueList(self.kInventoryKey, inv)
            item.SetPossessor(self)


        def SubInventory(self, item):
            itid  = item.tid
            inv   = self.GetValueList(self.kInventoryKey, [])
            count = 0
            place = None
            for i,v in enumerate(inv):
                if v == itid:
                    count += 1
                    place  = i
            if place is None:
                raise NotPossible(_('item "%(item)s" not in inventory') % {'item':item.Name()})
            del inv[place]
            if count != 0:
                pass
            self.SetValueList(self.kInventoryKey, inv)
            item.SetPossessor(None)


        def GetInventory(self):
            inv = self.GetValueList(self.kInventoryKey, [])
            items = list()
            for i in inv:
                item = GAME.GetId(i)
                items.append(item)
            return items


        def SearchInventory(self, namex):
            namex.xval() # verify input
            inv = self.GetValueList(self.kInventoryKey, [])
            items = xdict(xlatekey=self.xlate, xpartkey=self.xpart)
            for i in inv:
                item = GAME.GetId(i)
                items[item.Name()] = item
            match = XLookup(items.keys(), namex, N_('item'), multiple=False)
            if not match:
                raise BadParameter(_('"%(item)s" not in inventory') % {'item':namex.xval()})
            return items[match]


        #---------------------------------------------------------------------#


        def SetPossessor(self, thing, location=''):
            if thing is None:
                self.SetValue('x:possessor', '')
            else:
                self.SetValue('x:possessor', '%s/%s/%s' % (thing.kGdbPrefix, thing.tid, location))


        def GetPossessor(self):
            possessor = self.GetValue('x:possessor')
            if not possessor:
                return (None, None)
            (gtype, tid, loc) = possessor.split('/')
            thing = GAME.GetId(tid)
            return (thing, xstr(loc, xlate='bdbloc:'))


        def CheckPossessor(self, thing):
            possessor = self.GetValue('x:possessor')
            if possessor is None:
                return None
            (gtype, tid, loc) = possessor.split('/')
            if gtype == thing.kGdbPrefix and tid == thing.tid:
                return xstr(loc, xlate='bdbloc:')
            return None


        #---------------------------------------------------------------------#


        def _GenerateInvExp(self, begin, end, div):
            rng = int(end) - int(begin)
            div = float(div)
            if div < 1.01:
                raise DatafillError(_('divisor %(div)0.2f must be >= %(amt)s') %
                                    {'div':div, 'amt':'1.01'})
            ldiv = math.log(div)
            rrng = math.exp((rng+1)*ldiv) - 0.001
            rand = self.rand.random() * (rrng-1.0) + 1.0
            rval = rng - int(math.log(rand)/ldiv) + int(begin)
            #print "GenerateInvExp:",rng,div,"->",ldiv,rrng,rand,rval
            return str(rval)

        kGenerateFuncs['inv-exp-random'] = _GenerateInvExp


        def _GenerateRandom(self, roll, *opts):
            usemsg = '@Random(roll, n=val, n=val [,...])'
            if len(opts) < 2:
                raise DatafillError(_('too few options -- use %(stmt)s'), {'stmt':usemsg})
            roll = int(self._GenerateExpression(roll)) # deity.Roll(roll)
            for o in opts:
                sep = o.find('=')
                if sep < 0 or not o[0].isdigit():
                    raise DatafillError(_('incorrect option "%(opt)s" -- use %(stmt)s')
                                        % {'opt':o, 'stmt':usemsg})
                num = int(o[:sep])
                if num >= roll:
                    return o[sep+1:]
            raise DatafillError(_('no match for random value %(value)d -- use %(stmt)s')
                                % {'value':roll,'stmt':usemsg})

        kGenerateFuncs['random'] = _GenerateRandom


        def _GenerateNormalDistribution(self, limit, sigma=None):
            usemsg = '@NormDist(limit, [sigma])'
            try:
                if limit.isdigit():
                    limit = int(limit)
                else:
                    limit = float(limit)
                if sigma is not None:
                    sigma = float(sigma)
                else:
                    sigma = limit * 0.341
            except:
                raise DatafillError(_('invalid options -- use %(stmt)s') % {'stmt':usemsg})
            dist = normval = random.normalvariate(0.0, sigma)
            if dist >= 0:
                if isinstance(limit, int):
                    dist = int(dist)
                if dist > limit:
                    dist = limit
            else:
                if isinstance(limit, int):
                    dist = -int(-dist)
                if dist < -limit:
                    dist = -limit
            #print "GenerateNormalDistribution(%s, %0.1f):" % (limit, sigma), normval, dist
            return str(dist)

        kGenerateFuncs['normdist'] = _GenerateNormalDistribution


        def _GenerateValueDistribution(self, center, minpercent, maxpercent=None, sigma=None):
            usemsg = '@ValueDist(center, min-percent, [max-percent], [sigma])'
            try:
                center = int(center)
                minpercent = int(minpercent)
                if maxpercent is not None: maxpercent = int(maxpercent)
            except:
                raise DatafillError(_('invalid options -- use %(stmt)s'), {'stmt':usemsg})
            if maxpercent is None:
                maxpercent = minpercent
            ndist = float(self._GenerateNormalDistribution('1.0', sigma))
            if ndist < 0:
                ndist = -int(-ndist * center * minpercent / 100.0 + 0.5)
            else:
                ndist =  int( ndist * center * minpercent / 100.0 + 0.5)
            #print "GenerateValueDistribution(%s, %s, %s):" % (center, percent, sigma), limit, ndist
            return '(%d + %d)' % (center, ndist)

        kGenerateFuncs['valuedist'] = _GenerateValueDistribution


        def _GenerateIf(self, *optlist):
            usemsg = '@If(cond, trueval, [elsecond, trueval, [...]], falseval)'
            opts = list()
            opts.extend(optlist)
            opts.reverse()
            while opts:
                o = opts.pop()
                # if we don't have a condition, it must be a final "else"
                if not self.kConditionOpRe.search(o):
                    if opts:
                        raise DatafillError(_('multiple "falseval" values -- use %(stmt)s'), {'stmt':usemsg})
                    return o
                # evaluate condition
                if not opts:
                    raise DatafillError(_('no value after condition "%(cond)s" -- use %(stmt)s'), {'cond':o, 'stmt':usemsg})
                v = opts.pop();
                if eval(o):
                    return v
            raise DatafillError(_('missing "falseval" at end -- use %(stmt)s'), {'stmt':usemsg})

        kGenerateFuncs['if'] = _GenerateIf


        #---------------------------------------------------------------------#


        def _GenerateInitialValue(self, key):
            if key == 'a:testhavebase': return 0
            return None


        def _GenerateDieRollRe(self, match):
            #print 'GenerateDieRoll:',match.string[match.start():match.end()]
            roll = deity.Roll(match.string[match.start():match.end()])
            return str(roll)


        def _GenerateBestDieRollRe(self, match):
            #print 'GenerateBestDieRoll:', match.groups()
            dice = list()
            deity.Roll(count=int(match.group(2)), die=int(match.group(3)), dice=dice)
            dice.sort(reverse=True)
            return str(sum(dice[:int(match.group(1))]))


        def _GenerateWorstDieRollRe(self, match):
            #print 'GenerateWorstDieRoll:', match.groups()
            dice = list()
            deity.Roll(count=int(match.group(2)), die=int(match.group(3)), dice=dice)
            dice.sort()
            return str(sum(dice[:int(match.group(1))]))


        def _GenerateInvExpRe(self, match):
            return self._GenerateInvExp(0, int(match.group(1))+1, float(match.group(2)))


        def _GenerateFunc(self, match):
            #print '_GenerateFunc:',match.groups()
            name = match.group(1)
            ostr = match.group(2)
            opts = list()
            if ostr.strip() != '':
                for o in ostr.split(','):
                    opts.append(o.strip())
            if name not in self.kGenerateFuncs:
                #print self.kGenerateFuncs
                raise DatafillError(_('no such function'))
            #print 'GenerateFunc:',name,ostr,opts
            val = self.kGenerateFuncs[name](self, *opts)
            # add parenthesis, if necessary
            if val[0].isdigit() and not val.isdigit():
                val = '(%s)' % val
            return val


        def _SubstituteExpression(self, expr, key=None):
            #print 'GameSystem::SubstituteExpression:',expr,key
            # dice roll substitutions
            expr = deity.kBestDieRollRe.sub(self._GenerateBestDieRollRe, expr)
            expr = deity.kWorstDieRollRe.sub(self._GenerateWorstDieRollRe, expr)
            expr = deity.kDieRollRe.sub(self._GenerateDieRollRe, expr)
            # other standard notations
            expr = self.kGenerateIExpRe.sub(self._GenerateInvExpRe, expr)
            return expr


        def _EvalExpression(self, expr, key=None):
            return str(eval(expr))


        def _GenerateExpression(self, expr, key=None):
            #print 'GameSystem::GenerateExpression:',expr,key
            expr = self._SubstituteExpression(expr, key)
            return self._EvalExpression(expr, key)


        def _GenerateItem(self, itemtype, key, val, allopts=False):
            # dependent upon user code
            try:
                expr = val

                # if this is a requirement, extract specific information
                rcompare = None
                if key[0] == 'r':
                    match = GameSystem.kRequirementValRe.match(val)
                    if not match:  # pragma: no cover
                        raise DatafillError(_('invalid "requirement" definition for %(type)s') % {'type':itemtype})
                    rcompare = match.group(1)
                    val = match.group(2)

                #print 'GenerateItems:',key,'->',val
                # first, do any substitutions
                def ValSub(match):
                    key = match.group(1)
                    val = self.GetValue(key)
                    if val is None:  # pragma: no cover
                        raise DatafillError(_('incomplete generator info for %(type)s -- %(key)s not found for "%(expr)s"') %
                                            {'type':itemtype, 'key':key, 'expr':expr})
                    return val
                val = self.kGenerateKeyRe.sub(ValSub, val)
                #print '- GenerateKeyRe ->',val
                cur = self.GetValue(key)
                #print '- CurrentValue ->',cur
                # second, do any function evaluations (only on non-literals)
                if val[0] != '"' and val[0] != "'":
                    while self.kGenerateFuncRe.search(val):
                        val = self.kGenerateFuncRe.sub(self._GenerateFunc, val)
                        #print '- GenerateFuncRe ->',val
                # third, reset to initial value if so requested
                if val[0:4] == 'base':
                    cur = self._GenerateInitialValue(key)
                    if cur is None:  # pragma: no cover
                        raise DatafillError(_('no "base" value for "%(key)s" of %(type)s for modifier "%(expr)s"') %
                                            {'type':itemtype, 'key':key, 'expr':expr})
                    val = val[4:].strip()
                    cur = str(cur)
                    #print '- GenerateInitialValue ->',cur,"with",val
                # fourth, do any simple processing
                val = self._SubstituteExpression(val, key)
                #print '- Substitutions ->',val
                evalable = self.kEvaluateRe.match(val)
                #print '- Evaluate-able "%s" ->'%val,evalable
                if evalable and (val[0] == '+' or val[0] == '*'):
                    # add this expression to existing value
                    op  = val[0]
                    val = self._EvalExpression(val[1:], key)
                    #print '- GenerateAdjustment ->',cur,op,val
                    if cur is None:
                        cur = self._GenerateInitialValue(key)
                        if cur is not None:
                            cur = str(cur)
                        #print '- GenerateInitialValue ->',cur,"from",self.__class__
                    if cur is None or (cur != '' and not cur[0].isdigit()):
                        #print '- Future Adjustment ->',op,val
                        # future adjustment
                        if op == '*':  # pragma: no cover
                            raise DatafillError(_('cannot use multiply operator for "%(key)s" of %(type)s with no base value') %
                                                {'type':itemtype, 'key':key, 'expr':expr})
                        if cur is None:
                            val = op + val
                        else:
                            val = op + str(eval(val + cur))
                    elif op == '+':
                        val = float(cur) + float(val)
                    elif op == '*':
                        val = float(cur) * float(val)
                    val = str(val)
                    #print '=>',val
                elif evalable:
                    # generate value based on both expression
                    val = self._EvalExpression(val, key)
                    #print '- EvalExpression ->',val
                elif cur is not None and ',' in val and ',' not in cur:
                    raise DatafillError(_('modifier value "%(expr)s" for "%(key)s" of %(type)s is multi-value but current (%(cur)s) is not') %
                                        {'type':itemtype, 'key':key, 'expr':expr, 'cur':cur})
                elif val.find('|') >= 0:
                    # choice of items
                    opts = val.split('|')
                    if allopts:
                        for o in opts:
                            o = o.strip()
                            if not o: continue
                            self._GenerateItem(itemtype, key, o)
                        return
                    else:
                        val = opts[self.rand.randint(0,len(opts)-1)].strip()
                else:
                    # direct copy/append
                    if (val[0] == '"' and val[-1] == '"') or (val[0] =="'" and val[-1] == "'"):
                        # extract quoted value
                        val = val[1:-1]
                #print '- final',key,'->',val
                # items can have a base type
                pos = key.rfind('/')
                if pos > 1:
                    base = key[pos+1:]
                    key  = key[0:pos]
                    bkey = 'x:base-%s' % key
                    self.SetValue(bkey, base)
                # store it back
                if key[0] == 'l':
                    if val == '': return
                    if cur is not None:
                        val = cur + self.kListSeparator + val

            # change errors in to something meaningful
            except DeityException as e:
                raise DatafillError(_('invalid generator expression "%(expr)s" for %(type)s "%(key)s" -- %(text)s')
                                    % {'expr':expr, 'type':itemtype, 'key':key, 'text':str(e)})

            # save it!
            self.SetValue(key, val)

            # if it's a requirement, adjust for actual user
            if key[0] == 'r':
                akey = 'a' + key[1:]
                if self.generateFor is not None:
                    userval = self.generateFor.GetValue(akey)
                    if userval:
                        if not eval("%s %s %s" % (userval, rcompare, val)):
                            raise NotPossible(_('character "%(char)s" does not meet "%(attr)s" requirement of %(val)s (was %(has)s)')
                                              % {'char':self.generateFor.Name(), 'attr':akey, 'val':val, 'has':userval})
                        val = userval
                self.SetValue(key, "%s %s" % (rcompare, val))
                self.SetValue(akey, val)


        def _GenerateItemInfo(self, type, allopts=False):
            for key in self.genitemorder:
                #print 'GenerateItemInfo: key:',key,'type:',type,'class:',self.__class__
                for val in self.geniteminfo[key]:
                    #print ' - next item info:',val
                    self._GenerateItem(type, key, val, allopts=allopts)


        def _GenerateItemOrder(self, type, rule):
            if not rule: return
            (key,sep,val) = rule.partition('=')
            if sep != '=':  # pragma: no cover
                raise DatafillError(_('invalid generator rule for %(type)s -- "%(rule)s" not in "key=val" form') %
                                    {'type':type, 'rule':rule})
            if len(key) < 3 or key[1] != ':' or key[0].lower() == 'x':  # pragma: no cover
                raise DatafillError(_('invalid generator key for %(type)s -- "%(key)s" not in "X:name" form') %
                                    {'type':type, 'key':key})
            if key not in self.geniteminfo:
                self.geniteminfo[key] = list()
                self.genitemorder.append(key)
            if val[0].isalnum() and key[0] != 'l':
                self.geniteminfo[key] = list()
            self.geniteminfo[key].append(val)


        def _GenerateItems(self, type, gs):
            genrec = deity.DbGet(BDB, '/%s/%s/%s/%s'%(self.kBdbPrefix, type, 'Generate', gs.lower()), default='')
            #print 'GenerateItems:',type,gs,genrec
            # process each record in turn
            for g in genrec.split(deity.beastiary.kListSep):
                self._GenerateItemOrder(type, g)


        def _DoGenerate(self, type):
            #print 'GameSystem::_DoGenerate:', type
            # do any parent types first
            isa = self.GetBdbInfo(type, 'ISA', default='')
            for t in isa.split(';'):
                if not t: continue
                self._DoGenerate(t)
            # update type list
            types = self.GetValue('x:types', '')
            if types != '':
                types = '%s;%s' % (type, types)
            else:
                types = type
            self.SetValue('x:types', types)
            # do game-specific and game-independent item generation
            self._GenerateItems(type, 'all')
            self._GenerateItems(type, GAME.name)


        def Generate(self, type, name=None, user=None, allopts=False):
            #print 'GameSystem::Generate:',type,name
            self.generateFor = user
            if self.tid is not None:
                raise NameError('cannot generate over existing thing "%s"' % self.Name())
            realtype = type
            modifier = ''
            sep = realtype.find('(')
            if sep > 0:
                realtype = type[:sep]
                modifier = type[sep+1:-1]
            info = self.GetBdbInfo(realtype, 'Source')
            if info is None:
                raise DatafillError(_('cannot generate: no such type "%(type)s"') % {'type':type})
            if info[0] == '@':
                realtype = info[1:]
            #print 'GameSystem::Generate:',type,name,realtype,"<=",info
            self.genitemorder = list()
            self.geniteminfo  = dict()
            self.MakeUnique()
            if name is None:
                name = type # '%s/%s' % (type, self.tid)
                if name[0] == '_':
                    name = name[1:]
            self.Rename(name)
            self.SetValue('x:type', type)
            self.SetValue('x:realtype', realtype)
            self._DoGenerate(realtype)
            # Modifiers can be either a default "+1" type thing or they can be
            # key=val pairs separated by semicolons.  An item can indicate
            # to what attributes each default should apply.
            defmods = self.GetTypeInfo('Modifiers', '').split(',')
            defidx = 0
            mods = modifier.split(';')
            if len(mods) == 1 and mods[0].find('=') < 0 and len(defmods) > 1:
                while (len(mods) < len(defmods)):
                    mods.append(mods[0])
            for m in mods:
                if defidx < len(defmods):
                    defmod = defmods[defidx]
                else:
                    defmod = None
                defidx += 1
                if not m: continue
                if '=' not in m:
                    if defmod:
                        m = defmod + '=' + m
                        #print "GenerateItem-defmod:",defmod,m
                        self._GenerateItemOrder(realtype, m)
                    else:
                        raise BadParameter(_('too many modifiers for "%(item)s" ("%(mods)s" vs "%(allowed)s")')
                                           % {'item':type, 'mods':modifier, 'allowed':';'.join(defmods)})
                else:
                    if len(m) > 1 and m[1] != ':':
                        m = 'a:' + m
                    m = m.replace('+=', '=+ ')
                    m = m.replace('-=', '=+ -')
                    #print "GenerateItem-explicit:",m
                    self._GenerateItemOrder(realtype, m)
            # Now that we've found all the values, execute them!
            self._GenerateItemInfo(realtype, allopts=allopts)
            # Clean up.
            self.genitemorder = None
            self.geniteminfo  = None


        #---------------------------------------------------------------------#


        def Load(self, lbs=False, groups=None):
            kg = 0.0
            if groups is None or 'inventory' in groups:
                for i in self.GetInventory():
                    kg += i.Weight()
            if lbs: return kg * deity.kLbsPerKg
            return kg


        def Weight(self, default=0.0, lbs=False):
            kg  = float(self.GetInfo('weight', default))
            kg += self.Load()
            if lbs: return kg * deity.kLbsPerKg
            return kg


    #=========================================================================#


    class Item(Thing):
        kGdbPrefix      = 'Item'
        kBdbPrefix      = 'Equip'


        def __init__(self, tid, **kwds):
            super(GameSystem.Item, self).__init__(tid, **kwds)


        def Aspect(self, aspectx=None):
            #print "Aspect:", self.Name(), self.tid, "==", aspectx
            if aspectx is None:
                aspect = None
                bestval = -1
                for a in GAME.kWeaponAspects:
                    aval = self.GetAttribute(a)
                    if aval is not None and aval > bestval:
                        bestval = aval
                        aspect = a
                if aspect is None:
                    raise DatafillError(_('%(item)s has no attack aspects'), {'item':self.Name()})
            else:
                aspect = XLookup(GAME.kWeaponAspects, aspectx, 'aspect')
                if self.GetAttribute(aspect) is None:
                    raise BadParameter(_('%(item)s has no aspect "%(aspect)s"') % {'item':self.Name(), 'aspect':aspect.xval()})
            return aspect


        def Skill(self, char, willopen=False):
            skilldef = self.GetInfo('skill')
            #print 'Item::Skill:',self,char,skilldef
            if not skilldef:
                raise DatafillError(_('item type "%(type)s" has no associated skill')
                                    % {'type':self.GetValue('x:realtype')})
            skill,sep,sbase = skilldef.partition('/')
            prevbase = char.GetValue('x:base-s:%s' % skill)
            if prevbase and sbase and prevbase != sbase:
                raise DatafillError(_('item type "%(type)s" with skill "%(skill)s" has base that conflicts with previous definition of "%(prevskill)s"')
                                    % {'type':self.GetValue('x:realtype'), 'skill':skilldef, 'prevskill':'%s/%s'%(skill,prevbase)})
            if sbase and not prevbase:
                char.SetValue('x:base-s:%s' % skill, sbase)
            if prevbase and not sbase:
                sbase = prevbase
            if char.GetSkill(skill) is None:
                skill = None
                if sbase:
                    if char.GetSkill(sbase) is not None:
                        skill = sbase
            if not skill:
                if willopen:
                    if sbase:
                        skill = sbase
                    else:
                        skill = skilldef
                else:
                    raise NotPossible('%(name)s does not have skill "%(skill)s"'
                                      % {'name':char.Name(), 'skill':self.GetInfo('skill')})

            return skill


        def SkillInfo(self, char, willopen=False):
            skill = self.Skill(char, willopen=open)
            if skill in GAME.kSkillInfo:
                return GAME.kSkillInfo[skill]
            skill = char.GetValue('x:base-s:%s' % skill)
            if skill is not None and skill in GAME.kSkillInfo:
                return GAME.kSkillInfo[skill]
            raise DatafillError('skill %(skill)s is not a standard skill and base type "%(base)s" is unknown'
                                % {'skill':skill, 'base':str(sbase)})


        def Break(self):
            (possessor, loc) = self.GetPossessor()
            possessor.UnwieldItem(self)
            possessor.SubInventory(self);


    #=========================================================================#


    class Character(Thing):
        kGdbPrefix      = 'Char'
        kBdbPrefix      = 'Beast'

        kArmorKey       = 'l:armor'
        kMountKey	= 'i:mount'
        kPromptKey	= 'x:prompt'	# ask for check results
        kStatusKey      = 'i:status'
        kWieldedKey     = 'd:wielded'
        kWornKey        = 'd:worn'


        def __init__(self, tid, **kwds):
            super(GameSystem.Character, self).__init__(tid, **kwds)


        #---------------------------------------------------------------------#


        def SetList(self, info, val, values=None):
            return self.SetValueList(info, val, 'l:', values)


        def GetList(self, info, default=None, values=None):
            return self.GetValueList(info, default, 'l:', values)


        #---------------------------------------------------------------------#


        def SetDict(self, info, val, values=None):
            return self.SetValueMap(info, val, 'd:', values)


        def GetDict(self, info, default=None, values=None):
            return self.GetValueMap(info, default, 'd:', values)


        #---------------------------------------------------------------------#


        def GetPrompt(self):
            return int(self.GetValue(self.kPromptKey, default='0'))


        def SetPrompt(self, val):
            self.SetValue(self.kPromptKey, str(int(val)))


        #---------------------------------------------------------------------#


        def SetSkill(self, skl, val, values=None):
            return self.SetValue(skl, str(val), 's:', values)


        def GetSkill(self, skl, default=None, values=None):
            val = self.GetValue(skl, None, 's:', values)
            if val is None: return default
            try:
                return int(float(val))
            except ValueError:
                return default


        def GetSkillStart(self, skl, recalc=False):
            return 0


        def GetSkills(self, order=None, categories=False, values=None):
            skvals = xdict(xlatekey=GAME.kSkillInfo.xlatekey, xpartkey=GAME.kSkillInfo.xpartkey)
            skills = self.GetValue(self.kSkillSetKey, '').split(';')
            sksort = list()
            #print 'GetSkills:', skills
            for s in skills:
                if not s: continue
                if s in GAME.kSkillInfo:
                    i = GAME.kSkillInfo[s]
                else:
                    sbase = self.GetValue('x:base-s:%s' % s)
                    if not sbase:
                        print (_('unknown base skill for "%(skill)s"') % {'skill':P_('skill:',s)})
                        continue
                    if sbase not in GAME.kSkillInfo:
                        print (_('invalid base skill "%(base)s" for skill "%(skill)s"')
                               % {'base':P_('skill:',sbase), 'skill':P_('skill:',s)})
                        continue
                    i = GAME.kSkillInfo[sbase]
                (cat,mor) = i.split(None,1)
                val = self.GetSkill(s, values=values)
                if val is None: continue
                (tag,nam) = cat.split('/')
                sksort.append('%s/%s/%s' % (tag, P_('skill:',nam), s))
                skvals[s] = val
            if order is None: return skvals

            sksort.sort()
            lastcat = ''
            for s in sksort:
                (tag,cat,nam) = s.split('/')
                if categories and cat != lastcat:
                    order.append(cat)
                    lastcat = cat
                order.append(nam)
            return skvals


        #---------------------------------------------------------------------#


        def GetDisplayStatus(self):
            return P_('status', self.GetInfo('status', 'standing'))


        #---------------------------------------------------------------------#


        def Generate(self, type, name=None, **kwds):
            GameSystem.Thing.Generate(self, type, name, **kwds)
            # make sure everything works
            try:
                self.GetSkills()
            # change errors in to something meaningful
            except DeityException as e:  # pragma: no cover
                raise DatafillError(_('invalid generator information for %(type)s -- %(text)s')
                                    % {'type':type, 'text':str(e)})
            # update worn list as it updates location armor
            self.SetWorn(self.GetWorn())
            # add held items to inventory
            items = self.GetList('starthave')
            if items:
                for item in items:
                    i = GAME.Item(None)
                    i.Generate(item, **kwds)
                    self.AddInventory(i)
            # add worn items to inventory
            items = self.GetList('startwear')
            if items:
                for item in items:
                    i = GAME.Item(None)
                    i.Generate(item, **kwds)
                    self.AddInventory(i)
                    self.WearItem(i)


        #---------------------------------------------------------------------#


        def LookupLocation(self, locx, loclist=None, multiple=False, defindex=None):
            if loclist is None:
                loclist = self.GetTypeInfo(deity.beastiary.kLocationsKey, aslist=True, keysonly=True)
            return XLookup(loclist, locx, N_('location'), defindex=defindex, multiple=multiple)


        def LookupWieldLocation(self, locx, loclist=None, multiple=False, defindex=None):
            if loclist is None:
                loclist = self.GetTypeInfo(deity.beastiary.kWieldKey, aslist=True, keysonly=True)
            return XLookup(loclist, locx, N_('wield location'), defindex=defindex, multiple=multiple)


        #---------------------------------------------------------------------#


        def SetWielded(self, vals):
            w = dict()
            for k,v in vals.iteritems():
                if v is None:
                    w[k] = ''
                else:
                    w[k] = v.tid
            return self.SetDict(self.kWieldedKey, w)


        def GetWielded(self, default={}, order=None):
            wielded = xdict(xlatekey=self.xlate, xpartkey=self.xpart)
            wtids   = self.GetDict(self.kWieldedKey)
            if not wtids or order is not None:
                wlocs = self.GetTypeInfo(deity.beastiary.kWieldKey, aslist=True, keysonly=True)
                if wlocs is None:
                    return xdict(default, xlatekey=self.xlate, xpartkey=self.xpart)
                if wtids is None:
                    wtids = dict()
                for l in wlocs:
                    if l in wtids and wtids[l] != '':
                        wielded[l] = GAME.Item(wtids[l])
                    else:
                        wielded[l] = None
                if order is not None:
                    order.extend(wlocs)
            else:
                for k,v in wtids.iteritems():
                    if v == '':
                        wielded[k] = None
                    else:
                        wielded[k] = GAME.Item(v)
            return wielded


        def WieldItem(self, item, locx=None, replace=False, defindex=None):
            if not isinstance(item, GameSystem.Thing):  # pragma: no cover
                raise Exception('cannot wield non-things')
            if defindex is None:
                attack = item.GetAttribute('attack', -1)
                defend = item.GetAttribute('defend', -1)
                if defend > attack:
                    defindex = 1
                else:
                    defindex = 0
            for attr in GAME.kBaseAttributes:
                rkey = 'r:%s' % attr
                condition = item.GetValue(rkey)
                if condition is not None:
                    aval = self.GetAttribute(attr)
                    if not eval('%d %s' % (aval, condition)):
                        Output(_('%(item)s requires %(attr)s %(condition)s but is %(value)d -- unwield or use appropriate modifiers')
                                 % {'item':item.Name(), 'attr':attr, 'condition':condition, 'value':aval}, '@red')
            wlocs   = xdict(xlatekey=self.xlate, xpartkey=self.xpart, xlateval=None)
            wielded = self.GetWielded()
            if not wielded:
                raise NotPossible(_('character race "%(race)s" cannot wield items') % {'race':self.GetValue('x:type','unknown').xval()})
            location = self.LookupWieldLocation(locx, defindex=defindex)
            if location not in wielded:  # pragma: no cover
                print 'error: WieldItem failed test that should have been covered by earlier test'
                raise NotPossible(_('cannot wield items in %(loc)s') % {'loc':location.xval()})
            self.SubInventory(item)
            try:
                if wielded[location] is not None:
                    if not replace:
                        raise NotPossible(_('%(item)s is already in %(loc)s') % {'item':wielded[location].Name(), 'loc':location.xval()})
                    self.AddInventory(wielded[location])
                    wielded[location].SetPossessor(self)
                    wielded[location] = None
            except Exception:
                self.AddInventory(item)
                self.SetWielded(wielded)
                raise

            wielded[location] = item
            item.SetPossessor(self, location)
            self.SetWielded(wielded)
            return location


        def UnwieldItem(self, item, unlocx=None):
            #print "UnwieldItem(",self,item,unlocx,")"
            wielded = self.GetWielded()
            try:
                loc = item.CheckPossessor(self)
            except ValueError as e:
                print 'malformed possessor id "%s" for %s maybe held by %s -- assuming yes' % (item.GetValue('x:possessor'), str(item), str(self))
                for l in wielded.keys():
                    if wielded[l] == item:
                        loc = xstr(l, xlate='bdbloc:')
                        break
            if loc is None or loc == '':
                raise NotPossible(_('%(item)s is not wielded by %(char)s') % {'item':item.Name(), 'char':self.Name()})
            if unlocx is not None and loc.xval() != unlocx.xval():
                raise NotPossible(_('%(item)s is wielded in %(loc1)s, not %(loc2)s') % {'item':item.Name(), 'loc1':loc.xval(), 'loc2':unlocx.xval()})
            if loc not in wielded or wielded[loc].tid != item.tid:  # pragma: no cover
                print 'error: UnWieldItem failed test that should have been covered by earlier test'
                raise NotPossible(_('%(item)s is not wielded by %(char)s') % {'item':item.Name(), 'char':self.Name()})
            wielded[loc] = None
            self.SetWielded(wielded)
            self.AddInventory(item)
            return loc


        def SearchWielded(self, namex, locx=None):
            worder  = list()
            wielded = self.GetWielded(order=worder)
            if locx:
                loc = self.LookupLocation(locx)
                if loc not in wielded or not wielded[loc]:
                    raise NotPossible(_('nothing in %(loc)s') % {'loc':loc.xval()})
                if wielded[loc].Name().find(namex.xval()) != 0:
                    raise NotPossible(_('"%(item)s" not in %(loc)s') % {'item':namex.xval(), 'loc':loc.xval()})
                return wielded[loc]
            istrs = xlist()
            items = list()
            for loc in worder:
                item = wielded[loc]
                if not item: continue
                istrs.append(item.Name())
                items.append(item)
            match = XLookup(istrs, namex, N_('item'), multiple=True, index=True)
            if not match:
                raise BadParameter(_('"%(item)s" not wielded') % {'item':namex.xval()})
            return items[match[0]]


        #---------------------------------------------------------------------#


        def SetMount(self, mount):
            #print 'SetMount:', self, mount, ':', self.tid, mount and mount.tid
            curmount = self.GetMount()
            if curmount:
                if mount == curmount: return
                riders = curmount.GetList('riders', [])
                riders.remove(self.tid)
                curmount.SetList('riders', riders)
            if mount is None:
                self.SetValue(self.kMountKey, None)
                return
            mtid = mount.tid
            riders = mount.GetList('riders', [])
            riders.append(self.tid)
            mount.SetList('riders', riders)
            self.SetValue(self.kMountKey, mtid)


        def GetMount(self, default=None):
            mtid = self.GetValue(self.kMountKey)
            if mtid is None:
                return default
            return GAME.Character(mtid)


        def MountCharacter(self, mount):
            curmount = self.GetMount()
            if curmount:
                raise NotPossible(_('%(char)s is already mounted on %(mount)s') % {'char':self.Name(), 'mount':curmount.Name()})
            self.SetMount(mount)
            self.SetStatus('mounted')


        def Dismount(self):
            curmount = self.GetMount()
            if curmount is None:
                raise NotPossible(_('%(char)s is not mounted') % {'char':self.Name(), 'mount':curmount.Name()})
            self.SetMount(None)
            self.SetStatus('standing')


        def SetWorn(self, vals):
            w = dict()
            for k,v in vals.iteritems():
                if not v:
                    raise NotPossible(_('cannot wear %(item)s on "nowhere"') % {'item':k.Name()})
                w[k.tid] = ';'.join(v)
            return self.SetDict(self.kWornKey, w)


        def GetWorn(self, default=[]):
            worn  = dict()
            wtids = self.GetDict(self.kWornKey)
            if wtids is not None:
                for w,l in wtids.iteritems():
                    item = GAME.Item(w)
                    worn[item] = l.split(';')
            return worn


        def WearItem(self, item):
            #print "WearItem:", self.Name(), "wears", item.Name()
            wlocs = item.GetTypeInfo('Cover', aslist=True, keysonly=False)
            worn  = self.GetWorn()
            if wlocs is None:
                raise NotPossible(_('%(item)s cannot be worn') % {'item':item.Name()})
            if not wlocs:
                # Cover with no locations means "everywhere"
                wlocs = list()
                for l in self.GetTypeInfo('Locations', aslist=True, keysonly=True):
                    wlocs.append('%s=0' % l)
            self.SubInventory(item)
            worn[item] = wlocs
            item.SetPossessor(self, '=')
            self.SetWorn(worn)


        def UnwearItem(self, item):
            loc = item.CheckPossessor(self)
            if loc is None or loc != '=':
                raise NotPossible(_('%(item)s is not worn by %(char)s') % {'item':item.Name(), 'char':self.Name()})
            worn = self.GetWorn()
            for w in worn.iterkeys():
                if w.tid == item.tid:
                    item = w
                    break
            else:  # pragma: no cover
                print 'error: UnWearItem failed test that should have been covered by earlier test'
                raise NotPossible(_('%(item)s is not worn by %(char)s') % {'item':item.Name(), 'char':self.Name()})
            self.AddInventory(item)
            item.SetPossessor(self)
            del worn[item]
            self.SetWorn(worn)


        def Load(self, lbs=False, groups=None):
            kg = 0
            if groups is None or 'inventory' in groups:
                kg += GameSystem.Thing.Load(self)
            if groups is None or 'worn' in groups:
                for i in self.GetWorn().iterkeys():
                    if i is not None:
                        kg += i.Weight()
            if groups is None or 'wielded' in groups:
                for i in self.GetWielded().itervalues():
                    if i is not None:
                        kg += i.Weight()
            if lbs: return kg * deity.kLbsPerKg
            return kg


        #---------------------------------------------------------------------#


        def UnarmedWeapon(self, locx, loclist=None):
            loc = self.LookupLocation(locx, loclist=loclist)
            loc = GAME.BaseLocation(loc)
            weapon = GAME.Item(None, tempobj=True)
            #print 'UnarmedWeapon:',loc,weapon,self.GetValue('x:types')
            for t in self.GetValue('x:types').split(';'):
                if not t: continue
                if t[0] != '_':
                    t = '_' + t
                weptype = '%s-%s' % (t, loc)
                #print weptype, weapon.GetBdbInfo(weptype, 'Source')
                if weapon.GetBdbInfo(weptype, 'Source'):
                    weapon.Generate(weptype)
                    return weapon
            raise NotPossible(_('cannot use %(location)s for unarmed combat') % {'location':loc.xval()})


        def AttackFromLocations(self, order=None, separate=None, loclist=None):
            locs = xdict(xlatekey=self.xlate)
            if loclist is None:
                loclist = self.GetTypeInfo(deity.beastiary.kLocationsKey, aslist=True, keysonly=True)
            if order is None:
                order = list()
            worder  = list()
            wielded = self.GetWielded(order=worder)
            # It's possible to be able to wield a weapon but not use that location unarmed.
            for l in worder:
                if wielded[l] is None:
                    try:
                        w = self.UnarmedWeapon(l, loclist=loclist)
                        wielded[l] = w
                    except NotPossible:
                        pass
                if wielded[l] is not None:
                    order.append(l)
            if separate and order:
                order.append(separate)
            for l in loclist:
                if l in wielded and wielded[l] is not None:
                    locs[l] = wielded[l]
                else:
                    try:
                        w = self.UnarmedWeapon(l, loclist=loclist)
                        locs[l] = w
                        if l not in wielded or wielded[l] is None:
                            order.append(l)
                    except NotPossible:
                        #print 'AttackFromLocations: no unarmed weapon for location "%s"' % l
                        pass
            #print 'AttackFromLocations:',order,locs,loclist
            return locs


    #==========================================================================#


    class CharListWidget(deity.widgets.SelectableItemList):
        CollapsedCharHeadings = Watched(dict())

        def __init__(self, parent, selected, status=False, **kwds):
            width  = GameSystem.Character.kNameStringMax+2 # max expected character name + index
            width += len('(alias)')                        # max expected alias length
            width += GameSystem.kCharacterStatesStringMax  # max status string (plus "[]")
            deity.widgets.SelectableItemList.__init__(self, parent, width=width, collapse=self.CollapsedCharHeadings, **kwds)
            self.selected = selected
            self.status   = None
            if status: self.status = dict()


        def DisplayedChars(self):
            chars = list()
            cdict = self.CollapsedCharHeadings.Get()
            for name,char in GAME.gameCharNames.Get().iteritems():
                group = char.GetValue('x:gamegroup')
                if group in cdict and not cdict[group]:
                    chars.append(char)
            return chars


        def ExpandHeading(self, heading):
            if deity.widgets.SelectableItemList.ExpandHeading(self, heading):
                self.DisplayRefresh()


        def DisplayRefresh(self):
            #print 'CharListWidget::DisplayRefresh:'
            changed = False

            if GAME.gameCharNames.IsChanged(self) + self.CollapsedCharHeadings.IsChanged(self):
                self.SetItems(GAME.gameCharDisplay)
                changed = True

            if self.status is not None:
                for name,char in GAME.gameCharNames.Get().iteritems():
                    status = char.GetDisplayStatus()
                    if name not in self.status or self.status[name] != status:
                        self.SetInfo(name, status)

            if not self.multi and self.selected.IsChanged(self):
                if self.selected.Get() is None:
                    self.SetSelection(self.GetSelection(),False)
                else:
                    sel = self.selected.Get();
                    self.SetSelection(sel)
                changed = True

            return changed


    #==========================================================================#


    class CharVisualWidget(wx.Panel):

        def __init__(self, parent, char, **kwds):
            wx.Panel.__init__(self, parent, **kwds)
            self.char = char

            self.sizer1 = wx.BoxSizer(wx.VERTICAL)
            self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
            self.sizer3 = wx.BoxSizer(wx.HORIZONTAL)
            self.fontBold = wx.Font(pointSize=wx.DEFAULT, family=wx.FONTFAMILY_DEFAULT,
                                    style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)
            self.group = wx.StaticText(self, wx.ID_ANY, style=wx.ALIGN_LEFT|wx.ST_NO_AUTORESIZE)
            self.sizer3.Add(self.group, 1, wx.EXPAND|wx.LEFT)
            self.name = wx.StaticText(self, wx.ID_ANY, style=wx.ALIGN_CENTER|wx.ST_NO_AUTORESIZE)
            self.name.SetFont(self.fontBold)
            self.sizer3.Add(self.name, 1, wx.EXPAND|wx.CENTER)
            self.race = wx.StaticText(self, wx.ID_ANY, style=wx.ALIGN_RIGHT|wx.ST_NO_AUTORESIZE)
            self.sizer3.Add(self.race, 1, wx.EXPAND|wx.RIGHT)
            self.sizer1.Add(self.sizer3, 0, wx.EXPAND)
            self.image = deity.widgets.OverlayableImage(self, style=wx.SUNKEN_BORDER)
            self.image.SetMinSize(deity.beastiary.kImageSize)
            self.image.SetMaxSize(deity.beastiary.kImageSize)
            self.sizer2.Add(self.image, 1, wx.EXPAND)
            # For armor, expand=True would be better but overexpands with wx 2.7
            self.armor = deity.widgets.InfoTable(self, flex=True, expand=True, width=26, style=wx.SUNKEN_BORDER)
            self.armor.SetMinSize(self.armor.GetBestSize())
            self.sizer2.Add(self.armor, 0, wx.EXPAND)
            self.sizer1.Add(self.sizer2, 3, wx.EXPAND)
            self.desc = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_MULTILINE|wx.TE_READONLY)
            self.sizer1.Add(self.desc, 1, wx.EXPAND)
            self.SetSizerAndFit(self.sizer1)


        def DisplayRefresh(self):
            #print 'CharVisualWidget::DisplayRefresh:',self.char,self.char.Get()
            changed = False

            if self.char.IsChanged(self):
                char = self.char.Get()

                if char is None:
                    self.armor.SetItems(None)
                    changed = True
                    self.group.SetLabel('')
                    self.name.SetLabel(_('no character selected'))
                    self.race.SetLabel('')

                else:
                    # info
                    name = char.Name()
                    player = char.GetInfo('player')
                    if player:
                        name += ' (%s)' % player
                    #print 'Combat::DisplayRefresh:',char,name,char.GetValue('x:type'),char.GetValue('x:realtype')
                    self.name.SetLabel(name)
                    self.race.SetLabel(char.GetValue('x:type','').xval())
                    self.group.SetLabel(char.GetValue('x:gamegroup',''))
                    # armor
                    locs  = char.GetTypeInfo(deity.beastiary.kLocationsKey, aslist=True, keysonly=True)
                    armor = xdict(xlatekey=GAME.xlate)
                    for l in locs:
                        a = char.GetValue('x:armor-%s'%l)
                        if a:
                            armor[l] = a.split(';')[0]
                        else:
                            armor[l] = '0'
                    #print ' - Armor:',armor,locs
                    self.armor.SetItems(armor, locs)
                    self.desc.Clear()
                    # description: wound list
                    wounds = char.GetList('wounds', [])
                    itext  = list()
                    for w in wounds:
                        wtext = GAME.WoundDescription(w, char)
                        if wtext == w:
                            itext.append(w)
                        else:
                            itext.append('%s (%s)' % (wtext, w))
                    if itext:
                        self.desc.AppendText(_('wounds: %s') % ', '.join(itext) + '\n\n')
                    locs  = list()
                    items = char.GetWielded(order=locs)
                    itext = GAME.xlist()
                    for l in locs:
                        i = items[l]
                        if not i: continue
                        itext.append('%s@%s' % (i.ShortDesc(),l.xval()))
                    if itext:
                        self.desc.AppendText(_('wielded: %s') % itext.xjoin(', ') + '\n\n')
                    else:
                        self.desc.AppendText(_('wielded: %s') % _('NOTHING') + '\n\n')
                    # description: mount
                    mount = char.GetMount()
                    if mount:
                        self.desc.AppendText(_('mount: %s') % mount.ShortDesc() + '\n\n')
                    # description: inventory list
                    items = char.GetInventory()
                    itext = list()
                    for i in items:
                        itext.append(i.ShortDesc())
                    if itext:
                        itext.sort()
                        self.desc.AppendText(_('equipment: %s') % ', '.join(itext) + '\n\n')
                    # description: worn/armor list
                    items = char.GetWorn()
                    itext = GAME.xlist()
                    for i in items.iterkeys():
                        itext.append(i.ShortDesc())
                    if itext:
                        # always put "natural" at the end
                        natural = None
                        for item in list(itext):
                            if item[0:7] == 'natural':
                                natural = item
                                itext.remove(natural)
                        itext.xsort()
                        if natural:
                            itext.append(natural)
                        self.desc.AppendText(_('worn: %s') % itext.xjoin(', ') + '\n\n')
                    # description: miscillaneous info
                    items = char.GetDisplayInfo()
                    itext = list()
                    for k,v in items.iteritems():
                        itext.append("%s=%s" % (k,v))
                    if itext:
                        itext.sort()
                        self.desc.AppendText(_('info: %s') % ', '.join(itext) + '\n\n')
                    # description: discarded/dropped item list
                    items = GAME.Thing(GAME.kCombatGroundTid).GetInventory()
                    itext = list()
                    for i in items:
                        itext.append(i.ShortDesc())
                    if itext:
                        itext.sort()
                        self.desc.AppendText(_('ground: %s') % ', '.join(itext) + '\n\n')
                    self.desc.SetInsertionPoint(0)
                    self.desc.ScrollLines(-9999)

            return changed


    #=========================================================================#


    class CharStatsWidget(wx.Panel):

        def __init__(self, parent, char, **kwds):
            wx.Panel.__init__(self, parent, **kwds)
            self.char = char
            self.sizer = wx.BoxSizer(wx.VERTICAL)
            infos = xdict(xlatekey=GAME.kGeneralInfos.xlate, xpartkey=GAME.kGeneralInfos.xpart)
            for i in GAME.kGeneralInfos:
                infos[i] = '- - -'
            self.info = deity.widgets.InfoTable(self, items=infos, order=GAME.kGeneralInfos, cols=1, style=wx.SUNKEN_BORDER)
            self.sizer.Add(self.info, 0, wx.EXPAND)
            attrs = xdict(xlatekey=GAME.kBaseAttributes.xlate, xpartkey=GAME.kBaseAttributes.xpart)
            for a in GAME.kBaseAttributes+GAME.kMoreAttributes:
                attrs[a] = 88
            #font = wx.Font(pointSize=wx.DEFAULT, family=wx.FONTFAMILY_TELETYPE,
            #               style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_NORMAL)
            self.base = deity.widgets.InfoTable(self, items=attrs, order=GAME.kBaseAttributes, template='%2d',
                                                cols=2, padding=3, spacing=' ', font=None, style=wx.SUNKEN_BORDER)
            self.sizer.Add(self.base, 0, wx.EXPAND)
            if GAME.kMoreAttributes:
                self.more = deity.widgets.InfoTable(self, items=attrs, order=GAME.kMoreAttributes, template='%2d',
                                                    cols=2, padding=3, spacing=' ', font=None, style=wx.SUNKEN_BORDER)
                self.sizer.Add(self.more, 0, wx.EXPAND)
            self.skills = deity.widgets.InfoList(self, padding=3, labels=GAME.kSkillInfo.keys(), style=wx.SUNKEN_BORDER)
            self.sizer.Add(self.skills, 1, wx.EXPAND)
            self.SetSizerAndFit(self.sizer)


        def DisplayRefresh(self):
            #print "CharStatsWidget::DisplayRefresh"
            changed = False

            if self.char.IsChanged(self):
                char = self.char.Get()
                sorder = list()
                if char is None:
                    infos  = xdict(xlatekey=GAME.kGeneralInfos.xlate, xpartkey=GAME.kGeneralInfos.xpart)
                    attrs  = xdict(xlatekey=GAME.kBaseAttributes.xlate, xpartkey=GAME.kBaseAttributes.xpart)
                    skills = xdict(xlatekey=GAME.kSkillInfo.xlatekey, xpartkey=GAME.kSkillInfo.xpartkey)
                else:
                    infos  = char.GetInfos()
                    attrs  = char.GetAttributes()
                    skills = char.GetSkills(sorder, categories=True)
                # infos
                self.info.SetItems(infos, GAME.kGeneralInfos)
                # attributes
                self.base.SetItems(attrs, GAME.kBaseAttributes)
                if GAME.kMoreAttributes:
                    self.more.SetItems(attrs, GAME.kMoreAttributes)
                # skills
                self.skills.SetItems(skills, sorder)
                changed = True

            return changed


    #=========================================================================#


    class CharGenerateWidget(wx.Panel):

        def __init__(self, parent, **kwds):
            wx.Panel.__init__(self, parent, **kwds)

            self.sizer = wx.BoxSizer(wx.VERTICAL)
            self.types = deity.widgets.SearchableListBox(self, wx.ID_ANY, style=wx.LB_SINGLE|wx.LB_SORT|wx.LB_NEEDED_SB)
            order = GAME.charTypeDict.keys()
            order.sort()
            for t in order:
                self.types.Append(t.xval())
            self.sizer.Add(self.types, 1, wx.EXPAND)
            self.button = wx.Button(self, wx.ID_ANY, label=P_('btn:generate'))
            #self.button.Bind(wx.EVT_BUTTON, self.OnGenerate)
            self.sizer.Add(self.button, 0, wx.EXPAND)
            self.SetSizerAndFit(self.sizer)


        def GetSelection(self):
            sel = self.types.GetStringSelection()
            if not sel: return None
            return GAME.charTypeDict[sel]


        def GetStringSelection(self):
            sel = self.types.GetStringSelection()
            if not sel: return None
            return sel


        def SetStringSelection(self, sel):
            return self.types.SetStringSelection(sel)


#        def GetClientData(self, index):
#            return self.types.GetClientData(index)


        def DisplayRefresh(self):
            #print "CharGenerateWidget::DisplayRefresh"
            changed = False
            return changed


    #=========================================================================#


    class CharEditValueManager(deity.editor.ValueManager):

        def __init__(self, char):
            deity.editor.ValueManager.__init__(self)
            self.char = char

        # This is just here for unit-test purposes.
        def ValueUpdated(self, var, new, old):
            if var != 's:unarmed':
                cur = self.GetValue('s:unarmed')
                cur = str(int(cur) - int(old) + int(new))
                self.ChangeValue('s:unarmed', cur)


    #-------------------------------------------------------------------------#


    class CharEditWidget(wx.Panel, deity.widgets.DWidget):

        def __init__(self, parent, char, **kwds):
            wx.Panel.__init__(self, parent, **kwds)
            self.parent = parent
            self.char = char
            self.curchar = None
            self.valuemgr = GAME.CharEditValueManager(char)
            self.skillnames = xlist(xlate='skill:')

            self.sizer1 = wx.BoxSizer(wx.VERTICAL)
            self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
            self.nameedit = wx.TextCtrl(self, wx.ID_ANY)
            self.nameedit.Bind(wx.EVT_KILL_FOCUS, self.OnNameChange)
            self.nameedit.Bind(wx.EVT_CHAR, self.OnNameChar)  # EVT_CHAR_HOOK doesn't work
            self.sizer2.Add(self.nameedit, 1, wx.EXPAND)
            self.groupedit = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY)
            self.Bind(wx.EVT_COMBOBOX, self.OnGroupChange, self.groupedit)
            self.sizer2.Add(self.groupedit, 0, wx.EXPAND)
            self.sizer1.Add(self.sizer2, 0, wx.EXPAND)

            self.sizer3 = wx.BoxSizer(wx.HORIZONTAL)
            self.curinfos = deity.widgets.EditList(self, 16, self.valuemgr, padding=3, labels=GAME.kGeneralInfos, style=wx.SUNKEN_BORDER)
            self.sizer3.Add(self.curinfos, 0, wx.EXPAND)
            self.curattrs = deity.widgets.EditList(self, '88', self.valuemgr, padding=3, labels=GAME.kBaseAttributes, style=wx.SUNKEN_BORDER)
            self.sizer3.Add(self.curattrs, 0, wx.EXPAND)
            self.curskills = deity.widgets.EditList(self, '188', self.valuemgr, padding=3, labels=GAME.kSkillInfo.keys(), scrollable=True, style=wx.SUNKEN_BORDER)
            self.sizer3.Add(self.curskills, 0, wx.EXPAND)
            self.sizer4 = wx.BoxSizer(wx.VERTICAL)
            self.newskills = deity.widgets.SearchableListBox(self, wx.ID_ANY, style=wx.LB_SINGLE|wx.LB_SORT|wx.LB_NEEDED_SB)
            self.sizer4.Add(self.newskills, 1, wx.EXPAND)
            self.btnskills = wx.Button(self, wx.ID_ANY, label=P_('btn:add skill'))
            self.btnskills.Bind(wx.EVT_BUTTON, self.OnAddSkill)
            self.sizer4.Add(self.btnskills, 0, wx.EXPAND)
            self.sizer3.Add(self.sizer4, 1, wx.EXPAND)
            self.sizer5 = wx.BoxSizer(wx.VERTICAL)
            self.inventory = wx.ListBox(self, wx.ID_ANY, style=wx.LB_SINGLE|wx.LB_SORT|wx.LB_NEEDED_SB)
            self.sizer5.Add(self.inventory, 1, wx.EXPAND)
            self.sizer3.Add(self.sizer5, 1, wx.EXPAND)
            self.btninv = wx.Button(self, wx.ID_ANY, label=P_('btn:remove item'))
            self.btninv.Bind(wx.EVT_BUTTON, self.OnSubItem)
            self.sizer5.Add(self.btninv, 0, wx.EXPAND)
            self.sizer6 = wx.BoxSizer(wx.VERTICAL)
            self.newitems = deity.widgets.SearchableListBox(self, wx.ID_ANY, style=wx.LB_SINGLE|wx.LB_SORT|wx.LB_NEEDED_SB)
            self.sizer6.Add(self.newitems, 1, wx.EXPAND)
            self.newitemopts = wx.TextCtrl(self, wx.ID_ANY)
            self.sizer6.Add(self.newitemopts, 0, wx.EXPAND)
            self.btnitems = wx.Button(self, wx.ID_ANY, label=P_('btn:add item'))
            self.Bind(wx.EVT_BUTTON, self.OnAddItem)
            self.sizer6.Add(self.btnitems, 0, wx.EXPAND)
            self.sizer3.Add(self.sizer6, 1, wx.EXPAND)
            self.sizer1.Add(self.sizer3, 1, wx.EXPAND)
            self.SetSizerAndFit(self.sizer1)

            groups = list()
            for g in GAME.charGroupThing.GetInventory():
                groups.append(g.Name())
            groups.sort()
            for g in groups:
                self.groupedit.Append(g)

            # item list
            self.newitems.Clear()
            for i in GAME.equipTypeDict.keys():
                self.newitems.Append(i.xval())


        def OnNameChar(self, ev):
            key = ev.GetKeyCode()
            if key == wx.WXK_SPACE:
                return  # no spaces allowed in names
            elif key == wx.WXK_RETURN:
                return self.OnNameChange(ev)
            ev.Skip()


        def OnAddItem(self, ev):
            item = self.newitems.GetStringSelection()
            opts = self.newitemopts.GetValue()
            #print 'CharEditWidget::OnAddItem:',item,opts
            if not item: return
            try:
                item  = XLookup(GAME.equipTypeDict.keys(), item, 'item')
                if opts:
                    opts = str(opts)
                    item += '(%s)' % opts
                it = GAME.Item(None)
                it.Generate(item)
                self.newitemopts.Clear()
                self.char.Alter().AddInventory(it)
            except DeityException as e:
                for msg in e:
                    Output(str(msg), '@error')
            self.DisplayRefresh()


        def OnAddSkill(self, ev):
            skill = self.newskills.GetStringSelection()
            #print 'CharEditWidget::OnAddSkill:',skill
            if not skill: return
            skill  = XLookup(self.skillnames, strx(skill), 'skill')
            #print ' - before:',self.char.Get().GetSkill(skill)
            self.char.Alter().SetSkill(skill, self.char.Get().GetSkillStart(skill))
            #print ' - after:',self.char.Get().GetSkill(skill)
            self.curchar = None
            self.DisplayRefresh()


        def OnGroupChange(self, ev):
            group = self.groupedit.GetStringSelection()
            if not group:
                return
            self.parent.app.CommandExecute(P_('cmd:group'), group)


        def OnNameChange(self, ev):
            name = self.nameedit.GetValue()
            #print 'CharEditWidget::OnNameChange:',name
            if not name:
                return
            self.parent.app.CommandExecute(P_('cmd:rename'), name)


        def OnSubItem(self, ev):
            item = self.inventory.GetStringSelection()
            #print 'CharEditWidget::OnSubItem:',item
            if not item: return
            item  = XLookup(self.invitems.keys(), item, 'item')
            it = self.invitems[item]
            possessor = it.GetValue('x:possessor')
            if possessor:
                gtype,tid,loc = possessor.split('/')
                #print ' -',possessor,'=>' ,gtype,tid,loc
                if loc == '=':
                    self.char.Get().UnwearItem(it)
                elif loc != '':
                    self.char.Get().UnwieldItem(it)
            self.char.Alter().SubInventory(it)
            self.DisplayRefresh()


        def DisplayRefresh(self):
            #print "CharEditWidget::DisplayRefresh"
            changed = False
            char = self.char.Get()

            if char != self.curchar:
                #print 'CharEditWidget::DisplayRefresh: character changed'
                self.nameedit.SetValue(char.Name())
                self.groupedit.SetStringSelection(char.GetValue('x:gamegroup', 'encounter'))
                sorder = xlist(xlate='skill:')
                self.valuemgr.Clear()
                if char is None:
                    infos  = xdict(xlatekey=GAME.kGeneralInfos.xlate, xpartkey=GAME.kGeneralInfos.xpart)
                    attrs  = xdict(xlatekey=GAME.kBaseAttributes.xlate, xpartkey=GAME.kBaseAttributes.xpart)
                    skills = xdict(xlatekey=GAME.kSkillInfo.xlatekey, xpartkey=GAME.kSkillInfo.xpartkey)
                    slist  = list()
                else:
                    infos  = char.GetInfos()
                    attrs  = char.GetAttributes()
                    skills = char.GetSkills(sorder, categories=True)
                    slist  = GAME.kSkillInfo.keys()
                # infos
                self.infos = dict()
                for i in GAME.kGeneralInfos:
                    self.infos[i] = 'i:'+i
                    if i in infos:
                        self.valuemgr.SetValue(self.infos[i], str(infos[i]))
                    else:
                        self.valuemgr.SetValue(self.infos[i], '')
                self.curinfos.SetItems(self.infos, GAME.kGeneralInfos)
                # attributes
                self.attrs = dict()
                for a in GAME.kBaseAttributes + GAME.kMoreAttributes:
                    self.attrs[a] = 'a:'+a
                    if a in attrs:
                        self.valuemgr.SetValue('a:'+a, str(attrs[a]))
                    else:
                        self.valuemgr.SetValue('a:'+a, '')
                aorder = GAME.kBaseAttributes.xlist()
                aorder.extend(GAME.kBaseAttributes)
                aorder.extend(GAME.kMoreAttributes)
                self.curattrs.SetItems(self.attrs, aorder)
                # skills
                self.skills = dict()
                for s in sorder:
                    if s in skills:
                        self.skills[s] = 's:'+s
                        self.valuemgr.SetValue('s:'+s, str(skills[s]))
                self.curskills.SetItems(self.skills, sorder)
                # skill list
                self.skillnames = xlist(xlate='skill:')
                self.newskills.Clear()
                for s in slist:
                    if s not in skills:
                        self.newskills.Append(P_('skill:',s))
                        self.skillnames.append(s)
                # inventory list
                self.inventory.Clear()
                # done
                changed = True

            if char is not None and (self.char.IsChanged(self) or self.char != self.curchar):
                wield  = char.GetWielded()
                worn   = char.GetWorn()
                items  = char.GetInventory()
                items.extend(wield.values())
                items.extend(worn.keys())

                # inventory list
                self.invitems = xdict(xlatekey=None)
                self.inventory.Clear()
                ilist = list()
                for i in items:
                    if i is None: continue
                    desc = i.ShortDesc()
                    self.invitems[desc] = i
                    ilist.append(desc)
                ilist.sort()
                for i in ilist:
                    self.inventory.Append(i)

            self.curchar = char
            return changed


###############################################################################
