###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Mode Windows
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import re
import wx
import wx.html
import deity.widgets

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class Mode(wx.Panel):
    kModifierRe	= re.compile(r'^[\+\-]\d+$')

    def __init__(self, parent, app, **kwds):
        wx.Panel.__init__(self, parent, **kwds)
        self.app = app
        self.cdata = None
        self.alias = dict()

        self.commandForce = False


    def SetClientData(self, data):
        self.cdata = data


    def GetClientData(self):
        return self.cdata


    def GetSelectedCharacter(self):
        char = self.app.curchar.Alter()
        if char is None:
            raise NotPossible(_('no character selected'))
        return char


    #--------------------------------------------------------------------------


    def DoDismount(self, cmd, *opts):
        '''Dismount from current mount.
        use: %(cmd)s'''

        #print 'DoDismount:',cmd,opts
        # verify and extract options
        if len(opts) != 0:
            raise BadParameter(_('incorrect number of parameters'))
        char = self.GetSelectedCharacter()
        mount = char.GetMount()
        if mount is None:
            raise NotPossible(_('%(name)s is not mounted') % {'name':char.Name()})
        char.Dismount()
        Output(_('%(name)s has dismounted from %(mount)s') % {'name':char.Name(), 'mount':mount.Name()})


    # Change displayed character (only changes parent app)
    def DoDisplay(self, cmd, *opts):
        '''Display a specific charcter in the information window.
        use: %(cmd)s <character-name>'''

        #print "Mode::Display:",opts
        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        name = opts[0]
        # do alias lookup
        if name in self.alias:
            name = self.alias[name]
        # verify existence
        char = GAME.MatchCharacterName(name)
        # make it so
        self.app.curchar.Set(char)
        Output(_('%(name)s is now displayed') % {'name':char.Name()})


    def DoForce(self, cmd, *opts):
        '''Force a command to succeed (overrides standad limitations)
        use: %(cmd)s <command> [options] [...]'''

        #print "Combat::Force:",cmd,opts
        # verify and extract options
        if not 1 <= len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        # make it so
        self.commandForce = True
        GAME.SetForce(True)
        try:
            self.app.CommandDispatch(opts[0], *opts[1:])
        finally:
            self.commandForce = False
            GAME.SetForce(False)


    def DoGenerate(self, cmd, *opts):
        '''Generate a new character of a given type and place it in a specific group with a certain name.
        - If the group is omitted, the "generated" group will be used.
        - If the name is omitted, a name based on the type will be created.
        use: %(cmd)s <character-type> [group] [name]'''

        #print "Mode::DoGenerate:",cmd,opts
        # verify and extract options
        if not 1 <= len(opts) <= 3:
            raise BadParameter(_('incorrect number of parameters'))
        ctype = XLookup(GAME.charTypeDict.keys(), opts[0], N_("character type"))
        cgrp  = 'encounter'
        cname = None
        if len(opts) > 1 and opts[1]: cgrp  = opts[1]
        if len(opts) > 2 and opts[2]: cname = opts[2]
        # generate name
        if not cname:
            base = ctype.xval()
            existing = list()
            for n in GAME.gameCharNames.Get().iterkeys():
                existing.append(n.lower())
            for index in xrange(1,100):
                cname = '%s%02d' % (base, index)
                if cname.lower() not in existing:
                    break
            else:
                raise NotPossible(_('already have 100 unnamed characters of type "%(chartype)s"')
                                  % {'chartype':ctype.xval()})
        # make it so
        char = GAME.Character(None)
        char.Generate(ctype, cname)
        GAME.AddCharacter(char, cgrp)
        self.app.curchar.Set(char)
        Output(_('created character of type "%(type)s" named "%(name)s" under "%(group)s"')
               % {'type':ctype.xval(), 'name':char.Name(), 'group':cgrp})


    def DoLet(self, cmd, *opts):
        '''Let another character other than the currently displayed one perform an action.
        use: %(cmd)s <character-name> <command> [options] [...]'''

        #print "Combat::Let:",cmd,opts
        # verify and extract options
        if not 2 <= len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        name = opts[0].lower()
        if name in self.alias:
            name = self.alias[name]
        achar = GAME.MatchCharacterName(name)
        # make it so
        oldchar = self.app.curchar.Get()
        self.app.curchar.Set(achar)
        self.curname = achar.Name()
        try:
            self.app.CommandDispatch(opts[1], *opts[2:])
        finally:
            self.app.curchar.Set(oldchar)
            if oldchar is not None:
                self.curname = oldchar.Name()

    def DoImprove(self, cmd, *opts):
        '''Improve a character's skill.
        use: %(cmd)s <skill> [+/-modifier] [roll]'''

        # verify and extract options
        if not 1 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        request  = None
        roll     = None
        modifier = None
        for o in opts:
            if request is None and o.isalpha():
                request = o
            elif modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})

        char = self.GetSelectedCharacter()
        skill = XLookup(GAME.kSkillInfo.keys(), request, N_('skill'))
        self.lastskilltest = skill
        char.SetPrompt(True)
        GAME.SkillImprove(char, skill, modifier=modifier, roll=roll, open=True)


    def DoMenuImprove(self, testdialogactions=None):
        skills = xlist(GAME.kSkillInfo.keys(), 'skill:')
        d = deity.dialogs.TestAbilityDialog(
            charlist=[c.Name() for c in self.charList.DisplayedChars()],
            abilitylist=skills,
            chardefault=(self.app.curchar.Get() and self.app.curchar.Get().Name()),
            abilitydefault=self.lastskilltest,
            allowall=True,
            text=N_('please select the skill being improved and the character performing that test.'),
            title=P_('title:skill test')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        char  = d.GetCharacter()
        skill = d.GetAbility()
        mod   = d.GetModifier()
        roll  = d.GetRoll()
        if mod and mod[0] != '-' and mod[0] != '+':
            mod = '+' + mod
        self.lastskilltest = skill
        self.app.CommandExecute(P_('cmd:improve'), char, skill.xval(), mod, roll)


    def DoMount(self, cmd, *opts):
        '''Mount another creature.
        use: %(cmd)s <mount>'''

        #print 'DoMount:',cmd,opts
        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        char = self.GetSelectedCharacter()
        mount = GAME.MatchCharacterName(strx(opts[0]))
        char.MountCharacter(mount)
        Output(_('%(name)s has mounted %(mount)s') % {'name':char.Name(), 'mount':mount.Name()})


    def DoUnwear(self, cmd, *opts):
        '''Remove an item currently being worn.
        The removed item goes into inventory.
        use: %(cmd)s <item>'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        item = strx(opts[0])
        char = self.GetSelectedCharacter()
        # find item in worn
        worn = char.GetWorn()
        items = xdict()
        for i in worn.iterkeys():
            items[i.Name()] = i
        item = XLookup(items.keys(), item, N_('worn item'))
        item = items[item]
        # unwear it
        char.UnwearItem(item)
        Output(_('%(char)s is no longer wearing "%(item)s"') % {'char':char.Name(), 'item':item.Name()})


    def DoWear(self, cmd, *opts):
        '''Wear an item currently in inventory.
        use: %(cmd)s <item>'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        char = self.GetSelectedCharacter()
        item = strx(opts[0])
        # find item in inventory
        item = char.SearchInventory(item)
        # wear it
        char.WearItem(item)
        Output(_('%(char)s is now wearing "%(item)s"') % {'char':char.Name(), 'item':item.Name()})


    def DoUnwield(self, cmd, *opts):
        '''Remove an item currently being wielded.
        The removed item goes into inventory.
        use: %(cmd)s <item> [location]'''

        # verify and extract options
        if not 1 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        item = strx(opts[0])
        locx = None
        if len(opts) > 1:
            locx = strx(opts[1])
        char = self.GetSelectedCharacter()
        # find item in wielded
        item = char.SearchWielded(item, locx)
        # unwield it
        location = char.UnwieldItem(item, locx)
        Output(_('%(char)s is no longer wielding "%(item)s" in %(loc)s') %
               {'char':char.Name(), 'item':item.Name(), 'loc':location.xval()})


    def DoWield(self, cmd, *opts):
        '''Wield an item currently in inventory.
        use: %(cmd)s <item> [location]'''

        # verify and extract options
        if not 1 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        item = strx(opts[0])
        locx  = ''
        if len(opts) > 1:
            locx = strx(opts[1])
        char = self.GetSelectedCharacter()
        # find item in inventory
        item = char.SearchInventory(item)
        # wield it
        location = char.WieldItem(item, locx)
        Output(_('%(char)s has wielded "%(item)s" in %(loc)s') %
               {'char':char.Name(), 'item':item.Name(), 'loc':location.xval()})


    def Activate(self, prompt=''):
        #print 'Mode::Activate',prompt
        self.app.CommandReset()
        self.app.CommandPrompt(prompt)


    def Deactivate(self):
        pass


###############################################################################


class Charedit(Mode):


    def __init__(self, parent, app, **kwds):
        Mode.__init__(self, parent, app, **kwds)
        # list of known characters
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.charList = GAME.CharListWidget(self, self.app.curchar, style=wx.LB_SINGLE)
        self.Bind(wx.EVT_LISTBOX, self.OnCharSelect, self.charList)
        self.sizer.Add(self.charList, 0, wx.EXPAND)
        # character editing
        self.charEdit = GAME.CharEditWidget(self, self.app.curchar)
        self.sizer.Add(self.charEdit, 1, wx.EXPAND)
        # character generation
        self.charGenerate = GAME.CharGenerateWidget(self, style=wx.SIMPLE_BORDER)
        self.charGenerate.Bind(wx.EVT_BUTTON, self.OnGenerate)
        self.sizer.Add(self.charGenerate, 0, wx.EXPAND)
        # put it all together
        self.SetSizerAndFit(self.sizer)


    def DisplayRefresh(self):
        #print "Charedit::DisplayRefresh"
        changed = False

        changed |= self.charList.DisplayRefresh()
        changed |= self.charEdit.DisplayRefresh()

        # Update character statistics display.
        #if self.app.currentChar.IsChanged(self):
            #changed = True
            #maxwidth = 0
            # make sure it shows selected
            #self.charList.SetStringSelection(self.app.currentChar.Get())

        #if changed:
        #    self.Fit()

        return changed


    def OnCharSelect(self, ev):
        #print "ChareditMode::OnCharSelect:",ev
        char = self.charList.GetSelection()
        if not char: return
        name = char.Name()
        self.app.CommandExecute(P_('cmd:display'),name)


    def OnGenerate(self, ev):
        sel = self.charGenerate.GetStringSelection()
        if not sel: return
        self.app.CommandExecute(P_('cmd:generate'), sel, _('generated'))


    def Activate(self):
        Mode.Activate(self, P_('mode:charedit'))
        self.app.CommandRegister(P_('cmd:delete'), self.DoDelete, 'chars', P_('menuitem:&delete...'), self.DoMenuDelete)
        self.app.CommandRegister(P_('cmd:dismount'), self.DoDismount)
        self.app.CommandRegister(P_('cmd:display'), self.DoDisplay)
        self.app.CommandRegister(P_('cmd:force'), self.DoForce)
        self.app.CommandRegister(P_('cmd:generate'), self.DoGenerate)
        self.app.CommandRegister(P_('cmd:group'), self.DoGroup)
        self.app.CommandRegister(P_('cmd:improve'), self.DoImprove, 'chars', P_('menuitem:skill &improve...'), self.DoMenuImprove)
        self.app.CommandRegister(P_('cmd:let'), self.DoLet)
        self.app.CommandRegister(P_('cmd:mount'), self.DoMount)
        self.app.CommandRegister(P_('cmd:rename'), self.DoRename)
        self.app.CommandRegister(P_('cmd:select'), self.DoDisplay)
        self.app.CommandRegister(P_('cmd:show'), self.DoDisplay)
        self.app.CommandRegister(P_('cmd:unwear'), self.DoUnwear)
        self.app.CommandRegister(P_('cmd:unwield'), self.DoUnwield)
        self.app.CommandRegister(P_('cmd:wear'), self.DoWear)
        self.app.CommandRegister(P_('cmd:wield'), self.DoWield)


    #-------------------------------------------------------------------------#


    def DoDelete(self, cmd, *names):
        '''Removes characters from the database.
        - name: name of character to remove (repeatable)
        use: %(cmd)s <character-name> [...]'''

        # verify and extract options
        if not 1 <= len(names):
            raise BadParameter(_('incorrect number of parameters'))
        remain = list(names)
        groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
        for g in groups:
            for i in g.GetInventory():
                for n in names:
                    if i.Name().lower() == n.lower():
                        remain.remove(n)
                        confirm = Query(_('remove %s from character list?') % i.Name(), title=_('remove character'))
                        if confirm:
                            GAME.SubCharacter(i)
                            Output(_('character %s has been removed') % i.Name())
        if remain:
            Output(_('could not find characters: %s') % ', '.join(remain), '@error')


    def DoGroup(self, cmd, *opts):
        '''Assign the current character to a specific group.
        use: %(cmd)s <group>'''
        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        char = self.GetSelectedCharacter()
        gname = opts[0]
        # lookup group
        try:
            gname = GAME.MatchCharacterGroup(gname.lower())
        except BadParameter as e:
            if not self.commandForce: raise
        # make it so
        GAME.AddCharacter(char, gname)
        Output(_('%(name)s is now in group "%(group)s"') % {'name':char.Name(), 'group':gname})


    def DoRename(self, cmd, *opts):
        '''Change the name of currently selected character.
        use: %(cmd)s <new-name>'''
        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        char = self.GetSelectedCharacter()
        name = opts[0]
        # make it so
        oldname = char.Name()
        char.Rename(name)
        GAME.UpdateGameChars()
        Output(_('%(oldname)s is now named "%(newname)s"') % {'oldname':oldname, 'newname':name})


    def DoMenuDelete(self, testdialogactions=None):
        names = list()
        groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
        for g in groups:
            for i in g.GetInventory():
                names.append(i.Name())
        d = deity.dialogs.ChooseOneDialog(
            names,
            text=N_('please select character to be deleted.'),
            title=P_('title:delete')
        )
        name = d.Run(testdialogactions=testdialogactions)
        if name is None:
            return
        self.app.CommandExecute(P_('cmd:delete'), name)


###############################################################################


class Explore(Mode):


    def __init__(self, parent, app, **kwds):
        Mode.__init__(self, parent, app, **kwds)

        self.lastskilltest = None

        # list of known characters
        self.sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        self.charList = GAME.CharListWidget(self, self.app.curchar, status=True, style=wx.LB_SINGLE)
        self.Bind(wx.EVT_LISTBOX, self.OnCharChange, self.charList)
        self.sizer1.Add(self.charList, 0, wx.EXPAND)
        # character visual
        self.charVisual = GAME.CharVisualWidget(self, self.app.curchar)
        self.sizer1.Add(self.charVisual, 1, wx.EXPAND)
        # character stats
        self.charStats = GAME.CharStatsWidget(self, self.app.curchar)
        self.sizer1.Add(self.charStats, 0, wx.EXPAND)

        # current environment
        self.sizer2 = wx.BoxSizer(wx.VERTICAL)
        self.sizer4 = wx.BoxSizer(wx.HORIZONTAL)
        self.envLabel = wx.StaticText(self, wx.ID_ANY, label=' '+P_('heading:environ')+': ', style=wx.ALIGN_CENTER_VERTICAL)
        self.sizer4.Add(self.envLabel, 0, wx.EXPAND)
        self.envSet = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY)
        self.Bind(wx.EVT_COMBOBOX, self.OnEnvironChange, self.envSet)
        self.sizer4.Add(self.envSet, 1, wx.EXPAND)
        self.sizer2.Add(self.sizer4, 0, wx.EXPAND)
        self.envDesc = wx.html.HtmlWindow(self, wx.ID_ANY, size=(250,-1), style=wx.SUNKEN_BORDER)
        self.sizer2.Add(self.envDesc, 1, wx.EXPAND)
        self.sizer3 = wx.BoxSizer(wx.HORIZONTAL)
        self.btnPlus = wx.Button(self, wx.ID_ANY, label='+')
        self.btnPlus.Bind(wx.EVT_BUTTON, self.OnPlus)
        self.sizer3.Add(self.btnPlus, 1, wx.EXPAND)
        self.btnMinus = wx.Button(self, wx.ID_ANY, label='-')
        self.btnMinus.Bind(wx.EVT_BUTTON, self.OnMinus)
        self.sizer3.Add(self.btnMinus, 1, wx.EXPAND)
        self.sizer2.Add(self.sizer3, 0, wx.EXPAND)
        self.sizer1.Add(self.sizer2, 0, wx.EXPAND)

        # character generation
        #self.charGenerate = GAME.CharGenerateWidget(self, style=wx.SIMPLE_BORDER)
        #self.charGenerate.Bind(wx.EVT_BUTTON, self.OnGenerate)
        #self.sizer1.Add(self.charGenerate, 0, wx.EXPAND)
        # put it all together
        self.SetSizerAndFit(self.sizer1)


    def OnCharChange(self, ev):
        #print "OnCharChange:",self.charList.GetStringSelection()
        char = self.charList.GetStringSelection()
        if char is None: return
        char = char.strip()
        self.app.CommandExecute(P_('cmd:display'), char)


    def OnEnvironChange(self, ev):
        #print 'OnEnvironChange:',self.envSet.GetStringSelection()
        if self.app.commandRunning:
            return
        env = self.envSet.GetStringSelection()
        env = self.envMap[env]
        self.app.CommandExecute(P_('cmd:environ'), env)


    def OnGenerate(self, ev):
        sel = self.charGenerate.GetStringSelection()
        #print 'Combat::OnGenerate:',sel,GAME.charTypeDict
        if not sel: return
        self.app.CommandExecute(P_('cmd:generate'), sel, _('encounter'))


    def OnMinus(self, ev):
        self.app.CommandExecute('-')


    def OnPlus(self, ev):
        self.app.CommandExecute('+')


    def DisplayRefresh(self):
        #print "DisplayRefresh: Explore"
        changed = False

        changed |= self.charList.DisplayRefresh()
        changed |= self.charVisual.DisplayRefresh()
        changed |= self.charStats.DisplayRefresh()

        if deity.world.CurWorld.IsChanged(self):
            self.envSet.Clear()
            self.envMap = dict()
            for e in WORLD.kEnvironments:
                self.envMap[P_('env:',e)] = e
            envlist = self.envMap.keys()
            envlist.sort()
            for e in envlist:
                self.envSet.Append(e)
            self.envSet.SetStringSelection(WORLD.EnvironGet().xval())

        desc = WORLD.EnvironGetHtml()
        #print 'SetPage:',desc
        self.envDesc.SetPage(desc)

        return changed


    def DoDelete(self, cmd, *names):
        '''Removes characters from the database.
        - name: name of character to remove (repeatable)
        use: %(cmd)s <character-name> [...]'''

        # verify and extract options
        if not 1 <= len(names):
            raise BadParameter(_('incorrect number of parameters'))
        remain = list(names)
        groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
        for g in groups:
            for i in g.GetInventory():
                for n in names:
                    if i.Name().lower() == n.lower():
                        remain.remove(n)
                        confirm = Query(_('remove %s from character list?') % i.Name(), title=_('remove character'))
                        if confirm:
                            GAME.SubCharacter(i)
                            Output(_('character %s has been removed') % i.Name())
        if remain:
            Output(_('could not find characters: %s') % ', '.join(remain), '@error')


    def DoDate(self, cmd, *opts):
        '''Set the current date in the world.
        use: %(cmd)s yyyy-mm-dd'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        date = opts[0]
        WORLD.TimeSetDate(date)


    def DoEncounter(self, cmd, *opts):
        '''Generate an encounter.
        - If no specific environment is specified, the current one will be used.
        use: %(cmd)s [environment[+|-]]'''

        # verify and extract options
        if not 0 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        env = None
        if len(opts) > 0:
            env = WORLD.EnvironLookup(opts[0])
        enc = WORLD.EncounterGetOutput(environ=env)
        if not enc:
            enc = _('no encounter')
        Output(enc)


    def DoEnviron(self, cmd, *opts):
        '''Set the current environment.
        A "+" or "-" modifier will indicate "lawful" or "lawless", respectively.
        use: %(cmd)s <environment>[+|-]'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        env = WORLD.EnvironLookup(opts[0])
        WORLD.EnvironSet(env)
        self.envSet.SetStringSelection(env.xval())
        Output(_('environment is now: %(env)s') % {'env':env.xval()})


    def DoTest(self, cmd, *opts):
        '''Make a skill test (used for learned abilities like climbing or swimming).
        use: %(cmd)s <skill> [+/-modifier] [roll]'''

        # verify and extract options
        if not 1 <= len(opts) <= 3:
            raise BadParameter(_('incorrect number of parameters'))
        request  = None
        roll     = None
        modifier = None
        for o in opts:
            if request is None and o.isalpha():
                request = o
            elif modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})

        char = self.GetSelectedCharacter()
        try:
            skill = XLookup(char.GetSkills().keys(), request, N_('skill'))
        except BadParameter:
            if not self.commandForce:
                raise
            skill = XLookup(GAME.kSkillInfo.keys(), request, N_('skill'))
        self.lastskilltest = skill
        char.SetPrompt(False)
        GAME.SkillCheck(char, skill, modifier=modifier, roll=roll)


    def DoTestAll(self, cmd, *opts):
        '''Make a skill check for all displayed characters.
        This is useful when checking which characters of a party notice a particular event.
        Results are sorted with the most successful results at the top.
        use: %(cmd)s <skill> [+/-modifier]'''

        # verify and extract options
        if not 1 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        request  = None
        modifier = None
        for o in opts:
            if modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif request is None and o.isalpha():
                request = o
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})

        skillset = set()
        for char in self.charList.DisplayedChars():
            skillset |= set(char.GetSkills().keys())
        skills = GAME.kSkillInfo.xkeylist()
        skills.extend(skillset)
        skill = XLookup(skills, request, N_('skill'))

        results = dict()
        for char in self.charList.DisplayedChars():
            char.SetPrompt(False)
            try:
                result = GAME.SkillCheck(char, skill, modifier=modifier)
                if result in results:
                    results[result] += ', ' + char.Name()
                else:
                    results[result] = char.Name()
            except (BadParameter, NotPossible) as e:
                Output('%s: %s' % (char.Name(), str(e)))

        Output('')
        rlist = results.keys()
        rlist.sort()
        for r in rlist:
            Output('%s: %s' % (r, results[r]))


    def DoMenuDelete(self, testdialogactions=None):
        names = list()
        groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
        for g in groups:
            for i in g.GetInventory():
                names.append(i.Name())
        d = deity.dialogs.ChooseOneDialog(
            names,
            text=N_('please select character to be deleted.'),
            title=P_('title:delete')
        )
        name = d.Run(testdialogactions=testdialogactions)
        if name is None:
            return
        self.app.CommandExecute(P_('cmd:delete'), name)


    def DoMenuTest(self, testdialogactions=None):
        skills = xlist(GAME.kSkillInfo.keys(), 'skill:')
        d = deity.dialogs.TestAbilityDialog(
            charlist=[c.Name() for c in self.charList.DisplayedChars()],
            abilitylist=skills,
            chardefault=(self.app.curchar.Get() and self.app.curchar.Get().Name()),
            abilitydefault=self.lastskilltest,
            allowall=True,
            text=N_('please select the skill being tested and the character performing that test.'),
            title=P_('title:skill test')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        char  = d.GetCharacter()
        skill = d.GetAbility()
        mod   = d.GetModifier()
        roll  = d.GetRoll()
        if mod and mod[0] != '-' and mod[0] != '+':
            mod = '+' + mod
        self.lastskilltest = skill
        if char == P_('select:all'):
            self.app.CommandExecute(P_('cmd:alltest'), skill.xval(), mod)
        else:
            self.app.CommandExecute(P_('cmd:test'), char, skill.xval(), mod, roll)


    def DoTime(self, cmd, *opts):
        '''Set the current time in the world.
        use: %(cmd)s hh:mm[:ss[.p]]'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        time = opts[0]
        WORLD.TimeSetTime(time)

        time = WORLD.TimeGetCurrent()
        for char in GAME.gameCharNames.Get().itervalues(): #self.charList.DisplayedChars():
            GAME.Update(char, time)

        self.app.curchar.Alter()


    def DoTimeAdjust(self, cmd, *opts):
        '''Adjust the current world time forward or backward ("+" or "-", respectively) by a given number of units (months, days, minutes, etc.).
        Adding a trailing colon (":") will trim the new time to be an even multiple of the adjustment.  For example, incrementing by minutes will trim off all seconds.
        This command is actually quite flexible in the format; units can be abbreviated and spaces between <num>, <unit>, and colon can be omitted.
        Multiple adjustments can be included in a single command: "+1month: +12h -10min" will advace to 10 minutes before noon on the first day of next month.
        use: +/- <num> <unit> [:] [[+/-] <num> <unit> [:]] [...]'''

        if cmd == '=': cmd = '+'
        if cmd == '_': cmd = '-'
        cmd += ' '.join(opts)
        WORLD.TimeAdjust(cmd)
        Output(WORLD.TimeGetOutput(WORLD.kStandardDateTimeFormat))

        time = WORLD.TimeGetCurrent()
        for char in GAME.gameCharNames.Get().itervalues(): #self.charList.DisplayedChars():
            GAME.Update(char, time)

        self.app.curchar.Alter()


    def DoTreat(self, cmd, *opts):
        '''Treat a wound on a character.
        Some sort of medical knowledge in the character doing the treatment is useful.
        - If the wound is omitted, the most grevious wound will be treated first.
        use: %(cmd)s <recipient> [wound] [+/-modifier] [roll]'''

        # verify and extract options
        recipient= None
        wound    = None
        roll     = None
        modifier = None
        for o in opts:
            if recipient is None:
                recipient = o
            elif modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            elif wound is None:
                wound = o
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        if recipient is None:
            raise BadParameter(_('incorrect number of parameters'))
        char = self.GetSelectedCharacter()
        recip = GAME.MatchCharacterName(recipient)

        time = WORLD.TimeGetCurrent()
        char.SetPrompt(False)
        recip.SetPrompt(False)
        GAME.WoundTreatment(recip, char, wound=wound, modifier=modifier, roll=roll, time=time['abs'])


    def Activate(self):
        Mode.Activate(self, P_('mode:explore'))
        self.app.CommandRegister('+', self.DoTimeAdjust)
        self.app.CommandRegister('=', self.DoTimeAdjust)
        self.app.CommandRegister('-', self.DoTimeAdjust)
        self.app.CommandRegister('_', self.DoTimeAdjust)
        self.app.CommandRegister(P_('cmd:alltest'), self.DoTestAll)
        self.app.CommandRegister(P_('cmd:date'), self.DoDate)
        self.app.CommandRegister(P_('cmd:delete'), self.DoDelete, 'chars', P_('menuitem:&delete...'), self.DoMenuDelete)
        self.app.CommandRegister(P_('cmd:dismount'), self.DoDismount)
        self.app.CommandRegister(P_('cmd:display'), self.DoDisplay)
        self.app.CommandRegister(P_('cmd:encounter'), self.DoEncounter, 'explore', P_('menuitem:force &encounter'))
        self.app.CommandRegister(P_('cmd:environ'), self.DoEnviron)
        self.app.CommandRegister(P_('cmd:force'), self.DoForce)
        self.app.CommandRegister(P_('cmd:generate'), self.DoGenerate)
        self.app.CommandRegister(P_('cmd:improve'), self.DoImprove, 'chars', P_('menuitem:skill &improve...'), self.DoMenuImprove)
        self.app.CommandRegister(P_('cmd:let'), self.DoLet)
        self.app.CommandRegister(P_('cmd:mount'), self.DoMount)
        self.app.CommandRegister(P_('cmd:select'), self.DoDisplay)
        self.app.CommandRegister(P_('cmd:show'), self.DoDisplay)
        self.app.CommandRegister(P_('cmd:test'), self.DoTest, 'explore', P_('menuitem:skill &test...'), self.DoMenuTest)
        self.app.CommandRegister(P_('cmd:time'), self.DoTime)
        self.app.CommandRegister(P_('cmd:treat'), self.DoTreat)
        self.app.CommandRegister(P_('cmd:unwear'), self.DoUnwear)
        self.app.CommandRegister(P_('cmd:unwield'), self.DoUnwield)
        self.app.CommandRegister(P_('cmd:wear'), self.DoWear)
        self.app.CommandRegister(P_('cmd:wield'), self.DoWield)


###############################################################################


class Combat(Mode):
    kHitLocRe	= re.compile(r'^0?[A-Za-z]+$')

    kClearOpts	= xlist((NP_('cmdopt:encounter'),NP_('cmdopt:ground')),
                        xlate='cmdopt:')

    def __init__(self, parent, app, **kwds):
        Mode.__init__(self, parent, app, **kwds)
        self.alias		= dict()
        self.submode		= ''
        self.actions		= dict()
        self.app		= app
        self.cmdsuccess		= False
        self.lastattrtest	= None
        self.lastskilltest	= None
        self.selections         = set()

        self.sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        # list of known characters (with "Fight!" button)
        self.sizer2 = wx.BoxSizer(wx.VERTICAL)
        self.charList = GAME.CharListWidget(self, self.app.curchar, status=True, style=wx.LB_MULTIPLE)
        self.Bind(wx.EVT_LISTBOX, self.OnCharSelect, self.charList)
        self.sizer2.Add(self.charList, 1, wx.EXPAND)
        self.charAction = wx.Button(self, wx.ID_ANY, label=_('begin round'))
        self.charAction.Bind(wx.EVT_BUTTON, self.OnAction)
        self.sizer2.Add(self.charAction, 0, wx.EXPAND)
        self.sizer1.Add(self.sizer2, 0, wx.EXPAND)
        # character visual
        self.charVisual = GAME.CharVisualWidget(self, self.app.curchar)
        self.sizer1.Add(self.charVisual, 1, wx.EXPAND)
        # character stats
        self.charStats = GAME.CharStatsWidget(self, self.app.curchar)
        self.sizer1.Add(self.charStats, 0, wx.EXPAND)
        # combat stats and actions
        self.charCombat = GAME.CombatAttackWidget(self, self.app.curchar)
        self.sizer1.Add(self.charCombat, 0, wx.EXPAND)
        # character generation
        self.charGenerate = GAME.CharGenerateWidget(self, style=wx.SIMPLE_BORDER)
        self.charGenerate.Bind(wx.EVT_BUTTON, self.OnGenerate)
        self.sizer1.Add(self.charGenerate, 0, wx.EXPAND)
        # put it all together
        self.SetSizerAndFit(self.sizer1)


    def DisplayRefresh(self):
        #print 'Combat::DisplayRefresh:',self.app.curchar,self.app.curchar.Get()
        changed = False
        self.letchar = None
        if self.submode == 'round':
            changed |= self.DisplayRefreshRound()
        else:
            self.app.CommandDefault(None)

        changed |= self.charList.DisplayRefresh()
        changed |= self.charVisual.DisplayRefresh()
        changed |= self.charCombat.DisplayRefresh()
        changed |= self.charStats.DisplayRefresh()
        changed |= self.charGenerate.DisplayRefresh()

        return changed


    def OnAction(self, ev):
        if self.submode == 'round':
            self.app.CommandExecute(P_('cmd:done'))
        else:
            self.app.CommandExecute(P_('cmd:round'))


    def OnCharSelect(self, ev):
        #print "CombatMode::OnCharSelect:", self.selections, self.charList.GetSelections()
        chars = self.charList.GetSelections()
        toadd = list()
        tosub = set([x.Name() for x in self.selections])
        for char in chars:
            if char not in self.selections:
                toadd.append(char.Name())
            else:
                tosub.remove(char.Name())
        self.selections = chars

        if tosub:
            self.app.CommandExecute(P_('cmd:subtract'), ' '.join(tosub))
        if toadd:
            self.app.CommandExecute(P_('cmd:add'), ' '.join(toadd))


    def OnGenerate(self, ev):
        sel = self.charGenerate.GetStringSelection()
        #print 'Combat::OnGenerate:',sel,GAME.charTypeDict
        if not sel: return
        self.app.CommandExecute(P_('cmd:generate'), sel, _('encounter'))


    def DoAdd(self, cmd, *opts):
        '''Add a character to combat.
        use: %(cmd)s <character-name|alias> [...]'''

        #print "CombatMode::Add:",cmd,opts
        # verify and extract options
        if not 1 <= len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        for opt in opts:
            name = opt.lower()
            if name in self.alias:
                name = self.alias[name]
            achar = GAME.MatchCharacterName(name)
            aname = achar.Name()
            # make it so
            self.charList.SetStringSelection(aname)
            self.app.curchar.Set(achar)
            Output(_('%(name)s has been added to combat') % {'name':aname})
            GAME.CombatEnter(achar)


    def DoAlias(self, cmd, *opts):
        '''Define another name (usually just a couple characters) by which a character can be referred.
        This avoids having to type long names during combat when several participants all start with the same sequence (common for generated encounters).
        use: %(cmd)s <character-name> <alias>'''

        #print "CombatMode::Alias:",cmd,opts
        # verify and extract options
        if not 2 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        name  = opts[0]
        achar = GAME.MatchCharacterName(name)
        aname = achar.Name()
        alias = opts[1].lower()
        # make it so
        if alias in self.alias:
            self.charList.SetAlias(self.alias[alias], None)
        self.charList.SetAlias(aname, alias)
        self.alias[alias] = aname
        Output(_('%(name)s can now be referred to as "%(alias)s".') % {'name':aname, 'alias':alias})


    def DoClear(self, cmd, *opts):
        '''Removes things left over from previous combat rounds.
        - encounter: clears all characters in the "encounter" group
        - ground: clears all items on the ground (dropped or fumbled)
        use: %(cmd)s <encounter|ground>'''

        # verify and extract options
        if not 1 <= len(opts) <= 1:
            raise BadParameter(_('incorrect number of parameters'))
        opt = XLookup(self.kClearOpts, strx(opts[0]), 'option')
        if opt == 'encounter':
            groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
            for g in groups:
                if g.Name() == P_('category:encounter'):
                    for i in g.GetInventory():
                        GAME.SubCharacter(i)
            self.selections = set()
            Output(_('characters from previous encounter have been cleared.'))
        elif opt == 'ground':
            ground = GAME.GetId(GAME.kCombatGroundTid)
            for i in ground.GetInventory():
                ground.SubInventory(i)
                i.Delete()
            Output(_('items on the ground have been cleared'))
        else:
            raise Exception, 'unknown clear option "%s" ("%s")' % (opt,opt.xval())


    def DoDisplay(self, cmd, *opts):
        return Mode.DoDisplay(self, cmd, *opts)


    def DoStatus(self, cmd, *opts):
        '''Set the status of a character.
        - standing: free standing and able to act
        - mounted: on a mount and able to act
        - held: restricted movement
        - prone: on the ground
        - shock: conscious but unable to think clearly
        - unconscious: knocked out
        - dead: dead
        use: %(cmd)s <character-name|alias> <state>'''

        #print "Combat:DoStatus:",cmd,opts
        # verify and extract options
        if not 2 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        name = opts[0].lower()
        if name in self.alias:
            name = self.alias[name]
        char = GAME.MatchCharacterName(name)
        # verify state
        state = XLookup(GAME.kCharacterStates, opts[1], N_('character state'))
        # make it so
        char.SetStatus(state)
        Output(_('%(name)s is now %(state)s') % {'name':char.Name(), 'state':state.xval()})


    def DoSubtract(self, cmd, *opts):
        '''Remove a character from combat.
        use: %(cmd)s <character-name|alias> [...]'''

        #print "CombatMode::Subtract:",cmd,opts
        # verify and extract options
        if not 1 <= len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        for opt in opts:
            name = opt.lower()
            if name in self.alias:
                name = self.alias[name]
            achar = GAME.MatchCharacterName(name)
            aname = achar.Name()
            # make it so
            self.charList.SetStringSelection(aname, False)
            Output(_('%(name)s has been removed from combat') % {'name':aname})
            GAME.CombatExit(achar)


    def DoMenuClear(self, testdialogactions=None):
        places = self.kClearOpts
        d = deity.dialogs.ChooseOneDialog(
            places,
            text=N_('please select the section to be cleared.'),
            title=P_('title:clear')
        )
        place = d.Run(testdialogactions=testdialogactions)
        if place is None:
            return
        self.app.CommandExecute(P_('cmd:clear'), place)


    def DoMenuStatus(self, testdialogactions=None):
        charlist=[c.Name() for c in GAME.gameCharDisplay.itervalues()]
        if not charlist:
            Output(_('there are no characters of which to affect the status.'))
            return
        d = deity.dialogs.GeneralSelectDialog(
            ({
                'name':		'char',
                'heading':	NP_('heading:character'),
                'list':		charlist,
                'default':	charlist[0],
                'sort':		False,
            }, {
                'name':		'status',
                'heading':	NP_('heading:character'),
                'xlist':	GAME.kCharacterStates,
                'default':	GAME.kCharacterStates[0].xval(),
                'sort':		False,
            }),
            text=N_('please select a character and the new state that should be assigned'),
            title=P_('title:change status'),
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        char = d.GetValueByName('char')
        stat = d.GetValueByName('status')
        self.app.CommandExecute(P_('cmd:status'),char,stat)


    def Activate(self, submode=''):
        Mode.Activate(self, P_('mode:combat'+submode))
        self.submode = submode

        if submode == 'round':
            self.charAction.SetLabel(P_('btn:end round'))
            self.app.CommandRegister(P_('cmd:aid'), self.DoRoundAid, 'action', P_('menuitem:&aid/heal...'), self.DoRoundMenuAid)
            self.app.CommandRegister(P_('cmd:alias'), self.DoRoundAlias)
            self.app.CommandRegister(P_('cmd:attack'), self.DoRoundAttack)
            self.app.CommandRegister(P_('cmd:busy'), self.DoRoundBusy)
            self.app.CommandRegister(P_('cmd:charge'), self.DoRoundMove)
            self.app.CommandRegister(P_('cmd:check'), self.DoRoundCheck, 'personal', P_('menuitem:attribute &check...'), self.DoRoundMenuCheck)
            self.app.CommandRegister(P_('cmd:die'), self.DoRoundDie, 'personal', P_('menuitem:d&ie'))
            self.app.CommandRegister(P_('cmd:dismount'), self.DoRoundDismount)
            self.app.CommandRegister(P_('cmd:done'), self.DoRoundDone, 'round', P_('menuitem:&end round'))
            self.app.CommandRegister(P_('cmd:draw'), self.DoRoundDraw, 'action', P_('menuitem:&draw...'), self.DoRoundMenuDraw)
            self.app.CommandRegister(P_('cmd:drop'), self.DoRoundDrop, 'action', P_('menuitem:d&rop...'), self.DoRoundMenuDrop)
            self.app.CommandRegister(P_('cmd:end'), self.DoRoundDone)
            self.app.CommandRegister(P_('cmd:extra'), self.DoRoundExtra, 'extra', P_('menuitem:ne&xt action is free'), self.DoRoundMenuExtra)
            self.app.CommandRegister(P_('cmd:force'), self.DoForce)
            self.app.CommandRegister(P_('cmd:grapple'), self.DoRoundAttack)
            self.app.CommandRegister(P_('cmd:grope'), self.DoRoundDraw)
            self.app.CommandRegister(P_('cmd:heal'), self.DoRoundAid)
            self.app.CommandRegister(P_('cmd:hold'), self.DoRoundHold)
            self.app.CommandRegister(P_('cmd:let'), self.DoRoundLet, 'extra', P_('menuitem:&let other character act for free'), self.DoRoundMenuLet)
            self.app.CommandRegister(P_('cmd:make'), self.DoRoundMake)
            self.app.CommandRegister(P_('cmd:missile'), self.DoRoundAttack)
            self.app.CommandRegister(P_('cmd:mount'), self.DoRoundMount)
            self.app.CommandRegister(P_('cmd:move'), self.DoRoundMove, 'action', P_('menuitem:&move'))
            self.app.CommandRegister(P_('cmd:pass'), self.DoRoundPass, 'action', P_('menuitem:&pass'))
            self.app.CommandRegister(P_('cmd:prompt'), self.DoRoundPrompt, 'extra', P_('menuitem:toggle pr&ompting'), self.DoRoundMenuPrompt)
            self.app.CommandRegister(P_('cmd:rise'), self.DoRoundStand)
            self.app.CommandRegister(P_('cmd:stand'), self.DoRoundStand, 'action', P_('menuitem:&stand'))
            self.app.CommandRegister(P_('cmd:test'), self.DoRoundTest, 'personal', P_('menuitem:skill &test...'), self.DoRoundMenuTest)
            self.app.CommandRegister(P_('cmd:trample'), self.DoRoundAttack)
            self.app.CommandRegister(P_('cmd:unwear'), self.DoRoundUnwear, 'adjust', P_('menuitem:remove item...'), self.DoRoundMenuUnwear)
            self.app.CommandRegister(P_('cmd:unwield'), self.DoRoundUnwield, 'adjust', P_('menuitem:&unwield item...'), self.DoRoundMenuUnwield)
            self.app.CommandRegister(P_('cmd:wear'), self.DoRoundWear, 'adjust', P_('menuitem:wear item...'), self.DoRoundMenuWear)
            self.app.CommandRegister(P_('cmd:wield'), self.DoRoundWield, 'adjust', P_('menuitem:&wield item...'), self.DoRoundMenuWield)
            self.app.CommandRegister(P_('cmd:wound'), self.DoRoundWound)
        else:
            self.charAction.SetLabel(P_('btn:begin round'))
            self.app.CommandRegister(P_('cmd:add'), self.DoAdd)
            self.app.CommandRegister(P_('cmd:alias'), self.DoAlias)
            self.app.CommandRegister(P_('cmd:clear'), self.DoClear, 'env', P_('menuitem:&clear...'), self.DoMenuClear)
            self.app.CommandRegister(P_('cmd:display'), self.DoDisplay)
            self.app.CommandRegister(P_('cmd:generate'), self.DoGenerate)
            self.app.CommandRegister(P_('cmd:round'), self.DoRound, 'round', P_('menuitem:begin &round'))
            self.app.CommandRegister(P_('cmd:status'), self.DoStatus, 'char', P_('menuitem:&status...'), self.DoMenuStatus)
            self.app.CommandRegister(P_('cmd:subtract'), self.DoSubtract)


    def Deactivate(self):
        for c in self.charList.GetSelections():
            c.SetPrompt(False)
            GAME.CombatExit(c)


    #---------------------------------------------------------------------#


    def DoRound(self, cmd, *opts):
        self.curname		= None
        self.curindex		= None
        self.roundList		= list()
        self.endRoundList	= list()
        self.participants	= list()

        #print 'DoRound:',cmd,opts,':',self.charList.GetSelections()
        GAME.CombatNewRound()

        counts = {}
        for c in self.charList.GetEntries():
            if not c: continue
            (group, loc) = c.GetPossessor()
            if group.Name() == 'players': continue
            prefix = c.Name()[0].lower()
            if prefix not in counts:
                counts[prefix] = 0
            counts[prefix] += 1
            newalias = '%c%d' % (prefix, counts[prefix])
            curalias = self.alias.get(c.Name(), None)
            if newalias != curalias:
                self.DoAlias(P_('cmd:alias'), c.Name(), newalias)
        for c in self.charList.GetSelections():
            i = c.CombatValue('initiative')
            r = deity.Roll(1, 100) - 1  # to break initiative ties
            c.SetPrompt(int(c.GetValue('x:combatprompt', default='0')))
            self.roundList.append('%03d.%02d/%s/%s'%(i,r,c.tid,c.Name()))
            self.participants.append(c)
        if len(self.roundList) < 2:
            raise NotPossible(_('must select at least two characters to begin a round'))

        self.roundList.sort()
        self.charCombat.SetParticipants(self.participants)
        self.Activate('round')
        Output("\n" + _('combat order:'), '@blue')
        for l in reversed(self.roundList):
            (ini,tid,nam) = l.split('/')
            c = GAME.GetId(tid)
            s = GAME.GetCombatSummary(c)
            if s is None: s = ""
            Output('%s - %s (%s)' % (nam, s, ini))


    def DoRoundAgain(self):
        if self.curindex is None:
            return
        # do nothing if same character is already next
        if self.roundList and self.roundList[-1] == self.curindex:
            return
        # sanity check
        if self.curindex is None:
            raise Exception('self.curindex is None')
        if self.curindex in self.roundList:
            raise Exception('self.curindex is not the last item in the round-list')
        # let same character go again
        self.roundList.append(self.curindex)
        self.curindex = None


    def DoRoundNext(self, extra=None):
        # return attack?
        if extra:
            self.roundList.append('---.--/%s/%s' % (extra.tid, extra.Name()))
        self.curindex = None


    def _PreActionCheck(self):
        char = self.GetSelectedCharacter()
        busy = char.GetInfo('busy')
        if busy:
            self.app.curchar.Alter().SetInfo('busy', None)
            Output(_('%(name)s has interrupted the activity is no longer busy.') % {'name':char.Name()}, '@blue')


    #--------------------------------------------------------------------------#


    def DoRoundAid(self, cmd, *opts):
        '''Provide aid to another character who is wounded.
        - If a location is omitted, the most grevious wound will be selected.
        use: %(cmd)s <recipient> [location] [roll] [+/-modifier]'''

        char = self.GetSelectedCharacter()
        # verify and extract options
        other    = None
        loc      = None
        roll     = None
        modifier = None
        for o in opts:
            #print 'DoRoundAid:',o,other,loc,roll,modifier
            if other is None:
                other = o
            elif modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            elif loc is None and o.isalnum():
                loc = o
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        # convert recipient alias
        if self.alias.has_key(other):
            other = self.alias[other]
        other = GAME.MatchCharacterName(other)
        if char.GetStatus() != 'standing':
            raise NotPossible(_('%(char)s must be standing to do this') % {'char':char.Name()})
        GAME.CombatFirstAid(char, other, locx=loc, roll=roll, mod=modifier)
        return self.DoRoundNext()


    def DoRoundAlias(self, cmd, *opts):
        '''Define another name (usually just a couple characters) by which a character can be referred.
        This avoids having to type long names during combat when several participants all start with the same sequence (common for generated encounters).
        use: %(cmd)s <character-name> <alias>'''

        self.DoAlias(cmd, *opts)
        return self.DoRoundAgain()


    def DoRoundAttack(self, cmd, *opts):
        '''Have the current character attack another character.
        There are many options to this command; it may be easier to use the graphical selections above.
        If this command will not do what is needed for the current game situation, just perform the action by hand and use the "wound" command to apply the final result.
        - opponent: who to attack
        - from: the body part of the attacker that is making the attack (determines weapon) [default: right hand]
        - aspect: what aspect (blunt, point, edge, etc.) of the weapon to use [default: aspect with highest attack value]
        - range: for ranged weapons, how far away (short, medium, long) the target is [default: short]
        - location: the target location or area (high, low) of the attack; prefix with "0" for no penalty [default: any]
        - mod: a modifier to apply to the attack roll; positive numbers are better
        - hit-roll: the result of a manually made to-hit roll (trailing "!" is required)
        - damage-roll: the result of a manually made damage roll (leading "!" is required)
        - "defend": required if the opponent is doing anything in return, including counter maneuvers
        use: %(cmd)s <opponent> [from:[aspect]] [@range] [0][location|area] [+/-mod] [[hit-roll]"!"[damage-roll]] ["defend" <maneuver> [from:[aspect]] [@range] [0][location|area] [+/-mod] [[hit-roll]"!"[damage-roll]]]'''

        #print 'Combat::DoRoundAttack:',cmd,opts
        # verify and extract options
        if not 1 <= len(opts) <= 10:
            raise BadParameter(_('incorrect number of parameters'))
        opts = list(opts)
        opts.reverse()
        opponent= opts.pop().lower()
        ause	= None
        aaspect	= None
        aloc    = None
        arange  = None
        amod	= None
        aroll	= None
        adamage	= None
        defense = None
        duse	= None
        daspect	= None
        dloc    = None
        drange  = None
        dmod	= None
        droll	= None
        ddamage	= None

        # attacker: extract strike-from location
        if opts:
            sep = opts[-1].find(':')
            if sep != -1:
                useaspect = opts.pop()
                ause = useaspect[:sep]
                aaspect = useaspect[sep+1:]
                if aaspect == '': aaspect = None
        # attacker: extract range
        if opts:
            if opts[-1][0] == '@':
                arange = opts.pop()[1:]
        # attacker: extract strike-to location/area
        if opts:
            if self.kHitLocRe.match(opts[-1]) and opts[-1].lower().find(P_('cmd:def')) != 0:
                aloc = opts.pop()
        # attacker: roll modifier
        if opts:
            if self.kModifierRe.match(opts[-1]):
                amod = opts.pop()
        # attacker: extract roll & damage
        if opts:
            sep = opts[-1].find('!')
            if sep != -1:
                rolldamage = opts.pop()
                aroll = rolldamage[:sep]
                adamage = rolldamage[sep+1:]
        if opts and opts[-1].isdigit() and aroll is None:
            aroll = opts.pop()
        # defender: separator
        if opts:
            defopt = opts.pop()
            if defopt.lower().find(P_('cmd:def')) != 0:
                raise BadParameter(_('unknown parameter "%(parm)s"') % {'parm':defopt})
        # defender: maneuver
        if opts:
            defense = opts.pop()
        # defender: extract strike-from location
        if opts:
            sep = opts[-1].find(':')
            if sep != -1:
                useaspect = opts.pop()
                duse = useaspect[:sep]
                daspect = useaspect[sep+1:]
                if daspect == '': daspect = None
        # defender: extract range
        if opts:
            if opts[-1][0] == '@':
                drange = opts.pop()[1:]
        # defender: extract strike-to location/area
        if opts:
            if self.kHitLocRe.match(opts[-1]):
                dloc = opts.pop()
        # defender: roll modifier
        if opts:
            if self.kModifierRe.match(opts[-1]):
                dmod = opts.pop()
        # defender: extract roll & damage
        if opts:
            sep = opts[-1].find('!')
            if sep != -1:
                rolldamage = opts.pop()
                droll = rolldamage[:sep]
                ddamage = rolldamage[sep+1:]
        if opts and opts[-1].isdigit() and droll is None:
            droll = opts.pop()
        # should be finished now
        if opts:
            raise BadParameter(_('unknown parameter "%(parm)s"') % {'parm':opts[-1]})
        # convert opponent alias
        if self.alias.has_key(opponent):
            opponent = self.alias[opponent]
        dchar = GAME.MatchCharacterName(opponent)
        # make it so
        self._PreActionCheck()
        achar = self.GetSelectedCharacter()
        counter = GAME.CombatAttack(achar, dchar,
                                    attackx=cmd,
                                    ausex=ause, aaspectx=aaspect, arangex=arange, alocx=aloc, amod=amod, aroll=aroll, adamage=adamage,
                                    defensex=defense,
                                    dusex=duse, daspectx=daspect, drangex=drange, dlocx=dloc, dmod=dmod, droll=droll, ddamage=ddamage)
        # all done
        self.DoRoundNext(counter)


    def DoRoundBusy(self, cmd, *opts):
        '''Mark this character as "busy" for some amount of time.
        Actions will be discouraged until the busy-time has passed after which the character can act.
        If no units are specified as part of the "busy-time" then it will be considered to be a number of combat rounds.
        use: %(cmd)s <busy-time>'''

        # verify and extract options
        if not 1 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        delay = opts[0]
        unbusy = WORLD.TimeGetCurrent()
        try:
            rounds = int(delay)
            delay = "%d%s" % (rounds * int(GAME.kCombatRoundTime[0]), GAME.kCombatRoundTime[1])
        except ValueError:
            pass
        WORLD.TimeAdjust(delay, unbusy)
        self.GetSelectedCharacter().SetInfo('busy', WORLD.TimeExport(unbusy))
        # don't save this in command history
        self.app.CommandLastClear()
        # see if we'll be able to act later this round
        rtime = WORLD.TimeGetCurrent()
        WORLD.TimeAdjust(''.join(GAME.kCombatRoundTime), rtime)
        if WORLD.TimeCompare(unbusy, rtime) < 0:
            self.DoRoundHold(cmd)  # act later
        else:
            self.DoRoundNext()  # all done


    def DoRoundCheck(self, cmd, *opts):
        '''Make an attribute check (used for physical attributes such as strength or endurance).
        use: %(cmd)s <attribute> [roll] [+/-modifier]'''

        # verify and extract options
        request  = None
        roll     = None
        modifier = None
        for o in opts:
            if modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            elif request is None and o.isalnum():
                request = o
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        attrlist = xlist(GAME.kBaseAttributes, xlate=GAME.kBaseAttributes.xlate)
        attrlist.extend(GAME.kMoreAttributes)
        attr = XLookup(attrlist, request, N_('attribute'))
        result = GAME.AttributeCheck(self.GetSelectedCharacter(), attr, roll, modifier)
        if result:
            result = 's'
        else:
            result = 'f'
        Output(_('%(attr)s check result: %(result)s') % {'attr':attr.xval(), 'result':P_('result:',result)})
        return self.DoRoundAgain()


    def DoRoundDie(self, cmd, *opts):
        '''Mark this character as dead.  This can be undone outside of a combat round with the "status" command.
        use: %(cmd)s'''

        char = self.GetSelectedCharacter()
        char.SetInfo('busy', None)
        char.SetStatus('dead')
        Output(_('%(char)s is now dead') % {'char':char.Name()})
        self.DoSubtract(cmd, char.Name())
        return self.DoRoundNext()


    def DoRoundDismount(self, cmd, *opts):
        '''Attempt to dismount during combat.
        This action generally costs the entire round.
        use: %(cmd)s [+/-modifier] [roll]'''

        # verify and extract options
        roll     = None
        modifier = None
        for o in opts:
            if modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        self._PreActionCheck()
        again = GAME.CombatDismount(self.GetSelectedCharacter(), roll=roll, modifier=modifier)
        return self.DoRoundNext(again)


    def DoRoundDone(self, cmd=None, opts=None):
        '''Finish this combat round without asking for actions of remaining characters.
        use: %(cmd)s'''

        WORLD.TimeAdjust(''.join(GAME.kCombatRoundTime))
        for c in self.charList.GetSelections():
            self.charList.SetInfo(c.Name(), c.GetDisplayStatus())
            if c.GetStatus() == 'dead':
                GAME.CombatExit(c)
                self.charList.SetSelection(c, False)
        self.Activate()


    def DoRoundDraw(self, cmd, *opts):
        '''Attempt to draw a weapon during combat.
        This action costs the entire round whether it is successful or not.
        - item: the weapon to draw (must be in inventory or on the ground)
        - location: the place (typically a hand) in which to hold the weapon [default: right hand]
        use: %(cmd)s <item> [location] [+/-modifier] [roll]'''

        # verify and extract options
        location = strx()
        item     = None
        roll     = None
        modifier = None
        for o in opts:
            if modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            elif item is None and o.isalnum():
                item = strx(o)
            elif not location and o.isalnum():
                location = strx(o)
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        if item is None:
            raise BadParameter(_('no item given'))
        self._PreActionCheck()
        GAME.CombatDraw(self.GetSelectedCharacter(), item, location, roll, modifier)
        return self.DoRoundNext()


    def DoRoundDrop(self, cmd, *opts):
        '''Drop a currently held item (goes on the ground).
        This action takes zero time and allows for further action this round.
        - location: the place from which an item is to be dropped [default: right hand]
        use: %(cmd)s <item> [location]'''

        # verify and extract options
        if not 1 <= len(opts) <= 2:
            raise BadParameter(_('incorrect number of parameters'))
        item = strx(opts[0])
        locx = None
        if len(opts) > 1:
            locx = strx(opts[1])
        char = self.GetSelectedCharacter()
        # find item in wielded
        item = char.SearchWielded(item, locx)
        # unwield it
        location = char.UnwieldItem(item, locx)
        char.SubInventory(item)
        # put it on the ground
        ground = GAME.GetId(GAME.kCombatGroundTid)
        ground.AddInventory(item)
        Output(_('%(char)s has dropped "%(item)s" from %(loc)s') %
               {'char':self.curname, 'item':item.Name(), 'loc':location.xval()})
        self.DoRoundAgain()


    def DoRoundExtra(self, cmd, *opts):
        '''Perform an extra command.
        Using this word before any other command will allow the current character to take another action once this command has completed.
        use: %(cmd)s <command> [option] [...]'''

        if not opts:
            raise BadParameter(_('incorrect number of parameters'))
        curindex = self.curindex
        self.curindex = None
        try:
            self.app.CommandDispatch(opts[0], *opts[1:])
        except Exception:
            self.curindex = curindex
            raise
        self.curindex = curindex


    def DoRoundHold(self, cmd, *opts):
        '''Have the current character hold their action until others have gone.
        If the character who did the "hold" decides to act before that time, it is necessary to have all other characters hold until that character becomes active again.
        use: %(cmd)s'''

        char = self.GetSelectedCharacter()
        busy = char.GetInfo('busy')
        if busy:
            busy = WORLD.TimeImport(busy)['abs']
            char.SetInfo('busy', '')  # can act later this round
        else:
            busy = 0.0
        self.endRoundList.append('%031.15f/%s/%s'%(busy,char.tid,char.Name()))

        return self.DoRoundNext()


    def DoRoundLet(self, cmd, *opts):
        '''Let another character take an action right now.
        Any character can be specified, allowing a character that has already acted this round to act again.
        The action specified here does not prevent that character from acting again later in the round if they have not already used their action.
        use: %(cmd)s <character|alias> <command> [options] [...]'''

        #print "Combat::Let:",cmd,opts
        # verify and extract options
        if not 2 <= len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        name = opts[0].lower()
        if name in self.alias:
            name = self.alias[name]
        achar = GAME.MatchCharacterName(name)
        # make it so
        oldchar = self.GetSelectedCharacter()
        self.app.curchar.Set(achar)
        curindex = self.curindex
        curname = self.curname
        self.curindex = None
        self.curname = achar.Name()
        try:
            self.app.CommandDispatch(opts[1], *opts[2:])
        except Exception:
            self.app.curchar.Set(oldchar)
            self.app.CommandLastClear()
            self.curindex = curindex
            raise
        self.app.curchar.Set(oldchar)
        self.curname = curname
        self.curindex = curindex
        self.app.CommandLastClear()
        return self.DoRoundAgain()


    def DoRoundMake(self, cmd, *opts):
        '''Cause another character to do something.
        use: %(cmd)s <character> <action> [option] [...]'''

        if len(opts) < 2:
            raise BadParameter(_('incorrect number of parameters'))
        name = opts[0]
        # do alias lookup
        if name in self.alias:
            name = self.alias[name]
        # verify existence
        other = GAME.MatchCharacterName(name)
        self._PreActionCheck()
        again = GAME.CombatMake(self.GetSelectedCharacter(), other, *opts[1:])
        return self.DoRoundNext(again)


    def DoRoundMount(self, cmd, *opts):
        '''Attempt to mount during combat.
        This action generally costs the entire round.
        - mount: the name of the beast to mount
        use: %(cmd)s <mount> [+/-modifier] [roll]'''

        #print 'DoRoundMount:',cmd,opts
        # verify and extract options
        if not 1 <= len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        mount = opts[0].lower()
        # do alias lookup
        if mount in self.alias:
            mount = self.alias[mount]
        roll  = None
        modifier = None
        for o in opts[1:]:
            if modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            else:
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        mount = GAME.MatchCharacterName(mount)
        self._PreActionCheck()
        again = GAME.CombatMount(self.GetSelectedCharacter(), mount, roll, modifier)
        return self.DoRoundNext(again)


    def DoRoundMove(self, cmd, *opts):
        '''Indicate that the character is on the move.
        The character will get another chance to act at the end of the round.
        If the character should not get a chance to act, use the "pass" command once the move has been completed.
        use: %(cmd)s'''

        char = self.GetSelectedCharacter()
        status = char.GetStatus()
        if status != 'standing' and status != 'mounted':
            raise NotPossible(_('%(state)s character cannot move') % {'state':status.xval()})
        self._PreActionCheck()
        GAME.CombatMove(char)
        # maybe go after everyone else
        self.DoRoundHold(cmd)


    def DoRoundPass(self, cmd, *opts):
        '''Do no action this round.
        If at a later point in the round, it is decided that this character should act, the "let" command can be used.
        use: %(cmd)s'''

        return self.DoRoundNext()


    def DoRoundPrompt(self, cmd, *opts):
        '''Enable or disable prompting for choices by this character.
        If "on", then a dialog will be presented for various rolls during combat.
        NPCs will usually want this "off" while PCs will usually want this "on" so they can roll the actions for their characters.
        If a character chooses not to roll for any given item, just closing the dialog will cause the result to be generated just as if prompting was turned off.
        use: %(cmd)s <on|off>'''

        #print "Combat::Prompt:",cmd,opts
        # verify and extract options
        if not 1 == len(opts):
            raise BadParameter(_('incorrect number of parameters'))
        flags = xlist((NP_('result:on'), NP_('result:off')), xlate='result:')
        flag = XLookup(flags, opts[0].lower(), N_('option'))
        flag = int(flag == flags[0])
        char = self.GetSelectedCharacter()
        char.SetPrompt(flag)
        char.SetValue('x:combatprompt', str(flag))
        if flag:
            Output(_('%(char)s will prompt for all roll results') % {'char':char.Name()})
        else:
            Output(_('%(char)s will not prompt for all roll results') % {'char':char.Name()})
        return self.DoRoundAgain()


    def DoRoundStand(self, cmd, *opts):
        '''Stand up.
        This action is used by prone characters that want to avoid the penalties associated with acting while on the ground.
        use: %(cmd)s'''

        char = self.GetSelectedCharacter()
        status = char.GetStatus()
        if status != 'prone':
            raise NotPossible(_('%(state)s character cannot stand') % {'state':status.xval()})
        if GAME.CombatActionTime(char, 'stand'):
            # positive time takes the round
            self.DoRoundNext()
        else:
            # zero time allows another action
            self.DoRoundAgain()
        char.SetStatus('standing')
        Output(_('%(char)s is now standing') % {'char':char.Name()})


    def DoRoundTest(self, cmd, *opts):
        '''Make a skill check (used for learned abilities like climbing or swimming).
        use: %(cmd)s <attribute> [roll] [+/-modifier]'''

        # verify and extract options
        request  = None
        roll     = None
        modifier = None
        for o in opts:
            if modifier is None and self.kModifierRe.match(o):
                modifier = int(o)
            elif roll is None and o.isdigit():
                roll = int(o)
            elif request is None and o.isalnum():
                request = o
            else:
                self.DoRoundAgain()
                raise BadParameter(_('unknown option "%(opt)s"') % {'opt':o})
        char = self.GetSelectedCharacter()
        try:
            skill = XLookup(char.GetSkills().keys(), request, N_('skill'))
        except BadParameter:
            if not self.commandForce:
                raise
            skill = XLookup(GAME.kSkillInfo.keys(), request, N_('skill'))
        result = GAME.SkillCheck(char, skill, roll, modifier)
        Output(_('%(skill)s test result: %(result)s') % {'skill':skill.xval(), 'result':P_('result:',result)})
        return self.DoRoundAgain()


    def DoRoundUnwear(self, cmd, *opts):
        '''Remove an item currently being worn.
        This action always succeeds and takes no time.  Use the "pass" command if no further actions should be allowed for this character.
        The removed item goes into inventory.
        use: %(cmd)s <item>'''

        self.DoUnwear(cmd, *opts)
        self.DoRoundAgain()


    def DoRoundWear(self, cmd, *opts):
        '''Wear an item currently in inventory.
        This action always succeeds and takes no time.  Use the "pass" command if no further actions should be allowed for this character.
        use: %(cmd)s <item>'''

        self.DoWear(cmd, *opts)
        self.DoRoundAgain()


    def DoRoundUnwield(self, cmd, *opts):
        '''Remove an item currently being wielded.
        This action always succeeds and takes no time.  Use the "pass" command if no further actions should be allowed for this character.
        The removed item goes into inventory.
        use: %(cmd)s <item> [location]'''

        self.DoUnwield(cmd, *opts)
        self.DoRoundAgain()


    def DoRoundWield(self, cmd, *opts):
        '''Wield an item currently in inventory.
        This action always succeeds and takes no time.  Use the "pass" command if no further actions should be allowed for this character.
        use: %(cmd)s <item> [location]'''

        self.DoWield(cmd, *opts)
        self.DoRoundAgain()


    def DoRoundWound(self, cmd, *opts):
        '''Cause a wound on another character.  This action always succeeds and takes the entire round.
        - aspect: what aspect (blunt, point, edge, etc.) was used in the attack
        - location: the target location or area (high, low) of the attack [default: any]
        - damage: the result of a manually made damage roll
        use: %(cmd)s <opponent> <aspect> [location|area] <damage>'''

        #print 'Combat::DoRoundWound:',cmd,opts
        # verify and extract options
        if not 3 <= len(opts) <= 4:
            raise BadParameter(_('incorrect number of parameters'))
        opts = list(opts)
        opts.reverse()
        opponent= opts.pop().lower()
        aspect	= strx(opts.pop().lower())
        location= None
        damage	= None
        # extract strike-to location/area
        if opts:
            if opts[-1].isalpha():
                location = opts.pop()
        # extract roll & damage
        if opts:
            if opts[-1].isdigit() or deity.kDieRollRe.match(opts[-1]):
                damage = opts.pop()
        # should be finished now
        if opts:
            raise BadParameter(_('unknown parameter "%(parm)s"') % {'parm':opts[-1]})
        # verify required parameters
        if damage is None:
            raise BadParameter(_('did not specify "%(parm)s"') % {'parm':_('damage')})
        # convert opponent alias
        if self.alias.has_key(opponent):
            opponent = self.alias[opponent]
        # make it so
        char = GAME.MatchCharacterName(opponent)
        self._PreActionCheck()
        counter = GAME.CombatWound(char, damage, aspect=aspect, location=location, attacker=self.GetSelectedCharacter())
        self.DoRoundNext(counter)


    def DoRoundMenuAid(self, testdialogactions=None):
        charlist=[c.Name() for c in GAME.gameCharDisplay.itervalues()]
        cur = self.letchar or self.app.curchar.Get()
        locs = cur.GetTypeInfo('Locations', aslist=True, keysonly=True)
        d = deity.dialogs.GeneralSelectDialog(
            ({
                'name':		'char',
                'heading':	NP_('heading:recipient'),
                'list':		charlist,
                'default':	cur.Name(),
                'sort':		False,
            }, {
                'name':		'loc',
                'heading':	NP_('heading:location'),
                'list':		locs,
                'default':	locs[0],
                'sort':		True,
            }, {
                'name':		'mod',
                'heading':	NP_('heading:modifier'),
                'text':		None,
                'default':	'',
            }, {
                'name':		'roll',
                'heading':	NP_('heading:roll'),
                'text':		None,
                'default':	'',
            }),
            text=N_('please select a character to heal/aid and the location to be treated.'),
            title=P_('title:heal/aid')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        char = d.GetValueByName('char')
        loc  = d.GetValueByName('loc')
        mod  = d.GetValueByName('mod')
        roll = d.GetValueByName('roll')
        #print 'char:',char, 'loc:',loc, 'mod:',mod, 'roll:',roll
        self._PreActionCheck()
        self.app.CommandInject(P_('cmd:heal') + " %s %s %s %s" % (char,loc,mod,roll), execute=True)


    def DoRoundMenuCheck(self, testdialogactions=None):
        attrlist = xlist(GAME.kBaseAttributes, xlate=GAME.kBaseAttributes.xlate)
        attrlist.extend(GAME.kMoreAttributes)
        d = deity.dialogs.TestAbilityDialog(
            charlist=None,
            abilitylist=attrlist,
            abilitydefault=self.lastattrtest,
            text=N_('please select the attribute being checked.'),
            title=P_('title:attribute check')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        attr = d.GetAbility()
        mod  = d.GetModifier()
        roll = d.GetRoll()
        if mod and mod[0] != '-' and mod[0] != '+':
            mod = '+' + mod
        self.lastattrtest = attr
        self.app.CommandInject(P_('cmd:check') + " %s %s %s" %(attr.xval(), mod, roll), execute=True)


    def DoRoundMenuDraw(self, testdialogactions=None):
        cur = self.letchar or self.app.curchar.Get()
        order = list()
        wielded = cur.GetWielded(order=order)
        items = list()
        locs  = list()
        for o in order:
            if wielded[o]:
                locs.append('%s (%s)' % (o.xval(),wielded[o].Name()))
            else:
                locs.append('%s' % (o.xval()))
        inv = cur.GetInventory()
        for i in inv:
            items.append(i.Name())
        inv = GAME.GetId(GAME.kCombatGroundTid).GetInventory()
        for i in inv:
            items.append(i.Name())
        if not items:
            Output(_('%(name)s has no items available to draw') % {'name':cur.Name()}, '@error')
            return
        if not locs:
            Output(_('%(name)s has no place to hold an item') % {'name':cur.Name()}, '@error')
            return
        d = deity.dialogs.GeneralSelectDialog(
            ({
                'name':		'item',
                'heading':	NP_('heading:item'),
                'list':		items,
                'default':	items[0],
                'sort':		True,
            }, {
                'name':		'loc',
                'heading':	NP_('heading:location'),
                'list':		locs,
                'default':	locs[0],
                'sort':		True,
            }, {
                'name':		'mod',
                'heading':	NP_('heading:modifier'),
                'text':		None,
                'default':	'',
            }, {
                'name':		'roll',
                'heading':	NP_('heading:roll'),
                'text':		None,
                'default':	'',
            }),
            text=N_('please select an item to draw and the location to hold it.'),
            title=P_('title:draw item')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        loc  = d.GetValueByName('loc')
        parts= loc.split(' ')
        loc  = parts[0]
        item = d.GetValueByName('item')
        mod  = d.GetValueByName('mod')
        roll = d.GetValueByName('roll')
        #print 'item:',item, 'loc:',loc, 'mod:',mod, 'roll:',roll
        self.app.CommandInject(P_('cmd:draw') + " %s %s %s %s" % (item,loc,mod,roll), execute=True)


    def DoRoundMenuDrop(self, testdialogactions=None):
        cur = self.letchar or self.app.curchar.Get()
        order = list()
        wielded = cur.GetWielded(order=order)
        items = xlist()
        for o in order:
            if wielded[o]:
                items.append("%s (%s)" % (wielded[o].Name(), o.xval()))
        if not items:
            Output(_('%(name)s is not holding any items') % {'name':cur.Name()}, '@error')
            return
        d = deity.dialogs.ChooseOneDialog(
            items,
            text=N_('please select the item to be dropped on the ground.'),
            title=P_('title:drop item')
        )
        item = d.Run(testdialogactions=testdialogactions)
        if item is None:
            return
        parts = item.split('(')
        self.app.CommandInject(P_('cmd:drop') + " %s%s" % (parts[0],parts[1][:-1]), execute=True)


    def DoRoundMenuExtra(self):
        self.app.CommandInject(P_('cmd:extra') + " ", execute=False)


    def DoRoundMenuLet(self):
        olist=[]
        cur = self.app.curchar.Get()
        for c in self.charList.DisplayedChars():
            if c != cur:
                olist.append(c.Name())
        olist.sort()
        d = deity.dialogs.ChooseOneDialog(
            olist=olist,
            text=N_('please select the character to perform an extra action.'),
            title=P_('title:extra action')
        )
        char = d.Run()
        if char is None:
            return
        self.letchar = GAME.MatchCharacterName(char)
        self.app.CommandInject(P_('cmd:let') + " %s " % char, execute=False)


    def DoRoundMenuPrompt(self):
        char = self.GetSelectedCharacter()
        prompt = char.GetValue('x:combatprompt', default='off')
        if prompt == 'off':
            prompt = NP_('result:on')
        else:
            prompt = NP_('result:off')
        self.app.CommandInject(P_('cmd:prompt') + " %s" % prompt, execute=True)


    def DoRoundMenuTest(self):
        skills = xlist(GAME.kSkillInfo.keys(), 'skill:')
        d = deity.dialogs.TestAbilityDialog(
            charlist=None,
            abilitylist=skills,
            abilitydefault=self.lastskilltest,
            text=N_('please select the skill being tested.'),
            title=P_('title:skill test')
        )
        if not d.Run():
            return
        skill = d.GetAbility()
        mod   = d.GetModifier()
        roll  = d.GetRoll()
        if mod and mod[0] != '-' and mod[0] != '+':
            mod = '+' + mod
        self.lastskilltest = skill
        self.app.CommandInject(P_('cmd:test') + " %s %s %s" %(skill.xval(), mod, roll), execute=True)


    def DoRoundMenuUnwear(self, testdialogactions=None):
        cur = self.letchar or self.app.curchar.Get()
        worn = cur.GetWorn()
        items = xlist()
        for i,l in worn.iteritems():
            items.append("%s" % (i.Name()))
        if not items:
            Output(_('%(name)s is not wearing anything') % {'name':cur.Name()}, '@error')
            return
        d = deity.dialogs.ChooseOneDialog(
            items,
            text=N_('please select the item to remove and store in inventory.'),
            title=P_('title:un-wear item')
        )
        item = d.Run(testdialogactions=testdialogactions)
        if item is None:
            return
        self.app.CommandInject(P_('cmd:unwear') + " %s" % (item), execute=True)


    def DoRoundMenuUnwield(self, testdialogactions=None):
        cur = self.letchar or self.app.curchar.Get()
        order = list()
        wielded = cur.GetWielded(order=order)
        items = xlist()
        for o in order:
            if wielded[o]:
                items.append("%s (%s)" % (wielded[o].Name(), o.xval()))
        if not items:
            Output(_('%(name)s is not holding any items') % {'name':cur.Name()}, '@error')
            return
        d = deity.dialogs.ChooseOneDialog(
            items,
            text=N_('please select the item to be un-wielded and placed back in inventory.'),
            title=P_('title:un-wield item')
        )
        item = d.Run(testdialogactions=testdialogactions)
        if item is None:
            return
        parts = item.split('(')
        self.app.CommandInject(P_('cmd:unwield') + " %s%s" % (parts[0],parts[1][:-1]), execute=True)


    def DoRoundMenuWield(self, testdialogactions=None):
        cur = self.letchar or self.app.curchar.Get()
        order = list()
        wielded = cur.GetWielded(order=order)
        items = list()
        locs  = list()
        for o in order:
            if wielded[o]:
                locs.append('%s (%s)' % (o.xval(),wielded[o].Name()))
            else:
                locs.append('%s' % (o.xval()))
        inv = cur.GetInventory()
        for i in inv:
            items.append(i.Name())
        #inv = GAME.GetId(GAME.kCombatGroundTid).GetInventory()
        #for i in inv:
        #    items.append(i.Name())
        if not items:
            Output(_('%(name)s has no items available to wield') % {'name':cur.Name()}, '@error')
            return
        if not locs:
            Output(_('%(name)s has no place to hold an item') % {'name':cur.Name()}, '@error')
            return
        d = deity.dialogs.GeneralSelectDialog(
            ({
                'name':		'item',
                'heading':	NP_('heading:item'),
                'list':		items,
                'default':	items[0],
                'sort':		True,
            }, {
                'name':		'loc',
                'heading':	NP_('heading:location'),
                'list':		locs,
                'default':	locs[0],
                'sort':		True,
            }),
            text=N_('please select an item to wield and the location to hold it.  wielding an item always succeeds and is considered to take no time.'),
            title=P_('title:wield item')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        loc  = d.GetValueByName('loc')
        parts= loc.split(' ')
        loc  = parts[0]
        item = d.GetValueByName('item')
        #print 'item:',item, 'loc:',loc
        self.app.CommandInject(P_('cmd:wield') + " %s %s" % (item,loc), execute=True)


    def DoRoundMenuWear(self, testdialogactions=None):
        cur = self.letchar or self.app.curchar.Get()
        items = list()
        #locs  = list()
        inv = cur.GetInventory()
        for i in inv:
            items.append(i.Name())
        if not items:
            Output(_('%(name)s has no items available to draw') % {'name':cur.Name()}, '@error')
            return
        #if not locs:
        #    Output(_('%(name)s has no place to hold an item') % {'name':cur.Name()}, '@error')
        #    return
        d = deity.dialogs.GeneralSelectDialog(
            ({
                'name':		'item',
                'heading':	NP_('heading:item'),
                'list':		items,
                'default':	items[0],
                'sort':		True,
            #}, {
            #    'name':		'loc',
            #    'heading':	NP_('heading:location'),
            #    'list':		locs,
            #    'default':	locs[0],
            #    'sort':		True,
            },),
            text=N_('please select an item to wear.  accouting for the time/incapacity associated with this action must be done by hand by the gm.'),
            title=P_('title:wear item')
        )
        if not d.Run(testdialogactions=testdialogactions):
            return
        #loc  = d.GetValueByName('loc')
        item = d.GetValueByName('item')
        #print 'item:',item, 'loc:',loc, 'mod:',mod, 'roll:',roll
        self.app.CommandInject(P_('cmd:wear') + " %s" % (item), execute=True)


    def DisplayRefreshRound(self):
        #print "CombatMode::DisplayRefreshRound: curname:", self.curname, "curindex:", self.curindex, "roundList:", self.roundList
        changed = False
        Output('')

        # Save just executed command
        if self.curname and self.app.CommandLast():
            self.actions[self.curname] = self.app.CommandLast()

        # Handle characters that were busy but can act later in the round.
        if self.curindex is None and not self.roundList and self.endRoundList:
            self.roundList = self.endRoundList
            self.roundList.sort(reverse=True)
            self.endRoundList = list()

        # Catch case of nothing to do
        if self.curindex is None and not self.roundList:
            Output(_('round is finished.'))
            self.curname = None
            #advance game time
            self.DoRoundDone()
            self.app.CommandDefault(None)
            return changed

        # Work on the next character in the list
        if not self.curindex:
            self.curindex = self.roundList.pop()
            (order,ctid,name) = self.curindex.split('/')

            # Get active character information
            char = GAME.Character(ctid)
            self.app.curchar.Set(char)
            self.curname = char.Name()
            lastact = ''
            if self.curname in self.actions:
                lastact = self.actions[self.curname]
            status = char.GetStatus()
            Output(_('%(char)s [%(status)s] has the next action') % {'char':self.curname, 'status':status.xval()})
            self.app.CommandPrompt(self.curname)

            # Handle new turns
            if order[0] != '-':
                GAME.CombatNewTurn(char)

            # Choose default command
            status = char.GetStatus()  # may have changed during NewTurn()
            busy = char.GetInfo('busy')
            if status == 'unconscious' or status == 'shock' or status == 'dead':
                self.app.CommandDefault(P_('cmd:pass'))
            elif status == 'prone':
                self.app.CommandDefault(P_('cmd:stand'))
            elif busy is not None:
                self.haveBusy = True
                if busy == '':
                    self.app.CommandDefault(P_('cmd:hold'))
                else:
                    self.app.CommandDefault(P_('cmd:pass'))
            else:
                self.app.CommandDefault(lastact)

        # Indicate if layout has changed
        return changed


###############################################################################
