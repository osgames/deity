###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common (generic functions, constants, etc.)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import re
import random
random.seed()

import deity.trandom


kLbsPerKg		= 2.2
kInchesPerCm		= 1 / 2.54
kInchesPerFoot		= 12

kBeastiaryFilename	= "beastiary.dby"
kDieRollRe		= re.compile(r'\b([1-9]\d*)?d([1-9]\d*)\b', re.I)
kBestDieRollRe		= re.compile(r'\b([1-9]\d*)\^([1-9]\d*)d([1-9]\d*)\b', re.I)
kWorstDieRollRe		= re.compile(r'\b([1-9]\d*)v([1-9]\d*)d([1-9]\d*)\b', re.I)


###############################################################################


def Roll(count, die=None, dice=None, rand=None, test=None):
    if die is None:
        roll = count
        pos = roll.find('d')
        die = int(roll[pos+1:])
        if pos > 0:
            count = int(roll[0:pos])
        else:
            count = 1

    if die < 1: return 0
    rsum = 0
    if rand is None:
        rand = deity.trandom.Random()
    for _ in xrange(count):
        roll  = rand.randint(1, die, test=test)
        rsum += roll
        if dice is not None: dice.append(roll)
    return rsum


###############################################################################


def DefaultInt(val, default=None):
    try:
        return int(float(val))
    except (TypeError,ValueError):
        return default


def DefaultFloat(val, default=None):
    try:
        return float(val)
    except (TypeError,ValueError):
        return default


###############################################################################


def MultipleChoiceExpand(item):
    if item is None: return None
    begin = item.find('(')
    end   = item.rfind(')')
    if begin == -1 or end == -1:
        return [item,]
    ilist = []
    for opt in item[begin+1:end].split('|'):
        ilist.append(item[:begin] + opt.strip() + item[end+1:])
    return ilist


def MultipleChoiceResolution(item, rand=None, test=None):
    #print 'MultipleChoiceResolution:',item,rand,test
    if item is None: return None
    if rand is None: rand = deity.trandom.Random()
    ilist = MultipleChoiceExpand(item)
    if len(ilist) == 1:
        return ilist[0]
    index = rand.randint(0, len(ilist)-1, test=test)
    return ilist[index]


###############################################################################


def ListKeyLookup(items, key):
    if items is None: return None
    for h in items:
        sep = h.find('=')
        if sep == -1:
            num = h
            val = h
        else:
            num = h[:sep]
            val = h[sep+1:]
        if float(key) <= float(num):
            return val
    return None


###############################################################################


def DbGet(db, item, default=None):
    #print 'DbGet:',item,default
    if item not in db:
        return default
    return db[item]


###############################################################################
