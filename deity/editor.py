###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common (editor classes)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


#import re

from deity.exceptions import *


###############################################################################


class ValueManager(object):

    def __init__(self):
        self.values = dict()
        self.notify = dict()

        self.stack = dict()
        self.valchanges = dict()
        self.depchanges = dict()


    def ClearValues(self):
        self.values = dict()


    def ClearNotify(self):
        self.notify = dict()


    def Clear(self):
        self.ClearValues()
        self.ClearNotify()


    def SetValue(self, key, val):
        if not isinstance(val, str):
            raise Exception('can only edit string values -- use validator to limit otherwise')
        if key in self.values:
            raise Exception('key "%s" cannot be set to "%s" because it already exists (value="%s")' % (key, val, self.values[key]))
        self.values[key] = val


    def GetValue(self, key, notify=None):
        if notify is not None:
            if key not in self.notify:
                self.notify[key] = dict()
            self.notify[key][notify] = None
        if key not in self.values:
            return None
        return self.values[key]


    def ChangeValue(self, key, val):
        if not isinstance(val, (str,unicode)):
            raise Exception('can only edit string values, not %s -- use validator to limit otherwise' % type(val))
        if key not in self.values:
            raise Exception('key "%s" cannot be changed to "%s" because doesn\'t exists' % (key, val))
        old = self.values[key]
        if val == old:
            return
        self.values[key] = val

        stackindex = len(self.stack)
        if stackindex == 0:
            self.valchanges = dict()
            self.depchanges = dict()
        else:
            if key in self.stack:
                raise Exception('circular dependency ending with "%s", coming from: %s' % (key, str(self.stack)))

        self.stack[key] = stackindex
        if key not in self.valchanges:
            self.valchanges[key] = old
        try:
            errmsg = self.ValueUpdated(key, val, old)
        except Exception as e:
            errmsg = str(e)
        if errmsg:
            Output(_(errmsg) % {'val':val, 'item':key, 'old':old}, '@error')
            self.values[key] = old

        del self.stack[key]

        # Call "notify" callbacks.  Note that we always call all callbacks
        # even if none of the variables they're interested in have changed.
        # This allows dependent objects to know that an update cycle has
        # occurred and, for example, clear any highlighting of changes made
        # in a prevnious cycle.
        if len(self.stack) == 0:
            calls = dict()
            # Create empty call lists.
            for n in self.notify:
                for c in self.notify[n]:
                    if c not in calls:
                        calls[c] = list()
            # Add info for changed values
            for key,old in self.valchanges.iteritems():
                if key in self.notify.iterkeys():
                    #print 'ValueManager::ChangeValue: dependent:',key
                    for n in self.notify[key]:
                        calls[n].append((key, self.values[key], old))
            # Call everyone with what changed.
            for c,l in calls.iteritems():
                c(l)


    def ValueUpdated(self, var, new, old):
        return None


###############################################################################
