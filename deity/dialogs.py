###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Common Dialogs
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import re
import wx

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class DDialog(wx.Dialog):

    def __init__(self, parent, **kwds):
        wx.Dialog.__init__(self, parent, **kwds)


class DModalDialog(DDialog):

    def Run(self, testdialogactions=None):  # pragma: no cover
        if testdialogactions is not None:
            self.testDialogActions = testdialogactions
        if 'testDialogActions' in self.__dict__ and self.testDialogActions is not None:
            self.Show()
            for a in self.testDialogActions:
                if isinstance(a, tuple):
                    # action can be an event or a direct method call with parameters
                    if isinstance(a[0], wx.Event):
                        if len(a) > 1 and a[1] is not None:
                            w = a[1]
                        else:
                            w = self
                        self.SetReturnCode(a[0].GetId())
                        w.ProcessEvent(a[0])
                    else:
                        a[0](*a[1:])
                elif isinstance(a, str):
                    eval(a)
                else:
                    if isinstance(a, wx.Event):
                        self.SetReturnCode(a.GetId())
                        self.ProcessEvent(a)
                    else:
                        a()
            result = self.GetReturnCode()
            self.Destroy()
            self.testDialogActions = None
        else:
            result = self.ShowModal()
            self.Destroy()
        return result


###############################################################################


class ChooseOptionsDialog(DModalDialog):

    def __init__(self, chosen, catorder, options, columns=2, padding=10, allowcancel=True, shownew=False, **kwds):
        DDialog.__init__(self, None, **kwds)
        self.options = options
        self.chosen  = chosen
        self.cancelOk= allowcancel

        heading_font = wx.Font(pointSize=14, family=wx.FONTFAMILY_DEFAULT,
                               style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

        # Create ordered list of all our categories, expected or unexpected
        category_order = xlist(xlate='heading:')
        category_dict  = dict()
        category_more  = xlist(xlate='heading:')
        for c in catorder:
            category_order.append(c)
            category_dict[c] = None
        for o in options.itervalues():
            c = o['category']
            if c not in category_dict:
                category_more.append(c)
                category_dict[c] = None
        category_more.xsort()
        self.categoryOrder = category_order
        self.categoryOrder.extend(category_more)

        # Organize all our options
        self.layout = list()
        for category in self.categoryOrder:
            self.layout.append(category)
            orderedoptions = dict()
            for option in options.itervalues():
                if option['category'] != category:
                    continue
                if option['order'] not in orderedoptions:
                    orderedoptions[option['order']] = list()
                orderedoptions[option['order']].append(option)
            orders = orderedoptions.keys()
            orders.sort()
            catopts = list()
            for order in orders:
                for o in orderedoptions[order]:
                    if catopts:
                        if 'options' in o or 'options' in catopts[0]:
                            self.layout.append(catopts)
                            catopts = list()
                    catopts.append(o)
            self.layout.append(catopts)

        # Create basic dialog structure
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        vsizer = wx.BoxSizer(wx.VERTICAL)
        hsizer.AddSpacer(padding)
        hsizer.Add(vsizer)
        hsizer.AddSpacer(padding)
        vsizer.AddSpacer(padding)

        # Put pieces into a grid on the dialog
        self.widgetRules = {}
        foundnew = False
        for block in self.layout:
            if isinstance(block, (str,unicode)):
                w = wx.StaticText(self, wx.ID_ANY, label=P_('heading:',block))
                w.SetFont(heading_font)
                vsizer.Add(w)
                vsizer.AddSpacer(4)
            elif 'options' in block[0]:
                osizer = wx.BoxSizer(wx.HORIZONTAL)
                o = block[0]
                w = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY|wx.CB_SORT)
                self.widgetRules[w] = o
                for v in o['options'].itervalues():
                    w.Append(P_('option:',v))
                osizer.Add(w)
                osizer.AddSpacer(4)
                l = P_('shortdesc:', o['short'])
                if shownew and o['rule'] not in chosen:
                    l += ' (*)'
                    foundnew = True
                ssizer = wx.BoxSizer(wx.VERTICAL)
                w = wx.StaticText(self, wx.ID_ANY, label=l)
                ssizer.Add((0,0), 1)
                ssizer.Add(w, 0, wx.EXPAND)
                ssizer.Add((0,0), 1)
                osizer.Add(ssizer, 0, wx.EXPAND)
                vsizer.Add(osizer)
                vsizer.AddSpacer(10)
            else:
                rowcount = (len(block) + columns - 1) // columns
                osizer = wx.GridSizer(rowcount, columns, vgap=4, hgap=padding)
                blockwidgets = []
                row = 0
                col = 0
                for i in xrange(rowcount * columns):
                    x = col * rowcount + row
                    if x < len(block):
                        o = block[x]
                        l = P_('shortdesc:', o['short'])
                        if shownew and o['rule'] not in chosen:
                            l += ' (*)'
                            foundnew = True
                        w = wx.CheckBox(self, wx.ID_ANY, label=l)
                        self.widgetRules[w] = o
                        if 'long' in o:
                            w.SetToolTip(wx.ToolTip(P_('longdesc:', o['long'])))
                    else:
                        o = None
                        w = wx.StaticText(self, wx.ID_ANY, label='')
                    osizer.Add(w)
                    if row > 0:
                        w.MoveAfterInTabOrder(blockwidgets[-columns])
                    blockwidgets.append(w)
                    col += 1
                    if col == columns:
                        row += 1
                        col  = 0
                vsizer.Add(osizer, 0, wx.EXPAND)
                vsizer.AddSpacer(padding)

        # Create action buttons
        bsizer = wx.BoxSizer(wx.HORIZONTAL)
        self.breset = wx.Button(self, wx.ID_RESET, P_('btn:reset'))
        self.bcancel= wx.Button(self, wx.ID_CANCEL)
        self.bok    = wx.Button(self, wx.ID_OK)
        self.bok.SetDefault()
        self.breset.Bind(wx.EVT_BUTTON, self.OnButtonReset)
        self.bcancel.Enable(allowcancel)
        bsizer.Add(self.breset, 0, 0)
        bsizer.AddStretchSpacer()
        bsizer.Add(self.bcancel, 0, 0)
        bsizer.Add(self.bok, 0, 0)

        # Complete dialog formation
        vsizer.AddSpacer(padding)
        if foundnew:
            w = wx.StaticText(self, wx.ID_ANY, label=_('(*) indicates new options in this version'))
            vsizer.Add(w)
        vsizer.Add(bsizer, 0, wx.EXPAND)
        vsizer.AddSpacer(padding)
        self.SetSizerAndFit(hsizer)

        # Update widgets with default values
        self.OnButtonReset(None)


    def OnButtonReset(self, ev):
        #print 'OnButtonReset:',ev
        for w,o in self.widgetRules.iteritems():
            rule = o['rule']
            if 'options' in o:
                if rule in self.chosen:
                    if self.chosen[rule] in o['options']:
                        choice = self.chosen[rule]
                    else:
                        choice = o['default']
                else:
                    choice = o['default']
                w.SetValue(o['options'][choice])
            else:
                if rule in self.chosen:
                    checked = (self.chosen[rule] == '' or self.chosen[rule] == 'yes')
                else:
                    checked = o['default']
                w.SetValue(checked)


    def Run(self, **kwds):
        result = DModalDialog.Run(self, **kwds)
        if result == wx.ID_OK or not self.cancelOk:
            chosen = self.chosen
            chosen.clear()
            for w,o in self.widgetRules.iteritems():
                value = w.GetValue()
                if 'options' in o:
                    for k,v in o['options'].iteritems():
                        if value == v:
                            value = k
                            break
                else:
                    if value:
                        value = 'yes'
                    else:
                        value = 'no'
                chosen[o['rule']] = value
            return True
        return False


###############################################################################


class ChooseOneDialog(DModalDialog):

    def __init__(self, olist, text=None, default=None,
                 padding=10, **kwds):
        DDialog.__init__(self, None, **kwds)

        if not default:
            default=olist[0]

        self.optionList = olist
        self.optionDefault = default

        # Create basic dialog structure
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        vsizer = wx.BoxSizer(wx.VERTICAL)
        hsizer.AddSpacer(padding)
        hsizer.Add(vsizer)
        hsizer.AddSpacer(padding)
        vsizer.AddSpacer(padding)

        # Create option widgets
        if text:
            vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=_(text)))
            vsizer.AddSpacer(padding)
        if olist:
            self.wolist = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY)
            for i in self.optionList:
                self.wolist.Append(i)
            vsizer.Add(self.wolist, 0, wx.EXPAND)
            vsizer.AddSpacer(padding)

        # Create action buttons
        bsizer = wx.BoxSizer(wx.HORIZONTAL)
        #self.breset = wx.Button(self, wx.ID_RESET, P_('btn:reset'))
        self.bcancel= wx.Button(self, wx.ID_CANCEL)
        self.bok    = wx.Button(self, wx.ID_OK)
        self.bok.SetDefault()
        #self.breset.Bind(wx.EVT_BUTTON, self.OnButtonReset)
        #bsizer.Add(self.breset, 0, 0)
        bsizer.AddStretchSpacer()
        bsizer.Add(self.bcancel, 0, 0)
        bsizer.Add(self.bok, 0, 0)

        # Complete dialog formation
        vsizer.AddSpacer(padding)
        vsizer.Add(bsizer, 0, wx.EXPAND)
        vsizer.AddSpacer(padding)
        self.SetSizerAndFit(hsizer)

        # Update widgets with default values
        self.OnButtonReset(None)


    def OnButtonReset(self, ev):
        if self.optionList is not None and self.optionDefault:
            self.wolist.SetValue(self.optionDefault)


    def GetOption(self):
        return self.wolist.GetValue()


    def Run(self, **kwds):
        result = DModalDialog.Run(self, **kwds)
        if result == wx.ID_OK:
            return self.GetOption()
        return None


###############################################################################


class GeneralSelectDialog(DModalDialog):

    def __init__(self, options, text=None,
                 padding=10, **kwds):
        DDialog.__init__(self, None, **kwds)
        self.options = options
        self.widgets = list()

        # Create basic dialog structure
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        vsizer = wx.BoxSizer(wx.VERTICAL)
        hsizer.AddSpacer(padding)
        hsizer.Add(vsizer)
        hsizer.AddSpacer(padding)
        vsizer.AddSpacer(padding)

        # Create option widgets
        if text:
            vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=_(text)))
            vsizer.AddSpacer(padding)
        for o in options:
            if 'heading' in o:
                vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=P_('heading:',o['heading'])))
            if 'list' in o or 'xlist' in o:
                style = wx.CB_READONLY
                if 'sort' in o and o['sort']:
                    style |= wx.CB_SORT
                w = wx.ComboBox(self, wx.ID_ANY, style=style)
                if 'list' in o:
                    for i in o['list']:
                        if i is not None:
                            w.Append(i)
                else:
                    for i in o['xlist']:
                        if i is not None:
                            w.Append(i.xval())
            elif 'text' in o:
                w = wx.TextCtrl(self, wx.ID_ANY)
            else:  # pragma: no cover
                raise Exception('unknown option info: %s' % str(o))
            self.widgets.append(w)
            vsizer.Add(w, 0, wx.EXPAND)
            vsizer.AddSpacer(padding)

        # Create action buttons
        bsizer = wx.BoxSizer(wx.HORIZONTAL)
        self.breset = wx.Button(self, wx.ID_RESET, P_('btn:reset'))
        self.bcancel= wx.Button(self, wx.ID_CANCEL)
        self.bok    = wx.Button(self, wx.ID_OK)
        self.bok.SetDefault()
        self.breset.Bind(wx.EVT_BUTTON, self.OnButtonReset)
        bsizer.Add(self.breset, 0, 0)
        bsizer.AddStretchSpacer()
        bsizer.Add(self.bcancel, 0, 0)
        bsizer.Add(self.bok, 0, 0)

        # Complete dialog formation
        vsizer.AddSpacer(padding)
        vsizer.Add(bsizer, 0, wx.EXPAND)
        vsizer.AddSpacer(padding)
        self.SetSizerAndFit(hsizer)

        # Update widgets with default values
        self.OnButtonReset(None)


    def OnButtonReset(self, ev):
        for i,o in enumerate(self.options):
            if 'default' in o:
                self.widgets[i].SetValue(o['default'])
            else:
                self.widgets[i].SetValue('')


    def GetValue(self, index):
        val = self.widgets[index].GetValue()
        if 'xlist' in self.options[index]:
            val = self.options[index]['xlist'].xfind(val)
        return val


    def GetValueByName(self, name):
        for i,o in enumerate(self.options):
            if o.get('name', None) == name:
                return self.GetValue(i)
        raise Exception('no value named "%s" in option list %s' % (name, str(self.options)))


    def Run(self, **kwds):
        result = DModalDialog.Run(self, **kwds)
        if result == wx.ID_OK:
            return True
        return None


###############################################################################


class TestAbilityDialog(DModalDialog):

    def __init__(self, charlist, abilitylist, text=None,
                 chardefault=None, abilitydefault=None, modifierdefault=None,
                 allowall=False,
                 padding=10, **kwds):
        DDialog.__init__(self, None, **kwds)

        if not chardefault:
            if allowall:
                chardefault=P_('select:all')
            elif charlist:
                chardefault=charlist[0]
        if not abilitydefault and len(abilitylist) > 0:
            abilitydefault=abilitylist[0]

        self.charList = charlist
        self.abilityList = abilitylist
        self.charDefault = chardefault
        self.abilityDefault = abilitydefault
        self.modifierDefault = modifierdefault or ''
        self.allowAll = allowall

        # Create basic dialog structure
        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        vsizer = wx.BoxSizer(wx.VERTICAL)
        hsizer.AddSpacer(padding)
        hsizer.Add(vsizer)
        hsizer.AddSpacer(padding)
        vsizer.AddSpacer(padding)

        # Create option widgets
        if text:
            vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=_(text)))
            vsizer.AddSpacer(padding)
        if charlist:
            vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=P_('heading:choose character performing test')))
            self.wcharlist = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY)
            if allowall:
                self.wcharlist.Append(P_('select:all'))
            for i in self.charList:
                self.wcharlist.Append(i)
            vsizer.Add(self.wcharlist, 0, wx.EXPAND)
            vsizer.AddSpacer(padding)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=P_('heading:choose ability to test')))
        self.wabilitylist = wx.ComboBox(self, wx.ID_ANY, style=wx.CB_READONLY|wx.CB_SORT)
        for i in self.abilityList:
            self.wabilitylist.Append(i.xval())
        vsizer.Add(self.wabilitylist)
        vsizer.AddSpacer(padding)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=P_('heading:test modifier (optional: +=good, -=bad)')))
        self.wmodifier = wx.TextCtrl(self, wx.ID_ANY)
        vsizer.Add(self.wmodifier)
        vsizer.AddSpacer(padding)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, label=P_('heading:rolled value (optional)')))
        self.wroll = wx.TextCtrl(self, wx.ID_ANY)
        vsizer.Add(self.wroll)

        # Create action buttons
        bsizer = wx.BoxSizer(wx.HORIZONTAL)
        self.breset = wx.Button(self, wx.ID_RESET, P_('btn:reset'))
        self.bcancel= wx.Button(self, wx.ID_CANCEL)
        self.bok    = wx.Button(self, wx.ID_OK)
        self.bok.SetDefault()
        self.breset.Bind(wx.EVT_BUTTON, self.OnButtonReset)
        bsizer.Add(self.breset, 0, 0)
        bsizer.AddStretchSpacer()
        bsizer.Add(self.bcancel, 0, 0)
        bsizer.Add(self.bok, 0, 0)

        # Complete dialog formation
        vsizer.AddSpacer(padding)
        vsizer.Add(bsizer, 0, wx.EXPAND)
        vsizer.AddSpacer(padding)
        self.SetSizerAndFit(hsizer)

        # Update widgets with default values
        self.OnButtonReset(None)


    def OnButtonReset(self, ev):
        if self.charList is not None and self.charDefault:
            self.wcharlist.SetValue(self.charDefault)
        if self.abilityDefault:
            for x in self.abilityList:
                if x == self.abilityDefault:
                    self.wabilitylist.SetValue(x.xval())
                    break
        if self.modifierDefault:
            self.wmodifier.SetValue(self.modifierDefault)
        self.wroll.SetValue('')


    def GetCharacter(self):
        return self.wcharlist.GetValue()


    def GetAbility(self):
        return self.abilityList.xfind(self.wabilitylist.GetValue())


    def GetModifier(self):
        return self.wmodifier.GetValue()


    def GetRoll(self):
        return self.wroll.GetValue()


    def Run(self, **kwds):
        result = DModalDialog.Run(self, **kwds)
        if result == wx.ID_OK:
            return True
        return None


###############################################################################
