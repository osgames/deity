###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Command Processing
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import gettext
import re

from deity.exceptions import *


###############################################################################


class CommandDispatch(object):
    kHelpRe = re.compile(r'^(\?|help)$', re.I)


    def __init__(self, qmarktext=None):
        self.alias     = dict()
        self.rehash    = False
        self.last      = ''
        self.qmarkText = qmarktext
        self.Reset()


    def Reset(self):
        self.funcs = dict()
        self.regex = dict()


    def Last(self):
        return self.last


    def Clear(self):
        self.last = ''


    def Register(self, cmd, func):
        if cmd in self.funcs:
            raise Exception, 'duplicate command "%s"' % cmd
        self.funcs[cmd] = func
        self.rehash = True


    def Match(self, regex, func):
        if type(regex) != type(self.kHelpRe):
            regex = re.compile(regex, re.I)
        self.regex[regex] = func


    def Rehash(self):
        funcs = self.funcs.keys()
        funcs.sort(lambda a,b: cmp(len(a), len(b)))
        self.alias = dict()

        for cmd in funcs:
            alias = cmd[0:-1]
            while alias != '' and alias not in self.funcs:
                if alias in self.alias:
                    self.alias[alias] += ' '+cmd
                else:
                    self.alias[alias] = cmd
                #print 'command alias for',cmd+':',alias,'('+self.alias[alias]+')'
                alias = alias[0:-1]

        self.rehash = False


    def Lookup(self, cmd):
        # update aliases hash
        if self.rehash: self.Rehash()

        # Check for an alias.
        if cmd in self.alias:
            alias = self.alias[cmd]
            if alias.find(' ') > 0:
                aliases = alias.split()
                aliases.sort()
                raise NotPossible((_('ambiguous abbreviation "%(abbr)s":')+' '.join(aliases)) % {'abbr':cmd})
            return alias

        # not an alias
        return cmd

    def Function(self, cmd):
        return self.funcs.get(cmd, None)


    def Dispatch(self, cmd, *opts):
        # expand any aliases
        cmd = self.Lookup(cmd)

        func = self.Function(cmd)
        if func is None:
            raise BadParameter(_('unknown command "%(cmd)s"') % {'cmd':cmd})

        # Execute appropriate method on appropriate object
        self.last = (cmd + ' ' + ' '.join(opts)).strip()
        return func(cmd, *opts)


    def Execute(self, cmd, *opts):
        #print 'Command::Execute:',cmd,opts
        # create single command line
        cmd = cmd.strip()
        if not cmd:
            return
        if opts:
            cmd += ' ' + ' '.join(opts)

        # show command summary
        if cmd[0] == '?':
            cmds = self.funcs.keys()
            cmds.sort()
            #self.output(_('known commands:'))
            text = ', '.join(cmds)
            if self.qmarkText:
                text += "\n\n" + "\n".join(self.qmarkText)
            return text

        # handle regex matches
        for r,f in self.regex.iteritems():
            m = r.match(cmd)
            if m:
                return f(cmd, m)

        # separate line in to command and options
        if cmd[0].isalnum():
            opts = cmd.split()
            cmd = opts[0].lower()
            opts[0:1] = ()
        else:
            opts = cmd[1:].split()
            cmd = cmd[0]

        # do it
        return self.Dispatch(cmd, *opts)


