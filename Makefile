###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity Makefile
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################

PROJECT=deity
VERSION=0.9
DFILES=	deity.py \
        deity/beastiary.py deity/commands.py deity/dcommon.py deity/dialogs.py \
        deity/editor.py deity/exceptions.py deity/gamesystem/gscommon.py	   \
        deity/gamesystem/harnmaster.py deity/gettext.py deity/modes.py		   \
        deity/test.py deity/trandom.py deity/watcher.py deity/widgets.py	   \
        deity/windows.py deity/world/kelestia/kcommon.py					   \
        deity/world/kelestia/kethira/kcommon.py								   \
        deity/world/kelestia/kethira/harn/hcommon.py						   \
        deity/world/wcommon.py deity/xtype.py

POFILES=locale/deity_bw.po

ifeq "Windows_NT" "${OS}"
PYTHON = /c/Python27/python.exe
else
PYTHON = /usr/bin/python2.7
endif

###############################################################################


all: stamp-gettext


beastiary.dby beastiary:
	rm -f $@
	${PYTHON} ./deity.py --beastiary-load beastiary/*.dat
	PYTHONPATH=. ${PYTHON} tests/*Beastiary*.py


release: clean
	cd .. && rm -f deity-$(VERSION).*
	cd .. && \
	tar cf deity-$(VERSION).tar deity/* --exclude=.svn --exclude=.git && \
	gzip -9f deity-$(VERSION).tar
	cd .. && \
	find deity/ \( -name .svn -o -name .git \) -prune -type f -o -type f | \
	zip -9q deity-$(VERSION).zip -@


test: beastiary.dby
	for t in tests/*.py; do \
		echo " "; echo $$t; \
		PYTHONPATH=. ${PYTHON} $$t || exit 1; \
	done


fasttest: beastiary.dby
	for t in tests/[0-8]*.py; do \
		echo " "; echo $$t; \
		PYTHONPATH=. ${PYTHON} $$t || exit 1; \
	done


coverage: beastiary.dby
	rm -f deity/*,cover deity/*/*,cover
	coverage -e
	for t in tests/[0-8]*.py; do \
		echo " "; echo $$t; \
		PYTHONPATH=. coverage -x $$t || exit 1; \
	done
	coverage -r deity/[a-z]*.py deity/*/[a-z]*.py
	coverage -a deity/[a-z]*.py deity/*/[a-z]*.py


csummary:
	coverage -r deity/[a-z]*.py deity/*/[a-z]*.py
	coverage -a deity/[a-z]*.py deity/*/[a-z]*.py


check:
	pychecker -q -#1000 deity/[a-z]*.py


locale/deity.pot: $(DFILES)
	-savelog -l $@ || mv $@ $@.0 || touch $@.0
	xgettext -s --no-wrap --default-domain=$(PROJECT) --output=- $^ \
		    --keyword=N_ --keyword=NP_ \
		| sed -e 's/charset=CHARSET/charset=UTF-8/' >$@
	b=`echo $@ | sed -e 's/\.[^\.]*$$//'`; \
	if ! diff -u -B -I 'POT-Creation-Date' $@ $@.0; then \
		for po in $(POFILES); do \
			if [ -e $$po ]; then \
				savelog -l $$po || mv $$po $$po.0; \
				msgmerge -s --no-wrap $$po.0 $@ > $$po; \
			else \
				cp $@ $$po; \
			fi; \
		done; \
		echo "Manual changes to translation strings are required."; \
		exit 1; \
	else \
		echo "No changes to translation strings."; \
	fi



stamp-gettext: locale/deity.pot $(POFILES)
	outdir="deity"; mkdir -p $$outdir; \
	for po in $(POFILES); do \
		mo=`echo $$po | sed -e 's,\(.*\)/\([^/]*\)_\([a-z]*\)\.po,\1/\3/LC_MESSAGES/\2.mo,g'`; \
		echo "po=$$po; mo=$$mo"; \
		if [ $$po -nt $$mo ]; then \
			mo="$$outdir/$$mo"; mkdir -p `dirname $$mo`; \
			msgfmt --output-file=$$mo $$po; \
		fi \
	done
	touch stamp-gettext


backup: clean
	rsync -av --delete ./ bcwhite@192.168.1.2:deity/


clean:
	find . -name "*~" -o -name "#*" | xargs rm -f
	find . -name "*.pyc" | xargs rm -f
	find . -name "*,cover" | xargs rm -f
	rm -f beastiary.dby saved.ddb
	rm -f .coverage
