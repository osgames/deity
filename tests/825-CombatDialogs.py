#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test combat mode)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.modes
import deity.gamesystem
import deity.gamesystem.harnmaster
import deity.world

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class CombatDialogTest1(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.wld = deity.world.World(deity.test.kTestWorldName)
        self.app = deity.test.App()

    def test_10_clear_characters(self):
        groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
        for g in groups:
            g.SetValueList(g.kInventoryKey, [])
        GAME.UpdateGameChars()
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertEqual([], GAME.gameCharDisplay.keys())
        mw.DoGenerate('gen', 'human', 'encounter', 'clearme')
        self.assertEqual(['z/encounter/clearme'], GAME.gameCharDisplay.keys())
        mw.DoMenuClear(testdialogactions=(
            """self.wolist.SetValue('encounter')""",
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual([], GAME.gameCharDisplay.keys())
        self.assertEqual([], GAME.gameCharNames.Get().keys())

    def test_11_clear_ground(self):
        ground = GAME.GetId(GAME.kCombatGroundTid)
        ground.SetValueList(ground.kInventoryKey, [])
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertEqual([], ground.GetInventory())
        char = GAME.Character(None)
        char.MakeUnique()
        ground.AddInventory(char)
        self.assertEqual([char], ground.GetInventory())
        mw.DoMenuClear(testdialogactions=(
            """self.wolist.SetValue('ground')""",
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual([], ground.GetInventory())

    def test_20_status(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        mw.DoGenerate('gen', 'human', 'encounter', 'statustest')
        self.assertEqual('statustest', self.app.curchar.Get().Name())
        char = self.app.curchar.Get()
        self.assertEqual('standing', char.GetInfo('status', default='standing'))
        mw.DoMenuStatus(testdialogactions=(
            """self.widgets[0].SetValue('statustest')""",
            """self.widgets[1].SetValue('prone')""",
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual('prone', char.GetInfo('status', default='standing'))


###############################################################################


class CombatDialogTest2(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.app = deity.test.App()
        self.mw  = deity.modes.Combat(Test_Win, self.app)
        self.mw.Activate()
        self.mw.DoGenerate('gen', 'human', 'encounter')
        self.ch1 = self.app.curchar.Get()
        self.cn1 = self.app.curchar.Get().Name()
        self.mw.DoGenerate('gen', 'umbath', 'encounter')
        self.ch2 = self.app.curchar.Get()
        self.cn2 = self.app.curchar.Get().Name()
        self.mw.DisplayRefresh()
        self.ch1.SetSkill('initiative', 20)
        self.mw.DoAlias('alias', self.cn1, 'c')
        self.mw.DoAdd('add', self.cn1)
        self.ch2.SetSkill('initiative', 10)
        self.mw.DoAlias('alias', self.cn2, 't')
        self.mw.DoAdd('add', self.cn2)

    def test_10_check(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)
        self.mw.DoRoundMenuCheck(testdialogactions=(
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))

    def test_20_drawdrop(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        self.ch1.AddInventory(i1)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)
        self.assertEqual(None, self.ch1.GetWielded()['lefthand'])
        self.mw.DoRoundMenuExtra()
        self.mw.DoRoundMenuDraw(testdialogactions=(
            """self.widgets[1].SetValue('lefthand')""",
            """self.widgets[3].SetValue('1')""",
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual(i1, self.ch1.GetWielded()['lefthand'])
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)
        self.mw.DoRoundMenuDrop(testdialogactions=(
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)),
        ))
        self.assertEqual(None, self.ch1.GetWielded()['lefthand'])
        self.assertEqual((GAME.GetId(GAME.kCombatGroundTid),''), i1.GetPossessor())
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)
        self.mw.DoRoundMenuDraw(testdialogactions=(
            """self.widgets[1].SetValue('lefthand')""",
            """self.widgets[3].SetValue('1')""",
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual(i1, self.ch1.GetWielded()['lefthand'])
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn2 , self.mw.curname)

    def test_30_wieldunwield(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        self.ch1.AddInventory(i1)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)
        self.assertEqual(None, self.ch1.GetWielded()['lefthand'])
        self.mw.DoRoundMenuWield(testdialogactions=(
            """self.widgets[1].SetValue('lefthand')""",
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual(i1, self.ch1.GetWielded()['lefthand'])
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)
        self.mw.DoRoundMenuUnwield(testdialogactions=(
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual(None, self.ch1.GetWielded()['lefthand'])
        self.assertEqual((self.ch1,''), i1.GetPossessor())
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn1, self.mw.curname)

    def test_40_aid(self):
        # FIXME: no support for healing others with different body types
        self.mw.DoSubtract('subtract', self.cn2)
        self.mw.DoGenerate('gen', 'human', 'encounter')
        self.ch2 = self.app.curchar.Get()
        self.cn2 = self.app.curchar.Get().Name()
        self.mw.DisplayRefresh()
        self.ch2.SetSkill('initiative', 10)
        self.mw.DoAlias('alias', self.cn2, 't')
        self.mw.DoAdd('add', self.cn1)
        self.mw.DoAdd('add', self.cn2)

        self.ch1.SetDict('bleeders', {'skull-G3blunt':'4'})
        self.ch1.SetValue('x:bloodloss', '0')
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn1, self.mw.curname)
        self.mw.DoRoundMenuAid(testdialogactions=(
            """self.widgets[0].SetValue('"""+self.cn1+"""')""",
            """self.widgets[1].SetValue('skull')""",
            """self.widgets[3].SetValue('99')""", # fail
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual({'skull-G3blunt':'0'}, self.ch1.GetDict('bleeders'))
        self.assertEqual('1', self.ch1.GetValue('x:bloodloss'))
        self.mw.DisplayRefresh()
        self.assertEqual(0, len(self.mw.roundList))
        self.assertEqual(self.cn2, self.mw.curname)
        self.mw.DoRoundMenuAid(testdialogactions=(
            """self.widgets[0].SetValue('"""+self.cn1+"""')""",
            """self.widgets[1].SetValue('skull')""",
            """self.widgets[3].SetValue('5')""", # success
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.assertEqual({}, self.ch1.GetDict('bleeders'))
        self.assertEqual('1', self.ch1.GetValue('x:bloodloss'))
        self.mw.DisplayRefresh()
        self.assertEqual(0, len(self.mw.roundList))

    def test_50_wearunwear(self):
        i1 = self.gs.Item(None)
        i1.Generate('vest-leather')
        self.ch1.AddInventory(i1)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn1, self.mw.curname)

        self.mw.DoRoundMenuWear(testdialogactions=(
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.mw.DisplayRefresh()
        self.assertEqual([i1], self.ch1.GetWorn().keys())
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn1, self.mw.curname)

        self.mw.DoRoundMenuUnwear(testdialogactions=(
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK),
        ))
        self.mw.DisplayRefresh()
        self.assertEqual([], self.ch1.GetWorn().keys())
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn1, self.mw.curname)


###############################################################################


if __name__ == '__main__':
    unittest.main()
