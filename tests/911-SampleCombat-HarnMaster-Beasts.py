#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (run full combat)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.trandom
import deity.windows
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *

import __builtin__
import wx
App  = wx.PySimpleApp()
Game = deity.gamesystem.harnmaster.HarnMaster3()
Main = deity.windows.Main(testmode=True);
Main.testmode = True
__builtin__.__dict__['Output'] = Main.Output


###############################################################################


class CombatTest_1_Beast(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        Main.CommandExecute('combat')

        # bear-lythian
        try:
            self.ct1a = GAME.MatchCharacterName('ct1a')
            GAME.SubCharacter(self.ct1a)
        except:
            pass
        self.ct1a = GAME.Character(None)
        self.ct1a.Generate('bear-lythian')
        self.ct1a.Rename('ct1a')
        GAME.AddCharacter(self.ct1a, 'encounter')

        # centaur
        try:
            self.ct1b = GAME.MatchCharacterName('ct1b')
            GAME.SubCharacter(self.ct1b)
        except:
            pass
        self.ct1b = GAME.Character(None)
        self.ct1b.Generate('centaur')
        self.ct1b.Rename('ct1b')
        GAME.AddCharacter(self.ct1b, 'encounter')
        item = GAME.Item(None)
        item.Generate('shortsword')
        self.ct1b.AddInventory(item)
        self.ct1b.WieldItem(item, defindex=0)

        # yelgri
        try:
            self.ct1c = GAME.MatchCharacterName('ct1c')
            GAME.SubCharacter(self.ct1c)
        except:
            pass
        self.ct1c = GAME.Character(None)
        self.ct1c.Generate('yelgri')
        self.ct1c.Rename('ct1c')
        GAME.AddCharacter(self.ct1c, 'encounter')
        item = GAME.Item(None)
        item.Generate('spear')
        self.ct1c.AddInventory(item)
        self.ct1c.WieldItem(item, defindex=0)

        # hru
        try:
            self.ct1d = GAME.MatchCharacterName('ct1d')
            GAME.SubCharacter(self.ct1d)
        except:
            pass
        self.ct1d = GAME.Character(None)
        self.ct1d.Generate('hru')
        self.ct1d.Rename('ct1d')
        GAME.AddCharacter(self.ct1d, 'encounter')

        # vlasta
        try:
            self.ct1e = GAME.MatchCharacterName('ct1e')
            GAME.SubCharacter(self.ct1e)
        except:
            pass
        self.ct1e = GAME.Character(None)
        self.ct1e.Generate('vlasta')
        self.ct1e.Rename('ct1e')
        GAME.AddCharacter(self.ct1e, 'encounter')

        # nolah
        try:
            self.ct1f = GAME.MatchCharacterName('ct1f')
            GAME.SubCharacter(self.ct1f)
        except:
            pass
        self.ct1f = GAME.Character(None)
        self.ct1f.Generate('nolah')
        self.ct1f.Rename('ct1f')
        GAME.AddCharacter(self.ct1f, 'encounter')
        item = GAME.Item(None)
        item.Generate('club(+1;+2)')
        self.ct1f.AddInventory(item)
        self.ct1f.WieldItem(item, defindex=0)

        # update
        Main.DisplayRefresh()

    def who(self, cur):
        while True:
            who = self.random.randint(0,5)
            if who == 0:
                who = self.ct1a
            elif who == 1:
                who = self.ct1b
            elif who == 2:
                who = self.ct1c
            elif who == 3:
                who = self.ct1d
            elif who == 4:
                who = self.ct1e
            elif who == 5:
                who = self.ct1f
            else:
                raise Exception
            if who.tid != cur.tid:
                return who

    def test_round(self):
        self.random = random = deity.trandom.Random()
        Main.CommandExecute('add ct1a')
        Main.CommandExecute('add ct1b')
        Main.CommandExecute('add ct1c')
        Main.CommandExecute('add ct1d')
        Main.CommandExecute('add ct1e')
        Main.CommandExecute('add ct1f')

        while (True):
            print 'begin round...'
            print "- %s: %s" % (self.ct1a.ShortDesc(), self.ct1a.GetInfo('status'))
            print "- %s: %s" % (self.ct1b.ShortDesc(), self.ct1b.GetInfo('status'))
            print "- %s: %s" % (self.ct1c.ShortDesc(), self.ct1c.GetInfo('status'))
            print "- %s: %s" % (self.ct1d.ShortDesc(), self.ct1d.GetInfo('status'))
            print "- %s: %s" % (self.ct1e.ShortDesc(), self.ct1e.GetInfo('status'))
            print "- %s: %s" % (self.ct1f.ShortDesc(), self.ct1f.GetInfo('status'))

            StandCount = 0
            ShockCount = 0
            DeadCount  = 0
            for c in (self.ct1a, self.ct1b, self.ct1c, self.ct1d, self.ct1e, self.ct1f):
                s = c.GetInfo('status')
                if s == 'standing':
                    StandCount += 1
                if s == 'shock':
                    ShockCount += 1
                if s == 'dead':
                    DeadCount += 1
            if DeadCount + ShockCount > 4:
                break

            print ' -',StandCount,'standing,',DeadCount,'dead'
            Main.CommandExecute('round')

            Attempts = 0
            while Main.modeCombat.submode == 'round':
                attack = Main.curchar.Get()
                defend = self.who(attack)
                if attack.GetInfo('status') == 'standing' and StandCount > 1 and defend.GetInfo('status') != 'standing':
                    print attack.ShortDesc(),attack.GetInfo('status'),'vs',defend.ShortDesc(),defend.GetInfo('status'),'not allowed'
                    Attempts += 1
                    if Attempts > 5:
                        Main.CommandExecute('pass')
                    continue
                Attempts = 0

                afrom  = ''
                dfrom  = ''
                aloc   = ''
                dloc   = ''
                if random.randint(0,2) == 0:
                    dmove = 'counterstrike'
                else:
                    dmove = 'dodge'

                # bear-lythian
                if attack == self.ct1a:
                    if random.randint(0,3) == 0:
                        afrom = 'face'
                    else:
                        afrom = 'foot'

                # centaur
                if attack == self.ct1b:
                    if random.randint(0,4) == 0:
                        afrom = 'foot'

                # yelgri
                if attack == self.ct1c:
                    rval = random.randint(0,2)
                    if rval == 0:
                        afrom = 'righthand'
                    elif rval == 1:
                        afrom = 'rightfoot'
                    else:
                        afrom = 'lefthand'

                # hru
                if attack == self.ct1d:
                    rval = random.randint(0,2)
                    if rval == 0:
                        afrom = 'rightfoot'
                    else:
                        afrom = 'righthand'

                # vlasta
                if attack == self.ct1e:
                    rval = random.randint(0,2)
                    if rval == 0:
                        afrom = 'leftlimb'
                    else:
                        afrom = 'face'
                    rval = random.randint(0,2)
                    if rval <= 1:
                        aloc = 'high'

                # nolah
                if attack == self.ct1f:
                    afrom = 'righthand'

                # attack
                aname = attack.Name()
                dname = defend.Name()
                print
                print attack.ShortDesc(),attack.GetInfo('status'),'vs',defend.ShortDesc(),defend.GetInfo('status')

                deity.test.TestUserResponse('kill?', True)
                deity.test.TestUserResponse('amputate?', True)
                deity.test.TestUserResponse('bodyblock?', True)
                deity.test.TestUserResponse('unarmed?', True)
                if attack.GetInfo('status') == 'unconscious' or attack.GetInfo('status') == 'shock' or attack.GetInfo('status') == 'dead':
                    command = 'pass'
                elif attack.GetInfo('status') == 'prone':
                    command = 'stand'
                elif defend.GetInfo('status') == 'unconscious' or defend.GetInfo('status') == 'shock':
                    deity.test.TestUserResponse('attackprone?', True)
                    command = 'attack %(dname)s %(afrom)s: %(aloc)s defend ignore' % {'dname':dname, 'afrom':afrom, 'aloc':aloc}
                elif defend.GetInfo('status') == 'prone':
                    deity.test.TestUserResponse('attackprone?', True)
                    command = 'attack %(dname)s %(afrom)s: %(aloc)s defend dodge' % {'dname':dname, 'afrom':afrom, 'aloc':aloc}
                elif defend.GetInfo('status') == 'dead': # bled out
                    command = 'pass'
                else:
                    print('%(atype)s: attack %(dtype)s %(afrom)s: %(aloc)s defend %(dmove)s %(dloc)s' %
                          {'aname':aname, 'dname':dname, 'afrom':afrom, 'aloc':aloc, 'dmove':dmove, 'dloc':dloc,
                           'atype':attack.ShortDesc(), 'dtype':defend.ShortDesc()})
                    command = 'attack %(dname)s %(afrom)s: %(aloc)s defend %(dmove)s' % {'dname':dname, 'afrom':afrom, 'aloc':aloc, 'dmove':dmove, 'dloc':dloc}

                #print '-',attack.Name(),':',command
                Main.CommandExecute(command)
                #print Main.io.output.GetValue()

        #App.MainLoop()


###############################################################################


if __name__ == '__main__':
    unittest.main()
