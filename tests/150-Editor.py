#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test editor classes)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test

from deity.editor import *
from deity.exceptions import *


###############################################################################


class ValueManagerTest(deity.test.TestCase):

    class ValueManagerUpdateChecker(deity.editor.ValueManager):

        def __init__(self, tester):
            self.tester = tester
            deity.editor.ValueManager.__init__(self)

        def ValueUpdated(self, var, new, old):
            self.tester.assertIn(var, self.stack)
            return None

    class ValueManagerUpdateFailer(ValueManagerUpdateChecker):

        def ValueUpdated(self, var, new, old):
            raise Exception('go away')


    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.vm = self.ValueManagerUpdateChecker(self)
        self.nkey = None
        self.nnew = None
        self.nold = None

    def notify(self, changes):
        self.nkey = changes[0][0]
        self.nnew = changes[0][1]
        self.nold = changes[0][2]

    def test_10_init(self):
        pass

    def test_20_SetValue(self):
        self.assertEqual(None, self.vm.GetValue('a'))
        self.vm.SetValue('a', '1')
        self.assertEqual('1', self.vm.GetValue('a'))
        self.assertRaises(Exception, self.vm.SetValue, 'a', '1')
        self.vm.ClearValues()
        self.assertEqual(None, self.vm.GetValue('a'))

    def test_21_SetValue_bad(self):
        self.assertRaises(Exception, self.vm.SetValue, 'a', 5)

    def test_30_Change(self):
        self.assertRaises(Exception, self.vm.ChangeValue, 'a', '2')
        self.vm.SetValue('a', '1')
        self.assertEqual('1', self.vm.GetValue('a'))
        self.vm.ChangeValue('a', '2')
        self.assertEqual('2', self.vm.GetValue('a'))
        self.assertRaises(Exception, self.vm.ChangeValue, 'a', 3)

    def test_40_notify(self):
        self.vm.SetValue('a', '1')
        self.assertEqual('1', self.vm.GetValue('a', notify=self.notify))
        self.assertEqual(None, self.nkey)
        self.vm.ChangeValue('a', '1')
        self.assertEqual(None, self.nkey)
        self.vm.ChangeValue('a', '2')
        self.assertEqual('a', self.nkey)
        self.assertEqual('1', self.nold)
        self.assertEqual('2', self.nnew)
        self.vm.ClearNotify()
        self.assertEqual({}, self.vm.notify)

    def test_50_update_failed(self):
        self.vm = self.ValueManagerUpdateFailer(self)
        self.vm.SetValue('a', '1')
        self.vm.ChangeValue('a', '2')
        output = deity.test.TestOutputGet()
        self.assertIn('go away', output)

    def test_60_circular_dependency(self):
        self.assertRaises(Exception, self.vm.ChangeValue, 'a', '2')
        self.vm.SetValue('a', '1')
        self.vm.stack = {'a': 0}
        self.assertRaises(Exception, self.vm.ChangeValue, 'a', '2')


###############################################################################


if __name__ == '__main__':
    unittest.main()
