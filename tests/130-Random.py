#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test random generator)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.trandom


###############################################################################


class Random_Test(deity.test.TestCase):

    def test_00(self):
        rand = deity.trandom.Random()
        self.assertEqual(False, rand.testmode)
        self.assertEqual({}, rand.testresults)
        rand.testset('a', 1)
        rand.testset('b', (2,3))
        self.assertEqual(True, rand.testmode)
        self.assertEqual({'a':[1], 'b':[3,2]}, rand.testresults)
        self.assertEqual(1, rand.testget('a', 0, 2))
        self.assertRaises(Exception, rand.testget, 'b', 1, 2)
        self.assertEqual(3, rand.testget('b', 2, 4))
        self.assertRaises(KeyError, rand.testget, 'c', 1, 2)
        self.assertEqual(True, rand.testmode)
        self.assertEqual({}, rand.testresults)
        rand.testend()
        self.assertEqual(False, rand.testmode)
        self.assertEqual({}, rand.testresults)
        rand.testset('d', 3)
        self.assertRaises(Exception, rand.testend)
        self.assertEqual(False, rand.testmode)
        self.assertEqual({}, rand.testresults)

    def test_10_random(self):
        rand = deity.trandom.Random()
        for i in xrange(0,100):
            self.assertTrue(0.0 <= rand.random('test') < 1.0)
        for i in xrange(0,100):
            rand.testset('test', 0.9)
            self.assertEqual(0.9, rand.random('test'))

    def test_20_randint(self):
        rand = deity.trandom.Random()
        for i in xrange(0,100):
            self.assertTrue(1 <= rand.randint(1,10,'test') <= 10)
        for i in xrange(0,100):
            rand.testset('test', 9)
            self.assertEqual(9, rand.randint(1,10,'test'))

    def chisquared_int(self, count, begin, end, random):
        choices = (end - begin + 1)
        average = count / choices
        counts  = [0,] * choices
        #print choices,average
        for i in xrange(0, count):
            r = random.randint(begin, end)
            counts[r - begin] += 1
            #print r,
        #print
        sqsum = 0.0
        for i in xrange(0,choices):
            dev = counts[i] - average
            sqsum += dev * dev * 1.0 / average
            #print counts[i],
        #print
        return sqsum

    def disabled_test_80_chi6(self):
        rng = deity.trandom.Random()
        chi = self.chisquared_int(100000, 1, 6, rng)
        print chi,"",
        self.assertLess(chi, 15.0)

    def disabled_test_81_chi20(self):
        rng = deity.trandom.Random()
        chi = self.chisquared_int(100000, 1, 20, rng)
        print chi,"",
        self.assertLess(chi, 35.0)

    def disabled_test_82_chi100(self):
        rng = deity.trandom.Random()
        chi = self.chisquared_int(100000, 1, 100, rng)
        print chi,"",
        self.assertLess(chi, 150.0)


###############################################################################


if __name__ == '__main__':
    unittest.main()
