#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test mode base)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.gamesystem
import deity.gamesystem.harnmaster
import deity.modes
import deity.world

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class ModeTestBase(deity.test.TestCase):

    def saveParams(self, *opts, **kwds):
        self.callOpts = opts
        self.callKwds = kwds


###############################################################################


class ModeBaseTest(ModeTestBase):

    def setUp(self):
        ModeTestBase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.app = deity.test.App()

    def test_10_clientdata(self):
        mw = deity.modes.Mode(Test_Win, self.app)
        self.assertEqual(None, mw.GetClientData())
        mw.SetClientData(42)
        self.assertEqual(42, mw.GetClientData())

    def test_20_activate(self):
        mw = deity.modes.Mode(Test_Win, self.app)
        mw.Activate('test123')
        self.assertEqual('test123', self.app.commandPrompt)

    def test_30_DoGenerate(self):
        mw = deity.modes.Mode(Test_Win, self.app)
        self.assertRaises(BadParameter, mw.DoGenerate, 'gen')
        self.assertRaises(BadParameter, mw.DoGenerate, 'gen', 1, 2)
        mw.DoGenerate('gen', 'human', 'players')
        ch = self.app.curchar.Get()
        self.assertEqual('players', ch.GetValue('x:gamegroup'))
        self.assertEqual('human01', ch.Name())
        pos,loc = ch.GetPossessor()


###############################################################################


class ModeCombatTest(ModeTestBase):

    def setUp(self):
        ModeTestBase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.wld = deity.world.World(deity.test.kTestWorldName)
        self.app = deity.test.App()
        self.callOpts = ()
        self.callKwds = {}

        self.mw  = deity.modes.Combat(Test_Win, self.app)
        self.ch  = list()
        self.cn  = list()
        self.mw.Activate()
        for i in xrange(0,2):
            self.mw.DoGenerate('gen', 'human', 'encounter')
            self.ch.append(self.app.curchar.Get())
            self.cn.append(self.app.curchar.Get().Name())
        self.mw.DisplayRefresh()
        for i in xrange(0,2):
            self.ch[i].SetSkill('initiative', (9-i)*10)
            self.mw.DoAlias('alias', self.cn[i], 'c%d'%i)
            self.mw.DoAdd('add', self.cn[i])


    def test_00_Initiative(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefreshRound()
        self.assertEqual(self.ch[0].Name(), self.app.curchar.Get().Name())
        self.assertEqual(self.cn[0], self.app.curchar.Get().Name())
        self.mw.DoRoundNext()
        self.mw.DisplayRefreshRound()
        self.assertEqual(self.ch[1].Name(), self.app.curchar.Get().Name())
        self.assertEqual(self.cn[1], self.app.curchar.Get().Name())


    def test_DoRoundAttack(self):
        self.gs.CombatAttack = self.saveParams
        self.mw.DoRound('round')
        self.mw.DisplayRefreshRound()

        self.mw.DoRoundAttack('attack', self.cn[1], *('righthand:point +100 5!12 defend dodge -100 99'.split(' ')))
        self.assertEqual((self.ch[0], self.ch[1]), self.callOpts)
        self.assertEqual({
                'attackx': 'attack',
                'ausex': 'righthand', 'aaspectx': 'point', 'arangex': None, 'alocx': None, 'amod': '+100', 'aroll': '5', 'adamage': '12',
                'defensex': 'dodge',
                'dusex': None, 'daspectx': None, 'drangex': None, 'dlocx': None, 'dmod': '-100', 'droll': '99', 'ddamage': None,
        }, self.callKwds)

        self.mw.DoRoundAttack('attack', self.cn[1], *('22 defend dodge 88'.split(' ')))
        self.assertEqual((self.ch[0], self.ch[1]), self.callOpts)
        self.assertEqual({
                'attackx': 'attack',
                'ausex': None, 'aaspectx': None, 'arangex': None, 'alocx': None, 'amod': None, 'aroll': '22', 'adamage': None,
                'defensex': 'dodge',
                'dusex': None, 'daspectx': None, 'drangex': None, 'dlocx': None, 'dmod': None, 'droll': '88', 'ddamage': None,
        }, self.callKwds)

        self.mw.DoRoundAttack('attack', self.cn[1], *('0hi 22 defend counterstrike lo'.split(' ')))
        self.assertEqual((self.ch[0], self.ch[1]), self.callOpts)
        self.assertEqual({
                'attackx': 'attack',
                'ausex': None, 'aaspectx': None, 'arangex': None, 'alocx': '0hi', 'amod': None, 'aroll': '22', 'adamage': None,
                'defensex': 'counterstrike',
                'dusex': None, 'daspectx': None, 'drangex': None, 'dlocx': 'lo', 'dmod': None, 'droll': None, 'ddamage': None,
        }, self.callKwds)

        self.mw.DoRoundAttack('attack', self.cn[1], *('face !2 defend counterstrike !1'.split(' ')))
        self.assertEqual((self.ch[0], self.ch[1]), self.callOpts)
        self.assertEqual({
                'attackx': 'attack',
                'ausex': None, 'aaspectx': None, 'arangex': None, 'alocx': 'face', 'amod': None, 'aroll': '', 'adamage': '2',
                'defensex': 'counterstrike',
                'dusex': None, 'daspectx': None, 'drangex': None, 'dlocx': None, 'dmod': None, 'droll': '', 'ddamage': '1',
        }, self.callKwds)

        self.mw.DoRoundAttack('attack', self.cn[1], *('defend counterstrike hand:'.split(' ')))
        self.assertEqual((self.ch[0], self.ch[1]), self.callOpts)
        self.assertEqual({
                'attackx': 'attack',
                'ausex': None, 'aaspectx': None, 'arangex': None, 'alocx': None, 'amod': None, 'aroll': None, 'adamage': None,
                'defensex': 'counterstrike',
                'dusex': 'hand', 'daspectx': None, 'drangex': None, 'dlocx': None, 'dmod': None, 'droll': None, 'ddamage': None,
        }, self.callKwds)

    def test_DoRoundBusy(self):
        self.gs.CombatAttack = self.saveParams
        self.mw.DoRound('round')
        self.mw.DisplayRefreshRound()
        self.assertEqual(None, self.ch[0].GetInfo('busy'))

        rtime = WORLD.TimeGetCurrent()
        self.mw.DoRoundBusy('busy', '2')
        WORLD.TimeAdjust(''.join(self.gs.kCombatRoundTime), rtime)
        WORLD.TimeAdjust(''.join(self.gs.kCombatRoundTime), rtime)
        self.assertEqual(WORLD.TimeExport(rtime), self.ch[0].GetInfo('busy'))

        rtime = WORLD.TimeGetCurrent()
        self.mw.DoRoundBusy('busy', '2h')
        WORLD.TimeAdjust('+2h', rtime)
        self.assertEqual(WORLD.TimeExport(rtime), self.ch[0].GetInfo('busy'))

        rtime = WORLD.TimeGetCurrent()
        self.mw.DoRoundBusy('busy', '7s')
        WORLD.TimeAdjust('+7s', rtime)
        self.assertEqual('', self.ch[0].GetInfo('busy'))


###############################################################################


class ModeExploreTest(ModeTestBase):

    def setUp(self):
        ModeTestBase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.wld = deity.world.World(deity.test.kTestWorldName)
        self.app = deity.test.App()
        self.callOpts = ()
        self.callKwds = {}

        self.mw  = deity.modes.Explore(Test_Win, self.app)
        self.ch  = list()
        self.cn  = list()
        self.mw.Activate()
        for i in xrange(0,2):
            self.mw.DoGenerate('gen', 'human', 'encounter')
            self.ch.append(self.app.curchar.Get())
            self.cn.append(self.app.curchar.Get().Name())
            self.gs.SkillLevel(self.app.curchar.Get(), 'swords', open=True)
        self.mw.DisplayRefresh()


    def test_DoMount(self):
        self.gs.Character.MountCharacter = self.saveParams
        self.app.curchar.Set(self.ch[0])
        self.assertRaises(BadParameter, self.mw.DoMount, 'mount')
        self.assertRaises(BadParameter, self.mw.DoMount, 'mount', self.cn[1], 'extra')
        self.mw.DoMount('mount', self.cn[1])
        self.assertEqual((self.ch[1],), self.callOpts)

    def test_DoDismount(self):
        self.gs.Character.Dismount = self.saveParams
        self.app.curchar.Set(self.ch[0])
        self.assertRaises(NotPossible, self.mw.DoDismount, 'dismount')  # not mounted
        self.ch[0].MountCharacter(self.ch[1])
        self.assertRaises(BadParameter, self.mw.DoDismount, 'dismount', 'extra')
        self.mw.DoDismount('dismount')
        self.assertEqual((), self.callOpts)

    def test_DoTest(self):
        self.gs.Character.SkillCheck = self.saveParams
        self.assertRaises(BadParameter, self.mw.DoTest, 'fooskill')
        self.mw.DoTest('test', 'swords')
        self.assertRaises(BadParameter, self.mw.DoTest, 'shortsword')
        self.gs.SkillLevel(self.app.curchar.Get(), 'shortsword/swords', open=True)
        self.mw.DoTest('test', 'shortsword')
        
    def test_DoTestAll(self):
        self.gs.Character.SkillCheck = self.saveParams
        self.assertRaises(BadParameter, self.mw.DoTestAll, 'fooskill')
        self.mw.DoTestAll('alltest', 'swords')
        self.assertRaises(BadParameter, self.mw.DoTestAll, 'shortsword')
        self.gs.SkillLevel(self.app.curchar.Get(), 'shortsword/swords', open=True)
        self.mw.DoTestAll('alltest', 'shortsword')
        
    def test_DoLet(self):
        self.gs.Character.MountCharacter = self.saveParams
        self.callOpts = None
        self.app.CommandExecute('display %s' % self.cn[0])
        self.mw.DisplayRefresh()
        self.assertEqual(self.ch[0], self.mw.app.curchar.Get())
        self.app.CommandExecute('let %s mount %s' % (self.cn[1], self.cn[0]))
        self.mw.DisplayRefresh()
        self.assertEqual(self.ch[0], self.mw.app.curchar.Get())
        self.assertEqual((self.ch[0],), self.callOpts)


###############################################################################


if __name__ == '__main__':
    unittest.main()
