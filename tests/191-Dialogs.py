#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test common dialogs)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.dialogs

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class ChooseOptionsDialogTest(deity.test.TestCase):

    kOptions = {
        'SomethingOne': {
            'category':	'cat1',
            'order':	10,
            'rule':	'SomethingOne',
            'default':	False,
            'short':	'Something One',
            'long':	'This is the first something.',
        },
        'SomethingTwo': {
            'category':	'cat1',
            'order':	20,
            'rule':	'SomethingTwo',
            'default':	True,
            'short':	'Something Two',
            'long':	'This is the second something.',
        },
        'SomethingThree': {
            'category':	'cat1',
            'order':	30,
            'rule':	'SomethingThree',
            'default':	False,
            'short':	'Something Three',
            'long':	'This is the third something.',
        },
        'SomethingFour': {
            'category':	'cat1',
            'order':	40,
            'rule':	'SomethingFour',
            'default':	True,
            'short':	'Something Four',
            'long':	'This is the fourth something.',
        },
        'SomethingFive': {
            'category':	'cat1',
            'order':	50,
            'rule':	'SomethingFive',
            'default':	True,
            'short':	'Something Five',
            'long':	'This is the fifth something.',
        },
        'SomethingElse': {
            'category':	'cat1',
            'order':	55,
            'rule':	'SomethingElse',
            'options':	{'1':NP_('option:abc'), '2':NP_('option:ijk'), '3':NP_('option:wxyz')},
            'default':	'2',
            'short':	'Something Else',
            'long':	'This is the else something.',
        },
        'SomethingOther': {
            'category':	'cat1',
            'order':	56,
            'rule':	'SomethingOther',
            'options':	{'1':NP_('option:123'), '2':NP_('option:456'), '3':NP_('option:789')},
            'default':	'1',
            'short':	'Something Other',
            'long':	'This is the other something.',
        },
        'SomethingSix': {
            'category':	'cat1',
            'order':	70,
            'rule':	'SomethingSix',
            'default':	True,
            'short':	'Something Six',
            'long':	'This is the sixth something.',
        },
        'SomethingAgain': {
            'category':	'cat1',
            'order':	75,
            'rule':	'SomethingAgain',
            'options':	{'hello':NP_('option:Hello, World!'), 'goodbye':NP_('option:Goodbye, World!'), 'back':NP_('option:Welcome back!')},
            'default':	'hello',
            'short':	'Something Again',
            'long':	'This is the again something.',
        },

        'AnotherOne': {
            'category':	'cat2',
            'order':	30,
            'rule':	'AnotherOne',
            'default':	False,
            'short':	'Another One',
            'long':	'This is the first other.',
        },
        'AnotherTwo': {
            'category':	'cat2',
            'order':	20,
            'rule':	'AnotherTwo',
            'default':	False,
            'short':	'Another Two',
            'long':	'This is the second other.',
        },

        'ChooseOne': {
            'category':	'aaa',
            'order':	50,
            'rule':	'ChooseOne',
            'default':	True,
            'short':	'Chose One',
            'long':	'This is the first choice.',
        },
    }

    kCatOrder   = ['cat1', 'cat2']

    kDefaultOptions = {}
    for r,o in kOptions.iteritems():
        v = o['default']
        if v == True: v = 'yes'
        if v == False: v = 'no'
        kDefaultOptions[o['rule']] = v


    def ruleWidget(self, dialog, rule):
        for w,r in dialog.widgetRules.iteritems():
            if r['rule'] == rule:
                return w
        self.assertTrue(False)

    def test_10_layout(self):
        c = {}
        d = deity.dialogs.ChooseOptionsDialog(c, self.kCatOrder, self.kOptions)
        self.assertEqual(['cat1', 'cat2', 'aaa'], d.categoryOrder)
        self.assertEqual(10,len(d.layout))
        self.assertEqual(4, len(d.layout[0]))
        self.assertEqual(5, len(d.layout[1]))
        self.assertEqual(1, len(d.layout[2]))
        self.assertEqual(1, len(d.layout[3]))
        self.assertEqual(1, len(d.layout[4]))
        self.assertEqual(1, len(d.layout[5]))
        self.assertEqual(4, len(d.layout[6]))
        self.assertEqual(2, len(d.layout[7]))
        self.assertEqual(3, len(d.layout[8]))
        self.assertEqual(1, len(d.layout[9]))

    def test_20_defaults(self):
        c = {'SomethingOne':'yes', 'SomethingTwo':'no', 'AnotherOne':'', 'SomethingElse':'3', 'SomethingAgain':'halt'}
        x = self.kDefaultOptions.copy()
        for k,v in c.iteritems():
            if v == True or v == '': v = 'yes'
            if v == False: v = 'no'
            x[k] = v
        x['SomethingAgain'] = 'hello'  # value above isn't valid -- revert to default
        d = deity.dialogs.ChooseOptionsDialog(c, self.kCatOrder, self.kOptions)
        d.testDialogActions = [
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        self.assertTrue(d.Run())
        self.assertEqual(x, c)

    def test_30_change_checkbox(self):
        c = {}
        x = self.kDefaultOptions.copy()
        d = deity.dialogs.ChooseOptionsDialog(c, self.kCatOrder, self.kOptions, allowcancel=False, shownew=True)
        d.testDialogActions = [
            (self.ruleWidget(d, 'SomethingTwo').SetValue, False),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        x['SomethingTwo'] = 'no'
        self.assertTrue(d.Run())
        self.assertEqual(x, c)

    def test_31_change_combobox(self):
        c = {}
        x = self.kDefaultOptions.copy()
        d = deity.dialogs.ChooseOptionsDialog(c, self.kCatOrder, self.kOptions)
        d.testDialogActions = [
            (self.ruleWidget(d, 'SomethingAgain').SetValue, 'Goodbye, World!'),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        x['SomethingAgain'] = 'goodbye'
        self.assertTrue(d.Run())
        self.assertEqual(x, c)

    def test_40_reset(self):
        c = {}
        x = self.kDefaultOptions.copy()
        d = deity.dialogs.ChooseOptionsDialog(c, self.kCatOrder, self.kOptions)
        d.testDialogActions = [
            (self.ruleWidget(d, 'SomethingTwo').SetValue, False),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_RESET), d.breset),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        self.assertTrue(d.Run())
        self.assertEqual(x, c)

    def test_50_cancel(self):
        c = {}
        d = deity.dialogs.ChooseOptionsDialog(c, self.kCatOrder, self.kOptions)
        d.testDialogActions = [
            (self.ruleWidget(d, 'SomethingTwo').SetValue, False),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_CANCEL))
        ]
        self.assertFalse(d.Run())
        self.assertEqual({}, c)


###############################################################################


class ChooseOneDialogTest(deity.test.TestCase):

    kCharacterList = ['char1','char3','char2']

    def test_10_ok(self):
        d = deity.dialogs.ChooseOneDialog(self.kCharacterList, text="choose something", title="Choose Test", default='char3')
        self.assertEqual('char3', d.wolist.GetValue())
        d.testDialogActions = [
            (d.wolist.SetValue, 'char1'),
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
        ]
        self.assertEqual('char1', d.Run())

    def test_20_cancel(self):
        d = deity.dialogs.ChooseOneDialog(self.kCharacterList, text="choose something", title="Choose Test", default='char3')
        d.testDialogActions = [
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_CANCEL),)  # make this one a tuple for coverage
        ]
        self.assertEqual(None, d.Run())

    #def test_30_reset(self):
    #    d = deity.dialogs.ChooseOneDialog(self.kCharacterList, text="choose something", title="Choose Test", default='char3')
    #    d.testDialogActions = [
    #        (d.wolist.SetValue, 'char1'),
    #        (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_RESET), d.breset),
    #        (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
    #    ]
    #    self.assertEqual('char3', d.Run())


###############################################################################


class GeneralSelectDialogTest(deity.test.TestCase):

    kCharacterList = ['char1','char3','char2']
    kAbilityList = xlist(['test1','test3','test2'], xlate='')

    kOptions = ({
        'name':		'char',
        'heading':	'Character',
        'list':		kCharacterList,
        'default':	'char3',
    }, {
        'name':		'ability',
        'heading':	'Ability',
        'xlist':	kAbilityList,
        'default':	'Case2',
    }, {
        'name':		'mod',
        'heading':	'Modifier',
        'text':		None,
        'default':	'+1',
    }, {
        'name':		'roll',
        'heading':	'Roll',
        'text':		None,
    });

    def test_10_ok(self):
        d = deity.dialogs.GeneralSelectDialog(self.kOptions, text="choose something", title="Choose Test")
        self.assertEqual('char3', d.widgets[0].GetValue())
        self.assertEqual('Case2', d.widgets[1].GetValue())
        self.assertEqual('+1', d.widgets[2].GetValue())
        d.testDialogActions = [
            (d.widgets[0].SetValue, 'char1'),
            (d.widgets[1].SetValue, 'Case1'),
            (d.widgets[2].SetValue, '-2'),
            ("""self.widgets[3].SetValue('19')"""),
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK)
        ]
        self.assertTrue(d.Run())
        self.assertEqual('char1', d.GetValue(0))
        self.assertEqual('test1', d.GetValue(1))
        self.assertEqual('Case1', d.GetValueByName('ability').xval())
        self.assertEqual('-2', d.GetValue(2))
        self.assertEqual('19', d.GetValue(3))
        self.assertRaises(Exception, d.GetValueByName, 'blah')

    def test_20_cancel(self):
        d = deity.dialogs.GeneralSelectDialog(self.kOptions, text="choose something", title="Choose Test")
        d.testDialogActions = [
            wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_CANCEL)
        ]
        self.assertFalse(d.Run())

    def test_30_reset(self):
        d = deity.dialogs.GeneralSelectDialog(self.kOptions, text="choose something", title="Choose Test")
        d.testDialogActions = [
            (d.widgets[0].SetValue, 'char1'),
            (d.widgets[1].SetValue, 'Case1'),
            (d.widgets[2].SetValue, '-1'),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_RESET), d.breset),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        self.assertTrue(d.Run())
        self.assertEqual('char3', d.GetValueByName('char'))
        self.assertEqual('Case2', d.GetValue(1).xval())
        self.assertEqual('+1', d.GetValue(2))
        self.assertEqual('', d.GetValue(3))


###############################################################################


class TestAbilityDialogTest(deity.test.TestCase):

    kCharacterList = ['char1','char3','char2']
    kAbilityList = xlist(['test1','test3','test2'], xlate='')

    def test_10_ok(self):
        d = deity.dialogs.TestAbilityDialog(self.kCharacterList, self.kAbilityList, text="choose something", title="Choose Test",
                                            abilitydefault='test2', modifierdefault='+1', allowall=True)
        self.assertEqual('all', d.wcharlist.GetValue())
        self.assertEqual('Case2', d.wabilitylist.GetValue())
        self.assertEqual('+1', d.wmodifier.GetValue())
        d.testDialogActions = [
            (d.wcharlist.SetValue, 'char1'),
            (d.wabilitylist.SetValue, 'Case1'),
            (d.wmodifier.SetValue, '-2'),
            (d.wroll.SetValue, '19'),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        self.assertTrue(d.Run())
        self.assertEqual('char1', d.GetCharacter())
        self.assertEqual('test1', d.GetAbility())
        self.assertEqual('Case1', d.GetAbility().xval())
        self.assertEqual('-2', d.GetModifier())
        self.assertEqual('19', d.GetRoll())

    def test_20_cancel(self):
        d = deity.dialogs.TestAbilityDialog(self.kCharacterList, self.kAbilityList, text="choose something", title="Choose Test",
                                            abilitydefault='test2', modifierdefault='+1')
        d.testDialogActions = [
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_CANCEL))
        ]
        self.assertFalse(d.Run())

    def test_30_reset(self):
        d = deity.dialogs.TestAbilityDialog(self.kCharacterList, self.kAbilityList, text="choose something", title="Choose Test",
                                            chardefault='char3', abilitydefault='test2', modifierdefault='+1')
        d.testDialogActions = [
            (d.wcharlist.SetValue, 'char1'),
            (d.wabilitylist.SetValue, 'Case1'),
            (d.wmodifier.SetValue, '-1'),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_RESET), d.breset),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        self.assertTrue(d.Run())
        self.assertEqual('char3', d.wcharlist.GetValue())
        self.assertEqual('Case2', d.wabilitylist.GetValue())
        self.assertEqual('+1', d.wmodifier.GetValue())
        self.assertEqual('', d.GetRoll())

    def test_50_nochars(self):
        d = deity.dialogs.TestAbilityDialog(None, self.kAbilityList, text="choose something", title="Choose Test",
                                            abilitydefault='test2', modifierdefault='+1')
        self.assertEqual('Case2', d.wabilitylist.GetValue())
        self.assertEqual('+1', d.wmodifier.GetValue())
        d.testDialogActions = [
            (d.wabilitylist.SetValue, 'Case1'),
            (d.wmodifier.SetValue, '-2'),
            (d.wroll.SetValue, '19'),
            (wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, wx.ID_OK))
        ]
        self.assertTrue(d.Run())
        self.assertEqual('test1', d.GetAbility())
        self.assertEqual('Case1', d.GetAbility().xval())
        self.assertEqual('-2', d.GetModifier())
        self.assertEqual('19', d.GetRoll())


###############################################################################


if __name__ == '__main__':
    unittest.main()
