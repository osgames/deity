#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test Kelestia/Harn module)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import random
import unittest
from test import test_support

import deity
import deity.test
import deity.trandom
import deity.world.kelestia.kethira
import deity.world.kelestia.kethira.harn

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class Harn_00_Test(deity.test.TestCase):

    def test_100_weather(self):
        GDB['/World/Kelestia/Kethira/CurTime'] = '720-01-01 01:10:20.30'
        GDB['/World/Kelestia/Kethira/TimeZone'] = '+0000'
        deity.world.kelestia.kethira.harn.Harn()
        w = WORLD.WeatherGetOutput()
        WORLD.weatherData = 'CB' + WORLD.weatherData[2:]
        w = WORLD.WeatherGetOutput()
        self.assertEqual('cool, with SE breeze(8-24), clear with fog/mist', w)
        # test year generation
        self.assertTrue('%s/WeatherData-719' % WORLD.gdbPrefix not in GDB)
        self.assertTrue('%s/WeatherData-720' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-721' % WORLD.gdbPrefix not in GDB)
        self.assertEqual(12*30*6*2, len(WORLD.weatherData))
        self.assertTrue('%s/WeatherLastYear' % WORLD.gdbPrefix in GDB)
        self.assertEqual('720', GDB['%s/WeatherLastYear' % WORLD.gdbPrefix])
        WORLD.TimeAdjust('+1y')
        w = WORLD.WeatherGetOutput()
        self.assertTrue('%s/WeatherData-721' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-722' % WORLD.gdbPrefix not in GDB)
        self.assertEqual(12*30*6*2, len(WORLD.weatherData))
        self.assertTrue('%s/WeatherLastYear' % WORLD.gdbPrefix in GDB)
        self.assertEqual('721', GDB['%s/WeatherLastYear' % WORLD.gdbPrefix])
        WORLD.TimeAdjust('+5y')
        w = WORLD.WeatherGetOutput()
        self.assertTrue('%s/WeatherData-722' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-723' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-724' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-725' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-726' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-727' % WORLD.gdbPrefix not in GDB)
        self.assertEqual(12*30*6*2, len(WORLD.weatherData))
        self.assertTrue('%s/WeatherLastYear' % WORLD.gdbPrefix in GDB)
        self.assertEqual('726', GDB['%s/WeatherLastYear' % WORLD.gdbPrefix])
        olddata = WORLD.weatherData
        WORLD.TimeAdjust('-3y')
        w = WORLD.WeatherGetOutput()
        self.assertNotEqual(olddata, WORLD.weatherData)
        self.assertEqual(12*30*6*2, len(WORLD.weatherData))
        self.assertTrue('%s/WeatherLastYear' % WORLD.gdbPrefix in GDB)
        self.assertEqual('726', GDB['%s/WeatherLastYear' % WORLD.gdbPrefix])
        WORLD.TimeAdjust('-17y')
        w = WORLD.WeatherGetOutput()
        self.assertTrue('%s/WeatherData-705' % WORLD.gdbPrefix not in GDB)
        self.assertTrue('%s/WeatherData-706' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-707' % WORLD.gdbPrefix not in GDB)
        self.assertEqual(12*30*6*2, len(WORLD.weatherData))
        self.assertTrue('%s/WeatherLastYear' % WORLD.gdbPrefix in GDB)
        self.assertEqual('726', GDB['%s/WeatherLastYear' % WORLD.gdbPrefix])
        WORLD.TimeAdjust('+10y')
        w = WORLD.WeatherGetOutput()
        self.assertTrue('%s/WeatherData-715' % WORLD.gdbPrefix not in GDB)
        self.assertTrue('%s/WeatherData-716' % WORLD.gdbPrefix in GDB)
        self.assertTrue('%s/WeatherData-717' % WORLD.gdbPrefix not in GDB)
        self.assertEqual(12*30*6*2, len(WORLD.weatherData))
        self.assertTrue('%s/WeatherLastYear' % WORLD.gdbPrefix in GDB)
        self.assertEqual('726', GDB['%s/WeatherLastYear' % WORLD.gdbPrefix])

    def test_110_events(self):
        GDB['/World/Kelestia/Kethira/CurTime'] = '720-04-30 08:00:00.0'
        GDB['/World/Kelestia/Kethira/TimeZone'] = '+0000'
        deity.world.kelestia.kethira.harn.Harn()
        x = WORLD._EnvironGetEventOutput()
        self.assertEqual('[Siem] Night of Silent Renewal (lay mass)\n[Naveh] Dezenaka (high mass)', x)

    def test_120_encounter(self):
        GDB['/World/Kelestia/Kethira/CurTime'] = '720-04-30 12:00:00.0'
        GDB['/World/Kelestia/Kethira/TimeZone'] = '+0000'
        deity.world.kelestia.kethira.harn.Harn()
        WORLD.EnvironSet('road+')
        random = deity.trandom.Random()
        random.testset('road+/day', 10)
        random.testset('wilderness/day', 20)
        random.testset('wilderness/day', 50)
        random.testset('*/*/11', 30)
        x = WORLD.EncounterGetOutput(random=random)
        random.testend()
        self.assertEqual('tracks/spore/sounds, bear, sleeping/hibernating/dormant', x)

    def test_121_encounter_many(self):
        GDB['/World/Kelestia/Kethira/CurTime'] = '720-01-01 00:00:00.0'
        GDB['/World/Kelestia/Kethira/TimeZone'] = '+0000'
        deity.world.kelestia.kethira.harn.Harn()
        for i in xrange(100):
            env = WORLD.kEnvironments[random.randint(0,len(WORLD.kEnvironments)-1)]
            WORLD.EnvironSet(env)
            hr  = random.randint(0,23)
            mn  = random.randint(0,59)
            WORLD.TimeSetTime('%02d:%02d' % (hr,mn))
            x = WORLD.EncounterGetOutput()
            self.assertNotEqual(None, x)


###############################################################################


if __name__ == '__main__':
    unittest.main()
