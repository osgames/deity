#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test gamesystem module)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.gamesystem
import deity.gamesystem.harnmaster
import deity.watcher

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class HarnMaster3_10_Char_Test(deity.test.TestCase):
    gs  = deity.gamesystem.harnmaster.HarnMaster3()
    wld = deity.world.World(deity.test.kTestWorldName)

    gs.charPrompter.SetPrompt(False)

    def test_100_init(self):
        self.assertEqual('HarnMaster3', self.gs.name)
        self.assertNotEqual(self.gs.Character.kGenerateFuncs.keys(), deity.gamesystem.GameSystem.Character.kGenerateFuncs.keys())
        self.assertTrue('harnmasterbirthday' in self.gs.Character.kGenerateFuncs)
        self.assertNotEqual(0, len(self.gs.charTypeDict))

    def test_200_GenerateBirthday(self):
        ch = self.gs.Character(None)
        ch.rand.testset('birth-month', 3)
        ch.rand.testset('birth-day',  13)
        self.assertEqual('Nolus 13', ch._GenerateHarnmasterBirthday())

    def test_210_GenerateSunsign(self):
        ch = self.gs.Character(None)
        self.assertRaises(DatafillError, ch._GenerateHarnmasterSunsign)
        ch.SetInfo('birthday', 'Nuzyael 1')
        self.assertEqual('LAD', ch._GenerateHarnmasterSunsign())
        ch.SetInfo('birthday', 'Nuzyael 4')
        self.assertEqual('ULA/LAD', ch._GenerateHarnmasterSunsign())
        ch.SetInfo('birthday', 'Nolus 2')
        self.assertEqual('FEN/AHN', ch._GenerateHarnmasterSunsign())
        ch.SetInfo('birthday', 'Azura 7')
        self.assertEqual('HIR/NAD', ch._GenerateHarnmasterSunsign())

    def test_300_starting_values(self):
        ch = self.gs.Character(None)
        ch.SetAttribute('str',18)
        ch.SetAttribute('sta',16)
        ch.SetAttribute('dex',14)
        ch.SetAttribute('agl',12)
        ch.SetAttribute('eye',10)
        ch.SetAttribute('hrg', 8)
        ch.SetAttribute('sml',17)
        ch.SetAttribute('int',15)
        ch.SetAttribute('aur',13)
        ch.SetAttribute('wil',11)
        ch.SetAttribute('voi', 9)
        ch.SetAttribute('cml', 7)
        ch.SetInfo('sunsign', 'ULA/SKO')
        self.assertEqual(15, ch.GetAttributeStart('end'))
        self.assertEqual(12, ch.GetAttributeStart('mov'))
        self.assertEqual(11, ch.GetSkillBase('initiative'))
        self.assertEqual(44, ch.GetSkillStart('initiative'))
        self.assertEqual(15+2, ch.GetSkillBase('climbing'))
        self.assertEqual(60+8, ch.GetSkillStart('climbing'))
        self.assertEqual(14+1, ch.GetSkillBase('swimming'))
        self.assertEqual(14+1, ch.GetSkillStart('swimming'))
        self.assertEqual(12+1, ch.GetSkillBase('naveh'))
        self.assertEqual(12+1, ch.GetSkillStart('naveh'))
        # errors
        self.assertRaises(DatafillError, ch.GetAttributeStart, 'foo')
        self.assertRaises(DatafillError, ch.GetSkillBase, 'foo/bar')

    def test_400_Generate_human(self):
        ch = self.gs.Character(None)
        ch.Generate('human')
        for a in self.gs.kBaseAttributes:
            if not (3 <= ch.GetAttribute(a) <= 21):
                print 'ch.GetAttribute(',a,'):',ch.GetAttribute(a)
                self.assertTrue(1 <= ch.GetAttribute(a) <= 21)
        for a in self.gs.kMoreAttributes:
            if not (1 <= ch.GetAttribute(a) <= 21):
                print 'ch.GetAttribute(',a,'):',ch.GetAttribute(a)
                self.assertTrue(1 <= ch.GetAttribute(a) <= 21)
        self.assertTrue(0 < ch.GetSkill('initiative'))
        self.assertTrue(0 < ch.GetSkill('climbing'))
        self.assertTrue(0 < ch.GetSkill('unarmed'))
        self.assertEqual(0, ch.GetPenalty('ip'))
        self.assertEqual(0, ch.GetPenalty('fp'))
        self.assertEqual(0, ch.GetPenalty('up'))
        self.assertEqual(0, ch.GetPenalty('ep'))
        self.assertEqual(0, ch.GetPenalty('pp'))
        self.assertRaises(KeyError, ch.GetPenalty, 'xx')

    def test_500_Armor(self):
        ch = self.gs.Character(None)
        ch.Generate('human')
        self.assertEqual('0;', ch.GetValue('x:armor-face'))
        a1 = self.gs.Item(None)
        a1.Generate('robe-cloth')
        ch.AddInventory(a1)
        ch.WearItem(a1)
        self.assertEqual('1,1,1,1;robe-cloth', ch.GetValue('x:armor-leftknee'))
        self.assertEqual('1,1,1,1;robe-cloth', ch.GetValue('x:armor-thorax'))
        self.assertEqual('0;', ch.GetValue('x:armor-face'))
        a2 = self.gs.Item(None)
        a2.Generate('vest-leather')
        ch.AddInventory(a2)
        ch.WearItem(a2)
        self.assertEqual('1,1,1,1;robe-cloth', ch.GetValue('x:armor-leftknee'))
        self.assertEqual('3,5,4,4;robe-cloth;vest-leather', ch.GetValue('x:armor-thorax'))
        self.assertEqual('0;', ch.GetValue('x:armor-face'))
        a3 = self.gs.Item(None)
        a3.Generate('natural')
        ch.AddInventory(a3)
        ch.WearItem(a3)
        self.assertEqual('1,1,1,1;robe-cloth;natural', ch.GetValue('x:armor-leftknee'))
        self.assertEqual('3,5,4,4;robe-cloth;vest-leather;natural', ch.GetValue('x:armor-thorax'))
        self.assertEqual('0;natural', ch.GetValue('x:armor-face'))
        ch.UnwearItem(a3)
        self.assertEqual('1,1,1,1;robe-cloth', ch.GetValue('x:armor-leftknee'))
        self.assertEqual('3,5,4,4;robe-cloth;vest-leather', ch.GetValue('x:armor-thorax'))
        self.assertEqual('0;', ch.GetValue('x:armor-face'))
        return
        a4 = self.gs.Item(None)
        a4.Generate('natural(1,2,3,4,5,6)')
        ch.AddInventory(a4)
        ch.WearItem(a4)
        self.assertEqual('1,2,3,4,5,6;natural(1,2,3,4,5,6)', ch.GetValue('x:armor-face'))
        self.assertEqual('2,3,4,5,5,6;robe-cloth;natural(1,2,3,4,5,6)', ch.GetValue('x:armor-leftknee'))
        self.assertEqual('4,7,7,8,5,6;robe-cloth;vest-leather;natural(1,2,3,4,5,6)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(a4)
        ch.UnwearItem(a2)
        self.assertEqual('1,1,1,1;robe-cloth', ch.GetValue('x:armor-leftknee'))
        self.assertEqual('0;', ch.GetValue('x:armor-face'))

    def test_501_Armor_mod(self):
        ch = self.gs.Character(None)
        ch.Generate('human')
        self.assertEqual('0;', ch.GetValue('x:armor-face'))
        it = self.gs.Item(None)
        it.Generate('vest-leather')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('2,4,3,3;vest-leather', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(1,2,3,4)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('1,2,3,4;vest-leather(1,2,3,4)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(5)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('5,5,5,5;vest-leather(5)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(5,4)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('5,4,3,3;vest-leather(5,4)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(+1,+2,+3,-4)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('3,6,6,1;vest-leather(+1,+2,+3,-4)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(+2)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('4,6,5,5;vest-leather(+2)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(+2,+0)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('4,4,3,3;vest-leather(+2,+0)', ch.GetValue('x:armor-thorax'))
        ch.UnwearItem(it)
        it = self.gs.Item(None)
        it.Generate('vest-leather(+2,0)')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual('4,0,3,3;vest-leather(+2,0)', ch.GetValue('x:armor-thorax'))

    def test_900_AttackFromLocations(self):
        ch = self.gs.Character(None)
        ch.Generate(xstr('aklash'))
        afo = list()
        afl = ch.AttackFromLocations(order=afo, separate='---').keys()
        afl.sort()
        self.assertEqual(['face','leftfoot','lefthand','leftknee','rightfoot','righthand','rightknee','skull'], afl)
        self.assertEqual(['righthand','lefthand','---','skull','face','rightknee','leftknee','rightfoot','leftfoot'], afo)


###############################################################################


class HarnMaster3_20_Gs_Test(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.harnmaster.HarnMaster3()
        self.gs.CombatNewRound()
        self.gs.charPrompter.SetPrompt(False)

    def charGen(self, type='human', name=None, armor1=None, item1=None, item2=None, wield1=None, wield2=None, mount=None):
        ch = self.gs.Character(None)
        ch.Generate(type, name=name)
        ch.SetAttribute('str', 13)
        ch.SetAttribute('end', 13)
        if armor1 is not None:
            self.a1 = self.gs.Item(None)
            self.a1.Generate(armor1)
            ch.AddInventory(self.a1)
            ch.WearItem(self.a1)
        if item1 is not None:
            self.i1 = self.gs.Item(None)
            self.i1.Generate(item1)
            ch.AddInventory(self.i1)
        if item2 is not None:
            self.i2 = self.gs.Item(None)
            self.i2.Generate(item2)
            ch.AddInventory(self.i2)
        if wield1 is not None:
            self.w1 = self.gs.Item(None)
            self.w1.Generate(wield1)
            ch.AddInventory(self.w1)
            ch.WieldItem(self.w1, 'righthand')
        if wield2 is not None:
            self.w2 = self.gs.Item(None)
            self.w2.Generate(wield2)
            ch.AddInventory(self.w2)
            ch.WieldItem(self.w2, 'lefthand')
        ch.SetStatus('standing')

        if mount is not None:
            self.mt = self.gs.Character(None)
            self.mt.Generate(mount)
            ch.MountCharacter(self.mt)
        return ch

    def test_010_WoundBreakdown(self):
        winfo = self.gs.WoundBreakdown('righthand-M1b')
        self.assertEqual('righthand', winfo['location'])
        self.assertEqual('M', winfo['severity'])
        self.assertEqual(1, winfo['damage'])
        self.assertEqual('b', winfo['aspect'])
        self.assertEqual('Mb', winfo['sevasp'])
        self.assertEqual(None, winfo['lastupdate'])
        self.assertEqual(None, winfo['healrate'])
        self.assertEqual(None, winfo['infection'])
        self.assertEqual('righthand-M1b///', self.gs.WoundCompose(winfo))
        winfo = self.gs.WoundBreakdown('face-G4blunt/H3/INF-H2/99')
        self.assertEqual('face', winfo['location'])
        self.assertEqual('G', winfo['severity'])
        self.assertEqual(4, winfo['damage'])
        self.assertEqual('blunt', winfo['aspect'])
        self.assertEqual('Gb', winfo['sevasp'])
        self.assertEqual(99, winfo['lastupdate'])
        self.assertEqual('H3', winfo['healrate'])
        self.assertEqual('INF-H2', winfo['infection'])
        self.assertEqual('face-G4blunt/H3/INF-H2/99', self.gs.WoundCompose(winfo))

    def test_020_WoundDescription(self):
        self.assertEqual('bruised righthand', self.gs.WoundDescription('righthand-M1b', None))
        self.assertEqual('sliced chest', self.gs.WoundDescription('chest-S2e', None))
        self.assertEqual('punctured groin', self.gs.WoundDescription('groin-G3p', None))
        self.assertEqual('shattered leftleg', self.gs.WoundDescription('leftleg-K4b', None))
        self.assertEqual('broken face', self.gs.WoundDescription('face-G4blunt', None))

    def test_030_WoundInfo(self):
        wlocs = set(self.gs.kLocationInjuryWoundMap.keys())
        flocs = set(self.gs.kLocationInjuryFlagsMap.keys())
        self.assertEqual(set(), wlocs ^ flocs)

    def test_090_UserTest(self):
        ch = self.charGen()
        self.assertEqual('s', self.gs.UserTest(ch, '5+5', '10', passfail=True))
        self.assertEqual('f', self.gs.UserTest(ch, '4+4', 5, passfail=True))
        self.assertEqual('cs', self.gs.UserTest(ch, '10', '50'))
        self.assertEqual('ms', self.gs.UserTest(ch, '1d3+11', '50'))
        self.assertEqual('mf', self.gs.UserTest(ch, '61', '50'))
        self.assertEqual('cf', self.gs.UserTest(ch, '60', '50'))
        ch.SetPrompt(True)
        deity.test.TestUserResponse('roll', 'cs')
        self.assertEqual('cs', self.gs.UserTest(ch, '3d6', '10', test='roll'))
        deity.test.TestUserResponse('roll', 'ms')
        self.assertEqual('ms', self.gs.UserTest(ch, '3d6+5', '10', test='roll', title='foo'))
        deity.test.TestUserResponse('roll', 'mf')
        self.assertEqual('mf', self.gs.UserTest(ch, '3d6-5', '10', test='roll'))
        deity.test.TestUserResponse('roll', 'cf')
        self.assertEqual('cf', self.gs.UserTest(ch, '3d6', '10', test='roll'))
        self.assertRaises(BadParameter, self.gs.UserTest, ch, 'blah', 8)

    def test_100_AttributeCheck(self):
        ch = self.charGen(wield1='battleaxe')
        ch.SetAttribute('end', 8)
        ch.SetAttribute('sml', 1)
        ch.SetAttribute('str', 3)
        ch.SetAttribute('dex', 6)
        ch.SetAttribute('int', 9)
        ch.SetAttribute('hrg', 12)
        ch.SetAttribute('agl', 15)
        ch.SetAttribute('eye', 18)
        self.gs.rand.testset('attrcheck-sml', (1,1,1))
        self.assertTrue(self.gs.AttributeCheck(ch, 'sml'))
        self.gs.rand.testend()
        self.assertTrue(self.gs.AttributeCheck(ch, 'str', 3))
        self.assertFalse(self.gs.AttributeCheck(ch, 'int', 11))
        self.assertTrue(self.gs.AttributeCheck(ch, 'int', 11, 2))
        self.assertFalse(self.gs.AttributeCheck(ch, 'hrg', 13))
        self.assertTrue(self.gs.AttributeCheck(ch, 'hrg', 12))
        self.assertFalse(self.gs.AttributeCheck(ch, 'agl', 16))
        self.assertTrue(self.gs.AttributeCheck(ch, 'agl', 15))
        it = self.gs.Item(None)
        it.Generate('vest-scale')
        ch.AddInventory(it)
        ch.WearItem(it)
        self.assertEqual(3, ch.GetPenalty('ep'))
        ch.SetValue('x:ipenalty', '1')
        ch.SetValue('x:fpenalty', '2')
        self.assertTrue(self.gs.AttributeCheck(ch, 'str', 3))
        self.assertFalse(self.gs.AttributeCheck(ch, 'hrg', 10))
        self.assertTrue(self.gs.AttributeCheck(ch, 'hrg', 9))
        self.assertFalse(self.gs.AttributeCheck(ch, 'agl', 10))
        self.assertTrue(self.gs.AttributeCheck(ch, 'agl', 9))
        self.assertRaises(BadParameter, self.gs.AttributeCheck, ch, 'eye', 'blah')
        ch.SetStatus('mounted')
        self.assertTrue(self.gs.AttributeCheck(ch, 'str', 3))
        self.assertFalse(self.gs.AttributeCheck(ch, 'hrg', 11))
        self.assertTrue(self.gs.AttributeCheck(ch, 'hrg', 10))
        self.assertFalse(self.gs.AttributeCheck(ch, 'agl', 13))
        self.assertTrue(self.gs.AttributeCheck(ch, 'agl', 12))

    def test_110_Skills(self):
        ch = self.charGen()
        self.assertLess(0, self.gs.SkillLevel(ch, 'pummel/unarmed', open=True))
        self.assertRaises(BadParameter, self.gs.SkillLevel, ch, 'foo/bar', open=True)

    def test_111_SkillCheck(self):
        ch = self.charGen(wield1='battleaxe')
        #self.assertAlmostEqual(3.6, ch.Load())
        #ch.SetAttribute('end', 8)
        #self.assertEqual(0, ch.GetPenalty('ep'))
        ch.SetAttribute('end', 7)
        self.assertEqual(1, ch.GetPenalty('ep'))
        self.assertEqual('s', self.gs.SkillCheck(ch, 's'))
        self.assertEqual('cf', self.gs.SkillCheck(ch, 'cf'))
        ch.SetValue('x:ipenalty', '1')
        ch.SetValue('x:fpenalty', '1')
        sk = ch.GetSkill('initiative') - 3*5 # 3 penalty points * 5
        self.assertEqual('cs', self.gs.SkillCheck(ch, 'initiative', roll=5))
        self.assertEqual('ms', self.gs.SkillCheck(ch, 'initiative', roll=4))
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'initiative', roll=96))
        self.assertEqual('cf', self.gs.SkillCheck(ch, 'initiative', roll=100, msg='fooled ya'))
        self.gs.rand.testset('skillcheck-initiative', sk)
        self.assertEqual(True, self.gs.SkillCheck(ch, 'initiative', passfail=True))
        self.gs.rand.testend()
        self.gs.rand.testset('skillcheck-initiative', sk+1)
        self.assertEqual(False, self.gs.SkillCheck(ch, 'initiative', passfail=True))
        self.gs.rand.testend()
        self.assertEqual(True,  self.gs.SkillCheck(ch, 'initiative', locmod=+8, itemmod=-16, modifier=+32, mult=0.5, roll=(sk+8-16+32)//2, passfail=True))
        self.assertEqual(False, self.gs.SkillCheck(ch, 'initiative', locmod=+8, itemmod=-16, modifier=+32, mult=0.5, roll=(sk+8-16+32)//2+1, passfail=True))
        self.assertRaises(BadParameter, self.gs.SkillCheck, ch, 'initiative', roll='blah')
        # derived skill check
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'unarmed', roll=99))
        self.assertRaises(BadParameter, self.gs.SkillCheck, ch, 'battleaxe')
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'battleaxe/axes', roll=99, open=True))
        # unknown skill check
        self.assertRaises(BadParameter, self.gs.SkillCheck, ch, 'blah', roll=1)
        self.assertRaises(BadParameter, self.gs.SkillCheck, ch, '', roll=1)
        # unlearned skill check
        self.assertRaises(NotPossible, self.gs.SkillCheck, ch, 'drawing', roll=6, passfail=True)
        self.assertRaises(NotPossible, self.gs.SkillCheck, ch, 'drawing', roll=5, passfail=True)
        # auto-open skill check
        self.assertRaises(NotPossible, self.gs.SkillCheck, ch, 'physician', roll=1)
        self.assertEqual(False, self.gs.SkillCheck(ch, 'physician', roll=99, passfail=True, open=True))
        self.assertEqual(True,  self.gs.SkillCheck(ch, 'physician', roll=1,  passfail=True, open=False))

    def test_112_SkillCheck_wearmod(self):
        ch = self.charGen(item1="_testring")
        ch.SetSkill('unarmed', 55)
        ch.SetAttribute('end', 7)
        self.assertEqual(0, ch.GetPenalty('ep'))
        self.assertEqual('+20', self.i1.GetValue('s:unarmed'))
        sk = ch.GetSkill('unarmed') - 0*5 # penalty points * 5
        self.assertEqual('ms', self.gs.SkillCheck(ch, 'unarmed', roll=sk-1))
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'unarmed', roll=sk+1))
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'unarmed', roll=sk-1+20))
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'unarmed', roll=sk+1+20))
        ch.WearItem(self.i1)
        self.assertEqual('ms', self.gs.SkillCheck(ch, 'unarmed', roll=sk-1))
        self.assertEqual('ms', self.gs.SkillCheck(ch, 'unarmed', roll=sk+1))
        self.assertEqual('ms', self.gs.SkillCheck(ch, 'unarmed', roll=sk-1+20))
        self.assertEqual('mf', self.gs.SkillCheck(ch, 'unarmed', roll=sk+1+20))

    def test_113_SkillImprove(self):
        ch = self.charGen()
        ch.SetAttribute('str', 10)
        ch.SetAttribute('dex', 10)
        ch.SetAttribute('agl', 10)
        ch.SetAttribute('end', 1)  # shouldn't affect results
        ch.SetValue('i:sunsign', '-')
        ch.SetSkill('unarmed', 55)  # 40 + 15
        self.assertEqual(10, ch.GetSkillBase('unarmed'))
        self.assertEqual(55, ch.GetSkill('unarmed'))
        self.assertFalse(self.gs.SkillImprove(ch, 'unarmed', roll=1))
        self.assertFalse(self.gs.SkillImprove(ch, 'unarmed', roll=1, modifier=10))
        self.assertFalse(self.gs.SkillImprove(ch, 'unarmed', roll=45))
        self.assertEqual(55, ch.GetSkill('unarmed'))
        self.assertTrue(self.gs.SkillImprove(ch, 'unarmed', roll=46))
        self.assertEqual(56, ch.GetSkill('unarmed'))
        self.assertFalse(self.gs.SkillImprove(ch, 'unarmed', roll=46))
        self.assertEqual(56, ch.GetSkill('unarmed'))
        self.gs.SkillLevel(ch, 'pummel/unarmed', open=True)
        self.assertEqual(56, ch.GetSkill('pummel'))
        self.assertFalse(self.gs.SkillImprove(ch, 'pummel', roll=46))
        self.assertTrue(self.gs.SkillImprove(ch, 'pummel', roll=47))
        self.assertEqual(56, ch.GetSkill('unarmed'))
        self.assertEqual(58, ch.GetSkill('pummel'))
        self.assertFalse(self.gs.SkillImprove(ch, 'unarmed', roll=46))
        self.assertTrue(self.gs.SkillImprove(ch, 'unarmed', roll=49))
        self.assertEqual(57, ch.GetSkill('unarmed'))
        self.assertEqual(59, ch.GetSkill('pummel'))
        self.assertTrue(self.gs.SkillImprove(ch, 'unarmed', roll=49))
        self.assertEqual(58, ch.GetSkill('unarmed'))
        self.assertEqual(59, ch.GetSkill('pummel'))
        self.assertTrue(self.gs.SkillImprove(ch, 'unarmed', roll=49))
        self.assertEqual(59, ch.GetSkill('unarmed'))
        self.assertEqual(59, ch.GetSkill('pummel'))
        self.assertTrue(self.gs.SkillImprove(ch, 'unarmed', roll=50))
        self.assertEqual(60, ch.GetSkill('unarmed'))
        self.assertEqual(60, ch.GetSkill('pummel'))
        self.assertRaises(BadParameter, self.gs.SkillImprove, ch, 'fooskill', roll=99)
        self.assertRaises(NotPossible, self.gs.SkillImprove, ch, 'swords', roll=99)
        self.assertTrue(self.gs.SkillImprove(ch, 'swords', roll=99, open=True))

    def test_115_GetCombatSummary(self):
        ch = self.charGen()
        ch.SetStatus('prone')
        self.assertEqual('prone', self.gs.GetCombatSummary(ch))
        ch.SetList('wounds', ['a', 'b'])
        self.assertEqual('prone, 2 wounds', self.gs.GetCombatSummary(ch))
        ch.SetDict('bleeders', {'a':1, 'b':2})
        self.assertEqual('prone, 2 wounds, 2 bleeders', self.gs.GetCombatSummary(ch))
        ch.SetValue('x:bloodloss', '1')
        self.assertEqual('prone, 2 wounds, 2 bleeders, bloodloss=1', self.gs.GetCombatSummary(ch))
        ch.SetDict('bleeders', {})
        self.assertEqual('prone, 2 wounds, bloodloss=1', self.gs.GetCombatSummary(ch))

    def test_120_CombatUpdateBloodloss(self):
        ch = self.charGen()
        ch.SetAttribute('end', 12)
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual(None, ch.GetDict('bleeders'))
        self.assertEqual(None, ch.GetValue('x:bloodloss'))
        ch.SetDict('bleeders', {'w1':'0', 'w2':'2'})
        self.gs.CombatExit(ch)
        self.assertIn('still bleeding from', deity.test.TestOutputGet())
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual({'w1':'1', 'w2':'3'}, ch.GetDict('bleeders'))
        self.assertEqual('0', ch.GetValue('x:bloodloss'))
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual({'w1':'2', 'w2':'4'}, ch.GetDict('bleeders'))
        self.assertEqual('0', ch.GetValue('x:bloodloss'))
        ch.SetValue('x:bloodloss', '11')
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual({'w1':'3', 'w2':'5'}, ch.GetDict('bleeders'))
        self.assertEqual('11', ch.GetValue('x:bloodloss'))
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual({'w1':'4', 'w2':'0'}, ch.GetDict('bleeders'))
        self.assertEqual('12', ch.GetValue('x:bloodloss'))
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual({'w1':'5', 'w2':'1'}, ch.GetDict('bleeders'))
        self.assertEqual('12', ch.GetValue('x:bloodloss'))
        self.assertEqual('standing', ch.GetInfo('status', 'standing'))
        self.gs.CombatUpdateBloodloss(ch)
        self.assertEqual({'w1':'0', 'w2':'2'}, ch.GetDict('bleeders'))
        self.assertEqual('13', ch.GetValue('x:bloodloss'))
        self.assertEqual('dead', ch.GetInfo('status', 'standing'))

    def test_130_CombatUpdateStatus_stand(self):
        ch = self.charGen()
        ch.SetAttribute('end', 12)
        ch.SetValue('x:ipenalty', '3')
        ch.SetStatus('unconscious')
        self.gs.rand.testset('waketest', (5,4,4))
        self.gs.CombatUpdateStatus(ch)
        self.gs.rand.testend()
        self.assertEqual('unconscious', ch.GetStatus())
        self.gs.rand.testset('waketest', (5,4,3))
        self.gs.rand.testset('standtest', (1,1,1))
        self.gs.CombatUpdateStatus(ch)
        self.gs.rand.testend()
        self.assertEqual('prone', ch.GetStatus())

    def test_131_CombatUpdateStatus_shock(self):
        ch = self.charGen()
        ch.SetAttribute('end', 12)
        ch.SetValue('x:ipenalty', '3')
        ch.SetStatus('unconscious')
        self.assertEqual(3, ch.GetPenalty('up'))
        self.gs.rand.testset('waketest', (1,1,1))  # pass
        self.gs.rand.testset('standtest', (6,6,6)) # fail
        self.gs.rand.testset('shockmovetest', 99)  # fail
        self.gs.CombatUpdateStatus(ch)
        self.assertEqual('shock', ch.GetStatus())
        self.assertIn('awakes in shock and cannot act', deity.test.TestOutputGet())
        self.gs.rand.testend()
        self.gs.rand.testset('shockmovetest', 99)  # fail
        self.gs.CombatUpdateStatus(ch)
        self.gs.rand.testend()
        self.assertEqual('shock', ch.GetStatus())
        self.assertIn('is in shock and cannot act', deity.test.TestOutputGet())
        self.gs.rand.testset('shockmovetest', 1)   # pass
        self.gs.CombatUpdateStatus(ch)
        self.gs.rand.testend()
        self.assertEqual('shock', ch.GetStatus())
        self.assertIn('is in shock but can move', deity.test.TestOutputGet())

        ch.SetStatus('unconscious')
        self.gs.rand.testset('waketest', (1,1,1))  # pass
        self.gs.rand.testset('standtest', (6,6,6)) # fail
        self.gs.rand.testset('shockmovetest', 1)   # pass
        self.gs.CombatUpdateStatus(ch)
        self.assertEqual('shock', ch.GetStatus())
        self.assertIn('awakes in shock but can move', deity.test.TestOutputGet())
        self.gs.rand.testend()

    def test_132_CombatUpdateStatus_busy(self):
        unbusy = WORLD.TimeGetCurrent()
        WORLD.TimeAdjust('1s', unbusy)
        ch = self.charGen()
        ch.SetInfo('busy', WORLD.TimeExport(unbusy))
        self.gs.CombatNewTurn(ch)
        self.assertIn('act at end of round', deity.test.TestOutputGet())

        unbusy = WORLD.TimeGetCurrent()
        WORLD.TimeAdjust('11s', unbusy)
        ch = self.charGen()
        ch.SetInfo('busy', WORLD.TimeExport(unbusy))
        self.gs.CombatNewTurn(ch)
        self.assertIn('acting now will interrupt', deity.test.TestOutputGet())

    def test_140_CombatActionTime(self):
        self.assertRaises(Exception, self.gs.CombatActionTime, None, 'blah')

    def test_141_CombatActionTime_stand(self):
        self.assertEqual(1, self.gs.CombatActionTime(None, 'stand', 'blah'))

    def test_150_CombatDraw(self):
        ch = self.charGen(item1='dagger', item2='knife')
        it = ch.SearchInventory(strx('dagger'))
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.gs.CombatDraw(ch, strx('dagger'), strx('lefthand'))
        self.assertEqual('failed dex check to wield dagger.', deity.test.TestOutputBuf)
        self.gs.rand.testend()
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.CombatDraw(ch, strx('dagger'), strx('lefthand'))
        self.gs.rand.testend()
        self.assertEqual({'righthand':None, 'lefthand':it}, ch.GetWielded())
        self.assertEqual('dagger now in lefthand.', deity.test.TestOutputBuf)
        it = ch.SearchInventory(strx('knife'))
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.CombatDraw(ch, strx('knife'), strx())
        self.gs.rand.testend()
        self.assertEqual('knife now in righthand.', deity.test.TestOutputBuf)

    def test_151_CombatDraw_ground(self):
        ground = GAME.GetId(GAME.kCombatGroundTid)
        ch = self.charGen(item1='dagger', item2='dagger')
        i1 = ch.SearchInventory(strx('dagger'))
        ch.SubInventory(i1)
        ground.AddInventory(i1)
        i2 = ch.SearchInventory(strx('dagger'))
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.CombatDraw(ch, strx('dagger'), strx('lefthand'))
        self.gs.rand.testend()
        self.assertEqual({'righthand':None, 'lefthand':i2}, ch.GetWielded())
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.CombatDraw(ch, strx('dagger'), strx('righthand'))
        self.gs.rand.testend()
        self.assertEqual({'righthand':i1, 'lefthand':i2}, ch.GetWielded())

    def test_160_CombatFirstAid(self):
        c1 = self.charGen()
        c2 = self.charGen()
        self.assertRaises(NotPossible, GAME.CombatFirstAid, c1, c2)
        c2.SetDict('bleeders', {'w1':'1'})
        c2.SetValue('x:bloodloss', '1')
        GAME.CombatFirstAid(c1, c2, roll=99)
        self.assertEqual({'w1':'0'}, c2.GetDict('bleeders'))
        self.assertEqual('2', c2.GetValue('x:bloodloss'))
        GAME.CombatFirstAid(c1, c2, roll=1)
        self.assertEqual({}, c2.GetDict('bleeders'))
        self.assertEqual('2', c2.GetValue('x:bloodloss'))

    def test_170_CombatMove(self):
        c1 = self.charGen()
        deity.test.TestOutputClear()
        GAME.CombatMove(c1)
        output = deity.test.TestOutputGet()
        self.assertIn(' can move ', output)

    def test_180_CombatMount(self):
        ch = self.charGen(item1='natural(1,1,1,1)')
        ch.WearItem(self.i1)
        mt = self.gs.Character(None)
        mt.Generate(xstr('warhorse'))
        mt.SetInfo('name', 'mandarb')
        ch.SetSkill('riding', 50)
        self.assertEqual(None, ch.GetSkill('mandarb'))
        self.gs.rand.testset('mountcheck', 100)  # CF
        self.gs.rand.testset('unhorsecheck', 1)  # MS
        self.assertEqual(None, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(50, ch.GetSkill('mandarb'))
        self.gs.rand.testset('mountcheck', 100)  # CF
        self.gs.rand.testset('unhorsecheck', 5)  # CS
        self.assertEqual(ch, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.gs.rand.testset('mountcheck', 99)  # MF
        self.gs.rand.testset('unhorsecheck', 1) # MS
        self.assertEqual(None, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(None, ch.GetList('wounds'))
        self.gs.rand.testset('mountcheck', 99)  # MF
        self.gs.rand.testset('unhorsecheck', 99)# MF
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('rangeloc', 50)
        self.assertEqual(None, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(None, ch.GetList('wounds'))
        self.gs.rand.testset('mountcheck', 99)  # MF
        self.gs.rand.testset('unhorsecheck',100)# CF
        self.gs.rand.testset('damage', (1,1,1,1))
        self.gs.rand.testset('rangeloc', 50)
        self.gs.rand.testset('shockroll', 1)
        self.assertEqual(None, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(['thorax-M1blunt'], ch.GetList('wounds'))
        self.gs.rand.testset('mountcheck', 1)   # MS
        self.assertEqual(None, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(mt, ch.GetMount())
        self.assertRaises(NotPossible, GAME.CombatMount, ch, mt)
        ch.SetMount(None)
        self.assertEqual(None, ch.GetMount())
        self.gs.rand.testset('mountcheck', 5)   # CS
        self.assertEqual(ch, GAME.CombatMount(ch, mt))
        self.gs.rand.testend()
        self.assertEqual(mt, ch.GetMount())

    def test_185_CombatDismount(self):
        ch = self.charGen(item1='natural(1,1,1,1)')
        ch.WearItem(self.i1)
        mt = self.gs.Character(None)
        mt.Generate(xstr('warhorse'))
        mt.SetInfo('name', 'mandarb')
        ch.SetSkill('riding', 50)
        self.assertRaises(NotPossible, GAME.CombatDismount, ch)
        ch.SetMount(mt)
        self.assertEqual(None, ch.GetSkill('mandarb'))
        self.gs.rand.testset('dismountcheck', 100) # CF
        self.gs.rand.testset('unhorsecheck', 1)    # MS
        self.assertEqual(None, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(50, ch.GetSkill('mandarb'))
        ch.SetMount(mt)
        self.gs.rand.testset('dismountcheck', 100) # CF
        self.gs.rand.testset('unhorsecheck', 5)    # CS
        self.assertEqual(ch, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        ch.SetMount(mt)
        self.gs.rand.testset('dismountcheck', 99)  # MF
        self.gs.rand.testset('unhorsecheck', 1)    # MS
        self.assertEqual(None, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(None, ch.GetList('wounds'))
        ch.SetMount(mt)
        self.gs.rand.testset('dismountcheck', 99)  # MF
        self.gs.rand.testset('unhorsecheck', 99)   # MF
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('rangeloc', 50)
        self.assertEqual(None, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(None, ch.GetList('wounds'))
        ch.SetMount(mt)
        self.gs.rand.testset('dismountcheck', 99)  # MF
        self.gs.rand.testset('unhorsecheck',100)   # CF
        self.gs.rand.testset('damage', (1,1,1,1))
        self.gs.rand.testset('rangeloc', 50)
        self.gs.rand.testset('shockroll', 1)
        self.assertEqual(None, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        self.assertEqual(['thorax-M1blunt'], ch.GetList('wounds'))
        ch.SetMount(mt)
        self.gs.rand.testset('dismountcheck', 1)   # MS
        self.assertEqual(None, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())
        ch.SetMount(mt)
        self.gs.rand.testset('dismountcheck', 5)   # CS
        self.assertEqual(ch, GAME.CombatDismount(ch))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetMount())

    def test_200_KillCheck(self):
        ch = self.charGen()
        self.assertEqual('G', self.gs._CombatSeverityAdjustment(ch, 'G', 40))
        deity.test.TestUserResponse('kill?', False)
        self.assertEqual('G', self.gs._CombatSeverityAdjustment(ch, 'K', 40))
        ch.SetAttribute('end', 10)
        self.gs.rand.testset('killendcheck', 1)
        self.gs.rand.testset('killendcheck', 2)
        self.gs.rand.testset('killendcheck', 3)
        self.gs.rand.testset('killendcheck', 4)
        deity.test.TestUserResponse('kill?', True)
        self.assertEqual('G', self.gs._CombatSeverityAdjustment(ch, 'K', 4))
        self.gs.rand.testend()
        self.gs.rand.testset('killendcheck', 1)
        self.gs.rand.testset('killendcheck', 2)
        self.gs.rand.testset('killendcheck', 3)
        self.gs.rand.testset('killendcheck', 5)
        deity.test.TestUserResponse('kill?', True)
        self.assertEqual('K', self.gs._CombatSeverityAdjustment(ch, 'K', 4))
        self.gs.rand.testend()

    def test_210_AmputateCheck(self):
        ch = self.charGen()
        ch.SetInfo('weight', '55') # kg
        self.gs.rules['CombatAmputate'] = 'no'
        self.assertFalse(self.gs._CombatAmputateCheck(ch, 'yes', 200, 'edge'))
        self.gs.rules['CombatAmputate'] = 'yes'
        self.assertFalse(self.gs._CombatAmputateCheck(ch, '', 200, 'edge'))
        self.assertFalse(self.gs._CombatAmputateCheck(ch, 'yes', 200, 'blunt'))
        self.gs.rand.testset('amputateweightcheck', 6)
        self.gs.rand.testset('amputateweightcheck', 6)
        deity.test.TestUserResponse('amputate?', True)
        self.assertFalse(self.gs._CombatAmputateCheck(ch, 'yes', 2, 'edge'))
        self.gs.rand.testend()
        self.gs.rand.testset('amputateweightcheck', 6)
        self.gs.rand.testset('amputateweightcheck', 6)
        self.gs.rand.testset('amputateweightcheck', 1)
        deity.test.TestUserResponse('amputate?', True)
        self.assertTrue(self.gs._CombatAmputateCheck(ch, 'yes', 3, 'edge'))
        self.gs.rand.testend()

    def test_220_Knockback(self):
        ch = self.charGen()
        ch.SetAttribute('str', 11)
        self.gs.rules['CombatKnockback'] = 'no'
        self.assertEqual(0, self.gs._CombatKnockbackAmount(ch, 'knockback=1', 20))
        self.gs.rules['CombatKnockback'] = 'yes'
        self.assertEqual(0, self.gs._CombatKnockbackAmount(ch, 'knockback=1', 11))
        self.assertEqual(1, self.gs._CombatKnockbackAmount(ch, ';knockback=1;', 12))
        self.assertEqual(0, self.gs._CombatKnockbackAmount(ch, '', 11))

    def test_230_Fumble(self):
        ch = self.charGen(wield1='knife', wield2='buckler')
        self.gs.rules['CombatFumble'] = 'no'
        self.assertEqual(None, self.gs._CombatDoFumble(ch, 'fumble=hand', 'G', 'leftelbow'))
        self.gs.rules['CombatFumble'] = 'yes'
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertIsNotNone(ch.GetWielded()['lefthand'])
        wlh = ch.GetWielded()['lefthand']
        self.assertEqual(-1, self.gs._CombatDoFumble(ch, 'fumble=hand', 'S', 'leftelbow'))
        self.assertIsNone(ch.GetWielded()['lefthand'])
        self.assertEqual(None, self.gs._CombatDoFumble(ch, 'fumble=hand', 'S', 'leftelbow'))
        self.assertIsNone(ch.GetWielded()['lefthand'])
        self.gs.rand.testend()
        ground = GAME.GetId(GAME.kCombatGroundTid)
        ground.SubInventory(wlh)
        ch.AddInventory(wlh)
        ch.WieldItem(wlh, 'lefthand')
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(None, self.gs._CombatDoFumble(ch, 'fumble=hand', 'G', 'leftelbow'))
        self.gs.rand.testend()
        self.assertEqual(None, self.gs._CombatDoFumble(ch, 'fumble=hand', 'M', 'leftelbow'))
        self.assertEqual(None, self.gs._CombatDoFumble(ch, '', 'G', 'leftelbow'))

    def test_240_Stumble(self):
        ch = self.charGen()
        ch.SetStatus('standing')
        self.gs.rules['CombatStumble'] = 'no'
        self.assertEqual(None, self.gs._CombatDoStumble(ch, 'stumble=prone', 'G', 0))
        self.assertEqual(None, self.gs._CombatDoStumble(ch, 'stumble=prone', 'G', 1))
        self.gs.rules['CombatStumble'] = 'yes'
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(-1, self.gs._CombatDoStumble(ch, 'stumble=prone', 'S', 0))
        self.gs.rand.testend()
        self.assertEqual('prone', ch.GetStatus())
        ch.SetStatus('standing')
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(None, self.gs._CombatDoStumble(ch, 'stumble=prone', 'G', 0))
        self.gs.rand.testend()
        self.assertEqual('standing', ch.GetStatus())
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(-1, self.gs._CombatDoStumble(ch, '', 'M', 1))
        self.gs.rand.testend()
        self.assertEqual('prone', ch.GetStatus())
        self.assertEqual(None, self.gs._CombatDoStumble(ch, 'stumble=prone', 'G', 0))
        ch.SetStatus('standing')
        self.assertEqual(None, self.gs._CombatDoStumble(ch, '', 'G', 0))
        self.assertEqual('standing', ch.GetStatus())

    def test_241_Stumble_horse(self):
        ch = self.charGen('warhorse')
        ch.SetStatus('standing')
        self.gs.rules['CombatStumble'] = 'yes'
        self.gs.rand.testset('skillcheck-dodge', 99)
        self.assertEqual(-1, self.gs._CombatDoStumble(ch, 'stumble=prone', 'S', 0))
        self.gs.rand.testend()
        self.assertEqual('prone', ch.GetStatus())
        ch.SetStatus('standing')
        self.gs.rand.testset('skillcheck-dodge', 1)
        self.assertEqual(None, self.gs._CombatDoStumble(ch, 'stumble=prone', 'G', 0))
        self.gs.rand.testend()
        self.assertEqual('standing', ch.GetStatus())
        self.gs.rand.testset('skillcheck-dodge', 99)
        self.assertEqual(-1, self.gs._CombatDoStumble(ch, '', 'M', 1))
        self.gs.rand.testend()
        self.assertEqual('prone', ch.GetStatus())
        self.assertEqual(None, self.gs._CombatDoStumble(ch, 'stumble=prone', 'G', 0))
        ch.SetStatus('standing')
        self.assertEqual(None, self.gs._CombatDoStumble(ch, '', 'G', 0))
        self.assertEqual('standing', ch.GetStatus())

    def test_250_WeaponDamage(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        i1.SetAttribute('quality', 11)
        c1 = self.charGen()
        c1.AddInventory(i1)
        c1.WieldItem(i1, 'righthand')
        i2 = self.gs.Item(None)
        i2.Generate('buckler')
        i2.SetAttribute('quality', 13)
        c2 = self.charGen()
        c2.AddInventory(i2)
        c2.WieldItem(i2, 'lefthand')
        self.gs.rules['CombatWeaponDamage'] = 'no'
        self.gs.rand.testset()
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(11, i1.GetAttribute('quality'))
        self.assertEqual(13, i2.GetAttribute('quality'))
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatWeaponPartialDamage'] = 'no'
        self.gs.rand.testset('attackerweapondamage', (1,1,1))
        self.gs.rand.testset('defenderweapondamage', (1,1,1))
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(11, i1.GetAttribute('quality'))
        self.assertEqual(13, i2.GetAttribute('quality'))
        self.gs.rules['CombatWeaponPartialDamage'] = 'yes'
        self.gs.rand.testset('attackerweapondamage', (1,1,1))
        self.gs.rand.testset('defenderweapondamage', (1,1,1))
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(10, i1.GetAttribute('quality'))
        self.assertEqual(12, i2.GetAttribute('quality'))
        self.gs.rand.testset('attackerweapondamage', (1,1,1))
        self.gs.rand.testset('defenderweapondamage', (1,1,1))
        self.gs._CombatDoWeaponDamage(c2, 'lefthand', c1, 'righthand')
        self.gs.rand.testend()
        self.assertEqual(9, i1.GetAttribute('quality'))
        self.assertEqual(11, i2.GetAttribute('quality'))
        self.gs.rand.testset('attackerweapondamage', (6,6,6))
        self.gs.rand.testset('defenderweapondamage', (6,6,6))
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(0, i1.GetAttribute('quality'))
        self.assertEqual(11, i2.GetAttribute('quality'))
        i1.SetAttribute('quality', 13)
        c1.AddInventory(i1)
        c1.WieldItem(i1, 'righthand')
        self.gs.rand.testset('attackerweapondamage', (6,6,6))
        self.gs.rand.testset('defenderweapondamage', (6,6,6))
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(13, i1.GetAttribute('quality'))
        self.assertEqual(0, i2.GetAttribute('quality'))
        i2.SetAttribute('quality', 15)
        c2.AddInventory(i2)
        c2.WieldItem(i2, 'lefthand')
        self.gs.rand.testset('attackerweapondamage', (1,1,1))
        self.gs.rand.testset('defenderweapondamage', (6,6,6))
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(13, i1.GetAttribute('quality'))
        self.assertEqual(0, i2.GetAttribute('quality'))
        i2.SetAttribute('quality', 11)
        c2.AddInventory(i2)
        c2.WieldItem(i2, 'lefthand')
        self.gs.rand.testset('attackerweapondamage', (6,6,6))
        self.gs.rand.testset('defenderweapondamage', (1,1,1))
        self.gs._CombatDoWeaponDamage(c1, 'righthand', c2, 'lefthand')
        self.gs.rand.testend()
        self.assertEqual(0, i1.GetAttribute('quality'))
        self.assertEqual(11, i2.GetAttribute('quality'))

    def test_300_Wound_badparam(self):
        ch = self.charGen()
        # bad damage
        self.assertRaises(BadParameter, self.gs.CombatWound, ch, 'x', xstr('blunt'))
        # bad aspect
        self.assertRaises(BadParameter, self.gs.CombatWound, ch, 1, xstr('unknown'))
        # bad location
        self.assertRaises(BadParameter, self.gs.CombatWound, ch, 1, xstr('blunt'), xstr('nowhere'))

    def test_310_Wound(self):
        ch = self.charGen(wield1='knife')
        ch.SetAttribute('str', 15)
        ch.SetAttribute('end', 12)
        ch.SetInfo('busy', 'foo')
        # M1 blow to the hand
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        self.assertNotEqual(None, ch.GetInfo('busy'))
        ch.SetValue('x:armor-lefthand', None)
        #self.gs.rand.testset('shockroll', 1)
        self.assertEqual(0, self.gs._CombatDoWound(ch, 4, strx('blunt'), strx('lefthand')))
        self.gs.rand.testend()
        self.assertEqual(None, ch.GetInfo('busy'))
        self.assertEqual(['lefthand-M1blunt'], ch.GetList('wounds'))
        self.assertEqual({}, ch.GetDict('bleeders'))
        self.assertEqual(1, ch.GetPenalty('ip'))
        self.assertEqual('standing', ch.GetStatus())
        # S2 slash to the groin
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(0, self.gs._CombatDoWound(ch, 8, strx('edge'), strx('groin')))
        self.gs.rand.testend()
        self.assertEqual(['groin-S2edge'], ch.GetList('wounds'))
        self.assertEqual({}, ch.GetDict('bleeders'))
        self.assertEqual(2, ch.GetPenalty('ip'))
        self.assertEqual('standing', ch.GetStatus())
        # G4 stab to the thorax
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.rand.testset('shockroll', (1,2,3,4))
        self.assertEqual(0, self.gs._CombatDoWound(ch, 16, strx('point'), strx('thorax')))
        self.gs.rand.testend()
        self.assertEqual(['thorax-G4point'], ch.GetList('wounds'))
        self.assertEqual({'thorax-G4point':'0'}, ch.GetDict('bleeders'))
        self.assertEqual(4, ch.GetPenalty('ip'))
        self.assertEqual('standing', ch.GetStatus())
        # G4 severing slash to the calf (amputate makes G5)
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        ch.SetInfo('weight', '10')
        self.gs.rand.testset('shockroll', (1,2,3,4,5))
        deity.test.TestUserResponse('amputate?', True)
        self.gs.rand.testset('amputateweightcheck', (1,1,1,1))
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(-1, self.gs._CombatDoWound(ch, 17, strx('edge'), strx('rightcalf')))
        self.gs.rand.testend()
        self.assertEqual(['rightcalf-G5edge'], ch.GetList('wounds'))
        self.assertEqual({'rightcalf-G5edge':'0'}, ch.GetDict('bleeders'))
        self.assertEqual(5, ch.GetPenalty('ip'))
        self.assertEqual('unconscious', ch.GetStatus())
        # K5 blow to the skull (no kill)
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        deity.test.TestUserResponse('kill?', False)
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.rand.testset('shockroll', (1,2,3,4,5))
        self.assertEqual(-1, self.gs._CombatDoWound(ch, 17, strx('blunt'), strx('skull')))
        self.gs.rand.testend()
        self.assertEqual(['skull-G5blunt'], ch.GetList('wounds'))
        self.assertEqual({'skull-G5blunt':'0'}, ch.GetDict('bleeders'))
        self.assertEqual(5, ch.GetPenalty('ip'))
        self.assertEqual('unconscious', ch.GetStatus())
        # K5 blow to the head (kill)
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetValue('x:ipenalty', '0')
        deity.test.TestUserResponse('kill?', True)
        self.gs.rand.testset('killendcheck', (6,6,6,6,6))
        self.assertEqual(0, self.gs._CombatDoWound(ch, 300, xstr('blunt'), xstr('skull')))
        self.gs.rand.testend()
        self.assertEqual(['skull-K5blunt'], ch.GetList('wounds'))
        self.assertEqual({}, ch.GetDict('bleeders'))
        self.assertEqual(5, ch.GetPenalty('ip'))
        self.assertEqual('dead', ch.GetStatus())
        # fumble
        ch.SetValue('x:ipenalty', '0')
        self.gs.rules['CombatFumble'] = 'yes'
        cw = ch.GetWielded()
        self.assertEqual('knife', cw['righthand'].Name())
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.gs.rand.testset('shockroll', (1,2,3,4))
        self.assertEqual(-1, self.gs._CombatDoWound(ch, 14, strx('edge'), strx('rightshoulder')))
        self.gs.rand.testend()
        cw = ch.GetWielded()
        self.assertEqual(None, cw['righthand'])
        # stumble
        ch.SetValue('x:ipenalty', '0')
        self.gs.rules['CombatStumble'] = 'yes'
        ch.SetStatus('standing')
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.gs.rand.testset('shockroll', (1,2,3,4))
        self.assertEqual(-1, self.gs._CombatDoWound(ch, 14, strx('blunt'), strx('rightknee')))
        self.gs.rand.testend()
        self.assertEqual('prone', ch.GetStatus())

    def test_311_Wound_armored(self):
        ch = self.charGen(armor1='vest-leather', wield1='knife')
        ch.SetAttribute('str', 15)
        ch.SetAttribute('end', 12)
        # insufficient slash to the chest
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        self.gs.rand.testset()
        self.assertEqual(None, self.gs._CombatDoWound(ch, 4, strx('edge'), strx('thorax')))
        self.gs.rand.testend()
        self.assertEqual([], ch.GetList('wounds'))
        # just a little bit more
        self.assertEqual(0, self.gs._CombatDoWound(ch, 5, strx('edge'), strx('thorax')))
        self.assertEqual(['thorax-M1edge'], ch.GetList('wounds'))

    def test_312_Wound_beast(self):
        ch = self.charGen(type='vlasta')
        ch.SetList('wounds', [])
        ch.SetDict('bleeders', {})
        ch.SetStatus('standing')
        ch.SetValue('x:ipenalty', '0')
        self.gs.rand.testset('damage', (1))
        self.gs.rand.testset('shockroll', (1))
        self.assertEqual(0, self.gs._CombatDoWound(ch, '1d6+3', strx('point'), strx('tail')))
        self.gs.rand.testend()
        self.assertNotEqual([], ch.GetList('wounds'))

    def combatGen(self):
        deity.test.TestOutputClear()
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'yes'
        self.gs.rules['CombatFumble'] = 'yes'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        self.gs.CombatNewRound()
        c1 = self.charGen(name='c1', wield1='keltan')
        c1.SetAttribute('dex', 2)
        c1.SetSkill('daggers', 42)
        c1.SetStatus('standing')
        w1 = c1.GetWielded()
        i1 = w1['righthand']
        c2 = self.charGen(name='c2', wield2='buckler')
        c2.SetAttribute('dex', 2)
        c2.SetSkill('shields', 64)
        c2.SetStatus('standing')
        w2 = c2.GetWielded()
        i2 = w2['lefthand']
        self.assertEqual('keltan', w1['righthand'].Name())
        self.assertEqual('buckler', w2['lefthand'].Name())
        i1.SetAttribute('quality', 2)
        i2.SetAttribute('quality', 99)
        #print 'CombatGen: c1:',w1
        #print 'CombatGen: c2:',w2
        return (c1, c2, i1, i2)

    def test_320_Attack_errors(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c1)  # attack yourself
        # can't attack unless standing
        c1.SetStatus('unconscious')
        self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c2)  # not conscious
        self.assertRaises(BadParameter, self.gs.CombatAttack, c2, c1) # no defense
        self.assertRaises(NotPossible, self.gs.CombatAttack, c2, c1, defensex='block')  # only "ignore"
        # must have valid defense maneuver and locations
        c1.SetStatus('standing')
        self.assertRaises(BadParameter, self.gs.CombatAttack, c1, c2, defensex='blah', ausex='righthand')
        self.assertRaises(BadParameter, self.gs.CombatAttack, c1, c2, defensex='block', ausex='blah')
        # can't attack to unknown locations
        deity.test.TestUserResponse('unarmed?', True)
        self.assertRaises(BadParameter, self.gs.CombatAttack, c1, c2, defensex='counterstrike', alocx='foo', dlocx='face')
        deity.test.TestUserResponse('unarmed?', True)
        self.assertRaises(BadParameter, self.gs.CombatAttack, c1, c2, defensex='counterstrike', alocx='face', dlocx='l')
        # can't attack prone characters
        c2.SetStatus('prone')
        deity.test.TestUserResponse('attackprone?', False)
        self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c2, defensex='block', ausex='righthand')
        # can't attack dead characters
        c2.SetStatus('dead')
        self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c2, defensex='block', ausex='righthand')
        # weapons not allowed for attack/defense
        c2.SetStatus('standing')
        i2.SetAttribute('defense', None)
        #self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c2, defensex='block', ausex='righthand', dusex='lefthand')
        self.gs.rand.testend()

    def test_321_Attack_open_skill(self):
        (c1, c2, i1, i2) = self.combatGen()
        i3 = GAME.Item(None)
        i3.Generate('shortsword')
        c1.AddInventory(i3)
        c1.UnwieldItem(i1)
        c1.WieldItem(i3, defindex=0)
        # don't have "sword" skill, but we will
        self.assertEqual(None, c1.GetSkill('shortsword'))
        self.assertEqual(None, c1.GetSkill('swords'))
        self.gs.rand.testset('attackerweapondamage', (1,1,1))
        self.gs.rand.testset('defenderweapondamage', (1,1,1))
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=99, defensex='block', droll=99)) # MF/MF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetSkill('shortsword'))  # specific skills never opened, only improved
        self.assertLess(0, c1.GetSkill('swords'))
        self.assertIn('opened skill "swords"', deity.test.TestOutputGet())

    def test_330_Attack_bf(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6)) # c1 fumble
        self.gs.rand.testset('attrcheck-dex', (6,6,6)) # c2 fumble
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=100, defensex='block', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetWielded()['righthand'])
        self.assertEqual(None, c2.GetWielded()['lefthand'])
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))

    def test_330_Attack_df(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, aroll=96, defensex='block', droll=100)) # MF/CF
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(None, c2.GetWielded()['lefthand'])
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))

    def test_330_Attack_af(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, aroll=100, defensex='counterstrike', droll=96)) # CF/MF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetWielded()['righthand'])
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))

    def test_330_Attack_block(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attackerweapondamage', (6,6,6))
        self.gs.rand.testset('defenderweapondamage', (6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, aroll=2, defensex='block', droll=4)) # MS/MS
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetWielded()['righthand'])  # i1.tid  broken
        self.assertEqual(i2, c2.GetWielded()['lefthand'])
        self.assertEqual(0, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=2, defensex='block', droll=4, adamage=11)) # MS/MS
        self.assertEqual(['leftforearm-S2blunt'], c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))

    def test_330_Attack_bs(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=100, defensex='dodge', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertNotEqual(None, c1.GetWielded()['righthand'])
        self.assertNotEqual(None, c2.GetWielded()['lefthand'])
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

    def test_330_Attack_ds(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c2, self.gs.CombatAttack(c2, c1, aroll=96, defensex='dodge', droll=100)) # MF/CF
        self.gs.rand.testend()
        self.assertNotEqual(None, c1.GetWielded()['righthand'])
        self.assertNotEqual(None, c2.GetWielded()['lefthand'])
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_330_Attack_as(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c1, self.gs.CombatAttack(c2, c1, aroll=100, defensex='dodge', droll=96)) # CF/MF
        self.gs.rand.testend()
        self.assertNotEqual(None, c1.GetWielded()['righthand'])
        self.assertNotEqual(None, c2.GetWielded()['lefthand'])
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

    def test_330_Attack_dta(self):
        (c1, c2, i1, i2) = self.combatGen()
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, aroll=96, defensex='block', droll=4))   # MF/MS => DTA
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=96, defensex='block', droll=4)) # MF/MS => DTA (only one DTA)
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_330_Attack_a1(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 25)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('damage', 1)
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=2, defensex='block', droll=84)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['leftshoulder-M1point'], c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_330_Attack_a2(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 55)
        self.gs.rand.testset('damage', (3,2))
        self.gs.rand.testset('shockroll', (1,2))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=2, defensex='counterstrike', droll=84)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(['thorax-S2blunt'], c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_330_Attack_a3(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 40)  # hand => fumble
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('damage', (2,3,4))
        self.gs.rand.testset('attrcheck-dex', (1,2,3))
        self.gs.rand.testset('shockroll', (1,2,3))
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, aroll=5, defensex='dodge', droll=100)) # CS/CF
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(None, c2.GetWielded()['lefthand'])
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['lefthand-S3point'], c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_330_Attack_a4(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 90)
        self.gs.rand.testset('rangeloc-multi', 1)
        self.gs.rand.testset('damage', (3,4,5,6)) # > 16 => G5
        self.gs.rand.testset('attrcheck-dex', (1,2,3))
        self.gs.rand.testset('shockroll', (6,6,6,6,6))
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, aroll=5, defensex='ignore')) # CS/cf
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['rightknee-G5point'], c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('unconscious', c2.GetStatus())

    def test_330_Attack_d1(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 25)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('damage', 3)
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=96, defensex='counterstrike', droll=2)) # MF/MS
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(['leftshoulder-M1blunt'], c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_330_Attack_b1(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 55)
        self.gs.rand.testset('damage', 4)
        self.gs.rand.testset('rangeloc', 25)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('damage', 3)
        self.gs.rand.testset('shockroll', (1,2,))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=2, defensex='counterstrike', droll=2)) # MS/MS
        self.gs.rand.testend()
        self.assertEqual(i1.tid, c1.GetWielded()['righthand'].tid)
        self.assertEqual(i2.tid, c2.GetWielded()['lefthand'].tid)
        self.assertEqual(2, i1.GetAttribute('quality'))
        self.assertEqual(99, i2.GetAttribute('quality'))
        self.assertEqual(['leftshoulder-M1blunt'], c1.GetList('wounds'))
        self.assertEqual(['thorax-S2point'], c2.GetList('wounds'))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_340_Attack_foot(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.gs.rand.testset()
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, ausex='rightfoot', aroll=96, defensex='block', dusex='lefthand', droll=2, alocx='0low')) # MF/MS => DTA
        self.gs.rand.testend()

    def test_340_Attack_bite(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='roundshield')
        c2 = self.charGen(type='aklash')
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, ausex='face', aroll=1, defensex='block', dusex='lefthand', droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual(['skull-S2point'], c1.GetList('wounds'))
        self.gs.rand.testset()
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c1, self.gs.CombatAttack(c2, c1, ausex='skull', aroll=99, defensex='block', dusex='lefthand', droll=1)) # MF/MS => DTA
        self.gs.rand.testend()

    def test_350_Attack_ranged_bad(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='sling', wield2='roundshield')
        c2 = self.charGen(type='human', wield1='shortbow')
        # can't attack ranged with attack="attack"
        self.assertRaises(NotPossible, self.gs.CombatAttack, c2, c1, arangex='short', defensex='block', aroll=1, droll=99) # MS/MF
        # can't grapple as defense for missile attack
        self.assertRaises(NotPossible, self.gs.CombatAttack, c2, c1, attackx='missile', defensex='grapple', aroll=1, droll=99) # MS/MF
        # can't block high-velocity weapon with body part
        c1.UnwieldItem(c1.GetWielded()['lefthand'])  # drop roundshield
        deity.test.TestUserResponse('bodyblock?', True)
        self.assertRaises(NotPossible, self.gs.CombatAttack, c2, c1, attackx='missile', defensex='block', aroll=1, droll=99) # MS/MF
        # can't attack ranged without a weapon
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.assertRaises(NotPossible, self.gs.CombatAttack, c2, c1, attackx='missile', defensex='dodge', aroll=1, droll=99) # MS/MF

    def test_350_Attack_ranged(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='sling', wield2='roundshield')
        c2 = self.charGen(type='human', wield1='shortbow')
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', defensex='block', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual(['skull-S2point'], c1.GetList('wounds'))

    def test_350_Attack_ranged_dodge(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='roundshield')
        c2 = self.charGen(type='human', wield1='shortbow')
        c1.SetSkill('dodge', 60)
        self.gs.SkillLevel(c2, 'shortbow/bows', open=True)
        c2.SetSkill('shortbow', 52)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', defensex='dodge', aroll=1, droll=29)) # MS/MS
        self.assertEqual([], c1.GetList('wounds', []))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', defensex='dodge', aroll=1, droll=31)) # MS/MF
        self.assertNotEqual([], c1.GetList('wounds', []))

    def test_350_Attack_ranged_distance(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='roundshield')
        c2 = self.charGen(type='human', wield1='shortbow')
        c1.SetSkill('dodge', 60)
        c2.SetSkill('bows', 52)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', defensex='dodge', aroll=53+5, droll=99)) # MF/MF
        self.assertEqual([], c1.GetList('wounds', []))
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', defensex='dodge', aroll=52+5, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertNotEqual([], c1.GetList('wounds', []))
        c1.SetList('wounds', [])
        c1.SetValue('x:ipenalty', '0')
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', arangex='long', defensex='dodge', aroll=53-40+5, droll=99)) # MF/MF
        self.assertEqual([], c1.GetList('wounds', []))
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', arangex='long', defensex='dodge', aroll=52-40+5, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertNotEqual([], c1.GetList('wounds', []))

    def test_350_Attack_ranged_wild(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'yes'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='roundshield')
        c2 = self.charGen(type='human', wield1='shortbow')
        c1.SetSkill('dodge', 60)
        c2.SetSkill('bows', 52)
        self.assertNotEqual(None, c2.GetWielded()['righthand'])
        self.gs.rand.testset()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, attackx='missile', defensex='dodge', aroll=100, droll=99)) # CF/MF
        self.gs.rand.testend()
        self.assertEqual([], c1.GetList('wounds', []))
        self.assertEqual(None, c2.GetWielded()['righthand'])

    def test_360_Attack_armored(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', armor1='breastplate-plate', wield1='roundshield')
        c2 = self.charGen(type='aklash')
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 60)
        self.gs.rand.testset('damage', (3,3))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, ausex='righthand', aroll=5, defensex='block', dusex='lefthand', droll=99)) # CS/MF
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1edge'], c1.GetList('wounds'))

    def test_361_Attack_unarmed_specialized(self):
        self.gs.rules['CombatWeaponDamage'] = 'yes'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='vlasta')
        c2 = self.charGen(type='vlasta')
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, ausex='face', aroll=99, defensex='counterstrike', dusex='face', droll=99, adamage=4)) # MF/MF (block)
        self.gs.rand.testend()
        self.assertEqual(['face-M1point'], c1.GetList('wounds', default=[]))
        self.assertEqual([], c2.GetList('wounds', default=[]))

    def test_362_Attack_block_hand_replace_nonhuman(self):
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='nolah')
        c2 = self.charGen(type='centaur')
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=01, defensex='block', droll=01, adamage=9)) # MS/MS (block)
        self.gs.rand.testend()
        self.assertEqual([], c1.GetList('wounds', default=[]))
        self.assertEqual(['leftarm-S2blunt'], c2.GetList('wounds', default=[]))

    def test_363_Attack_short_from_loc(self):
        self.gs.rules['CombatWeaponDamage'] = 'no'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='bear-lythian')
        c2 = self.charGen(type='human')
        self.gs.rand.testset()
        self.gs.rand.testset('rangeloc', 60)
        self.gs.rand.testset('damage', (1,1))
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, ausex='foot', aaspectx='edge', aroll=5, defensex='block', dusex='lefthand', droll=99)) # CS/MF
        self.gs.rand.testend()
        self.assertEqual(['thorax-S2edge'], c2.GetList('wounds'))

    def mountedGen(self):
        deity.test.TestOutputClear()
        self.gs.rules['CombatWeaponDamage'] = 'no'
        self.gs.rules['CombatStumble'] = 'yes'
        self.gs.rules['CombatFumble'] = 'yes'
        self.gs.rules['CombatUnhorse'] = 'yes'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        self.gs.CombatNewRound()
        c1 = self.charGen(name='c1', wield1='broadsword', mount='warhorse', armor1='natural(2,2,2,2)')
        self.w1.SetAttribute('edge', 2)
        self.w1.SetAttribute('point', 0)
        self.w1.SetAttribute('blunt', 0)
        c2 = self.charGen(name='c2', wield1='shortsword', wield2='roundshield', mount='warhorse', armor1='natural(2,2,2,2)')
        self.w1.SetAttribute('edge', 0)
        self.w1.SetAttribute('point', 1)
        self.w1.SetAttribute('blunt', 0)
        c3 = self.charGen(name='c3', wield1='battlesword')
        self.w1.SetAttribute('edge', 3)
        self.w1.SetAttribute('point', 0)
        self.w1.SetAttribute('blunt', 0)
        c1.SetAttribute('str', 11)
        c1.SetAttribute('end', 11)
        c2.SetAttribute('str', 12)
        c2.SetAttribute('end', 12)
        c3.SetAttribute('str', 13)
        c3.SetAttribute('end', 13)
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())
        self.assertEqual('standing', c3.GetStatus())
        return (c1, c2, c3)

    def test_370_Attack_mounted_bf(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6)) # c1 fumble
        self.gs.rand.testset('attrcheck-dex', (6,6,6)) # c2 fumble
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=100, defensex='block', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetWielded()['righthand'])
        self.assertEqual(None, c2.GetWielded()['lefthand'])

    def test_370_Attack_mounted_df(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, aroll=99, defensex='block', droll=100)) # MF/CF
        self.gs.rand.testend()
        self.assertNotEqual(None, c1.GetWielded()['righthand'].tid)
        self.assertEqual(None, c2.GetWielded()['lefthand'])

    def test_370_Attack_mounted_af(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, aroll=100, defensex='counterstrike', droll=99)) # CF/MF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetWielded()['righthand'])
        self.assertNotEqual(None, c2.GetWielded()['lefthand'].tid)

    def test_370_Attack_mounted_block(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset()
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=1, defensex='block', droll=1)) # MS/MS
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))

    def test_370_Attack_mounted_bs(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('unhorsecheck', 1)  # MS (attacker)
        self.gs.rand.testset('unhorsecheck', 5)  # CS (defender)
        self.assertEqual(c1, self.gs.CombatAttack(c2, c1, aroll=100, defensex='dodge', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

        self.gs.rand.testset('unhorsecheck', 99)  # MF (attacker)
        self.gs.rand.testset('unhorsecheck',100)  # CF (defender)
        self.gs.rand.testset('damage', (3,))      # 1d (attacker)
        self.gs.rand.testset('damage', (1,2,3,4,))# 4d (defender)
        self.gs.rand.testset('rangeloc', 60)      #    (attacker)
        self.gs.rand.testset('rangeloc', 71)      #    (defender)
        self.gs.rand.testset('shockroll', (1,1))  # 2d (defender)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=100, defensex='dodge', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual(['groin-S2blunt'],  c1.GetList('wounds'))  # fell hard
        self.assertEqual(['thorax-M1blunt'], c2.GetList('wounds'))  # fell soft
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

    def test_370_Attack_mounted_ds(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('unhorsecheck', 1)  # MS (defender)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=99, defensex='dodge', droll=100)) # MF/CF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_370_Attack_mounted_as(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('unhorsecheck', 99)  # MF (attacker)
        self.gs.rand.testset('damage', (3,))      # 1d (attacker)
        self.gs.rand.testset('rangeloc', 60)      #    (attacker)
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=100, defensex='dodge', droll=99)) # CF/MF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['thorax-M1blunt'], c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

    def test_370_Attack_mounted_dta(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset()
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, aroll=99, defensex='block', droll=1))   # MF/MS => DTA
        self.gs.rand.testend()
        self.gs.rand.testset()
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=99, defensex='dodge', droll=1)) # MF/MS => DTA (too many)
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_370_Attack_mounted_a1(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=2, defensex='block', droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['skull-M1edge'], c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_370_Attack_mounted_a2(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('rangeloc', 60)  # thorax
        self.gs.rand.testset('damage', (5,5)) # +1p == STR(11)
        self.gs.rand.testset('shockroll', (1,2,3))
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=1, defensex='counterstrike', droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual(['thorax-S3point'], c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_370_Attack_mounted_a3(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('rangeloc', 60)         # thorax
        self.gs.rand.testset('damage', (5,5,1))      # +2e > STR(12)
        self.gs.rand.testset('unhorsecheck', 99)     # MF
        self.gs.rand.testset('damage', (1,))         # fall soft (-2 armor)
        self.gs.rand.testset('rangeloc', 60)         # thorax
        self.gs.rand.testset('shockroll', (1,2,3))   # from hit
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=5, defensex='dodge', droll=100)) # CS/CF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['thorax-S3edge'], c2.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

    def test_370_Attack_mounted_a4(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('rangeloc', 60)           # thorax
        self.gs.rand.testset('damage', (5,5,1,1))      # +2e > STR(12)
        self.gs.rand.testset('unhorsecheck', 95)       # CF
        self.gs.rand.testset('damage', (1,2,3,4))      # fall hard (-2 armor)
        self.gs.rand.testset('rangeloc', 71)           # groin
        self.gs.rand.testset('shockroll', (1,2,3,2,1)) # from hit + fall
        self.assertEqual(None, self.gs.CombatAttack(c2, c1, aroll=5, defensex='ignore', droll=99)) # CS/MF
        self.gs.rand.testend()
        self.assertEqual(['thorax-S3point', 'groin-S2blunt'], c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_370_Attack_mounted_d2(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', (6,6))
        self.gs.rand.testset('unhorsecheck', 99)
        self.gs.rand.testset('damage', (3))
        self.gs.rand.testset('rangeloc', 60)
        self.gs.rand.testset('shockroll', (1,2,3,4))
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, aroll=99, defensex='counterstrike', droll=5)) # MF/CS
        self.gs.rand.testend()
        self.assertEqual(['skull-S3point', 'thorax-M1blunt'], c1.GetList('wounds'))
        self.assertEqual(None, c2.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_370_Attack_mounted_b2(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('damage', (6,6))  # defender
        self.gs.rand.testset('rangeloc', 55)
        self.gs.rand.testset('unhorsecheck', 99)
        self.gs.rand.testset('damage', (3))
        self.gs.rand.testset('shockroll', (1,1,1,1))
        self.gs.rand.testset('rangeloc', 60)
        self.gs.rand.testset('damage', (6,6))  # attacker
        self.gs.rand.testset('rangeloc', 35)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('unhorsecheck', 100)
        self.gs.rand.testset('damage', (2,2,2,2))
        self.gs.rand.testset('rangeloc', 60)
        self.gs.rand.testset('shockroll', (1,2,3,4,5))
        self.gs.CombatAttack(c1, c2, aroll=5, defensex='counterstrike', droll=5) # CS/CS # self.assertEqual(None, but may not
        self.gs.rand.testend()
        self.assertEqual(['thorax-S3point', 'thorax-S2blunt'], c1.GetList('wounds'))
        self.assertEqual(['thorax-S3edge', 'leftelbow-M1blunt'], c2.GetList('wounds'))
        self.assertEqual('unconscious', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('damage', (6,6))  # defender
        self.gs.rand.testset('rangeloc', 1)
        deity.test.TestUserResponse('kill?', False)
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.gs.rand.testset('shockroll', (1,1,1,1))
        self.gs.rand.testset('damage', (6,6))  # attacker
        self.gs.rand.testset('rangeloc', 100)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('unhorsecheck', 100)
        self.gs.rand.testset('damage', (2,2,2,2))
        self.gs.rand.testset('rangeloc', 60)
        self.gs.rand.testset('shockroll', (1,2,3,4,5,6))
        self.gs.CombatAttack(c1, c3, aroll=5, defensex='counterstrike', droll=5) # CS/CS # self.assertEqual(None, but may not
        self.gs.rand.testend()
        self.assertEqual(['leftfoot-G4edge', 'thorax-S2blunt'], c1.GetList('wounds'))
        self.assertEqual(['skull-G4edge'], c3.GetList('wounds'))
        self.assertEqual('unconscious', c1.GetStatus())
        self.assertEqual('standing', c3.GetStatus())

    def test_371_Trample_mounted_as(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('steedcmdcheck', 1)
        self.gs.rand.testset('skillcheck-dodge', 1)  # steed stumble
        self.gs.rand.testset('unhorsecheck', 99)     # MF (attacker)
        self.gs.rand.testset('damage', (3,))         # 1d (attacker)
        self.gs.rand.testset('rangeloc', 60)         #    (attacker)
        self.assertEqual(None, self.gs.CombatAttack(c1, c3, attackx='trample', aroll=100, defensex='dodge', droll=99)) # CF/MF => AS
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1blunt'], c1.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c3.GetStatus())

    def test_371_Trample_mounted_a1(self):
        c1, c2, c3 = self.mountedGen()
        self.gs.rand.testset('steedcmdcheck', 1)
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('shockroll', (1,2))
        self.assertEqual(None, self.gs.CombatAttack(c1, c3, attackx='trample', aroll=2, defensex='dodge', droll=99)) # MS/MF => A1
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['skull-S2blunt'], c3.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('standing', c3.GetStatus())

    def test_375_Attack_steed_bs(self):
        c1, c2, c3 = self.mountedGen()
        h1 = c1.GetMount()
        self.gs.rand.testset('attrcheck-dex', (1,1,1))  # S (attacker)
        self.gs.rand.testset('skillcheck-dodge', 1)     # S (steed)
        self.gs.rand.testset('unhorsecheck', 1)         # S (rider)
        self.assertEqual(None, self.gs.CombatAttack(c3, h1, aroll=100, defensex='dodge', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(None, h1.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('standing', h1.GetStatus())

        self.gs.rand.testset('attrcheck-dex', (6,6,6))  # F (attacker)
        self.gs.rand.testset('skillcheck-dodge', 99)    # F (steed)
        self.gs.rand.testset('damage', (1,2,3,4,))# 4d (defender)
        self.gs.rand.testset('rangeloc', 71)      #    (defender)
        self.gs.rand.testset('shockroll', (1,1))  # 2d (defender)
        self.assertEqual(None, self.gs.CombatAttack(c3, h1, aroll=100, defensex='dodge', droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual(['groin-S2blunt'],  c1.GetList('wounds'))  # fell hard
        self.assertEqual(None, h1.GetList('wounds'))  # fell soft
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('prone', h1.GetStatus())

    def test_375_Attack_steed_a1(self):
        c1, c2, c3 = self.mountedGen()
        h1 = c1.GetMount()
        self.gs.rules['CombatMinorWoundShock'] = 'yes'
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('shockroll', 1)
        self.gs.rand.testset('unriderinitiative', 99)  # MF
        self.gs.rand.testset('unhorsecheck', 1)        # MS
        self.assertEqual(None, self.gs.CombatAttack(c3, h1, aroll=2, defensex='dodge', droll=99)) # MS/MF => A*1
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['skull-M1edge'], h1.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())

    def test_375_Attack_steed_a2(self):
        c1, c2, c3 = self.mountedGen()
        h1 = c1.GetMount()
        self.gs.rand.testset('rangeloc', 90)           # hindlimb
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('damage', (6,6))
        self.gs.rand.testset('skillcheck-dodge', 1)    # S (steed)
        self.gs.rand.testset('shockroll', (1,1))
        self.gs.rand.testset('unhorsecheck', 1)        # MS
        self.assertEqual(None, self.gs.CombatAttack(c3, h1, aroll=5, defensex='dodge', droll=99)) # CS/MF => A*2
        self.gs.rand.testend()
        self.assertEqual(None, c1.GetList('wounds'))
        self.assertEqual(['lefthindlimb-S2edge'], h1.GetList('wounds'))
        self.assertEqual('mounted', c1.GetStatus())

        c1, c2, c3 = self.mountedGen()
        h1 = c1.GetMount()
        self.gs.rand.testset('rangeloc', 90)           # hindlimb
        self.gs.rand.testset('rangeloc-multi', 0)
        self.gs.rand.testset('damage', (6,6))
        self.gs.rand.testset('skillcheck-dodge', 99)   # F (steed)
        self.gs.rand.testset('shockroll', (1,1))
        self.gs.rand.testset('damage', (1,2,3,4))      # fall hard (-2 armor)
        self.gs.rand.testset('rangeloc', 71)           # groin
        self.gs.rand.testset('shockroll', (1,2))       # from hit + fall
        self.assertEqual(c3, self.gs.CombatAttack(c3, h1, aroll=5, defensex='dodge', droll=99)) # CS/MF => A*2
        self.gs.rand.testend()
        self.assertEqual(['lefthindlimb-S2edge'], h1.GetList('wounds'))
        self.assertEqual(['groin-S2blunt'], c1.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())

    def test_375_Attack_steed_a4(self):
        c1, c2, c3 = self.mountedGen()
        h1 = c1.GetMount()
        self.gs.rules['CombatKnockback'] = 'no'
        self.gs.rand.testset('rangeloc', 75)           # abdomen
        self.gs.rand.testset('damage', (5,5,1,1))      #
        self.gs.rand.testset('shockroll', (1,1,1))     # worhorse end
        self.gs.rand.testset('unriderinitiative', 99)  # MF
        self.gs.rand.testset('unhorsecheck', 100)      # CF
        self.gs.rand.testset('damage', (1,2,3,4))      # fall hard (-2 armor)
        self.gs.rand.testset('rangeloc', 71)           # groin
        self.gs.rand.testset('shockroll', (1,2))       # from hit + fall
        self.assertEqual(None, self.gs.CombatAttack(c3, h1, aroll=5, defensex='ignore', droll=99)) # CS/MF => A*4
        self.gs.rand.testend()
        self.assertEqual(['abdomen-S3edge'], h1.GetList('wounds'))
        self.assertEqual(['groin-S2blunt'], c1.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())

        c1, c2, c3 = self.mountedGen()
        h1 = c1.GetMount()
        self.gs.rand.testset('rangeloc', 75)           # abdomen
        self.gs.rand.testset('damage', (5,5,5,5))      #
        deity.test.TestUserResponse('kill?', False)
        self.gs.rand.testset('shockroll', (6,6,6,6,6)) # worhorse end
        self.gs.rand.testset('damage', (1,2,3,4))      # fall hard (-2 armor)
        self.gs.rand.testset('rangeloc', 71)           # groin
        self.gs.rand.testset('shockroll', (1,2))       # from hit + fall
        self.assertEqual(c3, self.gs.CombatAttack(c3, h1, aroll=5, defensex='ignore', droll=99)) # CS/MF => A*4
        self.gs.rand.testend()
        self.assertEqual(['abdomen-G5edge'], h1.GetList('wounds'))
        self.assertEqual(['groin-S2blunt'], c1.GetList('wounds'))
        self.assertEqual('prone', c1.GetStatus())

    def test_400_Grapple_bad(self):
        self.gs.rules['CombatWeaponDamage'] = 'no'
        self.gs.rules['CombatStumble'] = 'no'
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatMinorWoundShock'] = 'no'
        c1 = self.charGen(type='human', wield1='dagger')
        c2 = self.charGen(type='human', wield1='dagger')
        self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c2, attackx='grapple', defensex='dodge')
        self.assertRaises(NotPossible, self.gs.CombatAttack, c1, c2, attackx='attack', defensex='grapple')

    def test_400_Grapple_both_grapple(self):
        (c1, c2, i1, i2) = self.combatGen()
        c1.UnwieldItem(i1)
        c2.UnwieldItem(i2)
        self.gs.rand.testset('holdstrength', (1,2,3, 1,2,3, 1,1,1, 6,6,6))  # tie, c1
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='grapple', aroll=100, droll=100)) # CF/CF
        self.gs.rand.testend()

    def test_400_Grapple_bs(self):
        (c1, c2, i1, i2) = self.combatGen()
        c1.UnwieldItem(i1)
        c2.UnwieldItem(i2)
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='grapple', aroll=100, droll=100)) # CF/CF
        self.gs.rand.testend()
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_400_Grapple_as(self):
        (c1, c2, i1, i2) = self.combatGen()
        c2.UnwieldItem(i2)
        self.assertIsNotNone(c1.GetWielded()['righthand'])
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=100, droll=99)) # CF/MF => AS => TA
        self.gs.rand.testend()
        self.assertIsNotNone(c1.GetWielded()['righthand'])
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_400_Grapple_ds(self):
        (c1, c2, i1, i2) = self.combatGen()
        c1.UnwieldItem(i1)
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='block', aroll=99, droll=100)) # MF/CF
        self.gs.rand.testend()
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_400_Grapple_block(self):
        (c1, c2, i1, i2) = self.combatGen()
        c2.UnwieldItem(i2)
        self.gs.rand.testset()
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=99, droll=99, adamage=1)) # MF/MF
        self.gs.rand.testend()
        self.assertEqual(['rightforearm-M1point'], c2.GetList('wounds', default=[]))
        self.assertEqual([], c1.GetList('wounds', default=[]))

    def test_400_Grapple_ah(self):
        (c1, c2, i1, i2) = self.combatGen()
        c1.UnwieldItem(i1)
        c2.UnwieldItem(i2)
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()

    def test_400_Grapple_dh(self):
        (c1, c2, i1, i2) = self.combatGen()
        c2.UnwieldItem(i2)
        self.gs.rand.testset()
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=99, droll=1)) # MF/MS
        self.gs.rand.testend()

    def test_400_Grapple_bh(self):
        (c1, c2, i1, i2) = self.combatGen()
        c1.UnwieldItem(i1)
        self.gs.rand.testset()
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='counterstrike', aroll=5, droll=5)) # CS/CS
        self.gs.rand.testend()

    def test_400_Grapple_bh(self):
        (c1, c2, i1, i2) = self.combatGen()
        c2.UnwieldItem(i2)
        self.gs.rand.testset()
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=5, droll=5)) # CS/CS
        self.gs.rand.testend()
        self.assertEqual([], c2.GetList('wounds', default=[]))
        self.assertEqual([], c1.GetList('wounds', default=[]))
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('standing', c2.GetStatus())

    def test_410_Grapple_mounted_bs(self):
        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))  # c2
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='grapple', aroll=100, droll=100)) # CF/CF
        self.gs.rand.testend()

    def test_410_Grapple_mounted_as(self):
        c1, c2, c3 = self.mountedGen()
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset('unhorsecheck', 1)    # MS
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=100, droll=99)) # CF/MF
        self.gs.rand.testend()

    def test_410_Grapple_mounted_ds(self):
        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        self.gs.rand.testset('unhorsecheck', 99)   # MF
        self.gs.rand.testset('damage', 3)
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='block', aroll=99, droll=100)) # MF/CF => DS
        self.gs.rand.testend()

    def test_410_Grapple_mounted_block(self):
        c1, c2, c3 = self.mountedGen()
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset()
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=99, droll=99, adamage=3)) # MF/MF
        self.gs.rand.testend()
        self.assertEqual(['rightforearm-M1edge'], c2.GetList('wounds', default=[]))
        self.assertEqual([], c1.GetList('wounds', default=[]))

    def test_410_Grapple_mounted_ah(self):
        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        c3.UnwieldItem(c3.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.assertEqual(c2, self.gs.CombatAttack(c3, c2, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual('prone', c3.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        c3.UnwieldItem(c3.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c3, self.gs.CombatAttack(c3, c2, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual('standing', c3.GetStatus())
        self.assertEqual('prone', c2.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.assertEqual(c3, self.gs.CombatAttack(c1, c3, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF => hold
        self.gs.rand.testend()
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c3.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        self.assertEqual(c1, self.gs.CombatAttack(c1, c3, attackx='grapple', defensex='dodge', aroll=1, droll=99)) # MS/MF
        self.gs.rand.testend()
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('prone', c3.GetStatus())

    def test_410_Grapple_mounted_dh(self):
        c1, c2, c3 = self.mountedGen()
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset()
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c2, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=99, droll=1)) # MF/MS
        self.gs.rand.testend()

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        c3.UnwieldItem(c3.GetWielded()['righthand'])
        deity.test.TestUserResponse('unarmed?', True)
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c3, self.gs.CombatAttack(c1, c3, attackx='attack', defensex='grapple', aroll=99, droll=1)) # MF/MS
        self.gs.rand.testend()
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c3.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        c3.UnwieldItem(c3.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c1, self.gs.CombatAttack(c1, c3, attackx='attack', defensex='grapple', aroll=99, droll=1)) # MF/MS
        self.gs.rand.testend()
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('prone', c3.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        c3.UnwieldItem(c3.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        #self.gs.rand.testset('unhorsecheck', 99)
        #self.gs.rand.testset('damage', 3)     # unhorse damage
        #self.gs.rand.testset('rangeloc', 70)  # unhorse damage
        deity.test.TestUserResponse('unarmed?', True)
        self.assertEqual(c3, self.gs.CombatAttack(c3, c1, attackx='attack', defensex='grapple', aroll=99, droll=1)) # MF/MS
        self.gs.rand.testend()
        self.assertEqual('prone', c1.GetStatus())
        self.assertEqual('standing', c3.GetStatus())

        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (1,1,1, 6,6,6))
        self.assertEqual(c1, self.gs.CombatAttack(c3, c1, attackx='attack', defensex='grapple', aroll=99, droll=1)) # MF/MS
        self.gs.rand.testend()
        self.assertEqual('standing', c1.GetStatus())
        self.assertEqual('prone', c3.GetStatus())

    def test_410_Grapple_mounted_bh(self):
        c1, c2, c3 = self.mountedGen()
        c1.UnwieldItem(c1.GetWielded()['righthand'])
        self.gs.rand.testset('holdstrength', (6,6,6, 1,1,1))
        self.gs.rand.testset('damage', (3,3,3,3))  # unhorse damage
        self.gs.rand.testset('rangeloc', 70)       # unhorse damage
        self.gs.rand.testset('shockroll', (1,2,3)) # fall hard
        self.assertEqual(c1, self.gs.CombatAttack(c1, c2, attackx='grapple', defensex='counterstrike', aroll=5, droll=5)) # CS/CS
        self.gs.rand.testend()

        c1, c2, c3 = self.mountedGen()
        c2.UnwieldItem(c2.GetWielded()['righthand'])
        self.gs.rand.testset()
        self.assertEqual(None, self.gs.CombatAttack(c1, c2, attackx='attack', defensex='grapple', aroll=5, droll=5)) # CS/CS
        self.gs.rand.testend()
        self.assertEqual([], c2.GetList('wounds', default=[]))
        self.assertEqual([], c1.GetList('wounds', default=[]))
        self.assertEqual('mounted', c1.GetStatus())
        self.assertEqual('mounted', c2.GetStatus())

    def test_450_Make_fumble(self):
        (c1, c2, i1, i2) = self.combatGen()
        self.assertIsNotNone(c2.GetWielded()['lefthand'])
        self.assertRaises(BadParameter, self.gs.CombatMake, c1, c2, 'fumble', 'righthand', 'foo')
        self.assertRaises(NotPossible, self.gs.CombatMake, c1, c2, 'fumble', 'righthand')
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(None, self.gs.CombatMake(c1, c2, 'fumble', 'lefthand'))
        self.gs.rand.testend()
        self.assertIsNotNone(c2.GetWielded()['lefthand'])
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(c1, self.gs.CombatMake(c1, c2, 'fumbl'))
        self.gs.rand.testend()
        self.assertIsNone(c2.GetWielded()['lefthand'])
        self.assertRaises(NotPossible, self.gs.CombatMake, c1, c2, 'fumble')
        self.gs.tacticalAdvantages = 0
        self.assertIsNotNone(c1.GetWielded()['righthand'])
        self.assertRaises(NotPossible, self.gs.CombatMake, c1, c2, 'fumble', 'lefthand')
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(None, self.gs.CombatMake(c2, c1, 'f', 'righthand'))
        self.gs.rand.testend()
        self.assertIsNotNone(c1.GetWielded()['righthand'])
        self.gs.commandForce = True
        self.gs.rand.testset()
        self.assertEqual(c2, self.gs.CombatMake(c2, c1, 'fumble'))
        self.gs.rand.testend()
        self.assertIsNone(c1.GetWielded()['righthand'])

    def test_451_Make_stumble(self):
        c1, c2, c3 = self.mountedGen()
        self.assertRaises(BadParameter, self.gs.CombatMake, c1, c3, 'stumble', 'foo')
        self.assertRaises(NotPossible, self.gs.CombatMake, c3, c1, 'stumble')
        self.assertEqual('standing', c3.GetStatus())
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.assertEqual(None, self.gs.CombatMake(c1, c3, 'stumbl'))
        self.gs.rand.testend()
        self.assertEqual('standing', c3.GetStatus())
        self.gs.rand.testset('attrcheck-dex', (6,6,6))
        self.assertEqual(c1, self.gs.CombatMake(c1, c3, 'st'))
        self.gs.rand.testend()
        self.assertEqual('prone', c3.GetStatus())
        self.assertRaises(NotPossible, self.gs.CombatMake, c1, c3, 'stumble')
        self.gs.tacticalAdvantages = 0
        c3.SetStatus('standing')
        self.gs.commandForce = True
        self.gs.rand.testset()
        self.assertEqual(c1, self.gs.CombatMake(c1, c3, 'stumble'))
        self.gs.rand.testend()
        self.assertEqual('prone', c3.GetStatus())

    def test_452_Make_unhorse(self):
        c1, c2, c3 = self.mountedGen()
        self.assertRaises(BadParameter, self.gs.CombatMake, c1, c3, 'unhorse', 'foo')
        self.assertRaises(NotPossible, self.gs.CombatMake, c1, c3, 'unhorse')
        self.assertEqual('mounted', c1.GetStatus())
        self.gs.rand.testset('unhorsecheck', 1)
        self.assertEqual(None, self.gs.CombatMake(c3, c1, 'unhors'))
        self.gs.rand.testend()
        self.assertEqual('mounted', c1.GetStatus())
        self.gs.rand.testset('unhorsecheck', 99)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('rangeloc', 50)
        self.assertEqual(None, self.gs.CombatMake(c3, c1, 'unh'))
        self.gs.rand.testend()
        self.assertEqual('prone', c1.GetStatus())
        self.assertRaises(NotPossible, self.gs.CombatMake, c3, c1, 'unhorse')
        self.gs.tacticalAdvantages = 0
        self.gs.commandForce = True
        self.gs.rand.testset('unhorsecheck', 1)
        self.gs.rand.testset('damage', 1)
        self.gs.rand.testset('rangeloc', 50)
        self.assertEqual(None, self.gs.CombatMake(c3, c2, 'unhorse'))
        self.gs.rand.testend()
        self.assertEqual('prone', c2.GetStatus())

    def test_500_Update_wounds_nt(self):
        ch = self.charGen()
        ch.SetAttribute('end', 10)
        ch.SetList('wounds', ['thorax-M1blunt', 'groin-S2point', 'abdomen-S3edge', 'face-G4blunt'])
        ch.SetValue('x:ipenalty', '10');
        deity.test.TestUserResponse('physkill', '')
        self.gs.rand.testset()
        self.gs.Update(ch, {'abs':100})
        self.assertEqual(['thorax-M1blunt/H5//100', 'groin-S2point/H4//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3*//100'], ch.GetList('wounds'))
        self.gs.Update(ch, {'abs':104})
        self.assertEqual(['thorax-M1blunt/H5//100', 'groin-S2point/H4//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3*//100'], ch.GetList('wounds'))
        self.assertEqual('10', ch.GetValue('x:ipenalty'))
        deity.test.TestUserResponse('physkill', '34')
        self.gs.rand.testset('healcheck', (100, 10*4+17+1, 10*4+17, 5))
        self.gs.Update(ch, {'abs':105})
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1blunt/H5//105', 'groin-S2point/H4//105', 'abdomen-S2edge/H4//105', 'face-G2blunt/H3*//105'], ch.GetList('wounds'))
        self.assertEqual('7', ch.GetValue('x:ipenalty'))
        ch.SetStatus('dead')
        self.gs.Update(ch, {'abs':110})

    def test_501_Update_wounds(self):
        ch = self.charGen()
        ch.SetAttribute('end', 10)
        ch.SetList('wounds', ['thorax-M1blunt/EE//100', 'groin-S2point/H5//100', 'abdomen-S3edge///100', 'face-G4blunt/H3*//100'])
        ch.SetValue('x:ipenalty', '1');
        deity.test.TestUserResponse('physkill', '34')
        self.gs.rand.testset('treatcheck', 31)
        self.gs.Update(ch, {'abs':100})
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point/H5//100', 'abdomen-S3edge/H5//100', 'face-G4blunt/H3*//100'], ch.GetList('wounds'))
        self.gs.Update(ch, {'abs':104})
        self.assertEqual(['groin-S2point/H5//100', 'abdomen-S3edge/H5//100', 'face-G4blunt/H3*//100'], ch.GetList('wounds'))
        self.assertEqual('1', ch.GetValue('x:ipenalty'))
        deity.test.TestUserResponse('physkill', '34')
        self.gs.rand.testset('healcheck', (10*5+17+1, 10*4+17, 5))
        self.gs.Update(ch, {'abs':105})
        self.gs.rand.testend()
        self.assertEqual(['groin-S2point/H5//105', 'abdomen-S2edge/H5//105', 'face-G2blunt/H3*//105'], ch.GetList('wounds'))
        self.assertEqual('0', ch.GetValue('x:ipenalty'))
        deity.test.TestUserResponse('physkill', '99')
        deity.test.TestOutputClear()
        self.gs.rand.testset('healcheck', (5, 5, 5))
        self.gs.Update(ch, {'abs':110})
        self.gs.rand.testend()
        self.assertEqual([], ch.GetList('wounds'))
        output = deity.test.TestOutputGet()
        self.assertIn('healed', output)
        self.assertIn('impairment', output)

    def test_502_Update_infection(self):
        ch = self.charGen()
        ch.SetAttribute('end', 10)
        ch.SetList('wounds', ['groin-S2point/H3//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3*//100'])
        at = self.charGen()
        at.SetSkill('physician', 32)
        self.gs.Update(ch, {'abs':100}, attending=at)
        self.gs.rand.testset('healcheck', (100, 100, 100)) # CF, CF, CF
        self.gs.Update(ch, {'abs':105}, attending=at)
        self.gs.rand.testend()
        self.assertEqual(['groin-S2point/H3/INF-H3/105', 'abdomen-S3edge/H4/INF-H4/105', 'face-G4blunt/H3*/INF-H3/105'], ch.GetList('wounds'))
        self.gs.rand.testset('infectioncheck', (5, 1, 100)) # CS, MS, CF
        self.gs.Update(ch, {'abs':106}, attending=at)
        self.gs.rand.testend()
        self.assertEqual(['groin-S2point/H3/INF-H5/106', 'abdomen-S3edge/H4/INF-H5/106', 'face-G4blunt/H3*/INF-H1/106'], ch.GetList('wounds'))
        self.assertEqual('standing', ch.GetInfo('status', 'standing'))
        self.gs.rand.testset('infectioncheck', (99, 10*5+32, 10*1+32+1)) # MF, MS, MF
        self.gs.Update(ch, {'abs':107}, attending=at)
        self.gs.rand.testend()
        self.assertEqual(['groin-S2point/H3/INF-H4/107', 'abdomen-S3edge/H4//107', 'face-G4blunt/H3*/INF-H0/107'], ch.GetList('wounds'))
        self.assertEqual('dead', ch.GetStatus())
        ch.SetStatus('standing')
        self.gs.rules['WoundInfections'] = False
        self.gs.Update(ch, {'abs':108}, attending=at)
        self.assertEqual(['groin-S2point/H3//107', 'abdomen-S3edge/H4//107', 'face-G4blunt/H3*//107'], ch.GetList('wounds'))

    def test_503_Update_bloodloss(self):
        ch = self.charGen()
        ch.SetAttribute('end', 10)
        ch.SetValue('x:bloodloss', '4')
        self.gs.Update(ch, {'abs':100})
        self.gs.rand.testset('bloodlosscheck', 10*6+1)
        self.gs.Update(ch, {'abs':105})
        self.gs.rand.testend()
        self.assertEqual('4', ch.GetValue('x:bloodloss'))
        self.gs.rand.testset('bloodlosscheck', 10*6-1)
        self.gs.Update(ch, {'abs':110})
        self.gs.rand.testend()
        self.assertEqual('3', ch.GetValue('x:bloodloss'))
        self.gs.rand.testset('bloodlosscheck', 10*6)
        self.gs.Update(ch, {'abs':115})
        self.gs.rand.testend()
        self.assertEqual('1', ch.GetValue('x:bloodloss'))
        self.assertNotEqual('0', ch.GetValue('x:bloodloss-time'))
        self.gs.rand.testset('bloodlosscheck', 1)
        self.gs.Update(ch, {'abs':120})
        self.gs.rand.testend()
        self.assertEqual('0', ch.GetValue('x:bloodloss'))
        self.assertEqual('0', ch.GetValue('x:bloodloss-time'))

    def test_505_Update_wounds_sindarin(self):
        ch = self.charGen('sindarin')
        ch.SetAttribute('end', 10)
        ch.SetList('wounds', ['thorax-M1blunt/EE//100', 'groin-S2point/H5//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3//100'])
        self.gs.Update(ch, {'abs':100})
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point/H5//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3//100'], ch.GetList('wounds'))
        deity.test.TestUserResponse('physkill', '33')
        self.gs.rand.testset('healcheck', (10*5+16+1, 1, 10*4+16, 2, 5, 5))
        self.gs.Update(ch, {'abs':105})
        self.gs.rand.testend()
        self.assertEqual(['groin-S1point/H5//105', 'abdomen-S1edge/H4//105'], ch.GetList('wounds'))

    def test_506_Update_infection_sindarin(self):
        ch = self.charGen('sindarin')
        ch.SetAttribute('end', 10)
        ch.SetList('wounds', ['thorax-M1blunt/EE//100', 'groin-S2point/H5//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3//100'])
        self.gs.Update(ch, {'abs':100})
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point/H5//100', 'abdomen-S3edge/H4//100', 'face-G4blunt/H3//100'], ch.GetList('wounds'))
        deity.test.TestUserResponse('physkill', '')
        self.gs.rand.testset('healcheck', (100, 100, 100, 100, 100, 100))
        self.gs.Update(ch, {'abs':105})
        self.gs.rand.testend()
        self.assertEqual(['groin-S2point/H5//105', 'abdomen-S3edge/H4//105', 'face-G4blunt/H3//105'], ch.GetList('wounds'))

    def test_510_WoundTreatment(self):
        ch1 = self.charGen()
        ch2 = self.charGen()
        ch2.SetSkill('physician', 42)
        self.assertRaises(NotPossible, self.gs.WoundTreatment, ch1, ch2)
        ch1.SetList('wounds', ['thorax-M1blunt/EE//100', 'groin-S2point///100', 'abdomen-S3edge///', 'face-G4blunt'])
        self.assertRaises(BadParameter, self.gs.WoundTreatment, ch1, ch2, 'blah')
        self.gs.WoundTreatment(ch1, ch2, 'thorax')
        self.assertIn('wound "thorax-M1blunt/EE//100" has already been treated', deity.test.TestOutputBuf)
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point///100', 'abdomen-S3edge///', 'face-G4blunt'], ch1.GetList('wounds'))
        self.gs.rand.testset('skillcheck-physician', 42+20+1) # MF
        self.gs.WoundTreatment(ch1, ch2, 'abdomen')
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point///100', 'abdomen-S3edge/H4//', 'face-G4blunt'], ch1.GetList('wounds'))
        self.gs.rand.testset('skillcheck-physician', 42+10-1) # MS
        self.gs.WoundTreatment(ch1, ch2, 'face')
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point///100', 'abdomen-S3edge/H4//', 'face-G4blunt/H4//'], ch1.GetList('wounds'))
        self.gs.rand.testset('skillcheck-physician', 42+15-25+3) # CF
        self.gs.WoundTreatment(ch1, ch2, 'groin', time=105)
        self.gs.rand.testend()
        self.assertEqual(['thorax-M1blunt/EE//100', 'groin-S2point/H3//100', 'abdomen-S3edge/H4//', 'face-G4blunt/H4//'], ch1.GetList('wounds'))


###############################################################################


class HarnMaster3_30_CharEditValueManager(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.harnmaster.HarnMaster3()
        self.gs.charPrompter.SetPrompt(False)
        self.ch = self.gs.Character(None)
        self.ch.Generate('human')
        self.cw = deity.watcher.Watched(self.ch)
        self.vm = self.gs.CharEditValueManager(self.cw)

    def test_10_init(self):
        pass

    def test_20_moreattrs(self):
        self.ch.SetValue('a:agl', '12')
        self.vm.SetValue('a:agl', '12')
        self.ch.SetValue('a:mov', '15.0')
        self.vm.SetValue('a:mov', '15')
        self.assertEqual('15', self.vm.GetValue('a:mov'))
        self.vm.ChangeValue('a:agl', '14')
        self.assertEqual('17', self.vm.GetValue('a:mov'))
        self.assertEqual('17', self.ch.GetValue('a:mov'))

    def test_30_skills(self):
        self.ch.SetValue('a:agl', '5')
        self.vm.SetValue('a:agl', '5')
        self.ch.SetValue('a:wil', '15')
        self.vm.SetValue('a:wil', '15')
        self.ch.SetValue('s:initiative', '51')
        self.vm.SetValue('s:initiative', '51')
        self.vm.ChangeValue('a:agl', '8')
        self.assertEqual('55', self.vm.GetValue('s:initiative'))
        self.assertEqual('55', self.ch.GetValue('s:initiative'))

    def test_40_birthday_sunsign(self):
        self.ch.SetValue('a:dex', '9')
        self.vm.SetValue('a:dex', '9')
        self.ch.SetValue('a:agl', '18')
        self.vm.SetValue('a:agl', '18')
        self.ch.SetValue('a:wil', '11')
        self.vm.SetValue('a:wil', '11')
        self.ch.SetValue('a:voi', '5')
        self.vm.SetValue('a:voi', '5')
        self.ch.SetValue('a:int', '8')
        self.vm.SetValue('a:int', '8')
        self.ch.SetValue('a:cml', '15')
        self.vm.SetValue('a:cml', '15')
        self.vm.SetValue('i:birthday', self.ch.GetValue('i:birthday'))
        self.vm.SetValue('i:sunsign', self.ch.GetValue('i:sunsign'))
        self.vm.ChangeValue('i:birthday', 'Nuzyael 12')
        self.assertEqual('ULA', self.vm.GetValue('i:sunsign'))
        self.ch.SetValue('s:unarmed', '50')
        self.ch.SetValue('s:riding', '60')
        self.ch.SetValue('s:halea', '70')
        self.vm.SetValue('s:unarmed', self.ch.GetValue('s:unarmed'))
        self.vm.SetValue('s:riding', self.ch.GetValue('s:riding'))
        self.vm.SetValue('s:halea', self.ch.GetValue('s:halea'))
        self.vm.ChangeValue('i:birthday', 'Morgat 1')
        self.assertEqual('MAS/LAD', self.vm.GetValue('i:sunsign'))
        self.assertEqual('50', self.vm.GetValue('s:unarmed'))
        self.assertEqual('56', self.vm.GetValue('s:riding'))
        self.assertEqual('77', self.vm.GetValue('s:halea'))

    def test_50_info_heightweight_metric(self):
        GAME.rules['ImperialMeasurements'] = 'no'
        self.ch.SetInfo('height', '254')
        self.ch.SetInfo('weight', '50')
        infos = self.ch.GetInfos()
        for i,v in infos.iteritems():
            self.vm.SetValue('i:'+i, v)

        self.assertEqual('254 cm', self.vm.GetValue('i:height'))
        self.vm.ChangeValue('i:height', '150cm')
        self.assertEqual('150cm', self.vm.GetValue('i:height'))
        self.assertEqual('150', self.ch.GetValue('i:height'))
        self.vm.ChangeValue('i:height', '120 cm')
        self.assertEqual('120 cm', self.vm.GetValue('i:height'))
        self.assertEqual('120', self.ch.GetValue('i:height'))

        self.assertEqual('50.0 kg', self.vm.GetValue('i:weight'))
        self.vm.ChangeValue('i:weight', '60kg')
        self.assertEqual('60kg', self.vm.GetValue('i:weight'))
        self.assertEqual('60', self.ch.GetValue('i:weight'))
        self.vm.ChangeValue('i:weight', '70.0 kg')
        self.assertEqual('70.0 kg', self.vm.GetValue('i:weight'))
        self.assertEqual('70.0', self.ch.GetValue('i:weight'))

    def test_51_info_heightweight_imperial(self):
        GAME.rules['ImperialMeasurements'] = 'yes'
        self.ch.SetInfo('height', '254')
        self.ch.SetInfo('weight', '50')
        infos = self.ch.GetInfos()
        for i,v in infos.iteritems():
            self.vm.SetValue('i:'+i, v)

        self.assertEqual('8\'4"', self.vm.GetValue('i:height'))
        self.vm.ChangeValue('i:height', '4\'2"')
        self.assertEqual('4\'2"', self.vm.GetValue('i:height'))
        self.assertEqual('127', self.ch.GetValue('i:height'))
        self.vm.ChangeValue('i:height', '100"')
        self.assertEqual('100"', self.vm.GetValue('i:height'))
        self.assertEqual('254', self.ch.GetValue('i:height'))
        self.vm.ChangeValue('i:height', '50"')
        self.assertEqual('50"', self.vm.GetValue('i:height'))
        self.assertEqual('127', self.ch.GetValue('i:height'))

        self.assertEqual('110.0 lbs', self.vm.GetValue('i:weight'))
        self.vm.ChangeValue('i:weight', '55lbs')
        self.assertEqual('55lbs', self.vm.GetValue('i:weight'))
        self.assertEqual('25.0', self.ch.GetValue('i:weight'))
        self.vm.ChangeValue('i:weight', '220.0 lbs')
        self.assertEqual('220.0 lbs', self.vm.GetValue('i:weight'))
        self.assertEqual('100.0', self.ch.GetValue('i:weight'))


###############################################################################


if __name__ == '__main__':
    unittest.main()
