#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (run full mounted combat)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.trandom
import deity.windows
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *

import __builtin__
import wx
App  = wx.PySimpleApp()
#Game = deity.gamesystem.harnmaster.HarnMaster3()
Main = deity.windows.Main(testmode=True);
Main.testmode = True
Game = Main.gs
Game.rules['CombatFumble'] = 'no'
__builtin__.__dict__['Output'] = Main.Output


###############################################################################


class CombatTestBase(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        GAME.rules['CombatWeaponDamage'] = 'no'
        Main.CommandExecute('combat')
        Main.CommandExecute('clear encounter')

        # fighter #1
        try:
            self.ct1a = GAME.MatchCharacterName('ct1a')
            GAME.SubCharacter(self.ct1a)
        except:
            pass
        self.ct1a = GAME.Character(None)
        self.ct1a.Generate('human-fighter')
        self.ct1a.Rename('ct1a')
        GAME.AddCharacter(self.ct1a, 'encounter')
        item = GAME.Item(None)
        item.Generate('broadsword')
        self.ct1a.AddInventory(item)
        self.ct1a.WieldItem(item, defindex=0)
        self.ct1a.SetAttribute('end', 50)
        self.ct1a.SetSkill('riding', 80)
        self.ct1a.SetSkill('swords', 60)
        self.ct1a.SetSkill('shields', 60)

        try:
            self.cm1a = GAME.MatchCharacterName('cm1a')
            GAME.SubCharacter(self.cm1a)
        except:
            pass
        self.cm1a = GAME.Character(None)
        self.cm1a.Generate('warhorse')
        self.cm1a.Rename('cm1a')
        GAME.AddCharacter(self.cm1a, 'encounter')

        # fighter #2
        try:
            self.ct1b = GAME.MatchCharacterName('ct1b')
            GAME.SubCharacter(self.ct1b)
        except:
            pass
        self.ct1b = GAME.Character(None)
        self.ct1b.Generate('human-fighter')
        self.ct1b.Rename('ct1b')
        GAME.AddCharacter(self.ct1b, 'encounter')
        item = GAME.Item(None)
        item.Generate('shortsword')
        self.ct1b.AddInventory(item)
        self.ct1b.WieldItem(item, defindex=0)
        item = GAME.Item(None)
        item.Generate('kiteshield')
        self.ct1b.AddInventory(item)
        self.ct1b.WieldItem(item, defindex=1)
        self.ct1b.SetAttribute('end', 50)
        self.ct1b.SetSkill('riding', 80)
        self.ct1b.SetSkill('swords', 80)
        self.ct1b.SetSkill('shields', 80)

        try:
            self.cm1b = GAME.MatchCharacterName('cm1b')
            GAME.SubCharacter(self.cm1b)
        except:
            pass
        self.cm1b = GAME.Character(None)
        self.cm1b.Generate('warhorse')
        self.cm1b.Rename('cm1b')
        GAME.AddCharacter(self.cm1b, 'encounter')

        # update
        Main.DisplayRefresh()

    def do_test_round(self):
        self.random = random = deity.trandom.Random()
        print "\n\n===============================================================================\n\n"

        Main.io.output.Clear()
        self.assertEqual([], Main.modeCombat.charList.GetSelections())
        Main.CommandExecute('add ct1a')
        Main.CommandExecute('add ct1b')
        print Main.io.output.GetValue()
        self.assertEqual([self.ct1a, self.ct1b], Main.modeCombat.charList.GetSelections())
        #print Game.rules

        while (True):
            print '\nbegin round...',self.ct1a.GetInfo('status'),self.ct1b.GetInfo('status')
            if self.ct1a.GetInfo('status') == 'dead' or self.ct1b.GetInfo('status') == 'dead':
                break
            if self.ct1a.GetInfo('status') == 'shock' or self.ct1b.GetInfo('status') == 'shock':
                break
            if self.ct1a.GetInfo('status') == 'unconscious' and self.ct1b.GetInfo('status') == 'unconscious':
                break

            Main.CommandExecute('round')

            while Main.modeCombat.submode == 'round':
                if Main.curchar.Get() == self.ct1a:
                    attack = self.ct1a
                    defend = self.ct1b
                    dmove  = 'block'
                elif Main.curchar.Get() == self.ct1b:
                    attack = self.ct1b
                    defend = self.ct1a
                    dmove  = 'counterstrike'
                    if random.randint(0,2) == 0:
                        defend = self.cm1a
                else:  # mount DTA
                    Main.CommandExecute('pass')
                    continue

                aname = attack.Name()
                dname = defend.Name()

                deity.test.TestUserResponse('kill?', True)
                deity.test.TestUserResponse('amputate?', True)
                deity.test.TestUserResponse('bodyblock?', True)
                deity.test.TestUserResponse('unarmed?', True)
                deity.test.TestUserResponse('damage', None)
                deity.test.TestUserResponse('rangeloc', None)
                if attack.GetStatus() == 'unconscious' or attack.GetStatus() == 'shock' or attack.GetStatus() == 'dead':
                    command = 'pass'
                elif attack.GetStatus() == 'prone':
                    command = 'stand'
                elif (attack == self.ct1a or self.kMountedChars > 1) and attack.GetStatus() != 'mounted' and random.randint(0,2) > 0:
                    mount = attack == self.ct1a and self.cm1a or self.cm1b
                    command = 'mount %s' % mount.Name()
                elif defend.GetStatus() == 'unconscious' or defend.GetStatus() == 'shock':
                    deity.test.TestUserResponse('attackprone?', True)
                    if attack.GetStatus() == 'mounted':
                        command = 'trample %s defend ignore' % dname
                    else:
                        command = 'attack %s righthand: defend ignore' % dname
                elif defend.GetStatus() == 'prone':
                    deity.test.TestUserResponse('attackprone?', True)
                    if attack.GetStatus() == 'mounted':
                        command = 'trample %s defend dodge' % dname
                    else:
                        command = 'attack %s righthand: defend dodge' % dname
                elif defend.GetStatus() == 'dead': # bled out
                    command = 'pass'
                else:
                    command = 'attack %(dname)s defend %(dmove)s' % {'dname':dname, 'dmove':dmove}

                print '-',attack.Name(),':',command
                Main.io.output.Clear()
                Main.CommandExecute(command)
                print Main.io.output.GetValue()
        #App.MainLoop()


class CombatTest_1_Mounted(CombatTestBase):
    kMountedChars = 1

    def test_round(self):
        self.do_test_round()


class CombatTest_2_Mounted(CombatTestBase):
    kMountedChars = 2

    def test_round(self):
        self.do_test_round()


###############################################################################


if __name__ == '__main__':
    unittest.main()
