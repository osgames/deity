#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test explore mode)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.modes
import deity.gamesystem.harnmaster
import deity.world.kelestia.kethira.harn

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class ExploreTest1(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.app = deity.test.App()

        self.gs = deity.gamesystem.harnmaster.HarnMaster3()
        self.gw = deity.world.kelestia.kethira.harn.Harn()
        self.mw = deity.modes.Explore(Test_Win, self.app)
        self.mw.DisplayRefresh()
        self.mw.Activate()

        chars = self.gs.gameCharNames.Get().values()
        for c in chars:
            self.gs.SubCharacter(c)

        self.ch1 = deity.gamesystem.GameSystem.Character(None)
        self.ch1.Generate(xstr('_testhuman'), 'superman')
        self.ch1.MakeUnique()
        self.ch1.SetAttribute('end', 19)
        self.ch1.SetSkill('initiative', 55)
        self.assertNotEqual(None, self.ch1.tid)
        self.gs.AddCharacter(self.ch1, 'players')

        self.ch2 = deity.gamesystem.GameSystem.Character(None)
        self.ch2.Generate(xstr('_testhuman'), 'spiderman')
        self.ch2.MakeUnique()
        self.ch2.SetAttribute('end', 18)
        self.ch2.SetSkill('awareness', 66)
        self.assertNotEqual(None, self.ch2.tid)
        self.gs.AddCharacter(self.ch2, 'players')

    def test_000_init(self):
        pass

    def test_110_timeset(self):
        self.app.CommandExecute('date 1-2-3')
        t = self.gw.TimeGetCurrent()
        self.assertEqual(1, t['year'])
        self.assertEqual(2, t['month'])
        self.assertEqual(3, t['day'])

        self.app.CommandExecute('time 4:5:6.7')
        t = self.gw.TimeGetCurrent()
        self.assertEqual(4, t['hour'])
        self.assertEqual(5, t['minute'])
        self.assertEqual(6, t['second'])
        self.assertEqual(7e8, t['nanosec'])

    def test_110_timeadjust(self):
        self.app.CommandExecute('date 1-2-3')
        self.app.CommandExecute('time 4:5:6.7')

        self.app.CommandExecute('+ 7y 6mo 5d 4h 3m 2s')
        t = self.gw.TimeGetCurrent()
        self.assertEqual(8, t['year'])
        self.assertEqual(8, t['month'])
        self.assertEqual(8, t['day'])
        self.assertEqual(8, t['hour'])
        self.assertEqual(8, t['minute'])
        self.assertEqual(8, t['second'])

        self.app.CommandExecute('-7y 6mo +5d 4h -3m 2s')
        t = self.gw.TimeGetCurrent()
        self.assertEqual( 1, t['year'])
        self.assertEqual( 2, t['month'])
        self.assertEqual(13, t['day'])
        self.assertEqual(12, t['hour'])
        self.assertEqual( 5, t['minute'])
        self.assertEqual( 6, t['second'])

    def test_120_timeupdate(self):
        ch = self.gs.Character(None)
        ch.Generate('human')
        ch.SetAttribute('end', 100)
        GAME.AddCharacter(ch, 'players')
        ch.SetList('wounds', ['thorax-M1blunt/EE//', 'groin-S2point/H5//', 'abdomen-S3edge/H4//', 'face-G4blunt/H3//'])
        self.app.CommandExecute('+ 1s')
        self.assertEqual(['thorax-M1blunt/EE//393', 'groin-S2point/H5//393', 'abdomen-S3edge/H4//393', 'face-G4blunt/H3//393'], ch.GetList('wounds'))
        deity.test.TestUserResponse('physkill', '34')
        self.gs.rand.testset('healcheck', (1, 2, 3))
        self.app.CommandExecute('+ 5d')
        self.gs.rand.testend()
        self.assertEqual(['groin-S1point/H5//398', 'abdomen-S2edge/H4//398', 'face-G3blunt/H3//398'], ch.GetList('wounds'))

    def test_200_alltest(self):
        self.app.CommandExecute('alltest awareness')

    def test_210_test(self):
        self.app.CommandExecute('display superman')
        self.app.CommandExecute('test initiative')
        self.app.CommandExecute('test initiative +5 77')
        self.assertRaises(BadParameter, self.app.CommandExecute, 'test initiative +5 77 99')
        self.assertRaises(BadParameter, self.app.CommandExecute, 'test dagger')
        deity.test.TestOutputClear()
        self.app.CommandExecute('force test dagger')
        self.assertIn('opened skill', deity.test.TestOutputGet())

    def test_300_treat(self):
        self.ch1.SetList('wounds', ['thorax-M1blunt/EE//', 'groin-S2point', 'abdomen-S3edge', 'face-G4blunt'])
        self.ch2.SetSkill('physician', 60)
        self.app.CommandExecute('display spiderman')
        self.app.CommandExecute('treat superman abdomen +10 19')
        self.assertEqual(['thorax-M1blunt/EE//', 'groin-S2point', 'abdomen-S3edge/H5//', 'face-G4blunt'], self.ch1.GetList('wounds'))


###############################################################################


if __name__ == '__main__':
    unittest.main()
