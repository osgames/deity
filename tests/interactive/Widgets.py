#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test common dialogs)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import re

import deity
import deity.test
import deity.widgets

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()


###############################################################################


class QueryBoxTest(unittest.TestCase):

    def test_98_fit_title(self):
        w = deity.widgets.QueryBox("See all title?",
                                   title="Can you see this entire title with question-mark?")
        self.assertEqual(True, w.Run())


###############################################################################


class ChooseBoxTest(unittest.TestCase):

    def test_98_fit_title(self):
        w = deity.widgets.ChooseBox("See all title?",
                                    xlist(("yes", "no")), title="Can you see this entire title with question-mark?")
        self.assertEqual("yes", w.Run())

    def test_99_escape(self):
        w = deity.widgets.ChooseBox("Press <ESC> to close this.",
                                    xlist(("not working",)), title="ESC to Exit")
        self.assertEqual(None, w.Run())


###############################################################################


class InputBoxTest(unittest.TestCase):

    def test_10_simple(self):
        w = deity.widgets.InputBox("Can you see this? (y/n)", title="simple")
        self.assertEqual('y', w.Run())

    def test_20_multiline(self):
        w = deity.widgets.InputBox("Multiline message... Can you see all of this extended text message that, by necessity, must wrap multiple lines? (y/n)",
                                   title="multiline")
        self.assertEqual('y', w.Run())

    def test_30_validator(self):
        w = deity.widgets.InputBox("Does the box below have keyboard focus by default and does it reject everything but a simple ''y'' or ''n'' response?",
                                   title="validate", validre=re.compile(r'^(y|n)$'))
        self.assertEqual('y', w.Run())

    def test_98_fit_title(self):
        w = deity.widgets.InputBox("See all title? (y/n)",
                                   title="Can you see this entire title with question-mark?")
        self.assertEqual('y', w.Run())

    def test_99_escape(self):
        w = deity.widgets.InputBox("Press <ESC> to close this.  Press <ENTER> if that doesn't work.", title="ESC to Exit")
        self.assertEqual(None, w.Run())


###############################################################################


class SelectBoxTest(unittest.TestCase):

    def test_98_fit_title(self):
        w = deity.widgets.SelectBox("See all title?",
                                    xlist(("yes", "no")),
                                    title="Can you see this entire title with question-mark?")
        self.assertEqual("yes", w.Run())

    def test_99_escape(self):
        w = deity.widgets.SelectBox("Press <ESC> to close this.",
                                    xlist(("click here", "if ESC", "does not work")),
                                    title="ESC to Exit")
        self.assertEqual(None, w.Run())


###############################################################################


if __name__ == '__main__':
    unittest.main()
