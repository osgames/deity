#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test common dialogs)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.dialogs

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')

kOptions = {
    'SomethingOne': {
        'category':	'cat1',
        'order':	10,
        'rule':		'SomethingOne',
        'default':	False,
        'short':	'Something One',
        'long':		'This is the first something.',
    },
    'SomethingTwo': {
        'category':	'cat1',
        'order':	20,
        'rule':		'SomethingTwo',
        'default':	True,
        'short':	'Something Two',
        'long':		'This is the second something.',
    },
    'SomethingThree': {
        'category':	'cat1',
        'order':	30,
        'rule':		'SomethingThree',
        'default':	False,
        'short':	'Something Three',
        'long':		'This is the third something.',
    },
    'SomethingFour': {
        'category':	'cat1',
        'order':	40,
        'rule':		'SomethingFour',
        'default':	True,
        'short':	'Something Four',
        'long':		'This is the fourth something.',
    },
    'SomethingFive': {
        'category':	'cat1',
        'order':	50,
        'rule':		'SomethingFive',
        'default':	True,
        'short':	'Something Five',
        'long':		'This is the fifth something.',
    },
    'SomethingElse': {
        'category':	'cat1',
        'order':	55,
        'rule':		'SomethingElse',
        'options':	{'1':NP_('option:abc'), '2':NP_('option:ijk'), '3':NP_('option:wxyz')},
        'default':	'2',
        'short':	'Something Else',
        'long':		'This is the else something.',
    },
    'SomethingOther': {
        'category':	'cat1',
        'order':	56,
        'rule':		'SomethingOther',
        'options':	{'1':NP_('option:123'), '2':NP_('option:456'), '3':NP_('option:789')},
        'default':	'1',
        'short':	'Something Other',
        'long':		'This is the other something.',
    },
    'SomethingSix': {
        'category':	'cat1',
        'order':	70,
        'rule':		'SomethingSix',
        'default':	True,
        'short':	'Something Six',
        'long':		'This is the sixth something.',
    },
    'SomethingAgain': {
        'category':	'cat1',
        'order':	75,
        'rule':		'SomethingAgain',
        'options':	{'hello':NP_('option:Hello, World!'), 'goodbye':NP_('option:Goodbye, World!'), 'back':NP_('option:Welcome back!')},
        'default':	'hello',
        'short':	'Something Again',
        'long':		'This is the again something.',
    },

    'AnotherOne': {
        'category':	'cat2',
        'order':	30,
        'rule':		'AnotherOne',
        'default':	False,
        'short':	'Another One',
        'long':		'This is the first other.',
    },
    'AnotherTwo': {
        'category':	'cat2',
        'order':	20,
        'rule':		'AnotherTwo',
        'default':	False,
        'short':	'Another Two',
        'long':		'This is the second other.',
    },

    'ChooseOne': {
        'category':	'aaa',
        'order':	50,
        'rule':		'ChooseOne',
        'default':	True,
        'short':	'Chose One',
        'long':		'This is the first choice.',
    },
}

kCatOrder   = ['cat1', 'cat2']


###############################################################################


class ChooseOptionsTest(unittest.TestCase):

    def test_10_init(self):
        c = {'SomethingOne':'yes', 'SomethingTwo':'no', 'AnotherOne':'', 'SomethingElse':'3', 'SomethingAgain':'halt'}
        d = deity.dialogs.ChooseOptionsDialog(c, kCatOrder, kOptions, columns=3, allowcancel=False, shownew=True, title="Test Dialog")
        self.assertEqual(['cat1', 'cat2', 'aaa'], d.categoryOrder)
        self.assertEqual(10,len(d.layout))
        self.assertEqual(4, len(d.layout[0]))
        self.assertEqual(5, len(d.layout[1]))
        self.assertEqual(1, len(d.layout[2]))
        self.assertEqual(1, len(d.layout[3]))
        self.assertEqual(1, len(d.layout[4]))
        self.assertEqual(1, len(d.layout[5]))
        self.assertEqual(4, len(d.layout[6]))
        self.assertEqual(2, len(d.layout[7]))
        self.assertEqual(3, len(d.layout[8]))
        self.assertEqual(1, len(d.layout[9]))
        r = d.Run()
        print r
        print c


###############################################################################


if __name__ == '__main__':
    unittest.main()
