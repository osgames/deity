#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (run full combat)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.trandom
import deity.windows
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *

import __builtin__
import wx
App  = wx.PySimpleApp()
#Game = deity.gamesystem.harnmaster.HarnMaster3()
Main = deity.windows.Main(testmode=True);
Main.testmode = True
Game = Main.gs
Game.rules['CombatFumble'] = 'no'
__builtin__.__dict__['Output'] = Main.Output


###############################################################################


class CombatTest_1_Ranged(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        Main.CommandExecute('combat')

        # fighter #1
        try:
            self.ct1a = GAME.MatchCharacterName('ct1a')
            GAME.SubCharacter(self.ct1a)
        except:
            pass
        self.ct1a = GAME.Character(None)
        self.ct1a.Generate('human-fighter')
        self.ct1a.Rename('ct1a')
        GAME.AddCharacter(self.ct1a, 'encounter')
        item = GAME.Item(None)
        item.Generate('longbow')
        self.ct1a.AddInventory(item)
        self.ct1a.WieldItem(item, defindex=0)
        self.ct1a.SetSkill('bows', 60)

        # fighter #2
        try:
            self.ct1b = GAME.MatchCharacterName('ct1b')
            GAME.SubCharacter(self.ct1b)
        except:
            pass
        self.ct1b = GAME.Character(None)
        self.ct1b.Generate('human-fighter')
        self.ct1b.Rename('ct1b')
        GAME.AddCharacter(self.ct1b, 'encounter')
        item = GAME.Item(None)
        item.Generate('sling')
        self.ct1b.AddInventory(item)
        self.ct1b.WieldItem(item, defindex=0)
        item = GAME.Item(None)
        item.Generate('roundshield')
        self.ct1b.AddInventory(item)
        self.ct1b.WieldItem(item, defindex=1)
        self.ct1b.SetSkill('slings', 80)

        # update
        Main.DisplayRefresh()

    def test_round(self):
        self.random = random = deity.trandom.Random()
        Main.CommandExecute('add ct1a')
        Main.CommandExecute('add ct1b')
        #print Game.rules

        while (True):
            #print 'begin round...',self.ct1a.GetInfo('status'),self.ct1b.GetInfo('status')
            if self.ct1a.GetInfo('status') == 'dead' or self.ct1b.GetInfo('status') == 'dead':
                break

            Main.CommandExecute('round')

            while Main.modeCombat.submode == 'round':
                attack = Main.curchar.Get()
                if attack == self.ct1a:
                    defend = self.ct1b
                else:
                    defend = self.ct1a

                afrom  = ''
                dfrom  = ''
                aloc   = ''
                dloc   = ''
                if random.randint(0,2) == 0:
                    dmove = 'dodge'
                else:
                    dmove = 'block'
                rval = random.randint(1,16)
                if rval < 2:
                    arange = '@extreme'
                elif rval < 4:
                    arange = '@long'
                elif rval < 8:
                    arange = '@medium'
                elif rval < 12:
                    arange = '@short'
                else:
                    arange = ''

                # attack
                aname = attack.Name()
                dname = defend.Name()
                #print
                #print attack.ShortDesc(),attack.GetInfo('status'),'vs',defend.ShortDesc(),defend.GetInfo('status'),arange

                deity.test.TestUserResponse('kill?', True)
                deity.test.TestUserResponse('amputate?', True)
                deity.test.TestUserResponse('bodyblock?', True)
                if attack.GetInfo('status') == 'unconscious' or attack.GetInfo('status') == 'shock' or attack.GetInfo('status') == 'dead':
                    Main.CommandExecute('pass')
                elif attack.GetInfo('status') == 'prone':
                    Main.CommandExecute('stand')
                elif defend.GetInfo('status') == 'unconscious' or defend.GetInfo('status') == 'shock':
                    deity.test.TestUserResponse('attackprone?', True)
                    Main.CommandExecute('missile %(dname)s %(arange)s %(aloc)s defend ignore'
                                        % {'dname':dname, 'aloc':aloc, 'arange':arange})
                elif defend.GetInfo('status') == 'prone':
                    deity.test.TestUserResponse('attackprone?', True)
                    Main.CommandExecute('missile %(dname)s %(arange)s %(aloc)s defend dodge'
                                        % {'dname':dname, 'aloc':aloc, 'arange':arange})
                elif defend.GetInfo('status') == 'dead': # bled out
                    Main.CommandExecute('pass')
                else:
                    #print('%(atype)s: attack %(dtype)s %(arange)s %(aloc)s defend %(dmove)s %(dloc)s' %
                    #      {'aname':aname, 'dname':dname, 'arange':arange, 'aloc':aloc, 'dmove':dmove, 'dloc':dloc,
                    #       'atype':attack.ShortDesc(), 'dtype':defend.ShortDesc()})
                    Main.CommandExecute('missile %(dname)s %(arange)s %(aloc)s defend %(dmove)s' %
                                        {'dname':dname, 'arange':arange, 'aloc':aloc, 'dmove':dmove, 'dloc':dloc})
        #App.MainLoop()


###############################################################################


if __name__ == '__main__':
    unittest.main()
