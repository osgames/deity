#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test "Watcher" class)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.watcher


###############################################################################


class WatcherTest(deity.test.TestCase):

    def test_all(self):
        watched = deity.watcher.Watched('foo')
        self.assertEqual('foo', watched.Get())
        self.assertTrue(watched.IsChanged(self))
        self.assertFalse(watched.IsChanged(self))
        watched.Set('bar')
        self.assertEqual('bar', watched.Get())
        self.assertTrue(watched.IsChanged(self))
        self.assertFalse(watched.IsChanged(self))
        watched.Alter()
        self.assertTrue(watched.IsChanged(self))
        self.assertFalse(watched.IsChanged(self))
        self.assertEqual('bar', watched.Get())
        
    

###############################################################################


if __name__ == '__main__':
    unittest.main()
