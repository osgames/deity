#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test char-edit mode)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.modes
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class ChareditTest1(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.app = deity.test.App()
        self.mw = deity.modes.Charedit(Test_Win, self.app)
        self.mw.DisplayRefresh()
        self.mw.Activate()

        self.ch = deity.gamesystem.GameSystem.Character(None)
        self.ch.Generate(xstr('_testhuman'), 'superman')
        self.ch.MakeUnique()
        self.assertNotEqual(None, self.ch.tid)
        self.gs.AddCharacter(self.ch, 'players')

    def test_000_init(self):
        self.assertEqual('players', self.ch.GetValue('x:gamegroup'))

    def test_100_display(self):
        self.app.CommandExecute('display superman')
        self.mw.DisplayRefresh()

    def test_110_editskill(self):
        self.ch.SetSkill('initiative', 55)
        self.app.CommandExecute('display superman')
        self.mw.DisplayRefresh()
        self.assertTrue('initiative' in self.mw.charEdit.curskills.keywidgetmap)

    def test_200_DoGroup(self):
        self.app.CommandExecute('display superman')
        self.app.CommandExecute('group encounter')
        self.assertEqual('encounter', self.ch.GetValue('x:gamegroup'))
        self.app.curchar.Set(self.ch)
        self.app.CommandExecute('group players')
        self.assertEqual('players', self.ch.GetValue('x:gamegroup'))

    def test_201_DoGroup(self):
        self.app.CommandExecute('display superman')
        self.app.CommandExecute('force group newgroup')
        self.assertEqual('newgroup', self.ch.GetValue('x:gamegroup'))
        self.app.curchar.Set(self.ch)
        self.app.CommandExecute('group players')
        self.assertEqual('players', self.ch.GetValue('x:gamegroup'))

    def test_210_DoRename(self):
        self.app.CommandExecute('display superman')
        self.app.CommandExecute('rename batman')
        self.assertEqual('batman', self.ch.Name())
        self.app.curchar.Set(self.ch)
        self.app.CommandExecute('rename spiderman')
        self.assertEqual('spiderman', self.ch.Name())


###############################################################################


if __name__ == '__main__':
    unittest.main()
