#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test "exception" classes)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.exceptions



###############################################################################


class DeityException_Test(deity.test.TestCase):

    def test_10_init(self):
        de = deity.exceptions.DeityException('testing...%(nums)s' % {'nums':'123'})
        self.assertEqual('testing...123', str(de))

    def test_20_multi(self):
        de = deity.exceptions.DeityException('a', 'b', 'c')
#        self.assertEqual(3, len(de))
        self.assertEqual('a', de[0])
        self.assertEqual('b', de[1])
        self.assertEqual('c', de[2])
        

###############################################################################


class NotPossible_Test(deity.test.TestCase):

    def test_10_init(self):
        de = deity.exceptions.NotPossible('no-way...%(nums)s' % {'nums':'123'})
        self.assertEqual('no-way...123', str(de))


###############################################################################


if __name__ == '__main__':
    unittest.main()
