#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test gamesystem module)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import wx
import unittest
from test import test_support

import deity
import deity.test
import deity.gamesystem
import deity.watcher

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class GameSystem_00_Test(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.GameSystem()

    def test_100_init(self):
        self.assertEqual('GameSystem', self.gs.name)
        self.assertEqual(11, self.gs.kCharacterStatesStringMax) # unconscious
        self.assertEqual(0, len(self.gs.gameCharDisplay))
        self.assertEqual(self.gs.kCharacterGroupsTid, self.gs.charGroupThing.tid)
        self.assertEqual(3, len(self.gs.charGroupThing.GetInventory()))
        cg = self.gs.charGroupThing.GetInventory()
        self.assertEqual('players', cg[0].Name())
        self.assertEqual('generated', cg[1].Name())
        self.assertEqual('encounter', cg[2].Name())

    def test_110_xlate(self):
        self.assertNotEqual('', self.gs.xlate)
        self.assertNotEqual(None, self.gs.xpart)
        xs = xstr('foo')
        xg = self.gs.xstr('foo')
        self.assertTrue(xg.xlate)
        self.assertNotEqual(None, xg.xpart)
        self.assertNotEqual(xs.xlate, xg.xlate)
        self.assertNotEqual(xs.xpart, xg.xpart)
        xs = xlist(['foo'])
        xg = self.gs.xstr(['foo'])
        self.assertTrue(xg.xlate)
        self.assertNotEqual(None, xg.xpart)
        self.assertNotEqual(xs.xlate, xg.xlate)
        self.assertNotEqual(xs.xpart, xg.xpart)

    def test_150_MatchCharacterGroup(self):
        self.assertEqual('encounter', self.gs.MatchCharacterGroup('e'))
        self.assertRaises(BadParameter, self.gs.MatchCharacterGroup, 'z')

    def test_200_Partition(self):
        v = xstr('a', xlate='test:', xpart=self.gs.xpart)
        self.assertEqual('B', v.xval())
        v = xstr('c(1)', xlate='test:', xpart=self.gs.xpart)
        self.assertEqual('D(1)', v.xval())
        v = xstr('ee(1,2,3)', xlate='test:', xpart=self.gs.xpart)
        self.assertEqual('FF(1,2,3)', v.xval())

    def test_300_XLookup(self):
        self.assertEqual('blunt', XLookup(self.gs.kWeaponAspects, 'blunt', 'something'))
        self.assertEqual([], XLookup(self.gs.kWeaponAspects, 'bluntx', 'something', multiple=True))
        self.assertEqual('blunt', XLookup(self.gs.kWeaponAspects, 'bl', 'something'))

    def test_310_RuleCheck(self):
        self.gs.rules['a'] = ''
        self.gs.rules['c'] = 'yes'
        self.gs.rules['d'] = 'no'
        self.gs.rules['e'] = 'hello'
        self.assertTrue(self.gs.RuleCheck('a'))
        self.assertFalse(self.gs.RuleCheck('b'))
        self.assertTrue(self.gs.RuleCheck('c'))
        self.assertFalse(self.gs.RuleCheck('d'))
        self.assertTrue(self.gs.RuleCheck('e','hello'))
        self.assertFalse(self.gs.RuleCheck('f','world'))

    def test_400_UserRoll(self):
        self.assertEqual(42, self.gs.UserRoll(None, 42))
        self.assertEqual(43, self.gs.UserRoll(None, '43'))
        self.assertEqual(44, self.gs.UserRoll(None, None, default='44'))
        self.gs.rand.testset('dice', (1,2,4))
        self.assertEqual( 7, self.gs.UserRoll(None, '3d6', test='dice'))
        self.gs.rand.testend()
        self.assertRaises(BadParameter, self.gs.UserRoll, None, 'abc')

    def test_410_UserCheck(self):
        self.assertEqual(True, self.gs.UserCheck(None, 5, '<', 6))
        self.assertEqual(False, self.gs.UserCheck(None, '5', '<', 5))
        self.assertEqual(True, self.gs.UserCheck(None, 5, '<=', '5'))
        self.assertEqual(False, self.gs.UserCheck(None, '5', '>', '6'))
        self.assertEqual(False, self.gs.UserCheck(None, 5, '>', 5))
        self.assertEqual(True, self.gs.UserCheck(None, 5, '>=', 5))

    def test_420_UserSelect(self):
        choices = xlist(('1=a', '10=bb', '100=ccc', '1000=dddd'))
        self.assertEqual('a', self.gs.UserSelect(None, choices, 1))
        self.assertEqual('bb', self.gs.UserSelect(None, choices, 10))
        self.assertEqual('ccc', self.gs.UserSelect(None, choices, 99))
        self.assertEqual('dddd', self.gs.UserSelect(None, choices, 888))
        self.assertEqual('a', self.gs.UserSelect(None, choices, '1d1'))
        self.assertEqual('bb', self.gs.UserSelect(None, choices, '2d4'))
        self.assertEqual('ccc', self.gs.UserSelect(None, choices, '11d6'))
        self.assertEqual('dddd', self.gs.UserSelect(None, choices, '5d6+100'))


###############################################################################


class GameSystem_01_ValidateModifier_Test(deity.test.TestCase):

    class ValueParent(object):

        def __init__(self, value):
            self.value = value

        def GetValue(self):
            return self.value;

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.vm = deity.gamesystem.GameSystem.ValidateModifier().Validate

    def validate(self, value):
        vp = self.ValueParent(value)
        return self.vm(vp)

    def test_10_true(self):
        self.assertTrue(self.validate('0'))
        self.assertTrue(self.validate('1'))
        self.assertTrue(self.validate('+10'))
        self.assertTrue(self.validate('-9.9'))

    def test_20_false(self):
        self.assertFalse(self.validate('abc'))
        self.assertFalse(self.validate(''))
        self.assertFalse(self.validate('1.2.3'))


###############################################################################


class GameSystem_10_Thing_Test(deity.test.TestCase):
    kTestCharTid = 'Thing-910'

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.GameSystem()

    def test_100_init_none(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        self.assertEqual(None, th.tid)
        self.assertEqual('', th.Name())
        self.assertEqual('<>', repr(th))
        self.assertRaises(KeyError,  th.GetValue, 'a')
        self.assertRaises(KeyError,  th.GetValue, 't:')
        self.assertRaises(KeyError,  th.GetValue, 't:a', 'b', 'x:')
        self.assertRaises(KeyError,  th.SetValue, 'a', 'b')
        self.assertRaises(KeyError,  th.SetValue, 't:', 'b')
        self.assertRaises(KeyError,  th.SetValue, 't:a', 'b', 'x:')
        th.SetValue('t:a', 'b')
        self.assertNotEqual(None, th.tid)
        self.assertEqual('b', th.GetValue('t:a', ''))

    def test_110_GdbKey(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        self.assertEqual('/Thing/_Thing/info', th._GdbInfoKey('info'))
        self.assertEqual('/%s/someval'%self.kTestCharTid, th._GdbKey('someval'))

    def test_120_GetBdbInfo(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        th.kBdbPrefix = 'Beast'
        self.assertEqual(None, th.GetBdbInfo('_testbase', 'nonexistent'))
        self.assertEqual('none', th.GetBdbInfo('_testbase', 'nonexistent', default='none'))
        self.assertEqual(type(str()), type(th.GetBdbInfo('_testbase', 'Image')))

    def test_150_GenerateInvExp(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        for _ in xrange(1000):
            val = th._GenerateInvExp(10, 20, 1.1)
            self.assertTrue(10 <= int(val) <= 20)
        self.assertRaises(DatafillError, th._GenerateInvExp, 10, 20, 1.0)

    def test_151_re_inverseexponential(self):
        m = deity.gamesystem.GameSystem.Thing.kGenerateIExpRe.match('[35/1.1]')
        self.assertTrue(m)
        self.assertEqual(('35','1.1'), m.groups())

    def test_160_GenerateRandom(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        for _ in xrange(100):
            val = th._GenerateRandom('1d5', '1=a', '2=b', '3=c', '4=d', '5=e')
            self.assertIn(val, 'abcde')
        self.assertRaises(DatafillError, th._GenerateRandom, '1=a')
        self.assertRaises(DatafillError, th._GenerateRandom, 'foo', 'bar')
        self.assertRaises(DatafillError, th._GenerateRandom, '6', '1=a', '2=b', '3=c')

    def test_170_GenerateNormalDistribution(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        for _ in xrange(100):
            val = th._GenerateNormalDistribution('10')
            self.assertTrue(-10 <= int(val) <= 10)
            val = th._GenerateNormalDistribution('1.0', sigma='0.5')
            self.assertTrue(-1.0 <= float(val) <= 1.0)
        self.assertRaises(DatafillError, th._GenerateNormalDistribution, 'foo')

    def test_171_GenerateValueDistribution(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        for _ in xrange(100):
            val = th._GenerateValueDistribution('100', '30')
            self.assertTrue(70 <= eval(val) <= 130)
            val = th._GenerateValueDistribution('1000', '10', '50', sigma='0.8')
            self.assertTrue(900 <= eval(val) <= 1500)
        self.assertRaises(DatafillError, th._GenerateValueDistribution, '1.0', '10')
        self.assertRaises(DatafillError, th._GenerateValueDistribution, '100', '1.5')

    def test_190_GenerateIf(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        self.assertEqual('a', th._GenerateIf('0==0', 'a', 'b'))
        self.assertEqual('b', th._GenerateIf('1==0', 'a', 'b'))
        self.assertEqual('a', th._GenerateIf('0==0', 'a', '0==1', 'b', 'c'))
        self.assertEqual('b', th._GenerateIf('1==0', 'a', '1==1', 'b', 'c'))
        self.assertEqual('c', th._GenerateIf('1==0', 'a', '2==1', 'b', 'c'))
        self.assertEqual('a', th._GenerateIf('0==0', 'a', '0==1', 'b', '0==2', 'c', 'd'))
        self.assertEqual('b', th._GenerateIf('1==0', 'a', '1==1', 'b', '1==2', 'c', 'd'))
        self.assertEqual('c', th._GenerateIf('2==0', 'a', '2==1', 'b', '2==2', 'c', 'd'))
        self.assertEqual('d', th._GenerateIf('3==0', 'a', '3==1', 'b', '3==2', 'c', 'd'))
        self.assertRaises(DatafillError, th._GenerateIf, '0==0')
        self.assertRaises(DatafillError, th._GenerateIf, '0==9', 'a')
        self.assertRaises(DatafillError, th._GenerateIf, '0==9', 'a', 'b', 'c')
        self.assertRaises(DatafillError, th._GenerateIf, '0==9', 'a', '1==9', 'b')

    def test_199_Function_notfound(self):
        th = deity.gamesystem.GameSystem.Thing(None)
        fn = '@NotFound(1, 2, 3)'
        match = th.kGenerateFuncRe.match(fn)
        self.assertTrue(match)
        self.assertRaises(DatafillError, th._GenerateFunc, match)

    def test_200_GetValue_10(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        self.assertEqual(None, th.GetValue('t:nonexistent'))
        self.assertEqual('no', th.GetValue('t:nonexistent', 'no'))

    def test_200_GetValue_20(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        GDB[th._GdbKey('t:zero')] = '0'
        self.assertEqual('0', th.GetValue('t:zero'))

    def test_210_SetValue_10(self):
        tw = deity.watcher.Watched(0)
        self.assertTrue(tw.IsChanged(self))
        self.assertFalse(tw.IsChanged(self))
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid, watcher=tw)
        self.assertFalse(tw.IsChanged(self))
        self.assertEqual(None, th.GetValue('t:one'))
        self.assertFalse(tw.IsChanged(self))
        th.SetValue('t:one', '1')
        self.assertTrue(tw.IsChanged(self))
        self.assertEqual('1', th.GetValue('t:one'))
        self.assertFalse(tw.IsChanged(self))
        th.SetValue('t:one', None)
        self.assertTrue(tw.IsChanged(self))
        self.assertEqual(None, th.GetValue('t:one'))
        self.assertFalse(tw.IsChanged(self))

    def test_210_SetValue_20(self):
        tw = deity.watcher.Watched(0)
        self.assertTrue(tw.IsChanged(self))
        self.assertFalse(tw.IsChanged(self))
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid, tempobj=True)
        th.WatchedBy(tw)
        self.assertEqual(th, tw.Get())
        tw.IsChanged(self)
        self.assertFalse(tw.IsChanged(self))
        th.WatchedBy(tw)
        self.assertFalse(tw.IsChanged(self))
        self.assertEqual(None, th.GetValue('two', type='t:'))
        self.assertFalse(tw.IsChanged(self))
        th.SetValue('two', '2', type='t:')
        self.assertTrue(tw.IsChanged(self))
        self.assertEqual('2', th.GetValue('two', type='t:'))
        self.assertEqual('2', th.GetValue('t:two'))
        self.assertFalse(tw.IsChanged(self))

    def test_210_SetValue_30(self):
        tc = dict()
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid, cache=tc)
        tv = {'t:preset1':'a', 't:preset2':'b', 't:preset3':'c'}
        self.assertTrue(th._GdbKey('t:preset1') not in GDB)
        self.assertEqual('a', th.GetValue('t:preset1', values=tv))
        th.SetValue('t:preset1', 'z', values=tv)
        self.assertEqual('z', th.GetValue('t:preset1', values=tv))
        self.assertEqual('z', th.GetValue('t:preset1'))
        self.assertTrue('t:preset1' in tv)
        self.assertEqual('z', tv['t:preset1'])
        self.assertTrue('t:preset1' in tc)
        self.assertEqual('z', tc['t:preset1'])
        self.assertTrue(th._GdbKey('t:preset1') in GDB)
        self.assertEqual('z', GDB[th._GdbKey('t:preset1')])

    def test_220_GetValueList(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid, tempobj=True)
        self.assertEqual(None, th.GetValueList('t:nonexistent'))
        self.assertEqual(['a','b','c'], th.GetValueList('t:nonexistent', ['a','b','c']))
        th.SetValue('t:empty', '')
        self.assertEqual([], th.GetValueList('t:empty'))
        th.SetValue('t:oneitem', '1')
        self.assertEqual(['1'], th.GetValueList('t:oneitem'))
        th.SetValue('t:twoitems', '1%s2'%th.kListSeparator)
        self.assertEqual(['1','2'], th.GetValueList('t:twoitems'))

    def test_230_SetValueList(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        xl = xlist()
        xl.extend('123')
        th.SetValueList('t:threeitems', xl)
        self.assertEqual(['1','2','3'], th.GetValueList('t:threeitems'))

    def test_240_GetValueMap(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        self.assertEqual(None, th.GetValueMap('t:nonexistent'))
        th.SetValue('t:empty', '')
        self.assertEqual({}, th.GetValueMap('t:empty'))
        th.SetValue('t:oneitem', '1=a')
        self.assertEqual({'1':'a'}, th.GetValueMap('t:oneitem'))
        th.SetValue('t:twoitems', '2=b%s3=c'%th.kListSeparator)
        self.assertEqual({'2':'b','3':'c'}, th.GetValueMap('t:twoitems'))

    def test_250_SetValueMap(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        xd = dict()
        xd['x']='1'; xd['y']='2'; xd['z']='3';
        th.SetValueMap('t:threeitems', xd)
        self.assertEqual({'x':'1','y':'2','z':'3'}, th.GetValueMap('t:threeitems'))

    def test_260_GetAttribute(self):
        th = deity.gamesystem.GameSystem.Thing(self.kTestCharTid)
        th.SetAttribute('something', 99)
        self.assertEqual(99, th.GetAttribute('something'))
        self.assertEqual('99', th.GetValue('a:something'))
        th.SetValue('a:something', '88')
        self.assertEqual(88, th.GetAttribute('something'))
        th.SetValue('a:something', '')
        self.assertEqual(None, th.GetAttribute('something'))

    def test_300_Generate_base(self):
        deity.gamesystem.GameSystem.Thing.kBdbPrefix = 'Beast'
        th = deity.gamesystem.GameSystem.Thing(None)
        self.assertRaises(DatafillError, th.Generate, 'nothing')
        th.Generate(xstr('_testbase(quality=+2)'))
        self.assertEqual(None, th.GetValue('x:gamegroup'))
        self.assertEqual((None,None), th.GetPossessor())
        self.assertTrue(th.IsA('_TestBase'))
        self.assertTrue(th.IsA('_testbase'))
        self.assertFalse(th.IsA('foobar'))
        # verify standard values
        self.assertEqual('testbase(quality=+2)', th.GetInfo('name'))
        self.assertEqual('_testbase(quality=+2)', th.GetValue('x:type'))
        self.assertEqual('_testbase', th.GetValue('x:realtype'))
        self.assertEqual('_testbase', th.GetValue('x:types'))
        self.assertEqual('testbase', th.Name())
        self.assertEqual('testbase(quality=+2)', th.ShortDesc())
        # verify string generation
        self.assertEqual('', th.GetInfo('emptykey'))
        #self.assertEqual('"', th.GetInfo('quotekey'))
        self.assertEqual('allbase', th.GetInfo('allkey'))
        self.assertEqual('gsbase', th.GetInfo('gskey'))
        self.assertEqual('gsbase', th.GetInfo('somekey'))
        # verify list generation
        self.assertEqual(['allbase','gsbase'], th.GetValueList('l:somekey'))
        self.assertEqual(['allchoicebase','gschoicebase'], th.GetValueList('l:samechoicekey'))
        self.assertEqual(['allmult1','allmult2'], th.GetValueList('l:multkey'))
        tl = th.GetValueList('l:diffchoicekey')
        self.assertEqual(2, len(tl))
        self.assertTrue(re.match(r'^allchoicebase[12]$', tl[0]))
        self.assertTrue(re.match(r'^gschoicebase[12]$', tl[1]))
        # verify number generation
        self.assertEqual( 1, th.GetAttribute('allkey'))
        self.assertEqual( 6, th.GetAttribute('somekey'))
        self.assertEqual(10, th.GetAttribute('addkey'))
        self.assertEqual(32, th.GetAttribute('multkey'))
        self.assertEqual( 9, th.GetAttribute('initkey'))
        self.assertEqual( 7, th.GetAttribute('alleqkey'))
        self.assertEqual(11, th.GetAttribute('addeqkey'))
        self.assertEqual(54, th.GetAttribute('multeqkey'))
        self.assertEqual( 6, th.GetAttribute('addinitkey'))
        self.assertEqual(16, th.GetAttribute('substkey'))
        # verify random amounts
        self.assertTrue(90 <= th.GetAttribute('ndist10key') <= 110)
        self.assertTrue(70 <= th.GetAttribute('ndist30key') <= 130)
        self.assertTrue(50 <= th.GetAttribute('ndist50key') <= 150)
        self.assertTrue(0 <= th.GetAttribute('ndist100key') <= 200)
        self.assertTrue(90 <= th.GetAttribute('ndist01key') <= 110)
        self.assertTrue(70 <= th.GetAttribute('ndist03key') <= 130)
        self.assertTrue(50 <= th.GetAttribute('ndist05key') <= 150)
        self.assertTrue(90 <= th.GetAttribute('vdist10key') <= 110)
        self.assertTrue(80 <= th.GetAttribute('vdist30key') <= 130)
        self.assertTrue(70 <= th.GetAttribute('vdist50key') <= 150)
        self.assertTrue(0 <= th.GetAttribute('vdist100key') <= 200)
        self.assertTrue(1 <= th.GetAttribute('oned6key') <= 6)
        self.assertTrue(3 <= th.GetAttribute('threed6key') <= 18)
        self.assertTrue(3 <= th.GetAttribute('best3of9d6key') <= 18)
        self.assertTrue(3 <= th.GetAttribute('wrst3of9d6key') <= 18)
        self.assertTrue(0 <= th.GetAttribute('invexp1key') <= 1)
        self.assertTrue(1 <= th.GetAttribute('invexp2key') <= 2)
        self.assertTrue(2 <= th.GetAttribute('invexp3key') <= 4)
        self.assertTrue(3 <= th.GetAttribute('invexp4key') <= 6)
        self.assertTrue(4 <= th.GetAttribute('invexp5key') <= 8)
        self.assertTrue(5 <= th.GetAttribute('invexp25key') <= 25)
        # verify random values
        ts = th.GetInfo('sex')
        self.assertTrue(ts == 'male' or ts == 'female')
        self.assertTrue(56 <= int(th.GetInfo('height')) <= 78)
        ts = th.GetInfo('frame')
        self.assertTrue(ts == 'scant' or ts == 'light' or ts == 'medium' or ts == 'heavy' or ts == 'massive')
        self.assertTrue(80 <= float(th.GetInfo('weight')) <= 120)
        # verify references
        self.assertEqual(1, th.GetAttribute('val1'))
        self.assertEqual(6, th.GetAttribute('valadd'))

    def test_310_Generate_derived(self):
        deity.gamesystem.GameSystem.Thing.kBdbPrefix = 'Beast'
        th = deity.gamesystem.GameSystem.Thing(None)
        th.Generate(xstr('_testchar(str=+1)'))
        self.assertTrue(th.IsA('_testchar'))
        self.assertTrue(th.IsA('_testbase'))
        self.assertFalse(th.IsA('foobar'))
        # verify standard values
        self.assertEqual('testchar(str=+1)', th.GetInfo('name'))
        self.assertEqual('_testchar(str=+1)', th.GetValue('x:type'))
        self.assertEqual('_testchar', th.GetValue('x:realtype'))
        self.assertEqual('_testchar;_testbase', th.GetValue('x:types'))
        # verify string generation
        self.assertEqual('', th.GetInfo('emptykey'))
        #self.assertEqual('"', th.GetInfo('quotekey'))
        self.assertEqual('allbase', th.GetInfo('allkey'))
        self.assertEqual('gsbase', th.GetInfo('gskey'))
        self.assertEqual('gsbase', th.GetInfo('somekey'))
        # verify list generation
        self.assertEqual(['allbase','gsbase'], th.GetValueList('l:somekey'))
        self.assertEqual(['allchoicebase','gschoicebase'], th.GetValueList('l:samechoicekey'))
        tl = th.GetValueList('l:diffchoicekey')
        self.assertEqual(2, len(tl))
        self.assertTrue(re.match(r'^allchoicebase[12]$', tl[0]))
        self.assertTrue(re.match(r'^gschoicebase[12]$', tl[1]))
        # verify references
        self.assertEqual(11, th.GetAttribute('val1'))
        self.assertEqual(16, th.GetAttribute('valadd'))
        self.assertEqual(88, th.GetAttribute('addkey'))
        # verify "base" reset (test GS always has base==0)
        self.assertEqual(99, th.GetAttribute('testhavebase'))

    def test_320_Generate_mod(self):
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        self.assertEqual(10, it1.GetAttribute('quality'))
        self.assertEqual( 5, it1.GetAttribute('attack'))
        self.assertEqual( 0, it1.GetAttribute('defend'))
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testknife(attack+=1;quality-=3;defend=1)'))
        self.assertEqual( 7, it2.GetAttribute('quality'))
        self.assertEqual( 6, it2.GetAttribute('attack'))
        self.assertEqual( 1, it2.GetAttribute('defend'))

    def test_321_Generate_mod_default(self):
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testdagger'))
        self.assertEqual(11, it1.GetAttribute('quality'))
        self.assertEqual( 5, it1.GetAttribute('attack'))
        self.assertEqual( 5, it1.GetAttribute('defend'))
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testdagger(+2)'))
        self.assertEqual(13, it2.GetAttribute('quality'))
        self.assertEqual( 7, it2.GetAttribute('attack'))
        self.assertEqual( 7, it2.GetAttribute('defend'))

    def test_322_Generate_mod_skills(self):
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testring(+2;+10)'))
        self.assertEqual('+2', it1.GetInfo('protection'))
        self.assertEqual('+20', it1.GetValue('s:unarmed'))
        self.assertEqual('+10', it1.GetValue('s:dodge'))

    def test_322_Generate_mod_bad(self):
        it1 = deity.gamesystem.GameSystem.Item(None)
        self.assertRaises(BadParameter, it1.Generate, xstr('_testring(+2;+10;+99)'))
        it2 = deity.gamesystem.GameSystem.Item(None)
        self.assertRaises(DatafillError, it2.Generate, xstr('_testdagger(+1,-3)'))

    def test_330_Generate_requirements(self):
        deity.gamesystem.GameSystem.Thing.kBdbPrefix = 'Beast'
        ch1 = deity.gamesystem.GameSystem.Thing(None)
        ch1.Generate(xstr('_testchar(str=12)'))
        self.assertEqual('12', ch1.GetValue('a:str'))
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testaxe'), user=ch1)
        self.assertEqual('12', it1.GetValue('a:str'))
        self.assertEqual('>= 12', it1.GetValue('r:str'))
        self.assertEqual('8', it1.GetValue('a:blunt'))
        ch2 = deity.gamesystem.GameSystem.Thing(None)
        ch2.Generate(xstr('_testchar(str=18)'))
        self.assertEqual('18', ch2.GetValue('a:str'))
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testaxe'), user=ch2)
        self.assertEqual('18', it2.GetValue('a:str'))
        self.assertEqual('>= 18', it2.GetValue('r:str'))
        self.assertEqual('9', it2.GetValue('a:blunt'))
        ch3 = deity.gamesystem.GameSystem.Thing(None)
        ch3.Generate(xstr('_testchar(str=11)'))
        self.assertEqual('11', ch3.GetValue('a:str'))
        it3 = deity.gamesystem.GameSystem.Item(None)
        self.assertRaises(NotPossible, it3.Generate, xstr('_testaxe'), user=ch3)

    def test_330_Generate_again(self):
        deity.gamesystem.GameSystem.Thing.kBdbPrefix = 'Beast'
        ch = deity.gamesystem.GameSystem.Thing(None)
        ch.Generate(xstr('_testchar'))
        self.assertRaises(NameError, ch.Generate, xstr('_testchar'))

    def test_400_Inventory(self):
        th1 = deity.gamesystem.GameSystem.Thing(None)
        th1.SetValueList(th1.kInventoryKey, [])
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        self.assertEqual((None,None), it1.GetPossessor())
        self.assertEqual(None, it1.CheckPossessor(th1))
        th1.AddInventory(it1)
        self.assertEqual([it1.tid], th1.GetValueList(th1.kInventoryKey))
        self.assertEqual((th1,''), it1.GetPossessor())
        self.assertEqual('', it1.CheckPossessor(th1))
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testdagger'))
        th1.AddInventory(it2)
        self.assertEqual([it1.tid, it2.tid], th1.GetValueList(th1.kInventoryKey))
        its = th1.GetInventory()
        self.assertEqual(2, len(its))
        self.assertEqual(it1.tid, its[0].tid)
        self.assertEqual(it2.tid, its[1].tid)
        # search items
        self.assertEquals(it1, th1.SearchInventory(xstr('testknife')))
        self.assertEquals(it2, th1.SearchInventory(xstr('testdagger')))
        self.assertRaises(BadParameter, th1.SearchInventory, strx('blah'))  # unknown
        self.assertRaises(BadParameter, th1.SearchInventory, strx('test'))  # ambiguous
        it2.Rename('blahdeblah')
        self.assertEqual(it2, th1.SearchInventory(strx('blah')))
        self.assertEqual(it1, th1.SearchInventory(strx('test')))
        # can't share items
        th2 = deity.gamesystem.GameSystem.Thing(None)
        self.assertEqual(None, it1.CheckPossessor(th2))
        self.assertRaises(NotPossible, th2.AddInventory, it1)
        # remove items
        its = th1.GetInventory()
        th1.SubInventory(its[0])
        self.assertEqual([its[1].tid], th1.GetValueList(th1.kInventoryKey))
        th1.SubInventory(its[1])
        self.assertEqual([], th1.GetValueList(th1.kInventoryKey))
        self.assertEqual((None,None), it1.GetPossessor())
        self.assertRaises(NotPossible, th1.SubInventory, its[0])
        self.assertRaises(NotPossible, th1.SubInventory, its[1])


###############################################################################


class GameSystem_20_Item_Test(deity.test.TestCase):
    kTestCharTid = 'Thing-920'

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.GameSystem()

    def test_100_init_none(self):
        it = deity.gamesystem.GameSystem.Item(None)
        self.assertEqual(None, it.tid)
        self.assertEqual('', it.Name())
        self.assertRaises(KeyError,  it.GetValue, 'a')
        self.assertRaises(KeyError,  it.GetValue, 't:')
        self.assertRaises(KeyError,  it.GetValue, 't:a', 'b', 'x:')
        self.assertRaises(KeyError,  it.SetValue, 'a', 'b')
        self.assertRaises(KeyError,  it.GetValue, 't:', 'b')
        self.assertRaises(KeyError,  it.SetValue, 't:a', 'b', 'x:')

    def test_110_init_id(self):
        it = deity.gamesystem.GameSystem.Item(None)
        del GDB[it._GdbInfoKey('nextid')]
        self.assertEqual(None, it.tid)
        self.assertTrue(it._GdbInfoKey('nextid') not in GDB)
        it.Generate(xstr('_testknife'))
        self.assertTrue(it._GdbInfoKey('nextid') in GDB)
        self.assertEqual('101', GDB[it._GdbInfoKey('nextid')])
        self.assertEqual('Item-100', it.tid)
        self.assertEqual('testknife', it.Name())
        self.assertEqual('_testknife', it.GetValue('x:type'))
        self.assertEqual('_testknife', it.GetValue('x:realtype'))

    def test_119_set_id(self):
        it = deity.gamesystem.GameSystem.Item(None)
        GDB[it._GdbInfoKey('nextid')] = '201'
        self.assertTrue(it._GdbInfoKey('nextid') in GDB)

    def test_120_init_aka(self):
        it = deity.gamesystem.GameSystem.Item(None)
        self.assertEqual('201', GDB[it._GdbInfoKey('nextid')])
        it.Generate(xstr('_testblade'))
        self.assertEqual('202', GDB[it._GdbInfoKey('nextid')])
        self.assertEqual('Item-201', it.tid)
        self.assertEqual('testblade', it.Name())
        self.assertEqual('_testblade', it.GetValue('x:type'))
        self.assertEqual('_testknife', it.GetValue('x:realtype'))

    def test_200_Weight(self):
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate(xstr('_testknife'))
        kw = it.Weight(default=-1, lbs=False)
        self.assertTrue(kw > 0)
        lw = it.Weight(default=-1, lbs=True)
        self.assertTrue(lw > 0)
        self.assertAlmostEqual(lw, kw*2.2, places=1)

    def test_210_Weight_Inventory(self):
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate(xstr('_testknife'))
        kw  = it.Weight()
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        it.AddInventory(it1)
        self.assertEqual(it1.Weight(), it.Load())
        self.assertEqual(kw+it1.Weight(), it.Weight())
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testrobe'))
        it.AddInventory(it2)
        self.assertEqual(it1.Weight()+it2.Weight(), it.Load())
        self.assertEqual(kw+it1.Weight()+it2.Weight(), it.Weight())

    def test_300_Aspect(self):
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate('_testknife')
        self.assertEqual('point', it.Aspect())
        self.assertEqual('edge', it.Aspect('e'))
        self.assertRaises(BadParameter, it.Aspect, 'yeehaw')
        self.assertRaises(BadParameter, it.Aspect, 'blunt')

    def test_301_Aspect_none(self):
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate('_testrobe')
        self.assertRaises(DatafillError, it.Aspect)

    def test_310_Skill(self):
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate('_testrobe')
        self.assertRaises(DatafillError, it.Skill, None)


###############################################################################


class GameSystem_30_Character_Test(deity.test.TestCase):
    kTestCharTid = 'Char-99'

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.GameSystem()

    def test_100_init_none(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        self.assertEqual(None, ch.tid)
        self.assertEqual('', ch.Name())
        self.assertRaises(KeyError,  ch.GetValue, 'a')
        self.assertRaises(KeyError,  ch.GetValue, 't:')
        self.assertRaises(KeyError,  ch.GetValue, 't:a', 'b', 'x:')
        self.assertRaises(KeyError,  ch.SetValue, 'a', 'b')
        self.assertRaises(KeyError,  ch.GetValue, 't:', 'b')
        self.assertRaises(KeyError,  ch.SetValue, 't:a', 'b', 'x:')

    def test_110_init_char(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        GDB[ch._GdbInfoKey('nextid')] = '99'
        ch.Generate(xstr('_testhuman'))
        self.assertEqual(self.kTestCharTid, ch.tid)
        self.assertEqual('testhuman', ch.GetInfo('name'))
        self.assertEqual('testhuman', ch.Name())
        self.assertEqual('testhuman', ch.ShortDesc())
        ch.Rename('George')
        self.assertEqual('George', ch.GetInfo('name'))
        self.assertEqual('George', ch.Name())
        self.assertEqual('George[testhuman]', ch.ShortDesc())
        ch.Rename('George')
        self.assertEqual('George', ch.GetInfo('name'))
        c1 = deity.gamesystem.GameSystem.Character(None)
        c1.Generate(xstr('_testhuman'))
        #self.assertRaises(NotPossible, c1.Rename, 'george')
        #c2 = self.gs.GetCharacter('george')
        #self.assertEqual(ch.tid, c2.tid)
        #self.assertEqual(ch.cache, c2.cache)

    def test_200_GetAttribute(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual(None, ch.GetAttribute('nonexistent'))
        self.assertEqual(0, ch.GetAttribute('nonexistent', default=0))
        ch.SetValue('a:attr1', '1')
        self.assertEqual(1, ch.GetAttribute('attr1'))

    def test_210_SetAttribute(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetAttribute('attr2', 2)
        self.assertEqual(2, ch.GetAttribute('attr2'))

    def test_220_GetAttributes(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ao = xlist()
        ch.SetAttribute('attr3', 3)
        GAME.kBaseAttributes = ['attr1', 'attr3']
        GAME.kMoreAttributes = ['attr2']
        ca = ch.GetAttributes(ao)
        self.assertEqual(3, len(ca.keys()))
        self.assertEqual(['attr1', 'attr3', 'attr2'], ao)
        self.assertTrue('attr1' in ca)
        self.assertEqual(1, ca['attr1'])
        self.assertTrue('attr2' in ca)
        self.assertEqual(2, ca['attr2'])
        self.assertTrue('attr3' in ca)
        self.assertEqual(3, ca['attr3'])

    def test_230_GetSkill(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual(None, ch.GetSkill('nonexistent'))
        self.assertEqual(0, ch.GetSkill('nonexistent', default=0))
        self.assertTrue('s' not in ch.dbitems or 'skl1' not in ch.dbitems['s'])
        ch.SetValue('s:skl1', '1')
        self.assertEqual(1, ch.GetSkill('skl1'))
        self.assertTrue('skl1' in ch.dbitems['s'])
        ch.SetValue('s:skl1', 'x')  # invalid int
        self.assertEqual(None, ch.GetSkill('skl1'))

    def test_240_SetSkill(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertTrue('s' not in ch.dbitems or 'skl2' not in ch.dbitems['s'])
        ch.SetSkill('skl2', 2)
        self.assertEqual(2, ch.GetSkill('skl2'))
        self.assertTrue('skl2' in ch.dbitems['s'])

    def test_250_GetSkills(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        so = xlist()
        GAME.kSkillInfo = xdict({'skl1':'a/cat1 something',
                                 'skl2':'a/cat1 else',
                                 'skl3':'b/cat2 altogether',
                                 'unarmed':'b/cat3 missing'},
                                xlatekey='skill:')
        ch.SetSkill('s:skl1',1)
        ch.SetSkill('s:skl2',2)
        ch.SetSkill('s:unarmed',4)
        cs = ch.GetSkills(so)
        self.assertTrue(isinstance(cs, xdict))
        self.assertEqual('skill:', cs.xlatekey)
        self.assertEqual(None, cs.xpartkey)
        self.assertEqual(5, len(cs.keys()))
        self.assertEqual(['skl1', 'skl2', 'bite', 'foot', 'unarmed'], so)
        self.assertTrue('skl1' in cs)
        self.assertEqual(1, cs['skl1'])
        self.assertTrue('skl2' in cs)
        self.assertEqual(2, cs['skl2'])
        self.assertTrue('skl3' not in cs)
        self.assertTrue('unarmed' in cs)
        self.assertEqual(4, cs['unarmed'])
        so = xlist()
        cs = ch.GetSkills(so, categories=True)
        self.assertEqual(5, len(cs.keys()))
        self.assertEqual(['cat1', 'skl1', 'skl2', 'cat3', 'bite', 'foot', 'unarmed'], so)
        # bad skill set (now just stderr statment instead of exception)
        #ch.SetValue(ch.kSkillSetKey, 'foo')
        #self.assertRaises(DatafillError, ch.GetSkills)
        #ch.SetValue('x:base-s:foo', 'bar')
        #self.assertRaises(DatafillError, ch.GetSkills)

    def test_300_GetSetInfo(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual(None, ch.GetInfo('nonexistent'))
        self.assertEqual('no', ch.GetInfo('nonexistent', default='no'))
        ch.SetInfo('i:info1', '1')
        self.assertEqual('1', ch.GetInfo('info1'))
        ch.SetInfo('i:info1', None)
        self.assertEqual(None, ch.GetInfo('info1'))

    def test_301_GetInfos(self):
        deity.gamesystem.GameSystem.kGeneralInfos = xlist(('height', 'weight'))
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetInfo('weight', '12.33')
        ch.SetInfo('height', '32.11')
        self.gs.rules['ImperialMeasurements'] = 'yes'
        infos = ch.GetInfos()
        self.assertEqual('27.1 lbs', infos['weight'])
        self.assertEqual('1\'1"', infos['height'])
        self.gs.rules['ImperialMeasurements'] = 'no'
        infos = ch.GetInfos()
        self.assertEqual('12.3 kg', infos['weight'])
        self.assertEqual('32 cm', infos['height'])

    def test_302_GetInfos(self):
        deity.gamesystem.GameSystem.kGeneralInfos = xlist(('height', 'weight'))
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetInfo('weight', str(140.0 / deity.kLbsPerKg))
        self.gs.rules['ImperialMeasurements'] = 'yes'
        infos = ch.GetInfos()
        self.assertEqual('140.0 lbs', infos['weight'])

    def test_310_GetSetList(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual(None, ch.GetList('nonexistent'))
        self.assertEqual(['a','b'], ch.GetList('nonexistent', default=['a','b']))
        ch.SetList('items1', ['x','y','z'])
        self.assertEqual(['x','y','z'], ch.GetList('items1'))

    def test_320_GetSetDict(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual(None, ch.GetDict('nonexistent'))
        self.assertEqual(['a','b'], ch.GetDict('nonexistent', default=['a','b']))
        ch.SetDict('items1', {'i':'1','j':'2','k':'3'})
        self.assertEqual({'i':'1','j':'2','k':'3'}, ch.GetDict('items1'))

    def test_400_WieldItem(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        self.assertEqual(None, it1.CheckPossessor(ch))
        self.assertRaises(BadParameter, ch.WieldItem, it1, strx('nowhere'))
        ch.AddInventory(it1)
        self.assertEqual('', it1.CheckPossessor(ch))
        self.assertRaises(BadParameter, ch.WieldItem, it1, strx('nowhere'))
        cwo= list()
        cw = ch.GetWielded(order=cwo)
        self.assertTrue('righthand' in cw)
        self.assertEqual(cw['righthand'], None)
        self.assertEqual(['righthand','lefthand'], cwo)
        self.assertEqual('righthand', ch.WieldItem(it1, strx('righthand')))
        self.assertEqual('righthand', it1.CheckPossessor(ch))
        poss = it1.GetValue('x:possessor')
        g,t,l = poss.split('/')
        self.assertEqual('Char', g)
        self.assertEqual(ch.tid, t)
        self.assertEqual('righthand', l)
        cwo= list()
        cw = ch.GetWielded(order=cwo)
        self.assertEqual(it1.tid, cw['righthand'].tid)
        self.assertEqual(None, cw['lefthand'])
        self.assertRaises(NotPossible, ch.WieldItem, it1, strx('lefthand'))
        self.assertEqual(['righthand','lefthand'], cwo)
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testknife'))
        ch.AddInventory(it2)
        self.assertRaises(BadParameter, ch.WieldItem, it2, strx('nowhere'))
        self.assertRaises(BadParameter, ch.WieldItem, it2, strx('head'))
        self.assertRaises(NotPossible, ch.WieldItem, it2, strx('righthand'))
        self.assertEqual('lefthand', ch.WieldItem(it2, strx('lefthand')))
        cwo= list()
        cw = ch.GetWielded(order=cwo)
        self.assertEqual(it1.tid, cw['righthand'].tid)
        self.assertEqual(it2.tid, cw['lefthand'].tid)
        self.assertEqual([], ch.GetList(ch.kInventoryKey))
        self.assertEqual(['righthand','lefthand'], cwo)
        it3 = deity.gamesystem.GameSystem.Item(None)
        it3.Generate(xstr('_testdagger'))
        ch.AddInventory(it3)
        self.assertRaises(NotPossible, ch.WieldItem, it3, strx('righthand'))
        self.assertEqual('righthand', ch.WieldItem(it3, strx('righthand'), replace=True))
        self.assertEqual('', it1.CheckPossessor(ch))
        self.assertEqual('righthand', it3.CheckPossessor(ch))
        ch.SubInventory(it1)

    def test_401_WieldItem_notpossible(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testthing'))
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch.AddInventory(it1)
        self.assertRaises(NotPossible, ch.WieldItem, it1, strx('chest'))

    def test_402_SearchWielded(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        cw = ch.GetWielded()
        it1 = cw['righthand']
        it2 = cw['lefthand']
        sw = ch.SearchWielded(it1.Name())
        self.assertEqual(it1, ch.SearchWielded(strx(it1.Name())))
        self.assertEqual(it2, ch.SearchWielded(strx(it2.ShortDesc()), strx('lefthand')))
        self.assertRaises(BadParameter, ch.SearchWielded, strx('blah'))
        self.assertRaises(BadParameter, ch.SearchWielded, strx('blah'), strx('nowhere'))
        self.assertRaises(NotPossible, ch.SearchWielded, strx('blah'), strx('lefthand'))

    def test_403_UnwieldItem(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        cw = ch.GetWielded()
        it1 = cw['righthand']
        it2 = cw['lefthand']
        self.assertNotEqual(None, it1)
        self.assertNotEqual(None, it2)
        self.assertNotEqual(it1.tid, it2.tid)
        self.assertEqual([], ch.GetList(ch.kInventoryKey))
        ch.UnwieldItem(it1)
        cw = ch.GetWielded()
        self.assertEqual(None, cw['righthand'])
        self.assertEqual([it1.tid], ch.GetList(ch.kInventoryKey))
        self.assertRaises(NotPossible, ch.UnwieldItem, it1)
        self.assertRaises(NotPossible, ch.UnwieldItem, it2, xstr('righthand'))
        ch.UnwieldItem(it2, xstr('lefthand'))
        self.assertEqual([it1.tid, it2.tid], ch.GetList(ch.kInventoryKey))
        self.assertRaises(NotPossible, ch.SearchWielded, strx(it2.ShortDesc()), strx('lefthand'))

    def test_404_WieldItem_default(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch.AddInventory(it1)
        cw = ch.GetWielded()
        self.assertTrue('righthand' in cw)
        self.assertEqual(cw['righthand'], None)
        ch.WieldItem(it1, defindex=0)
        cw = ch.GetWielded()
        self.assertEqual(it1.tid, cw['righthand'].tid)
        self.assertEqual(None, cw['lefthand'])
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testknife'))
        ch.AddInventory(it2)
        ch.WieldItem(it2, defindex=1)
        cw = ch.GetWielded()
        self.assertEqual(it1.tid, cw['righthand'].tid)
        self.assertEqual(it2.tid, cw['lefthand'].tid)
        self.assertEqual([], ch.GetList(ch.kInventoryKey))
        ch.UnwieldItem(it1)
        ch.UnwieldItem(it2)

    def test_405_WieldItem_autodefault(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch.AddInventory(it1)
        self.assertEqual({'righthand':None, 'lefthand':None}, ch.GetWielded())
        ch.WieldItem(it1)
        self.assertEqual({'righthand':it1, 'lefthand':None}, ch.GetWielded())
        ch.UnwieldItem(it1)
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testshield'))
        ch.AddInventory(it2)
        self.assertEqual({'righthand':None, 'lefthand':None}, ch.GetWielded())
        ch.WieldItem(it2)
        self.assertEqual({'righthand':None, 'lefthand':it2}, ch.GetWielded())
        ch.UnwieldItem(it2)

    def test_410_WearItem(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testvest'))
        self.assertRaises(NotPossible, ch.SetWorn, {it1:[]})
        self.assertRaises(NotPossible, ch.WearItem, it1)
        ch.AddInventory(it1)
        cw = ch.GetWorn()
        self.assertEqual(0, len(cw))
        ch.WearItem(it1)
        self.assertEqual([], ch.GetList(ch.kInventoryKey))
        cw = ch.GetWorn()
        self.assertEqual(1, len(cw))
        for w,l in cw.iteritems():
            self.assertEqual(w.tid, it1.tid)
            poss = w.GetValue('x:possessor')
            g,t,l = poss.split('/')
            self.assertEqual('Char', g)
            self.assertEqual(ch.tid, t)
            self.assertEqual('=', l)
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testrobe'))
        ch.AddInventory(it2)
        ch.WearItem(it2)
        cw = ch.GetWorn()
        self.assertEqual(2, len(cw))
        self.assertEqual([], ch.GetList(ch.kInventoryKey))

    def test_411_WearItem_unwearable(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch.AddInventory(it1)
        self.assertRaises(NotPossible, ch.WearItem, it1)
        ch.SubInventory(it1)
        self.assertEqual([], ch.GetList(ch.kInventoryKey))

    def test_412_UnwearItem(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        cw = ch.GetWorn()
        self.assertEqual(2, len(cw))
        (it1, it2) = cw.keys()
        self.assertNotEqual(None, it1)
        self.assertNotEqual(None, it2)
        self.assertNotEqual(it1.tid, it2.tid)
        self.assertEqual([], ch.GetList(ch.kInventoryKey))
        ch.UnwearItem(it1)
        cw = ch.GetWorn()
        self.assertEqual(1, len(cw))
        self.assertEqual([it1.tid], ch.GetList(ch.kInventoryKey))
        self.assertRaises(NotPossible, ch.UnwearItem, it1)
        ch.UnwearItem(it2)
        cw = ch.GetWorn()
        self.assertEqual(0, len(cw))
        self.assertEqual([it1.tid, it2.tid], ch.GetList(ch.kInventoryKey))

    def test_420_MountCharacter(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        cm = deity.gamesystem.GameSystem.Character(None)
        cm.Generate(xstr('_testthing'))
        ch.MountCharacter(cm)
        self.assertEqual(cm, ch.GetMount())
        self.assertEqual([ch.tid], cm.GetList('riders'))
        self.assertRaises(NotPossible, ch.MountCharacter, cm)
        ch.Dismount()
        self.assertEqual(None, ch.GetInfo('mount'))
        self.assertEqual([], cm.GetList('riders', []))

    def test_500_Weight_Inventory(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        ch.SetDict(ch.kWieldedKey, {})
        ch.SetDict(ch.kWornKey, {})
        kw  = ch.Weight()
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch.AddInventory(it1)
        self.assertEqual(it1.Weight(), ch.Load())
        self.assertEqual(kw+it1.Weight(), ch.Weight())
        cw = ch.GetWielded()
        ch.WieldItem(it1, xstr('righthand'))
        self.assertEqual(it1.Weight(), ch.Load())
        self.assertEqual(kw+it1.Weight(), ch.Weight())
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testrobe'))
        ch.AddInventory(it2)
        self.assertEqual(it1.Weight()+it2.Weight(), ch.Load())
        self.assertEqual(kw+it1.Weight()+it2.Weight(), ch.Weight())
        ch.WearItem(it2)
        self.assertEqual(it1.Weight()+it2.Weight(), ch.Load())
        self.assertEqual(kw+it1.Weight()+it2.Weight(), ch.Weight())

    def test_600_UnarmedWeapon(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertRaises(NotPossible, ch.UnarmedWeapon, 'chest')
        uw = ch.UnarmedWeapon('leftarm')
        self.assertEqual(0,  uw.GetAttribute('attack'))
        self.assertEqual(15, uw.GetAttribute('defend'))
        self.assertEqual('unarmed', uw.GetInfo('skill'))
        self.assertEqual('testbase-arm', uw.Name())
        uw = ch.UnarmedWeapon('rightleg')
        self.assertEqual(5, uw.GetAttribute('attack'))
        self.assertEqual(5, uw.GetAttribute('defend'))
        self.assertEqual('unarmed', uw.GetInfo('skill'))
        self.assertEqual('testbase-leg', uw.Name())

    def test_601_UnarmedWeapon_derivedskill(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual('unarmed', ch.GetValue('x:base-s:bite'))
        uw = ch.UnarmedWeapon('head')
        self.assertEqual(0,  uw.GetAttribute('attack'))
        self.assertEqual(0, uw.GetAttribute('defend'))
        self.assertEqual('testbase-head', uw.Name())
        self.assertEqual('bite/unarmed', uw.GetInfo('skill'))

    def test_610_AttackFromLocations(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetList(ch.kInventoryKey, [])
        ch.SetDict(ch.kWieldedKey, {})
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch.AddInventory(it1)
        ch.WieldItem(it1, defindex=0)
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testdagger'))
        ch.AddInventory(it2)
        ch.WieldItem(it2, defindex=1)
        fo = list()
        fl = ch.AttackFromLocations(order=fo, separate='---')
        self.assertNotEqual(None, fl)
        flk = fl.keys()
        flk.sort()
        self.assertEqual(['head','leftarm','leftfoot','lefthand','leftleg',
                          'rightarm','rightfoot','righthand','rightleg'], flk)
        for l in flk:
            #print l,fl[l]
            self.assertTrue(isinstance(fl[l], deity.gamesystem.GameSystem.Item))
        self.assertEqual(it1, fl['righthand'])
        self.assertEqual(it2, fl['lefthand'])
        self.assertEqual(['righthand','lefthand','---','head','rightarm','leftarm',
                          'rightleg','leftleg','rightfoot','leftfoot'], fo)
        fo2 = list()
        fl2 = ch.AttackFromLocations(order=fo2)
        self.assertEqual(fl, fl2)
        # make sure order list has 1:1 correspondence with dictionary
        self.assertNotEqual({}, fl2)
        for l in fo2:
            self.assertNotEqual(None, fl2[l])
            del fl2[l]
        self.assertEqual({}, fl2)
        # one more, just for coverage
        fl3 = ch.AttackFromLocations()
        self.assertEqual(fl, fl3)

    def test_900_Generate_equipped(self):
        th = deity.gamesystem.GameSystem.Character(None)
        th.Generate(xstr('_testcharequipped'))
        self.assertEqual('_testcharequipped;_testchar;_testbase', th.GetValue('x:types'))
        i = th.GetInventory()
        self.assertEqual(1, len(i))
        self.assertTrue('_testknife'  == i[0].GetValue('x:type') or
                        '_testdagger' == i[0].GetValue('x:type'))
        w = th.GetWorn()
        self.assertEqual(2, len(w))

    def test_901_Generate_equipped_allopts(self):
        th = deity.gamesystem.GameSystem.Character(None)
        th.Generate(xstr('_testcharequipped'), allopts=True)
        self.assertEqual('_testcharequipped;_testchar;_testbase', th.GetValue('x:types'))
        i = th.GetInventory()
        self.assertEqual(2, len(i))
        self.assertEqual('_testknife',  i[0].GetValue('x:type'))
        self.assertEqual('_testdagger', i[1].GetValue('x:type'))
        w = th.GetWorn()
        self.assertEqual(2, len(w))

    def test_902_WieldItem_requirements(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testhuman'))
        ch.SetAttribute('str', 18)
        self.assertEqual('18', ch.GetValue('a:str'))
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testaxe'), user=ch)
        ch.AddInventory(it1)
        ch.WieldItem(it1, strx('righthand'))
        ch.UnwieldItem(it1)
        ch.SetAttribute('str', 17)
        deity.test.TestOutputClear()
        ch.WieldItem(it1, strx('righthand'))
        output = deity.test.TestOutputGet()
        self.assertIn('str >= 18', output)


###############################################################################


class GameSystem_80_Combined_Test(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.GameSystem()

    def test_100_Item_Skill(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testhuman'))
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate('_testdagger')
        self.assertEqual('dagger', it.GetInfo('skill'))
        self.assertRaises(NotPossible, it.Skill, ch)
        self.assertEqual('dagger', it.Skill(ch, willopen=True))
        ch.SetSkill('dagger', 22)
        self.assertEqual('dagger', it.Skill(ch))
        self.assertEqual('c/combat something', it.SkillInfo(ch))

    def test_101_Item_Skill_withbase(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testhuman'))
        it = deity.gamesystem.GameSystem.Item(None)
        it.Generate('_testknife')
        self.assertEqual('knife/dagger', it.GetInfo('skill'))
        self.assertRaises(NotPossible, it.Skill, ch)
        self.assertEqual('dagger', it.Skill(ch, willopen=True))
        ch.SetSkill('dagger', 22)
        self.assertEqual('dagger', it.Skill(ch))
        ch.SetSkill('knife', 33)  # will now use specific skill
        self.assertEqual('knife', it.Skill(ch))
        self.assertEqual('c/combat something', it.SkillInfo(ch))
        # check auto base-lookup
        it.SetValue('i:skill', 'knife')
        self.assertEqual('knife', it.GetInfo('skill'))
        self.assertEqual('knife', it.Skill(ch))
        self.assertEqual('c/combat something', it.SkillInfo(ch))
        ch.SetSkill('knife', None)
        self.assertEqual('knife', it.GetInfo('skill'))
        self.assertEqual('dagger', it.Skill(ch))
        self.assertEqual('c/combat something', it.SkillInfo(ch))
        # verify base-skill mismatch
        it.SetValue('i:skill', 'knife/unarmed')
        self.assertRaises(DatafillError, it.Skill, ch)

    def test_400_Inventory(self):
        ch1 = deity.gamesystem.GameSystem.Character(None)
        ch1.Generate(xstr('_testhuman'))
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testknife'))
        ch1.AddInventory(it1)
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testrobe'))
        ch1.AddInventory(it2)
        # can't share items
        ch2 = deity.gamesystem.GameSystem.Character(None)
        ch2.Generate(xstr('_testhuman'))
        self.assertRaises(NotPossible, ch2.AddInventory, it1)
        self.assertRaises(NotPossible, ch2.AddInventory, it2)
        ch1.WieldItem(it1, 'righthand')
        ch1.WearItem(it2)
        self.assertRaises(NotPossible, ch2.AddInventory, it1)
        self.assertRaises(NotPossible, ch2.AddInventory, it2)


###############################################################################


class GameSystem_90_Test(deity.test.TestCase):
    kTestCharTid = 'Char-42'

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = deity.gamesystem.GameSystem()
        self.wld = deity.world.World(deity.test.kTestWorldName)

    def test_000_AddCharacter(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        GDB[ch._GdbInfoKey('nextid')] = '42'
        ch.Generate(xstr('_testhuman(str=+3)'), 'superman')
        self.assertEqual('superman', ch.Name())
        self.assertEqual('superman(testhuman:str=+3)', ch.ShortDesc())
        self.assertNotEqual(None, ch.tid)
        self.assertEqual(None, ch.GetValue('x:gamegroup'))
        cc = self.gs.GetId(ch.tid)
        self.assertEqual(ch.tid, cc.tid)
        self.assertEqual(ch.Name(), cc.Name())
        self.assertEqual(ch, cc)
        self.assertEqual(None, cc.GetValue('x:something'))
        ch.SetValue('x:something', 'nothing')
        self.assertEqual('nothing', cc.GetValue('x:something'))
        self.assertEqual((None,None), ch.GetPossessor())
        self.assertRaises(BadParameter, self.gs.AddCharacter, ch, 'foo-group')
        self.gs.AddCharacter(ch, 'players')
        self.assertEqual('players', ch.GetValue('x:gamegroup'))
        self.assertEqual(['a/players/superman'], self.gs.gameCharDisplay.keys())

    def test_001_GetId_bad(self):
        self.assertRaises(Exception, self.gs.GetId, 'foo-bar')

    def test_010_UserRoll(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetPrompt(True)
        self.assertEqual(42, self.gs.UserRoll(ch, 42))
        self.assertEqual(43, self.gs.UserRoll(ch, '43'))
        self.assertEqual(44, self.gs.UserRoll(ch, None, default='44'))
        deity.test.TestUserResponse('dice-1', '7')
        deity.test.TestUserResponse('dice-2', '14')
        self.assertEqual( 7, self.gs.UserRoll(ch, '3d6', test='dice-1', title='foo'))
        self.assertEqual(14, self.gs.UserRoll(ch, '3d6', test='dice-2'))
        self.assertRaises(BadParameter, self.gs.UserRoll, ch, 'blah')

    def test_020_UserCheck(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetPrompt(True)
        self.assertEqual(True, self.gs.UserCheck(ch, '5', '<', 6))
        self.assertEqual(False, self.gs.UserCheck(ch, 5, '<', '5'))
        deity.test.TestUserResponse('roll', 'succeed')
        self.assertEqual(True, self.gs.UserCheck(ch, '3d6', '<=', 8, test='roll', title='foo'))
        deity.test.TestUserResponse('roll', 'fail')
        self.assertEqual(False, self.gs.UserCheck(ch, '3d6', '<=', 8, test='roll'))
        self.assertRaises(BadParameter, self.gs.UserCheck, ch, 'blah', '<', 8)

    def test_030_UserSelect(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetPrompt(False)
        choices = xlist(('1=a', '10=bb', '100=ccc', '1000=(dddd|eeee)'))
        self.assertEqual('a', self.gs.UserSelect(ch, choices, 1))
        self.assertEqual('bb', self.gs.UserSelect(ch, choices, '2d4'))
        self.assertEqual('ccc', self.gs.UserSelect(ch, choices, '1d6+10'))
        ch.SetPrompt(True)
        deity.test.TestUserResponse('roll', 'a <= 1')
        self.assertEqual('a', self.gs.UserSelect(ch, choices, '1d1000', test='roll', title='foo'))
        deity.test.TestUserResponse('roll', 'bb <= 10')
        self.assertEqual('bb', self.gs.UserSelect(ch, choices, '1d1000', test='roll'))
        deity.test.TestUserResponse('roll', 'ccc <= 100')
        self.assertEqual('ccc', self.gs.UserSelect(ch, choices, '1d1000', test='roll'))
        deity.test.TestUserResponse('roll', 'dddd <= 1000')
        self.assertEqual('dddd', self.gs.UserSelect(ch, choices, '1d1000', test='roll'))
        deity.test.TestUserResponse('roll', 'eeee <= 1000')
        self.assertEqual('eeee', self.gs.UserSelect(ch, choices, '1d1000', test='roll'))
        self.assertRaises(BadParameter, self.gs.UserSelect, ch, choices, 'blah', test='roll')

    def test_100_HitLocation(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetPrompt(False)
        ct = ch.GetValue('x:realtype')
        at = ch.GetValue('x:types')
        ch.SetValue('x:realtype', 'nothing')
        ch.SetValue('x:types', 'nothing')
        self.assertRaises(BadParameter, self.gs.HitLocation, ch, xstr('head'))
        ch.SetValue('x:realtype', ct)
        ch.SetValue('x:types', at)
        self.assertRaises(BadParameter, self.gs.HitLocation, ch, xstr('nowhere'))
        self.assertEqual('head', self.gs.HitLocation(ch, xstr('head')))
        self.assertEqual('lefthand', self.gs.HitLocation(ch, xstr('lefthand')))
        self.assertEqual('rightleg', self.gs.HitLocation(ch, xstr('rightleg')))
        self.assertTrue(isinstance(self.gs.HitLocation(ch, xstr('head')), xstr))
        self.assertTrue(isinstance(self.gs.HitLocation(ch, None), xstr))

    def test_110_HitLocation_high(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        range = list()
        self.gs.rand.testset('rangeloc', 1)
        self.assertEqual('head', self.gs.HitLocation(ch, xstr('high'), range))
        self.assertEqual(['high'], range)
        self.gs.rand.testend()
        self.gs.rand.testset('rangeloc', 30)
        self.assertEqual('head', self.gs.HitLocation(ch, xstr('high')))
        self.gs.rand.testend()
        self.gs.rand.testset('rangeloc', 31)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.assertEqual('leftshoulder', self.gs.HitLocation(ch, xstr('high')))
        self.gs.rand.testend()
        self.gs.rand.testset('rangeloc', 100)
        self.gs.rand.testset('rangeloc-multi', 1)
        self.assertEqual('rightshoulder', self.gs.HitLocation(ch, xstr('high')))
        self.gs.rand.testend()
        for i in xrange(0,10):
            hl = self.gs.HitLocation(ch, xstr('high'))
            self.assertTrue(hl == 'head' or hl == 'leftshoulder' or hl == 'rightshoulder')

    def test_120_HitLocation_low(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.gs.rand.testset('rangeloc', 1)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.assertEqual('leftleg', self.gs.HitLocation(ch, xstr('low')))
        self.gs.rand.testset('rangeloc', 70)
        self.gs.rand.testset('rangeloc-multi', 1)
        self.assertEqual('rightleg', self.gs.HitLocation(ch, xstr('low')))
        self.gs.rand.testset('rangeloc', 71)
        self.gs.rand.testset('rangeloc-multi', 0)
        self.assertEqual('leftfoot', self.gs.HitLocation(ch, xstr('low')))
        self.gs.rand.testset('rangeloc', 100)
        self.gs.rand.testset('rangeloc-multi', 1)
        self.assertEqual('rightfoot', self.gs.HitLocation(ch, xstr('low')))
        self.gs.rand.testend()
        for i in xrange(0,10):
            hl = self.gs.HitLocation(ch, xstr('low'))
            self.assertTrue(hl == 'leftleg' or hl == 'rightleg' or hl == 'leftfoot' or hl == 'rightfoot')

    def test_130_HitLocation_badrange(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertRaises(BadParameter, self.gs._HitRangeLocation, ch, xstr('foo'))

    def test_140_HitFromLocation(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        cl = self.gs.HitFromLocation(ch, 'righthand')
        self.assertEqual('righthand', cl)
        cl = self.gs.HitFromLocation(ch, 'hand')
        self.assertEqual('righthand', cl)
        cl = self.gs.HitFromLocation(ch, 'leftfoot')
        self.assertEqual('leftfoot', cl)
        cl = self.gs.HitFromLocation(ch, 'foot')
        self.assertEqual('rightfoot', cl)

    def test_150_BaseLocation(self):
        prefix = []
        self.assertEqual('foot', self.gs.BaseLocation('rightfrontfoot', prefix))
        self.assertEqual(['rightfront'], prefix)
        prefix = []
        self.assertEqual('hand', self.gs.BaseLocation('lefthand', prefix))
        self.assertEqual(['left'], prefix)
        prefix = []
        self.assertEqual('hoof', self.gs.BaseLocation('rightforehoof', prefix))
        self.assertEqual(['rightfore'], prefix)
        prefix = []
        self.assertEqual('claw', self.gs.BaseLocation('lefthindclaw', prefix))
        self.assertEqual(['lefthind'], prefix)
        prefix = []
        self.assertEqual('wing', self.gs.BaseLocation('rightwing', prefix))
        self.assertEqual(['right'], prefix)
        prefix = []
        self.assertEqual('foot', self.gs.BaseLocation('foot', prefix))
        self.assertEqual([''], prefix)

    def test_200_WoundInfo(self):
        self.gs.kLocationInjuryWoundMap = {
            'head':	('1=scratch', '2=gash', '3=split open'),
            'shoulder':	('1=scratch', '2=gash', '3=smashed'),
            'arm':	('1=scratch', '2=gash', '3=broken'),
            'hand':	('1=scratch', '2=gash', '3=gone'),
            'chest':	('1=scratch', '2=gash', '3=impaled'),
            'leg':	('1=scratch', '2=gash', '3=broken'),
            'foot':	('1=scratch', '2=gash', '3=staked'),
            '':		('1=scratch', '2=gash', '3=beyond repair'),
        }
        self.gs.kLocationInjuryFlagsMap = {
            'head':	'duck',
            'shoulder':	'beef',
            'arm':	'wrestle',
            'hand':	'andhand',
            'chest':	'out',
            'leg':	'up',
            'foot':	'30cm',
            '':		'nothing',
        }
        self.assertEqual(('scratch','duck'), self.gs.WoundInfo(xstr('head')))
        self.assertEqual(('scratch','beef'), self.gs.WoundInfo(xstr('leftshoulder'),1))
        self.assertEqual(('gash','wrestle'), self.gs.WoundInfo(xstr('rightarm'),2))
        self.assertEqual(('gone','andhand'), self.gs.WoundInfo(xstr('lefthand'),3))
        self.assertEqual(('beyond repair','nothing'), self.gs.WoundInfo(xstr('elsewhere'),3))
        self.assertRaises(BadParameter, self.gs.WoundInfo, xstr('chest'), 4)

    def test_210_CombatSummary(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        ch.SetInfo('status', 'fubar')
        self.assertEqual('fubar', self.gs.GetCombatSummary(ch))

    def test_300_CombatNewTurn_busy(self):
        ch = deity.gamesystem.GameSystem.Character(self.kTestCharTid)
        self.assertEqual(None, ch.GetInfo('busy'))
        # busy until now
        ch.SetInfo('busy', WORLD.TimeExport())
        self.assertNotEqual(None, ch.GetInfo('busy'))
        self.gs.CombatNewTurn(ch)
        self.assertEqual(None, ch.GetInfo('busy'))
        # busy until start of next round
        btime = WORLD.TimeGetCurrent()
        WORLD.TimeAdjust(''.join(self.gs.kCombatRoundTime), btime)
        ch.SetInfo('busy', WORLD.TimeExport(btime))
        self.gs.CombatNewTurn(ch)
        self.assertEqual(WORLD.TimeExport(btime), ch.GetInfo('busy'))
        # busy until end of this round
        WORLD.TimeAdjust('-1s', btime)
        ch.SetInfo('busy', WORLD.TimeExport(btime))
        self.gs.CombatNewTurn(ch)
        self.assertEqual('', ch.GetInfo('busy'))
        # advance to next round
        WORLD.TimeAdjust(''.join(self.gs.kCombatRoundTime))
        self.gs.CombatNewTurn(ch)
        self.assertEqual(None, ch.GetInfo('busy'))


###############################################################################


if __name__ == '__main__':
    unittest.main()
