#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test combat mode)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.modes
import deity.gamesystem
import deity.gamesystem.harnmaster
import deity.world

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class CombatTest1(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.wld = deity.world.World(deity.test.kTestWorldName)
        self.app = deity.test.App()

    def test_10_init(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.DisplayRefresh()
        mw.Activate()
        self.app.CommandExecute('')

    def test_20_generate(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        mw.charGenerate.SetStringSelection('human')
        self.assertEqual(0, len(mw.charList.names))
        self.assertEqual(None, self.app.curchar.Get())
        mw.OnGenerate(None)
        mw.DisplayRefresh()
        self.assertNotEqual(0, len(mw.charList.names))
        self.assertNotEqual(None, self.app.curchar.Get())
        ch = self.app.curchar.Get()
        self.assertEqual('encounter', ch.GetValue('x:gamegroup'))
        self.assertEqual('human01', ch.Name())
        mw.OnGenerate(None)
        mw.DisplayRefresh()
        ch = self.app.curchar.Get()
        self.assertEqual('encounter', ch.GetValue('x:gamegroup'))
        self.assertEqual('human02', ch.Name())

    def test_30_alias(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        mw.DoGenerate('gen', 'human', 'encounter')
        mw.DisplayRefresh()
        self.assertNotEqual(0, len(mw.charList.names))
        self.assertNotEqual(None, self.app.curchar.Get())
        ch1 = self.app.curchar.Get()
        mw.DoAlias('alias', ch1.Name(), 'aa')
        self.assertEqual({'aa':ch1.Name()}, mw.alias)
        self.assertRaises(BadParameter, mw.DoAlias, 'alias', ch1.Name(), 'zz', 'toomanyparms')
        self.assertRaises(BadParameter, mw.DoAlias, 'alias', 'nobody', 'zz')

    def test_40_display(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertEqual(None, self.app.curchar.Get())
        mw.DoGenerate('gen', 'human', 'encounter', 'disptest1')
        mw.DoGenerate('gen', 'human', 'encounter', 'disptest2')
        mw.DisplayRefresh()
        self.assertEqual('disptest2', self.app.curchar.Get().Name())
        mw.DoDisplay('disp', 'disptest1')
        self.assertEqual('disptest1', self.app.curchar.Get().Name())
        mw.DoDisplay('disp', 'disptest2')
        self.assertEqual('disptest2', self.app.curchar.Get().Name())
        self.assertRaises(BadParameter, mw.DoDisplay, 'disp', 'disptest1', 'toomanyparms')
        self.assertRaises(BadParameter, mw.DoDisplay, 'disp', 'nobody')

    def test_50_addsubtract(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertEqual(None, self.app.curchar.Get())
        mw.DoGenerate('gen', 'human', 'encounter', 'astest')
        self.assertEqual('astest', self.app.curchar.Get().Name())
        mw.DisplayRefresh()
        self.assertEqual([], mw.charList.GetStringSelections())
        self.app.curchar.Set(None)
        mw.DoAdd('add', 'astest')
        self.assertEqual(['astest'], mw.charList.GetStringSelections())
        self.assertNotEqual(None, self.app.curchar.Get())
        self.assertEqual('astest', self.app.curchar.Get().Name())
        mw.DoAdd('add', 'astest')
        self.assertEqual(['astest'], mw.charList.GetStringSelections())
        mw.DoSubtract('sub', 'astest')
        self.assertEqual([], mw.charList.GetStringSelections())
        mw.DoSubtract('sub', 'astest')
        self.assertEqual([], mw.charList.GetStringSelections())
        self.assertRaises(BadParameter, mw.DoAdd, 'add', 'astest', 'toomanyparms')
        # aliased
        mw.DoAlias('alias', 'astest', 'h1')
        mw.DoAdd('add', 'h1')
        self.assertEqual(['astest'], mw.charList.GetStringSelections())
        mw.DoSubtract('sub', 'h1')
        self.assertEqual([], mw.charList.GetStringSelections())

    def test_60_status(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        mw.DoGenerate('gen', 'human', 'encounter', 'statustest')
        self.assertEqual('statustest', self.app.curchar.Get().Name())
        char = self.app.curchar.Get()
        self.assertEqual('standing', char.GetStatus())
        mw.DoStatus('status', 'statustest', 'prone')
        self.assertEqual('prone', char.GetStatus())
        self.assertRaises(BadParameter, mw.DoStatus, 'status', 'statustest', 'blubbering')
        self.assertRaises(BadParameter, mw.DoStatus, 'status', 'statustest', 'prone', 'toomanyparms')
        self.assertRaises(BadParameter, mw.DoStatus, 'status', 'nobody', 'prone')

    def test_70_clear(self):
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertRaises(BadParameter, mw.DoClear, 'clear', 'blah')

    def test_71_clear_characters(self):
        groups = GAME.GetId(GAME.kCharacterGroupsTid).GetInventory()
        for g in groups:
            g.SetValueList(g.kInventoryKey, [])
        GAME.UpdateGameChars()
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertEqual([], GAME.gameCharDisplay.keys())
        mw.DoGenerate('gen', 'human', 'encounter', 'clearme')
        self.assertEqual(['z/encounter/clearme'], GAME.gameCharDisplay.keys())
        mw.DoClear('clear', 'en')
        self.assertEqual([], GAME.gameCharDisplay.keys())
        self.assertEqual([], GAME.gameCharNames.Get().keys())

    def test_72_clear_ground(self):
        ground = GAME.GetId(GAME.kCombatGroundTid)
        ground.SetValueList(ground.kInventoryKey, [])
        mw = deity.modes.Combat(Test_Win, self.app)
        mw.Activate()
        self.assertEqual([], ground.GetInventory())
        char = GAME.Character(None)
        char.MakeUnique()
        ground.AddInventory(char)
        self.assertEqual([char], ground.GetInventory())
        self.app.CommandExecute('clear gr')
        #mw.DoClear('clear', 'gr')
        self.assertEqual([], ground.GetInventory())


###############################################################################


class CombatTest2(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.wld = deity.world.World(deity.test.kTestWorldName)
        self.app = deity.test.App()
        self.mw  = deity.modes.Combat(Test_Win, self.app)
        self.ch  = list()
        self.cn  = list()
        self.mw.Activate()
        for i in xrange(0,3):
            self.mw.DoGenerate('gen', 'human', 'encounter')
            self.ch.append(self.app.curchar.Get())
            self.cn.append(self.app.curchar.Get().Name())
        self.mw.DisplayRefresh()
        for i in xrange(0,3):
            self.ch[i].SetSkill('initiative', (i+1)*10)
            self.mw.DoAlias('alias', self.cn[i], 'c%d'%i)
            self.mw.DoAdd('add', self.cn[i])

    def tearDown(self):
        deity.test.TestCase.tearDown(self)
        for i in xrange(0,3):
            self.ch[i].Rename('deprecated')
        

    def test_10_round(self):
        self.assertEqual('', self.mw.submode)
        self.mw.OnAction(None)
        self.assertEqual('round', self.mw.submode)
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertTrue(self.ch[0] in self.mw.charList.GetSelections())
        self.ch[0].SetStatus('dead')
        self.mw.OnAction(None)
        self.assertEqual('', self.mw.submode)
        self.assertTrue(self.ch[0] not in self.mw.charList.GetSelections())

    def test_15_prompt(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual(False, self.ch[2].GetPrompt())
        self.assertEqual(None, self.ch[2].GetValue('x:combatprompt'))
        self.app.CommandExecute('prompt on')
        self.assertEqual(True, self.ch[2].GetPrompt())
        self.assertEqual('1', self.ch[2].GetValue('x:combatprompt'))
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('prompt of')
        self.assertEqual(False, self.ch[2].GetPrompt())
        self.assertEqual('0', self.ch[2].GetValue('x:combatprompt'))
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_16_prompt_history(self):
        self.assertEqual(WORLD, self.wld)
        starttime = self.wld.TimeGetCurrent()
        self.ch[2].SetValue('x:combatprompt', '1')
        self.assertEqual(False, self.ch[1].GetPrompt())
        self.assertEqual(False, self.ch[2].GetPrompt())
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(False, self.ch[1].GetPrompt())
        self.assertEqual(True, self.ch[2].GetPrompt())
        self.assertEqual(starttime, self.wld.TimeGetCurrent())
        self.mw.DoRoundDone('done')
        self.assertNotEqual(starttime, self.wld.TimeGetCurrent())
        self.mw.DisplayRefresh()
        self.mw.Deactivate()
        self.assertEqual(False, self.ch[1].GetPrompt())
        self.assertEqual(False, self.ch[2].GetPrompt())

    def test_20_alias(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('alias', self.cn[0], 'h0')
        self.assertEqual(self.cn[0], self.mw.alias['h0'])
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_22_pass(self):
        self.mw.DoRound('round')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('pass') # forfeit action
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)

    def test_24_hold(self):
        self.mw.DoRound('round')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('hold')
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(1, len(self.mw.endRoundList))
        self.assertEqual(self.cn[1], self.mw.curname)

    def test_26_die(self):
        self.mw.DoRound('round')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual('standing', self.ch[2].GetStatus())
        self.assertEqual(3, len(self.mw.charList.GetSelections()))
        self.app.CommandExecute('die')
        self.assertEqual('dead', self.ch[2].GetStatus())
        self.assertEqual(2, len(self.mw.charList.GetSelections()))
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)

    def test_30_test(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.mw.DoRoundTest('test', 'init', '91', '-100')
        self.assertEqual('initiative test result: mf', deity.test.TestOutputBuf)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.mw.DoRoundTest('test', 'init', '91', '+100')
        self.assertEqual('initiative test result: ms', deity.test.TestOutputBuf)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.mw.DoRoundTest, 'test', 'blah')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

        self.gs.SkillLevel(self.app.curchar.Get(), 'swords', open=True)
        self.mw.DoRoundTest('test', 'swords')
        self.assertRaises(BadParameter, self.mw.DoRoundTest, 'shortsword')
        self.gs.SkillLevel(self.app.curchar.Get(), 'shortsword/swords', open=True)
        self.mw.DoRoundTest('test', 'shortsword')

        self.assertRaises(BadParameter, self.app.CommandExecute, 'test flails')
        deity.test.TestOutputClear()
        self.app.CommandExecute('force test flails')
        self.assertIn('opened skill', deity.test.TestOutputGet())

    def test_32_check(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.mw.DoRoundCheck('check', 'str', '17', '-100')
        self.assertEqual('str check result: f', deity.test.TestOutputBuf)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.mw.DoRoundCheck('check', 'str', '17', '+100')
        self.assertEqual('str check result: s', deity.test.TestOutputBuf)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.mw.DoRoundCheck, 'check', 'blah')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_40_extra_pass(self):
        self.mw.DoRound('round')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('extra pass') # forfeit action
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('extra pas') # forfeit action (abbreviated)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_41_extra_hold(self):
        self.mw.DoRound('round')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('extra hold')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_42_extra_check(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.mw.DoRoundExtra('extra', 'check', 'str', '17', '-100')
        self.assertEqual('str check result: f', deity.test.TestOutputBuf)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_43_extra_fail(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.mw.DoRoundExtra, 'extra', 'check')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_45_let_pass(self):
        self.mw.DoRound('round')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('let %s pass' % self.cn[0])
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_46_let_hold(self):
        self.mw.DoRound('round')
        self.app.CommandExecute('alias', self.cn[0], 'h0')
        self.assertEqual(3, len(self.mw.roundList))
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('let h0 hold')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_47_let_check(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.mw.DoRoundLet('let', self.cn[0], 'check', 'str', '17', '-100')
        self.assertEqual('str check result: f', deity.test.TestOutputBuf)
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_49_let_lastcommand(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual({}, self.mw.actions)
        self.mw.DoRoundLet('let', self.cn[0], 'prompt', 'off')
        self.mw.DisplayRefresh()
        self.assertEqual({}, self.mw.actions)
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_50_stand(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.ch[2].SetStatus('prone')
        self.app.CommandExecute('stand')
        self.assertEqual('standing', self.ch[2].GetStatus())
        self.assertRaises(NotPossible, self.app.CommandExecute, 'stand')
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)

    def test_55_move(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('move')
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(1, len(self.mw.endRoundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.ch[1].SetStatus('prone')
        self.assertRaises(NotPossible, self.app.CommandExecute, 'move')

    def test_56_charge(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('charge')
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(1, len(self.mw.endRoundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.ch[1].SetStatus('prone')
        self.assertRaises(NotPossible, self.app.CommandExecute, 'charge')

    def test_60_wield(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        i2 = self.gs.Item(None)
        i2.Generate('dagger')
        self.ch[2].AddInventory(i1)
        self.ch[2].AddInventory(i2)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'wield knifey')
        self.app.CommandExecute('wield knife lefthand')
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':None, 'lefthand':i1}, self.ch[2].GetWielded())
        self.app.CommandExecute('wield dagger')
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':i2, 'lefthand':i1}, self.ch[2].GetWielded())
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'wield')

    def test_61_unwield(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        i1.Rename('knife1')
        i2 = self.gs.Item(None)
        i2.Generate('knife')
        i2.Rename('knife2')
        self.ch[2].AddInventory(i1)
        self.ch[2].AddInventory(i2)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.app.CommandExecute('wield knife2 lefthand')
        self.mw.DisplayRefresh()
        self.app.CommandExecute('wield knife')
        self.mw.DisplayRefresh()
        cw = self.ch[2].GetWielded()
        self.assertEqual({'righthand':i1, 'lefthand':i2}, self.ch[2].GetWielded())
        self.app.CommandExecute('unwield knife')
        self.mw.DisplayRefresh()
        cw = self.ch[2].GetWielded()
        self.assertEqual({'righthand':None, 'lefthand':i2}, self.ch[2].GetWielded())
        self.app.CommandExecute('wield knife')
        self.mw.DisplayRefresh()
        self.app.CommandExecute('unwield knife2')
        self.mw.DisplayRefresh()
        cw = self.ch[2].GetWielded()
        self.assertEqual({'righthand':i1, 'lefthand':None}, self.ch[2].GetWielded())
        self.app.CommandExecute('wield knife lefthand')
        self.mw.DisplayRefresh()
        self.app.CommandExecute('unwield knife lefthand')
        self.mw.DisplayRefresh()
        cw = self.ch[2].GetWielded()
        self.assertEqual({'righthand':i1, 'lefthand':None}, self.ch[2].GetWielded())
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_62_wield_shield(self):
        i1 = self.gs.Item(None)
        i1.Generate('roundshield')
        self.ch[2].AddInventory(i1)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('wield roundshield')
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':None, 'lefthand':i1}, self.ch[2].GetWielded())

    def test_65_draw(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        i2 = self.gs.Item(None)
        i2.Generate('dagger')
        self.ch[2].AddInventory(i1)
        self.ch[2].AddInventory(i2)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertRaises(BadParameter, self.app.CommandExecute, 'draw')
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.gs.rand.testset('attrcheck-dex', (1,1,1))
        self.app.CommandExecute(u'extra draw knife lefthand')
        self.gs.rand.testend()
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':None, 'lefthand':i1}, self.ch[2].GetWielded())
        self.gs.rand.testset('attrcheck-dex', (6,6,5))
        self.app.CommandExecute('draw dagger +99')
        self.gs.rand.testend()
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':i2, 'lefthand':i1}, self.ch[2].GetWielded())
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)

    def test_66_drop(self):
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        self.ch[2].AddInventory(i1)
        self.assertEqual((self.ch[2],''), i1.GetPossessor())
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.app.CommandExecute('wield knife lefthand')
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':None, 'lefthand':i1}, self.ch[2].GetWielded())
        self.assertEqual([], GAME.GetId(GAME.kCombatGroundTid).GetInventory())
        self.app.CommandExecute('drop knife')
        self.mw.DisplayRefresh()
        self.assertEqual({'righthand':None, 'lefthand':None}, self.ch[2].GetWielded())
        self.assertEqual([i1], GAME.GetId(GAME.kCombatGroundTid).GetInventory())
        self.assertEqual((GAME.GetId(GAME.kCombatGroundTid),''), i1.GetPossessor())
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_70_wound(self):
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatStumble'] = 'no'
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        self.ch[1].AddInventory(i1)
        self.ch[1].WieldItem(i1, defindex=0)
        i2 = self.gs.Item(None)
        i2.Generate('dagger')
        self.ch[2].AddInventory(i2)
        self.ch[2].WieldItem(i2, defindex=0)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.app.CommandExecute('wound %s point 5' % self.cn[1])
        self.assertNotEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.mw.DisplayRefresh()
        if len(self.mw.roundList) == 2:
            self.app.CommandExecute('pass')
            self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'wound')
        self.assertRaises(BadParameter, self.app.CommandExecute, 'wound', self.cn[1])
        self.app.CommandExecute('wound %s point 2d6' % self.cn[2])

    def test_75_aid(self):
        self.ch[0].SetDict('bleeders', {'skull-G3blunt':'4'})
        self.ch[0].SetValue('x:bloodloss', '0')
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'aid')
        self.app.CommandExecute('aid %s 99' % self.cn[0]) # fail
        self.assertEqual({'skull-G3blunt':'0'}, self.ch[0].GetDict('bleeders'))
        self.assertEqual('1', self.ch[0].GetValue('x:bloodloss'))
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.app.CommandExecute('aid %s 94 +45' % self.cn[0])
        self.assertEqual({}, self.ch[0].GetDict('bleeders'))
        self.assertEqual('1', self.ch[0].GetValue('x:bloodloss'))
        self.mw.DisplayRefresh()
        self.assertEqual(0, len(self.mw.roundList))
        self.assertEqual(self.cn[0], self.mw.curname)

    def test_80_attack(self):
        self.gs.rules['CombatFumble'] = 'no'
        self.gs.rules['CombatStumble'] = 'no'
        i1 = self.gs.Item(None)
        i1.Generate('knife')
        self.ch[1].AddInventory(i1)
        self.ch[1].WieldItem(i1, defindex=0)
        i2 = self.gs.Item(None)
        i2.Generate('dagger')
        self.ch[2].AddInventory(i2)
        self.ch[2].WieldItem(i2, defindex=0)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        deity.test.TestUserResponse('kill?', False)  # just in case
        self.app.CommandExecute('attack %s righthand:point +100 5!12 defend dodge -100 99' % self.cn[1])
        self.assertNotEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.ch[1].SetStatus('standing')
        self.mw.DisplayRefresh()
        if len(self.mw.roundList) == 2:  # shockroll may create TA
            self.app.CommandExecute('pass')
            self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'attack')
        self.app.CommandExecute('extra attack %s rightha 5 def d' % self.cn[2])
        self.mw.DisplayRefresh()
        if len(self.mw.roundList) == 2:  # shockroll may create TA
            self.app.CommandExecute('pass')
            self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'attack')
        self.app.CommandExecute('attack %s rightha !0 def d 99!1d6' % self.cn[0])
        self.mw.DisplayRefresh()
        self.assertEqual(0, len(self.mw.roundList))
        self.assertEqual(self.cn[0], self.mw.curname)
        deity.test.TestUserResponse('unarmed?', True)
        self.app.CommandExecute('attack %s def dodge' % self.cn[1])

    def test_85_grapple(self):
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.app.CommandExecute('grapple %s righthand:blunt +100 5!12 defend grapple -100 99' % self.cn[1])
        self.assertEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.assertTrue('standing' == self.ch[1].GetStatus() and 'prone' == self.ch[2].GetStatus() or
                        'standing' == self.ch[2].GetStatus() and 'prone' == self.ch[1].GetStatus())
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList)) # somebody got tactical-advantage
        self.assertTrue('standing' == self.ch[1].GetStatus() and self.cn[1] == self.mw.curname or
                        'standing' == self.ch[2].GetStatus() and self.cn[2] == self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'grapple')

    def test_86_missile(self):
        i1 = self.gs.Item(None)
        i1.Generate('shortbow')
        self.ch[2].AddInventory(i1)
        self.ch[2].WieldItem(i1, 'righthand')
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.app.CommandExecute('missile %s righthand: +100 5!12 defend dodge -100 99' % self.cn[1])
        self.assertNotEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))
        self.assertEqual(self.cn[1], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'missile')

    def test_87_missile_range(self):
        i1 = self.gs.Item(None)
        i1.Generate('shortbow')
        self.ch[2].AddInventory(i1)
        self.ch[2].WieldItem(i1, 'righthand')
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.app.CommandExecute('missile %s righthand: @long +100 5!12 defend dodge -100 99' % self.cn[1])
        self.assertNotEqual(None, self.ch[1].GetList('wounds'))
        self.assertEqual(None, self.ch[2].GetList('wounds'))
        self.mw.DisplayRefresh()
        self.assertEqual(1, len(self.mw.roundList))

    def test_90_wear(self):
        i1 = self.gs.Item(None)
        i1.Generate('vest-leather')
        self.ch[2].AddInventory(i1)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertRaises(BadParameter, self.app.CommandExecute, 'wear robey')
        self.assertRaises(BadParameter, self.app.CommandExecute, 'wear')
        self.app.CommandExecute('wear vest')
        self.mw.DisplayRefresh()
        self.assertEqual([i1], self.ch[2].GetWorn().keys())
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_91_unwear(self):
        i1 = self.gs.Item(None)
        i1.Generate('vest-leather')
        self.ch[2].AddInventory(i1)
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)
        self.app.CommandExecute('extra wear vest')
        self.mw.DisplayRefresh()
        self.assertEqual([i1], self.ch[2].GetWorn().keys())
        self.assertEqual(2, len(self.mw.roundList))
        self.assertRaises(BadParameter, self.app.CommandExecute, 'unwear robey')
        self.assertRaises(BadParameter, self.app.CommandExecute, 'unwear')
        self.app.CommandExecute('unwear vest')
        self.mw.DisplayRefresh()
        self.assertEqual([], self.ch[2].GetWorn().keys())
        self.assertEqual(2, len(self.mw.roundList))
        self.assertEqual(self.cn[2], self.mw.curname)

    def test_95_mount(self):
        m1 = self.gs.Character(None)
        m1.Generate('warhorse')
        self.gs.AddCharacter(m1, 'encounter')
        self.mw.DoAlias('alias', m1.Name(), 'my-horse')
        self.mw.DoRound('round')
        self.mw.DisplayRefresh()
        self.assertEqual(self.cn[2], self.mw.curname)
        self.assertEqual('standing', self.ch[2].GetStatus())
        self.assertRaises(BadParameter, self.app.CommandExecute, 'mount unknown')
        self.assertRaises(BadParameter, self.app.CommandExecute, 'mount')
        self.app.CommandExecute('force mount my-horse')
        self.assertEqual('mounted', self.ch[2].GetStatus())
        self.assertRaises(BadParameter, self.app.CommandExecute, 'dismount unknown')
        self.app.CommandExecute('force dismount')
        self.assertEqual('standing', self.ch[2].GetStatus())


###############################################################################


if __name__ == '__main__':
    unittest.main()
