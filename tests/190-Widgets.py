#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test general widgets)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.editor
import deity.widgets

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


#class QueryTest(deity.test.TestCase):
#
#    def test_user(self):
#        qw = deity.widgets.QueryBox('please click "yes"')
#        self.assertEqual(True, qw.Run())
#        qw = deity.widgets.QueryBox('please click "no"')
#        self.assertEqual(False, qw.Run())


###############################################################################


class SelectableItemListTest(deity.test.TestCase):

    def test_10_init(self):
        sw = deity.widgets.SelectableItemList(Test_Win, 200)

    def test_20_list(self):
        sw = deity.widgets.SelectableItemList(Test_Win, 200)
        il = xdict(xsepkey='/', xlatekey=(None, 'test:',''))
        il['1/a/test1'] = 'first'
        il['1/a/test2'] = 'second'
        il['1/c/test3'] = ['third']
        sw.SetItems(il)
        self.assertEqual(['B:',sw.kDisplayIndent+'Case1',sw.kDisplayIndent+'Case2','D:',sw.kDisplayIndent+'Case3'], sw.display)
        self.assertEqual(1, sw.IndexFromNamex('test1'))
        self.assertEqual(2, sw.IndexFromValue('second'))
        self.assertEqual(4, sw.IndexFromValue(il['1/c/test3']))
        self.assertEqual('1/a/test1', sw.GetString(1))
        self.assertEqual('1/B/Case1', sw.GetString(1).xval())
        self.assertEqual(None, sw.GetSelection())
        self.assertEqual(None, sw.GetStringSelection())
        sw.SetStringSelection('test1')
        self.assertEqual('first', sw.GetSelection())
        self.assertEqual('test1', sw.GetStringSelection())
        self.assertEqual('Case1', sw.GetStringSelection().xval())
        sw.SetSelection('second')
        self.assertEqual('second', sw.GetSelection())
        self.assertEqual('test2', sw.GetStringSelection())
        # info
        #self.assertRaises(BadParameter, sw.SetInfo, 'blah', 'i') # no longer produces an error; just silently ignored
        self.assertEqual(sw.kDisplayIndent+'Case2', sw.display[2])
        self.assertEqual({}, sw.infos)
        sw.SetInfo('test2', 'unwary')
        self.assertEqual({'test2':'unwary'}, sw.infos)
        self.assertEqual(sw.kDisplayIndent+'Case2 [unwary]', sw.display[2])
        sw.SetInfo('test2', None)
        self.assertEqual({}, sw.infos)
        self.assertEqual(sw.kDisplayIndent+'Case2', sw.display[2])
        sw.SetInfo('test2', None)
        self.assertEqual({}, sw.infos)
        # alias
        #self.assertRaises(BadParameter, sw.SetAlias, 'blah', 'a') # no longer produces an error; just silently ignored
        self.assertEqual({}, sw.aliases)
        sw.SetAlias('test1', 't1')
        self.assertEqual({'test1':'t1'}, sw.aliases)
        self.assertEqual(sw.kDisplayIndent+'Case1 (t1)', sw.display[1])
        sw.SetAlias('test1', None)
        self.assertEqual({}, sw.aliases)
        self.assertEqual(sw.kDisplayIndent+'Case1', sw.display[1])
        sw.SetAlias('test1', None)
        self.assertEqual({}, sw.aliases)

    def test_20_multi(self):
        sw = deity.widgets.SelectableItemList(Test_Win, 200, style=wx.LB_MULTIPLE)
        il = xdict(xsepkey='/', xlatekey=(None, 'test:',''))
        il['1/a/test1'] = 'first'
        il['1/a/test2'] = 'second'
        il['1/c/test3'] = 'third'
        sw.SetItems(il)
        self.assertEqual(['B:',sw.kDisplayIndent+'Case1',sw.kDisplayIndent+'Case2','D:',sw.kDisplayIndent+'Case3'], sw.display)
        self.assertEqual(1, sw.IndexFromNamex('test1'))
        self.assertEqual(2, sw.IndexFromValue('second'))
        self.assertEqual(4, sw.IndexFromValue(il['1/c/test3']))
        self.assertEqual('1/a/test1', sw.GetString(1))
        self.assertEqual('1/B/Case1', sw.GetString(1).xval())
        self.assertEqual([], sw.GetSelections())
        self.assertEqual([], sw.GetStringSelections())
        sw.SetStringSelection('test1')
        self.assertEqual(['first'], sw.GetSelections())
        self.assertEqual(['test1'], sw.GetStringSelections())
        self.assertEqual('Case1', sw.GetStringSelections()[0].xval())
        sw.SetSelection('second')
        self.assertEqual(['first','second'], sw.GetSelections())
        self.assertEqual(['test1','test2'], sw.GetStringSelections())
        sw.SetStringSelection('test1', False)
        self.assertEqual(['second'], sw.GetSelections())
        self.assertEqual(['test2'], sw.GetStringSelections())
        sw.SetSelection('second', False)
        self.assertEqual([], sw.GetSelections())
        self.assertEqual([], sw.GetStringSelections())


###############################################################################


class InfoTableTest(deity.test.TestCase):

    def test_10_init(self):
        it = deity.widgets.InfoTable(Test_Win)

    def test_20_fill(self):
        kv = xdict(xlatekey='test:', xlateval='')
        kv['a'] = 'test1'
        kv['c'] = 'test2'
        kv['e'] = 'test3'
        ko = ('e', 'c', 'a')
        it = deity.widgets.InfoTable(Test_Win, items=kv, order=ko)


###############################################################################


class InfoTableTest(deity.test.TestCase):

    def test_10_init(self):
        it = deity.widgets.InfoTable(Test_Win)

    def test_20_fill(self):
        kv = xdict(xlatekey='test:', xlateval='')
        kv['a'] = 'test1'
        kv['c'] = 'test2'
        kv['e'] = 'test3'
        ko = ('e', 'c', 'a')
        it = deity.widgets.InfoTable(Test_Win, items=kv, order=ko)


###############################################################################


class EditListTest(deity.test.TestCase):

    def test_10_init(self):
        it = deity.widgets.EditList(Test_Win, '888')

    def test_20_fill(self):
        kv = xdict(xlatekey='test:', xlateval='')
        kv['a'] = '11'
        kv['c'] = '22'
        kv['e'] = '33'
        ko = xlist(xlate='test:')
        ko.extend(('e', 'c', 'a'))
        it = deity.widgets.EditList(Test_Win, '888', labels=ko)
        it.SetItems(kv, ko)
        self.assertTrue('a' in it.keywidgetmap)

    def test_30_change(self):
        kv = xdict({'a':'11', 'b':'22', 'c':'33'})
        it = deity.widgets.EditList(Test_Win, '888')
        it.SetItems(kv)
        it.Change('a', '24')
        self.assertEqual('24', kv['a'])

    def test_35_edit(self):
        kv = xdict({'a':'11', 'b':'22', 'c':'33'})
        it = deity.widgets.EditList(Test_Win, '888')
        it.SetItems(kv)
        aw = it.keywidgetmap['a']
        aw.SetValue('42')
        ev = wx.FocusEvent()
        ev.SetEventObject(aw)
        it.OnKillFocus(ev)
        self.assertEqual('42', kv['a'])

    def test_40_valmgr(self):
        vm = deity.editor.ValueManager()
        vm.SetValue('x', '11')
        vm.SetValue('y', '22')
        vm.SetValue('z', '33')
        kv = xdict({'a':'x', 'b':'y', 'c':'z'})
        it = deity.widgets.EditList(Test_Win, '888', vm)
        it.SetItems(kv)
        aw = it.keywidgetmap['a']
        self.assertEqual('11', vm.GetValue('x'))
        self.assertEqual('11', aw.GetValue())
        aw.SetValue('42')
        ev = wx.FocusEvent()
        ev.SetEventObject(aw)
        it.OnKillFocus(ev)
        self.assertEqual('x', kv['a'])
        self.assertEqual('42', vm.GetValue('x'))
        vm.ChangeValue('x', '99')
        self.assertEqual('99', aw.GetValue())


###############################################################################


if __name__ == '__main__':
    unittest.main()
