#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test "gettext" functions)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.gettext


###############################################################################


class GetTextTest(deity.test.TestCase):

    def test_gettext(self):
        self.assertEqual('foo', _('foo'))
        self.assertEqual('bar', gettext('bar'))
        self.assertEqual('Case1', _('test1'))
        self.assertEqual('Case2', gettext('test2'))

    def test_ngettext(self):
        self.assertEqual('bar',   ngettext('foo', 'bar', 0))
        self.assertEqual('foo',   ngettext('foo', 'bar', 1))
        self.assertEqual('bar',   ngettext('foo', 'bar', 2))
        self.assertEqual('Case2', ngettext('test1', 'test2', 0))
        self.assertEqual('Case1', ngettext('test1', 'test2', 1))
        self.assertEqual('Case2', ngettext('test1', 'test2', 2))

    def test_gettext_p(self):
        self.assertEqual('foo', NP_('test:foo'))
        self.assertEqual('bar', P_('test:bar'))
        self.assertEqual('too', P_('test:','too'))
        self.assertEqual('sna-foo', P_('test:','sna-foo'))
        self.assertEqual('a', NP_('test:a'))
        self.assertEqual('D', P_('test:c'))
        self.assertEqual('F', P_('test:','e'))
        self.assertEqual('Case1', P_('','test1'))
    
    def test_gettext_n(self):
        self.assertEqual('foo', N_('foo'))
        self.assertEqual('test1', N_('test1'))
    

###############################################################################


if __name__ == '__main__':
    unittest.main()
