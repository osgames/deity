#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test game window)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.windows
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *

import wx
Test_App = wx.PySimpleApp()
#Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class MainTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.harnmaster.HarnMaster3()
        self.app = deity.test.App()

    def CommandFunc(self, *ignore):
        self.commandCall += 1

    def test_10_commands(self):
        self.commandCall = 0
        mw = deity.windows.Main(testmode=True)
        mw.CommandReset()
        mw.CommandRegister("doit", self.CommandFunc, "test", "do it")
        self.assertEqual(1, mw.menuActions.GetMenuItemCount())
        self.assertEqual(1, len(mw.menuActionsFuncs))
        mw.CommandExecute("doit")
        self.assertEqual(1, self.commandCall)
        mw.OnMenuActions(wx.CommandEvent(0,mw.menuActionsFuncs.keys()[0]))
        self.assertEqual(2, self.commandCall)

    def test_20_cmdinject(self):
        mw = deity.windows.Main(testmode=True)
        mw.io.SetInputData("")
        self.assertEqual("", mw.io.GetInputData())
        mw.CommandInject("help ")
        self.assertEqual("help ", mw.io.GetInputData())
        mw.CommandInject("combat", execute=True)


###############################################################################


if __name__ == '__main__':
    unittest.main()
