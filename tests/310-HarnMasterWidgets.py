#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test HarnMaster widgets)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.watcher import *
from deity.xtype import *
from deity.exceptions import *

import wx
Test_WxApp = wx.PySimpleApp()
Test_DApp  = deity.test.App()
Test_Mode  = deity.test.Mode(None, Test_DApp)
Test_Game  = deity.gamesystem.harnmaster.HarnMaster3()

###############################################################################


class CombatAttackWidgetTest(deity.test.TestCase):

    def command(self, cmd, *opts):
        self.cmd = cmd
        self.opts = opts

    def setUp(self):
        deity.test.TestCase.setUp(self)
        try:
            self.hw1a = GAME.MatchCharacterName('hw1a')
        except:
            self.hw1a = GAME.Character(None)
            self.hw1a.Generate('human')
            self.hw1a.Rename('hw1a')
            GAME.AddCharacter(self.hw1a, 'encounter')
            self.hw1a.SetSkill('initiative', 90)
        try:
            self.hw1b = GAME.MatchCharacterName('hw1b')
        except:
            self.hw1b = GAME.Character(None)
            self.hw1b.Generate('orc-small')
            self.hw1b.Rename('hw1b')
            GAME.AddCharacter(self.hw1b, 'encounter')
            self.hw1b.SetSkill('initiative', 80)
        self.curchar = Watched(None)
        self.ca = GAME.CombatAttackWidget(Test_Mode, self.curchar)

    def test_10_FieldRefresh(self):
        last = dict()
        self.assertRaises(Exception, self.ca.FieldRefresh, last, None, 'nothing')

    def test_20_DisplayRefresh(self):
        self.ca.DisplayRefresh()
        self.curchar.Set(self.hw1a)
        self.ca.DisplayRefresh()

    def test_50_SetParticipants(self):
        cl = (self.hw1a, self.hw1b)
        self.ca.SetParticipants(cl)
        self.assertEqual(self.hw1a.Name(), self.ca.dname.GetString(0))
        self.assertEqual(self.hw1b, self.ca.dname.GetClientData(1))
        self.ca.DisplayRefresh()

    def test_90_OnAttack(self):
        Test_DApp.CommandRegister('attack', self.command)
        self.test_50_SetParticipants()
        self.curchar.Set(self.hw1a)
        self.ca.DisplayRefresh()
        self.ca.aweapon.SetSelection(0)
        self.ca.OnAWeaponChange(None)
        self.ca.aloc.SetSelection(1)
        self.ca.amodify.SetValue('1')
        self.ca.aroll.SetValue('2')
        self.ca.dname.SetSelection(1)
        self.ca.OnDNameChange(None)
        self.ca.dcmd.SetStringSelection('counterstrike')
        self.ca.OnDCmdChange(None)
        self.ca.dweapon.SetSelection(0)
        self.ca.OnDWeaponChange(None)
        self.ca.dloc.SetSelection(3)
        self.ca.dmodify.SetValue('-4')
        self.ca.droll.SetValue('8')
        self.ca.OnAttack(None)
        self.assertEqual('attack', self.cmd)
        self.assertEqual(('hw1b','righthand:blunt','high','+1','2','defend',
                          'counterstrike','righthand:blunt','low','-4','8'),
                         self.opts)


###############################################################################


if __name__ == '__main__':
    unittest.main()
