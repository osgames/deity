#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (verify beastiary completeness)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.windows
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class BeastiaryCompletenessTester():

    def test_10_equipment(self):
        for t in self.gs.equipTypeDict.itervalues():
            x = self.gs.Item(None)
            x.Generate(xstr(t), allopts=True)
            xname = x.Name()
            self.assertLess(0, float(x.GetInfo('weight')), xname)

            if x.IsA('_meeleeweapon'):
                self.assertIsNotNone(x.GetInfo('skill'), xname)
                self.assertLess(0, x.GetAttribute('quality'), xname)
                self.assertIsNotNone(x.GetAttribute('attack'), xname)
                self.assertIsNotNone(x.GetAttribute('defend'), xname)
                found = False
                for aspect in deity.gamesystem.GameSystem.kWeaponAspects:
                    astr = x.GetAttribute(aspect)
                    if astr is not None:
                        found = True
                        aval = int(astr)
                self.assertTrue(found, xname)

            if x.IsA('_meeleedefense'):
                self.assertIsNotNone(x.GetInfo('skill'), xname)
                self.assertLess(0, x.GetAttribute('quality'), xname)
                self.assertIsNotNone(x.GetAttribute('attack'), xname)
                self.assertIsNotNone(x.GetAttribute('defend'), xname)


    def test_20_beasts(self):
	print ""
        for t in self.gs.charTypeDict.itervalues():
            #print t
            x = self.gs.Character(None)
            x.Generate(xstr(t), allopts=True)
            xname = x.Name()
	    info = dict(type=t, name=xname)
	    self.assertTrue(x.IsA('_Any'), '%(type)s is not derived from "_Any"' % info)

            # verify basic info
            for i in ('height', 'weight'):
                ival = x.GetInfo(i)
		info["key"] = i
		info["val"] = ival
                if ival is not None:
                    try:
                        self.assertLess(0, float(ival), '%(type)s has info "%(key)s" value %(val)s less than 1' % info)
                    except ValueError:
                        self.assertTrue(0, '%(type)s has info "%(key)s" invalid numerical value "%(val)s"' % info)

            # verify attributes
            for a in self.gs.kBaseAttributes + self.gs.kMoreAttributes:
                aval = x.GetAttribute(a)
		info["key"] = a
		info["val"] = aval
                if aval is not None:
                    self.assertLessEqual(0, aval, '%(type)s has attribute "%(key)s" value %(val)d less than 1' % info)

            # verify hit zones
            #print ''
            #print '===>',t,'<==='
            hitlocs = x.GetTypeInfo('Locations', asdict=True)
            #print 'hitlocs:',hitlocs
            self.assertNotEqual(None, hitlocs, xname)
            hitranges = x.GetTypeInfo('Hitranges', aslist=True, keysonly=True, default=[])
            hitranges.append('')
            #print 'hitranges:',hitranges
            for r in hitranges:
		info["area"] = r
                if r:
                    locs = x.GetTypeInfo('Hit-%s'%r, aslist=True)
                else:
                    locs = x.GetTypeInfo('Hit', aslist=True)
                #print ' -',r,'=>',locs
                self.assertNotEqual(None, locs, xname)
                for l in locs:
                    n,s,l = l.partition('=')
		    info["loc"] = l
                    for i in deity.MultipleChoiceExpand(l):
			self.assertIn(i, hitlocs, '%(type)s has no hit-location %(loc)s (referenced by area "%(area)s")' % info)

            # verify wield zones
            wlocs = x.GetTypeInfo(deity.beastiary.kWieldKey, aslist=True, keysonly=True)
            if wlocs:
                for i in wlocs:
		    self.assertIn(i, hitlocs, '%(type)s has no location %(loc)s (referenced by wield locations)' % info)


###############################################################################


class BestiaryCompletenessTest_HarnMaster(deity.test.TestCase, BeastiaryCompletenessTester):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs = Game = deity.gamesystem.harnmaster.HarnMaster3()


###############################################################################


if __name__ == '__main__':
    unittest.main()
