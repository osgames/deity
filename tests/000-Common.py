#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test common functions)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.trandom


###############################################################################


class RollTest(deity.test.TestCase):

    def test_roll(self):
        for i in xrange(0,100):
            r = deity.Roll(1,6)
            self.assertTrue(1 <= r <= 6)
            r = deity.Roll(1,20)
            self.assertTrue(1 <= r <= 20)
            r = deity.Roll(3,6)
            self.assertTrue(1 <= r <= 18)
            r = deity.Roll('1d6')
            self.assertTrue(1 <= r <= 6)
            r = deity.Roll('d20')
            self.assertTrue(1 <= r <= 20)
            r = deity.Roll('3d6')
            self.assertTrue(1 <= r <= 18)
            d = list()
            r = deity.Roll(6,6,d)
            self.assertTrue(1 <= r <= 36)
            self.assertEqual(6,len(d))
            for r in d:
                self.assertTrue(1 <= r <= 6)

    def test_roll_random(self):
        rand = deity.trandom.Random()
        rand.testset('roll', 1)
        rand.testset('roll', 2)
        rand.testset('roll', 3)
        self.assertEqual(6, deity.Roll('3d6', rand=rand, test='roll'))
        rand.testend()

    def test_mcr(self):
        self.assertEqual(None, deity.MultipleChoiceResolution(None))
        self.assertEqual('abc', deity.MultipleChoiceResolution('abc'))
        self.assertEqual('xyz', deity.MultipleChoiceResolution('x(y)z'))
        self.assertEqual('xyz', deity.MultipleChoiceResolution('x( y )z'))
        for i in xrange(0,20):
            v = deity.MultipleChoiceResolution('a(x| y |z)b')
            self.assertTrue(v == 'axb' or v == 'ayb' or v == 'azb')

    def test_mcr_trandom(self):
        rand = deity.trandom.Random()
        rand.testset('test', 0)
        rand.testset('test', 1)
        rand.testset('test', 2)
        self.assertEqual('axb', deity.MultipleChoiceResolution('a(x|y|z)b', rand=rand, test='test'))
        self.assertEqual('ayb', deity.MultipleChoiceResolution('a(x|y|z)b', rand=rand, test='test'))
        self.assertEqual('azb', deity.MultipleChoiceResolution('a(x|y|z)b', rand=rand, test='test'))
        rand.testend()

    def test_lkl_direct(self):
        items = ['1','2','3','5','8']
        self.assertEqual('1', deity.ListKeyLookup(items, 0))
        self.assertEqual('1', deity.ListKeyLookup(items, 1))
        self.assertEqual('2', deity.ListKeyLookup(items, 2))
        self.assertEqual('3', deity.ListKeyLookup(items, 3))
        self.assertEqual('5', deity.ListKeyLookup(items, 4.0))
        self.assertEqual('5', deity.ListKeyLookup(items, 5.0))
        self.assertEqual('8', deity.ListKeyLookup(items, 5.1))
        self.assertEqual(None,deity.ListKeyLookup(items, 9))

    def test_lkl_assigned(self):
        items = ['1=a','2=b','3=c','5=d','8=e']
        self.assertEqual('a', deity.ListKeyLookup(items, 0))
        self.assertEqual('a', deity.ListKeyLookup(items, 1))
        self.assertEqual('b', deity.ListKeyLookup(items, 2))
        self.assertEqual('c', deity.ListKeyLookup(items, 3))
        self.assertEqual('d', deity.ListKeyLookup(items, 4.0))
        self.assertEqual('d', deity.ListKeyLookup(items, 5.0))
        self.assertEqual('e', deity.ListKeyLookup(items, 5.1))
        self.assertEqual(None,deity.ListKeyLookup(items, 9))


###############################################################################


class DefaultTest(deity.test.TestCase):

    def test_int(self):
        self.assertEqual(1, deity.DefaultInt('1'))
        self.assertEqual(2, deity.DefaultInt(None, default=2))
        self.assertEqual(3, deity.DefaultInt('3.3', default=2))
        self.assertEqual(4, deity.DefaultInt('a', default=4))

    def test_float(self):
        self.assertEqual(1.1, deity.DefaultFloat('1.1'))
        self.assertEqual(2.2, deity.DefaultFloat(None, default=2.2))
        self.assertEqual(3.3, deity.DefaultFloat('3.3', default=2.2))
        self.assertEqual(4.4, deity.DefaultFloat('a', default=4.4))


###############################################################################


if __name__ == '__main__':
    unittest.main()
