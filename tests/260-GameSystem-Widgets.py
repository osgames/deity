#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test gamesystem widgets)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.editor
import deity.modes
import deity.gamesystem

from deity.xtype import *
from deity.exceptions import *
from deity.watcher import *

import wx
Test_App = wx.PySimpleApp()
Test_Win = wx.Frame(None, wx.ID_ANY, 'deity test')


###############################################################################


class CharListWidgetTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.GameSystem()
        self.app = deity.test.App()

    def test_10_init(self):
        cw = Watched(None)
        mw = self.gs.CharListWidget(Test_Win, cw)
        mw.DisplayRefresh()

    def test_20_withchar(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testhuman'), 'superman')
        ch.SetInfo('status','prone')
        self.gs.AddCharacter(ch, 'encounter')
        self.assertEqual(['z/encounter/superman'], self.gs.gameCharDisplay.keys())
        cw = Watched(None)
        mw = self.gs.CharListWidget(Test_Win, cw, status=True)
        mw.DisplayRefresh()
        self.assertEqual(None, mw.GetSelection())
        ch.WatchedBy(cw)
        mw.DisplayRefresh()
        self.assertEqual(ch, mw.GetSelection())

    def test_30_multichar(self):
        ch1 = deity.gamesystem.GameSystem.Character(None)
        ch1.Generate(xstr('_testhuman'), 'superman')
        ch1.SetInfo('status','prone')
        ch2 = deity.gamesystem.GameSystem.Character(None)
        ch2.Generate(xstr('_testhuman'), 'batman')
        self.gs.AddCharacter(ch1, 'encounter')
        self.gs.AddCharacter(ch2, 'encounter')
        gcs = self.gs.gameCharDisplay.keys()
        gcs.sort()
        self.assertEqual(['z/encounter/batman','z/encounter/superman'], gcs)
        cw = Watched(None)
        mw = self.gs.CharListWidget(Test_Win, cw, status=True, style=wx.LB_MULTIPLE)
        mw.DisplayRefresh()
        self.assertEqual([], mw.GetSelections())


###############################################################################


class CharVisualWidgetTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.GameSystem()
        self.app = deity.test.App()

    def test_10_init(self):
        cw = Watched(None)
        mw = self.gs.CharVisualWidget(Test_Win, cw)
        mw.DisplayRefresh()

    def test_20_withchar(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testhuman'), 'superman')
        ch.SetValue('x:armor-head', 'hole') 
        ch.SetList('wounds', ['nothing'])
        cw = Watched(None)
        mw = self.gs.CharVisualWidget(Test_Win, cw)
        mw.DisplayRefresh()
        ch.WatchedBy(cw)
        mw.DisplayRefresh()
        self.assertEqual('wounds: nothing\n\nwielded: NOTHING\n\n', mw.desc.GetValue())
        it1 = deity.gamesystem.GameSystem.Item(None)
        it1.Generate(xstr('_testvest(protection=+1)'))
        ch.AddInventory(it1)
        it2 = deity.gamesystem.GameSystem.Item(None)
        it2.Generate(xstr('_testknife(attack=+2)'))
        ch.AddInventory(it2)
        it3 = deity.gamesystem.GameSystem.Item(None)
        it3.Generate(xstr('_testdagger'))
        gnd = deity.gamesystem.GameSystem.Thing(deity.gamesystem.GameSystem.kCombatGroundTid)
        gnd.AddInventory(it3)
        mw.DisplayRefresh()
        self.assertEqual('wounds: nothing\n\n'
                         'wielded: NOTHING\n\n'
                         'equipment: testknife(attack=+2), testvest(protection=+1)\n\n'
                         'ground: testdagger\n\n',
                         mw.desc.GetValue())
        ch.WearItem(it1)
        ch.WieldItem(it2, 'righthand')
        mw.DisplayRefresh()
        self.assertEqual('wounds: nothing\n\n'
                         'wielded: testknife(attack=+2)@righthand\n\n'
                         'worn: testvest(protection=+1)\n\n'
                         'ground: testdagger\n\n',
                         mw.desc.GetValue())


###############################################################################


class CharStatsWidgetTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.GameSystem()
        self.app = deity.test.App()

    def test_10_init(self):
        cw = Watched(None)
        mw = self.gs.CharVisualWidget(Test_Win, cw)
        mw.DisplayRefresh()


###############################################################################


class CharGenerateWidgetTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.gs  = deity.gamesystem.GameSystem()
        self.app = deity.test.App()

    def test_10_init(self):
        mw = self.gs.CharGenerateWidget(Test_Win)
        mw.DisplayRefresh()

    def test_20_select(self):
        self.gs.charTypeDict['testtype'] = 42
        mw = self.gs.CharGenerateWidget(Test_Win)
        mw.DisplayRefresh()
        self.assertEqual(None, mw.GetSelection())
        self.assertEqual(None, mw.GetStringSelection())
        mw.SetStringSelection('testtype')
        self.assertEqual(42, mw.GetSelection())
        self.assertEqual('testtype', mw.GetStringSelection())


###############################################################################


class CharEditWidgetTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.vm  = deity.editor.ValueManager()
        self.gs  = deity.gamesystem.GameSystem()
        self.app = deity.test.App()

        self.gs.kGeneralInfos = xlist(('height', 'weight', 'frame'))
        self.gs.kBaseAttributes = xlist(('str', 'dex', 'int'))
        self.gs.kMoreAttributes = xlist()

    def test_10_init(self):
        cw = Watched(None)
        mw = self.gs.CharEditWidget(Test_Win, cw)
        mw.DisplayRefresh()

    def test_20_char(self):
        ch = deity.gamesystem.GameSystem.Character(None)
        ch.Generate(xstr('_testhuman'), 'superman')
        ci = ch.GetInfos()
        ca = ch.GetAttributes()
        cs = ch.GetSkills()
        cw = Watched(ch)
        mw = self.gs.CharEditWidget(Test_Win, cw)
        mw.DisplayRefresh()
        self.assertEqual(ci['height'], mw.valuemgr.GetValue('i:height'))
        self.assertEqual(str(ca['str']), mw.valuemgr.GetValue('a:str'))
        self.assertEqual(str(cs['unarmed']), mw.valuemgr.GetValue('s:unarmed'))
        mw.valuemgr.ChangeValue('a:str', str(int(ca['str'])+3))
        self.assertEqual(str(int(ca['str'])+3), mw.valuemgr.GetValue('a:str'))
        self.assertEqual(str(int(cs['unarmed'])+3), mw.valuemgr.GetValue('s:unarmed'))


###############################################################################


if __name__ == '__main__':
    unittest.main()
