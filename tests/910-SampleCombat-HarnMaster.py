#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (run full combat)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.trandom
import deity.windows
import deity.gamesystem
import deity.gamesystem.harnmaster

from deity.xtype import *
from deity.exceptions import *

import __builtin__
import wx
App  = wx.PySimpleApp()
Game = deity.gamesystem.harnmaster.HarnMaster3()
Main = deity.windows.Main(testmode=True);
Main.testmode = True
__builtin__.__dict__['Output'] = Main.Output


###############################################################################


class CombatTest_Human_Unarmed(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        try:
            self.ct1a = GAME.MatchCharacterName('ct1a')
            GAME.SubCharacter(self.ct1a)
        except:
            pass

        self.ct1a = GAME.Character(None)
        self.ct1a.Generate('human')
        self.ct1a.Rename('ct1a')
        GAME.AddCharacter(self.ct1a, 'encounter')
        self.ct1a.SetSkill('initiative', 90)
        self.ct1a.SetValue('x:promptunarmed', 'no')

        try:
            self.ct1b = GAME.MatchCharacterName('ct1b')
            GAME.SubCharacter(self.ct1b)
        except:
            pass

        self.ct1b = GAME.Character(None)
        self.ct1b.Generate('orc-small')
        self.ct1b.Rename('ct1b')
        self.ct1b.SetValue('x:promptunarmed', 'no')
        GAME.AddCharacter(self.ct1b, 'encounter')
        self.ct1b.SetSkill('initiative', 80)

        Main.DisplayRefresh()

    def test_round(self):
        random = deity.trandom.Random()
        Main.CommandExecute('combat')
        Main.CommandExecute('add ct1a')
        Main.CommandExecute('add ct1b')
        while self.ct1a.GetInfo('status') != 'dead' and self.ct1b.GetInfo('status') != 'dead':
            #print 'begin round...',self.ct1a.GetInfo('status'),self.ct1b.GetInfo('status')
            Main.CommandExecute('round')

            while Main.modeCombat.submode == 'round':
                if Main.curchar.Get() == self.ct1a:
                    attack = self.ct1a
                    defend = self.ct1b
                else:
                    attack = self.ct1b
                    defend = self.ct1a
                aname = attack.Name()
                dname = defend.Name()

                deity.test.TestUserResponse('kill?', True)
                deity.test.TestUserResponse('amputate?', True)
                deity.test.TestUserResponse('bodyblock?', True)
                if attack.GetInfo('status') == 'unconscious' or attack.GetInfo('status') == 'shock' or attack.GetInfo('status') == 'dead':
                    command = 'pass'
                elif attack.GetInfo('status') == 'prone' and random.randint(1,4) <= 3:
                    command = 'stand'
                elif defend.GetInfo('status') == 'unconscious' or defend.GetInfo('status') == 'shock':
                    deity.test.TestUserResponse('attackprone?', True)
                    command = 'attack %s defend ignore' % dname
                elif defend.GetInfo('status') == 'prone':
                    deity.test.TestUserResponse('attackprone?', True)
                    command = 'attack %s defend dodge' % dname
                elif defend.GetInfo('status') == 'dead': # bled out
                    command = 'pass'
                elif random.randint(1,10) <= 3:
                    command = 'busy %ds' % random.randint(0,15)
                else:
                    rval = random.randint(1,6)
                    if rval <= 4:
                        attack = 'attack'
                    else:
                        attack = 'grapple'
                    rval = random.randint(1,8)
                    if rval <= 3:
                        defend = 'dodge'
                    elif rval <= 5:
                        defend = 'block'
                    elif rval <= 7:
                        defend = 'counterstrike'
                    else:
                        defend = 'grapple'
                    command = '%s %s defend %s' % (attack, dname, defend)

                Main.io.output.Clear()
                Main.CommandExecute(command)
                print Main.io.output.GetValue()

        #App.MainLoop()


###############################################################################


class CombatTest_NonHuman1(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        GAME.rules['CombatAmputate'] = 'no'

        try:
            self.ct2a = GAME.MatchCharacterName('ct2a')
            GAME.SubCharacter(self.ct2a)
        except:
            pass

        self.ct2a = GAME.Character(None)
        self.ct2a.Generate('aklash')
        self.ct2a.Rename('ct2a')
        GAME.AddCharacter(self.ct2a, 'encounter')

        try:
            self.ct2b = GAME.MatchCharacterName('ct2b')
            GAME.SubCharacter(self.ct2b)
        except:
            pass

        self.ct2b = GAME.Character(None)
        self.ct2b.Generate('human-fighter')
        self.ct2b.Rename('ct2b')
        GAME.AddCharacter(self.ct2b, 'encounter')
        item = GAME.Item(None)
        item.Generate('broadsword')
        self.ct2b.AddInventory(item)
        self.ct2b.WieldItem(item, defindex=0)
        item = GAME.Item(None)
        item.Generate('kiteshield')
        self.ct2b.AddInventory(item)
        self.ct2b.WieldItem(item, defindex=1)

        try:
            self.ct2c = GAME.MatchCharacterName('ct2c')
            GAME.SubCharacter(self.ct2c)
        except:
            pass

        self.ct2c = GAME.Character(None)
        self.ct2c.Generate('human-fighter')
        self.ct2c.Rename('ct2c')
        GAME.AddCharacter(self.ct2c, 'encounter')
        item = GAME.Item(None)
        item.Generate('broadsword')
        self.ct2c.AddInventory(item)
        self.ct2c.WieldItem(item, defindex=0)
        item = GAME.Item(None)
        item.Generate('kiteshield')
        self.ct2c.AddInventory(item)
        self.ct2c.WieldItem(item, defindex=1)

        Main.DisplayRefresh()

    def test_round(self):
        random = deity.trandom.Random()
        Main.CommandExecute('combat')
        Main.CommandExecute('add ct2a')
        Main.CommandExecute('add ct2b')
        Main.CommandExecute('add ct2c')

        while ((self.ct2a.GetInfo('status') != 'dead') and
               (self.ct2b.GetInfo('status') != 'dead' or self.ct2c.GetInfo('status') != 'dead')):
            #print 'begin round...',self.ct2a.GetInfo('status'),self.ct2b.GetInfo('status')
            Main.CommandExecute('round')

            while Main.modeCombat.submode == 'round':
                if Main.curchar.Get() == self.ct2a:
                    attack = self.ct2a
                    defend = None
                    while defend is None:
                        if random.randint(0,1) == 0:
                            if self.ct2b.GetInfo('status') == 'standing' or self.ct2c.GetInfo('status') != 'standing':
                                defend = self.ct2b
                        else:
                            if self.ct2c.GetInfo('status') == 'standing' or self.ct2b.GetInfo('status') != 'standing':
                                defend = self.ct2c
                    dloc   = ''
                    rval   = random.randint(0,5)
                    if rval == 0:
                        aloc = 'face:'
                    elif rval < 2:
                        aloc = 'lefthand:'
                    else:
                        aloc = 'righthand:'
                    rval   = random.randint(0,5)
                    if rval == 0:
                        dmove = 'counterstrike'
                    elif rval < 2:
                        dmove = 'block'
                    else:
                        dmove = 'dodge'
                else:
                    attack = Main.curchar.Get()
                    defend = self.ct2a
                    aloc   = ''
                    dloc   = ''
                    rval   = random.randint(0,5)
                    if rval == 0:
                        dmove = 'counterstrike'
                        if random.randint(0,1) == 0:
                            dloc = 'righthand'
                        else:
                            dloc = 'leftforearm'
                    elif rval < 2:
                        dmove = 'block'
                        if random.randint(0,1) == 0:
                            dloc = 'righthand'
                        else:
                            dloc = 'leftforearm'
                    else:
                        dmove = 'dodge'
                aname = attack.Name()
                dname = defend.Name()

                deity.test.TestUserResponse('kill?', True)
                if attack.GetInfo('status') == 'unconscious' or attack.GetInfo('status') == 'shock' or attack.GetInfo('status') == 'dead':
                    Main.CommandExecute('pass')
                elif attack.GetInfo('status') == 'prone':
                    Main.CommandExecute('stand')
                elif defend.GetInfo('status') == 'unconscious' or defend.GetInfo('status') == 'shock':
                    deity.test.TestUserResponse('attackprone?', True)
                    Main.CommandExecute('attack %s righthand: defend ignore' % dname)
                elif defend.GetInfo('status') == 'prone':
                    deity.test.TestUserResponse('attackprone?', True)
                    Main.CommandExecute('attack %s righthand: defend dodge' % dname)
                elif defend.GetInfo('status') == 'dead': # bled out
                    Main.CommandExecute('pass')
                else:
                    #print('%(aname)s: attack %(dname)s %(aloc)s defend %(dmove)s %(dloc)s' %
                    #      {'aname':aname, 'dname':dname, 'aloc':aloc, 'dmove':dmove, 'dloc':dloc})
                    Main.io.output.Clear()
                    Main.CommandExecute('attack %(dname)s %(aloc)s defend %(dmove)s' %
                                        {'dname':dname, 'aloc':aloc, 'dmove':dmove, 'dloc':dloc})
                    print Main.io.output.GetValue()
        #App.MainLoop()


###############################################################################


if __name__ == '__main__':
    unittest.main()
