#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test "gettext" functions)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.xtype

from deity.exceptions import *


###############################################################################


class X_10_String_Test(deity.test.TestCase):

    def test_00_init(self):
        self.assertEqual('', deity.xtype.xstr())
        self.assertEqual('test', deity.xtype.xstr('test'))

    def test_05_not(self):
        s = deity.xtype.xstr()
        self.assertTrue(not s)
        s = deity.xtype.xstr('')
        self.assertTrue(not s)
        s = deity.xtype.xstr('blah')
        self.assertFalse(not s)

    def test_10_xstr(self):
        x1 = deity.xtype.xstr('u', xlate='a', xpart='b', xsep='c')
        x2 = x1.xstr('v')
        self.assertTrue(isinstance(x2, deity.xtype.xstr))
        self.assertEqual(x1.xlate, x2.xlate)
        self.assertEqual(x1.xpart, x2.xpart)
        self.assertEqual(x1.xsep,  x2.xsep)
        self.assertEqual('v', x2)

    def test_11_slice(self):
        x1 = deity.xtype.xstr('abcdefghi')
        x2 = x1[3:6]
        self.assertEqual('def', x2)
        #self.assertTrue(isinstance(x2, deity.xtype.xstr))

    def test_20_xval(self):
        self.assertEqual('test1', deity.xtype.xstr('test1').xval())
        self.assertEqual('Case1', deity.xtype.xstr('test1', '').xval())
        self.assertEqual('B', deity.xtype.xstr('a', 'test:').xval())
        self.assertEqual('D', deity.xtype.xstr('c', xlate='test:').xval())
        self.assertEqual('', deity.xtype.xstr('', xlate='test:').xval())

    def test_21_xprefix(self):
        self.assertTrue(deity.xtype.xstr('test1').xprefix('test'))
        self.assertTrue(deity.xtype.xstr('test1', xlate='').xprefix('case'))

    def test_30_xmultistr(self):
        x1 = deity.xtype.xstr('a/a/test1', xlate=('test:',None,''), xsep='/')
        self.assertEqual('B/a/Case1', x1.xval())

    def test_40_xlate(self):
        xm = deity.xtype.xlist(xsep='/', xlate=('',None,'test:','test:'))
        self.assertEqual('Case1/a/Z/foo', xm._xlate('test1/a/1/foo'))
        self.assertEqual('', xm._xlate(''))

    def test_50_xenumerate(self):
        xm = deity.xtype.xlist(xsep='/', xlate=('',None,'test:','test:'))
        xm.extend(['test1/a/1/foo', 'test2/c/2/bar'])
        self.assertEqual({'test1/a/1/foo':'Case1/a/Z/foo', 'test2/c/2/bar':'Case2/c/Y/bar'}, dict(xm.xenumerate()))
        self.assertEqual({0:'Case1/a/Z/foo', 1:'Case2/c/Y/bar'}, dict(xm.xenumerate(index=True)))

    def test_55_xitems(self):
        xm = deity.xtype.xlist(xsep='/', xlate=('',None,'test:','test:'))
        xm.extend(['test1/a/1/foo', 'test2/c/2/bar'])
        self.assertEqual(['Case1/a/Z/foo', 'Case2/c/Y/bar'], xm.xitems())

    def test_60_xparts(self):
        xs = deity.xtype.xstr('test1/a/1/foo', xsep='/', xlate=('',None,'test:','test:'))
        xl = xs.xsplit()
        self.assertEqual(4, len(xl))
        self.assertEqual('test1', xl[0])
        self.assertEqual('Case1', xl[0].xval())
        self.assertEqual('a', xl[1])
        self.assertEqual('a', xl[1].xval())
        self.assertEqual('1', xl[2])
        self.assertEqual('Z', xl[2].xval())
        self.assertEqual('foo', xl[3])
        self.assertEqual('foo', xl[3].xval())

    def test_70_xlists(self):
        xm = deity.xtype.xlist(xsep='/', xlate=('','test:',None,'test:'))
        xm.extend(['test1/1/a/foo', 'test1/2/c/bar'])
        xl = list(xm.xsplit())
        self.assertEqual([['test1','1','a','foo'],['test1','2','c','bar']], xl)
        self.assertEqual(['Case1/Z/a/foo','Case1/Y/c/bar'], xm.xitems())
        xm.xsort()
        self.assertEqual(['Case1/Y/c/bar','Case1/Z/a/foo'], xm.xitems())
        xm.xsort(cmp=cmp)
        self.assertEqual(['Case1/Y/c/bar','Case1/Z/a/foo'], xm.xitems())

    def test_80_xsplit(self):
        xs = deity.xtype.xstr('test1/a/1/foo', xsep='/', xlate=('',None,'test:','test:'))
        xl = xs.xsplit()
        self.assertEqual(['test1','a','1','foo'], xl)
        self.assertEqual('Case1', xl[0].xval())
        self.assertEqual('a', xl[1].xval())
        self.assertEqual('Z', xl[2].xval())
        self.assertEqual('foo', xl[3].xval())


class X_20_StringX_Test(deity.test.TestCase):

    def test_00_init(self):
        self.assertEqual('', deity.xtype.strx().xval())
        self.assertEqual('test', deity.xtype.strx('test').xval())

    def test_01_init_xstr(self):
        x = deity.xtype.xstr('test1', xlate='')
        s = deity.xtype.strx(x)
        self.assertEqual('Case1', deity.xtype.strx(x).xval())

    def test_10_not(self):
        s = deity.xtype.strx()
        self.assertTrue(not s)
        s = deity.xtype.strx('')
        self.assertTrue(not s)
        s = deity.xtype.strx('blah')
        self.assertFalse(not s)

    def test_20_str(self):
        s = deity.xtype.strx('foo')
        self.assertNotEqual('foo', str(s))
        self.assertIn('foo', str(s))


###############################################################################


class X_30_List_Test(deity.test.TestCase):
    kRemap = {'Z':'A', 'Y':'D', 'X':'C', 'W':'B'}

    def key(self, c):
        if c not in self.kRemap: return c
        return self.kRemap[c]

    def xpart(self, text, info=None):
        if info is None:
            (t,s,e) = text.partition('.')
            return (t,e)
        else:
            return '%s.%s' % (text,info[1])

    def test_init_1(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(('a', 'b'))
        self.assertEqual(['a', 'b'], xl)
        self.assertEqual('test:', xl.xlate)

    def test_init_2(self):
        xl = deity.xtype.xlist(('a', 'b'), xlate='test:')
        self.assertEqual(['a', 'b'], xl)
        self.assertEqual('test:', xl.xlate)

    def test_init_3(self):
        xl = deity.xtype.xlist((None, 'b'), xlate='test')
        self.assertEqual(None, xl[0])
        self.assertEqual('b', xl[1])

    def test_xstr(self):
        x1 = deity.xtype.xlist(xlate='a', xpart='b', xsep='c')
        x2 = x1.xstr('v')
        self.assertTrue(isinstance(x2, deity.xtype.xstr))
        self.assertEqual(x1.xlate, x2.xlate)
        self.assertEqual(x1.xpart, x2.xpart)
        self.assertEqual(x1.xsep,  x2.xsep)
        self.assertEqual('v', x2)

    def test_xlist(self):
        x1 = deity.xtype.xlist(xlate='a', xpart='b', xsep='c')
        x2 = x1.xlist()
        self.assertTrue(isinstance(x2, deity.xtype.xlist))
        self.assertEqual(x1.xlate, x2.xlate)
        self.assertEqual(x1.xpart, x2.xpart)
        self.assertEqual(x1.xsep,  x2.xsep)

    def test_append(self):
        xl = deity.xtype.xlist(xlate='test:')
        self.assertEqual(0, len(xl))
        xl.append('something')
        self.assertEqual('something', xl[0])
        self.assertTrue(isinstance(xl[0], deity.xtype.xstr))
        self.assertEqual('test:', xl[0].xlate)
        for x in xl:
            self.assertEqual('something', x)
            self.assertTrue(isinstance(x, deity.xtype.xstr))
        xl.append(99)
        self.assertEqual(2, len(xl))
        xl.extend(['foo', 'bar'])
        self.assertEqual(4, len(xl))
        self.assertTrue('foo' in xl)
        self.assertFalse('foosh' in xl)

    def test_xenumerate_1(self):
        xl = deity.xtype.xlist()
        xl.extend(['foo', 'bar', 'too(me)'])
        xe = list(xl.xenumerate())
        self.assertEqual([('foo','foo'), ('bar','bar'), ('too(me)','too(me)')], xe)

    def test_xenumerate_2(self):
        xl = deity.xtype.xlist()
        xl.extend(['foo', 'hi(you|me|them)!', 'bar'])
        xi = list(xl.__iter__())
        self.assertEqual(['foo', 'hiyou!', 'hime!', 'hithem!', 'bar'], xi)
        self.assertTrue('hime!' in xl)
        self.assertEqual([('foo','foo'), ('hiyou!','hiyou!'), ('hime!','hime!'), ('hithem!','hithem!'), ('bar','bar')], list(xl.xenumerate()))
        self.assertEqual([(0,'foo'), (1,'hiyou!'), (1,'hime!'), (1,'hithem!'), (2,'bar')], list(xl.xenumerate(index=True)))

    def test_xitems(self):
        xl = deity.xtype.xlist()
        xl.extend(['foo', 'bar', 'too'])
        self.assertEqual(['foo', 'bar', 'too'], xl.xitems())

    def test_xitems_x(self):
        xl = deity.xtype.xlist(xlate='')
        xl.extend(['test1', 'test2', 'test3'])
        self.assertEqual(['Case1', 'Case2', 'Case3'], xl.xitems())
        
    def test_xitems_x2(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(['a', 'c', 'e', 'g'])
        self.assertEqual(['B', 'D', 'F', 'H'], xl.xitems())

    def test_xitems_xm(self):
        xl = deity.xtype.xlist(xlate='', xpart=self.xpart)
        xl.extend(['test1.a', 'test2.b', 'test3.c', ''])
        self.assertEqual(['Case1.a', 'Case2.b', 'Case3.c', ''], xl.xitems())

    def test_xsearch(self):
        xl = deity.xtype.xlist()
        xl.extend(['foo', 'bar', 'too'])
        self.assertEqual('foo', xl.xsearch('foo'))
        self.assertEqual(0,     xl.xsearch('foo', index=True))
        self.assertEqual('bar', xl.xsearch('.ar'))
        self.assertEqual(1,     xl.xsearch('.ar', index=True))
        self.assertFalse(xl.xsearch('oo'))

    def test_xsearch_x1(self):
        xl = deity.xtype.xlist(xlate='')
        xl.extend(['test1', 'test2', 'test3'])
        self.assertEqual('test1', xl.xsearch('case1'))
        self.assertEqual(0,       xl.xsearch('case1', index=True))
        self.assertEqual('test2', xl.xsearch('.*se2'))
        self.assertEqual(1,       xl.xsearch('.*se2', index=True))
        self.assertFalse(xl.xsearch('.*test.*'))

    def test_xsearch_x2(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(['a', 'c', 'e', 'g'])
        self.assertEqual('a', xl.xsearch('b'))
        self.assertEqual(0,   xl.xsearch('b', index=True))
        self.assertEqual('c', xl.xsearch('.*d.*'))
        self.assertEqual(1,   xl.xsearch('.*d.*', index=True))
        self.assertFalse(xl.xsearch('.*a.*'))

    def test_xfind(self):
        xl = deity.xtype.xlist(xlate='')
        xl.extend(['test1', 'test2', 'test3'])
        self.assertEqual('test1', xl.xfind('Case1'))
        self.assertEqual(0,       xl.xfind('Case1', index=True))
        
    def test_xprefix(self):
        xl = deity.xtype.xlist()
        xl.extend(['fee', 'fi', 'fo', 'fum'])
        self.assertEqual(4, len(xl.xprefix('f')))
        self.assertEqual(1, len(xl.xprefix('fe')))
        self.assertEqual(0, len(xl.xprefix('foo')))
        self.assertEqual(4, len(xl.xprefix('f', index=True)))
        self.assertEqual(1, len(xl.xprefix('fe', index=True)))
        self.assertEqual(0, len(xl.xprefix('foo', index=True)))

    def test_xprefix_x(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(['a', 'aa', 'c', 'cc', 'e', 'ee', 'g'])
        self.assertEqual(2, len(xl.xprefix('b')))
        self.assertEqual(1, len(xl.xprefix('dd')))
        self.assertEqual(0, len(xl.xprefix('foo')))
        self.assertEqual(2, len(xl.xprefix('b', index=True)))
        self.assertEqual(1, len(xl.xprefix('dd', index=True)))
        self.assertEqual(0, len(xl.xprefix('foo', index=True)))

    def test_xsort(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(['1', '3', '2'])
        sl = xl; sl.sort()
        self.assertEqual(['1', '2', '3'], sl)
        sl = xl; sl.xsort()
        self.assertEqual(['3', '2', '1'], sl)
        self.assertEqual(['X', 'Y', 'Z'], sl.xitems())
        sl = xl; sl.xsort(reverse=True)
        self.assertEqual(['1', '2', '3'], sl)

    def test_xsort_key(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(['1', '2', '3', '4'])
        sl = xl; sl.xsort(key=self.key)
        self.assertEqual(['1', '4', '3', '2'], sl)
        self.assertEqual(['Z', 'W', 'X', 'Y'], sl.xitems())
        sl = xl; sl.xsort(key=self.key, reverse=True)
        self.assertEqual(['2', '3', '4', '1'], sl)

    def test_find(self):
        xl = deity.xtype.xlist(xlate='')
        xl.extend(['test1', 'test2', 'test3'])
        self.assertEqual('test1', xl.find('test1'))
        self.assertEqual(1,       xl.find('test2', index=True))
        
    def test_prefix(self):
        xl = deity.xtype.xlist()
        xl.extend(['fee', 'fi', 'fo', 'fum'])
        self.assertEqual(4, len(xl.prefix('f')))
        self.assertEqual(1, len(xl.prefix('fe')))
        self.assertEqual(0, len(xl.prefix('foo')))
        self.assertEqual(4, len(xl.prefix('f', index=True)))
        self.assertEqual(1, len(xl.prefix('fe', index=True)))
        self.assertEqual(0, len(xl.prefix('foo', index=True)))

    def test_xjoin_nothing(self):
        xl = deity.xtype.xlist(xlate='test:')
        self.assertEqual('', xl.xjoin(', '))

    def test_xjoin_list(self):
        xl = deity.xtype.xlist(xlate='test:')
        xl.extend(['a', 'c', 'e', 'g'])
        self.assertEqual('B, D, F, H', xl.xjoin(', '))


###############################################################################


class X_40_Dict_Test(deity.test.TestCase):

    def test_init_1(self):
        xd = deity.xtype.xdict(xlatekey='test:')
        xd['a'] = 1
        self.assertEqual(['a'], xd.keys())
        self.assertEqual('test:', xd.keys().xlate)
        self.assertEqual([1], xd.values())
#       self.assertEqual(None, xd.values().xlate)

    def test_init_2(self):
        xd = deity.xtype.xdict({'a':1}, xlatekey='test:')
        self.assertEqual(['a'], xd.keys())
        self.assertEqual('test:', xd.keys().xlate)
        self.assertEqual([1], xd.values())
#       self.assertEqual(None, xd.values().xlate)

    def test_xkeydict(self):
        xd1 = deity.xtype.xdict(xlatekey='lkey:', xpartkey='pkey', xsepkey='skey', xlateval='lval:', xpartval='pval', xsepval='sval')
        xd2 = xd1.xkeydict({'a':'1'})
        self.assertEqual('lkey:', xd2.xlatekey)
        self.assertEqual('pkey',  xd2.xpartkey)
        self.assertEqual('skey',  xd2.xsepkey)
        self.assertEqual(None, xd2.xlateval)
        self.assertEqual(None, xd2.xpartval)
        self.assertEqual(None, xd2.xsepval)
        self.assertEqual({'a':'1'}, xd2)

    def test_xkeystr(self):
        x1 = deity.xtype.xdict(xlatekey='a', xpartkey='b', xsepkey='c')
        x2 = x1.xkeystr('v')
        self.assertTrue(isinstance(x2, deity.xtype.xstr))
        self.assertEqual(x1.xlatekey, x2.xlate)
        self.assertEqual(x1.xpartkey, x2.xpart)
        self.assertEqual(x1.xsepkey,  x2.xsep)
        self.assertEqual('v', x2)

    def test_xkeylist(self):
        x1 = deity.xtype.xdict(xlatekey='a', xpartkey='b', xsepkey='c')
        x2 = x1.xkeylist()
        self.assertTrue(isinstance(x2, deity.xtype.xlist))
        self.assertEqual(x1.xlatekey, x2.xlate)
        self.assertEqual(x1.xpartkey, x2.xpart)
        self.assertEqual(x1.xsepkey,  x2.xsep)
        self.assertEqual([], x2)

    def test_xvalstr(self):
        x1 = deity.xtype.xdict(xlateval='a', xpartval='b', xsepval='c')
        x2 = x1.xvalstr('v')
        self.assertTrue(isinstance(x2, deity.xtype.xstr))
        self.assertEqual(x1.xlateval, x2.xlate)
        self.assertEqual(x1.xpartval, x2.xpart)
        self.assertEqual(x1.xsepval,  x2.xsep)
        self.assertEqual('v', x2)

    def test_xvallist(self):
        x1 = deity.xtype.xdict(xlateval='a', xpartval='b', xsepval='c')
        x2 = x1.xvallist()
        self.assertTrue(isinstance(x2, deity.xtype.xlist))
        self.assertEqual(x1.xlateval, x2.xlate)
        self.assertEqual(x1.xpartval, x2.xpart)
        self.assertEqual(x1.xsepval,  x2.xsep)
        self.assertEqual([], x2)

    def test_keys(self):
        xd = deity.xtype.xdict(xlatekey='test:')
        xd['a'] = 1; xd['b'] = 2; xd['c'] = 3
        xk = xd.keys(); xk.sort()
        self.assertEqual(['a', 'b', 'c'], xk)
        self.assertEqual(type(deity.xtype.xlist()), type(xk))
        self.assertEqual('test:', xk.xlate)

    def test_values(self):
        xd = deity.xtype.xdict(xlateval='test:')
        xd[1] = 'a'; xd[2] = 'b'; xd[3] = 'c'
        xv = xd.values(); xv.sort()
        self.assertEqual(['a', 'b', 'c'], xv)
        self.assertEqual(type(deity.xtype.xlist()), type(xv))
        self.assertEqual('test:', xv.xlate)

    def test_iteritems(self):
        xd = deity.xtype.xdict(xlatekey='test:', xlateval='')
        xd['1'] = 'test1'; xd['2'] = 'test2'; xd['3'] = None
        xi = list(xd.iteritems())
        xi.sort()
        self.assertEqual([('1','test1'), ('2','test2'), ('3',None)], xi)
        self.assertTrue(isinstance(xi[0][0], deity.xtype.xstr))
        self.assertTrue(isinstance(xi[0][1], deity.xtype.xstr))
        self.assertEqual('test:', xi[0][0].xlate)
        self.assertEqual('', xi[0][1].xlate)
        self.assertEqual(xd.items(), list(xd.iteritems()))

    def test_iterkeys(self):
        xd = deity.xtype.xdict(xlatekey='test:', xlateval='')
        xd['1'] = 'test1'; xd['2'] = 'test2'; xd['3'] = None
        xk = list(xd.iterkeys())
        xk.sort()
        self.assertEqual(['1', '2', '3'], xk)
        self.assertTrue(isinstance(xk[0], deity.xtype.xstr))
        self.assertEqual('test:', xk[0].xlate)
        self.assertEqual(xd.keys(), list(xd.iterkeys()))

    def test_itervalues(self):
        xd = deity.xtype.xdict(xlatekey='test:', xlateval='')
        xd['1'] = 'test1'; xd['2'] = 'test2'; xd['3'] = None
        xv = list(xd.itervalues())
        xv.sort()
        self.assertEqual([None, 'test1', 'test2'], xv)
        self.assertTrue(isinstance(xv[1], deity.xtype.xstr))
        self.assertEqual('', xv[1].xlate)
        self.assertEqual(xd.values(), list(xd.itervalues()))


###############################################################################


class X_90_XLookup(deity.test.TestCase):

    def test_10_LookupStrx(self):
        l1 = deity.xtype.xlist(xlate='')
        self.assertRaises(BadParameter, deity.xtype.XLookup, l1, deity.xtype.strx('case9'), 'test')
        l1.extend(('test1','test2','test3'))
        self.assertEqual('test1', deity.xtype.XLookup(l1, deity.xtype.strx('case1'), 'test'))
        self.assertEqual(0, deity.xtype.XLookup(l1, deity.xtype.strx('case1'), 'test', index=True))
        self.assertEqual(['test1','test2','test3'], deity.xtype.XLookup(l1, deity.xtype.strx('case'), 'test', multiple=True))
        self.assertEqual(int, type(deity.xtype.XLookup(l1, deity.xtype.strx('case'), 'test', multiple=True, index=True)[0]))
        self.assertEqual([0, 1, 2], deity.xtype.XLookup(l1, deity.xtype.strx('case'), 'test', multiple=True, index=True))
        self.assertEqual(int, type(deity.xtype.XLookup(l1, 'test', 'test', multiple=True, index=True)[0]))
        self.assertEqual([0, 1, 2], deity.xtype.XLookup(l1, 'test', 'test', multiple=True, index=True))
        self.assertRaises(BadParameter, deity.xtype.XLookup, l1, deity.xtype.strx('case'), 'test')
        self.assertEqual('test1', deity.xtype.XLookup(l1, deity.xtype.xstr('test1',xlate=''), 'test'))
        self.assertEqual([], deity.xtype.XLookup(l1, deity.xtype.xstr('test',xlate=''), 'test', multiple=True))
        self.assertEqual('test1', deity.xtype.XLookup(l1, 'test1', 'test'))
        self.assertEqual(['test1','test2','test3'], deity.xtype.XLookup(l1, 'test', 'test', multiple=True))
        self.assertEqual('test2', deity.xtype.XLookup(l1, None, 'test', defindex=1))
        self.assertEqual(1, deity.xtype.XLookup(l1, None, 'test', defindex=1, index=True))
        self.assertRaises(BadParameter, deity.xtype.XLookup, l1, None, 'test', defindex=99)
        self.assertRaises(BadParameter, deity.xtype.XLookup, l1, None, 'test')
        self.assertRaises(BadParameter, deity.xtype.XLookup, l1, '', 'test')
        self.assertRaises(BadParameter, deity.xtype.XLookup, l1, 'foo', 'test')

    def test_20_LookupStrx_prefix(self):
        l1 = deity.xtype.xlist(xlate='test:')
        l1.extend(('aa', 'a'))
        self.assertEqual('a', deity.xtype.XLookup(l1, deity.xtype.strx('B'), 'test'))
        self.assertEqual(['a','aa'], deity.xtype.XLookup(l1, deity.xtype.strx('B'), 'test', multiple=True))
        


###############################################################################


if __name__ == '__main__':
    unittest.main()
