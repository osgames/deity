#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test command dispatcher)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.commands

from deity.exceptions import *


###############################################################################


class CommandDispatchTest(deity.test.TestCase):

    def setUp(self):
        deity.test.TestCase.setUp(self)
        self.a  = None
        self.aa = None
        self.ab = None
        self.bb = None
        self.bc = None
        self.cd = None
        self.re = None

    def VerifyVals(self, vals):
        pass

    def cmd_a(self, key, *vals):
        self.VerifyVals(vals)
        self.a = key + ''.join(vals)
        return 'a';

    def cmd_aa(self, key, *vals):
        self.VerifyVals(vals)
        self.aa = key + ''.join(vals)
        return 'aa';

    def cmd_ab(self, key, *vals):
        self.VerifyVals(vals)
        self.ab = key + ''.join(vals)
        return 'ab';

    def cmd_bb(self, key, *vals):
        self.VerifyVals(vals)
        self.bb = key + ''.join(vals)
        return 'bb';

    def cmd_bc(self, key, *vals):
        self.VerifyVals(vals)
        self.bc = key + ''.join(vals)
        return 'bc';

    def cmd_cd(self, key, *vals):
        self.VerifyVals(vals)
        self.cd = key + ''.join(vals)
        return 'cd';

    def cmd_re(self, key, match):
        self.re = key + ''.join(match.groups())
        return 're';

    def test_10_funcs(self):
        qmtext = ('question', 'one', 'two')
        cmds = deity.commands.CommandDispatch(qmarktext=qmtext)
        cmds.Register('a',  self.cmd_a)
        cmds.Register('aa', self.cmd_aa)
        cmds.Register('ab', self.cmd_ab)
        cmds.Register('bb', self.cmd_bb)
        cmds.Register('bc', self.cmd_bc)
        cmds.Register('cd', self.cmd_cd)
        self.assertRaises(Exception, cmds.Register, 'a', self.cmd_a)
        self.assertEqual(6, len(cmds.funcs.keys()))
        # function look-up
        self.assertEqual(self.cmd_aa, cmds.Function('aa'))
        self.assertEqual(self.cmd_bb, cmds.Function('bb'))
        # dispatch
        self.assertEqual('a', cmds.Dispatch('a', '1'))
        self.assertEqual('a1', self.a)
        self.assertEqual('a 1', cmds.Last())
        self.assertEqual('aa', cmds.Dispatch('aa', '11'))
        self.assertEqual('aa11', self.aa)
        self.assertEqual('ab', cmds.Dispatch('ab', '12'))
        self.assertEqual('ab12', self.ab)
        self.assertEqual('bb', cmds.Dispatch('bb', '22'))
        self.assertEqual('bb22', self.bb)
        self.assertEqual('bc', cmds.Dispatch('bc', '23'))
        self.assertEqual('bc23', self.bc)
        self.assertEqual('cd', cmds.Dispatch('cd', '3'))
        self.assertEqual('cd3', self.cd)
        self.assertRaises(NotPossible, cmds.Dispatch, 'b')
        self.assertRaises(BadParameter, cmds.Dispatch, 'd')
        self.assertEqual('cd 3', cmds.Last())
        # execute
        self.assertEqual('a', cmds.Execute('a 1'))
        self.assertEqual('a1', self.a)
        self.assertEqual('a 1', cmds.Last())
        self.assertEqual('aa', cmds.Execute('aa','11'))
        self.assertEqual('aa11', self.aa)
        self.assertEqual('ab', cmds.Execute('ab 12'))
        self.assertEqual('ab12', self.ab)
        self.assertEqual('bb', cmds.Execute('bb','22'))
        self.assertEqual('bb22', self.bb)
        self.assertEqual('bc', cmds.Execute('bc 23'))
        self.assertEqual('bc23', self.bc)
        self.assertEqual('cd', cmds.Execute('c','3'))
        self.assertEqual('cd3', self.cd)
        self.assertRaises(NotPossible, cmds.Execute, 'b')
        self.assertEqual('cd 3', cmds.Last())
        # help
        self.assertEqual('a, aa, ab, bb, bc, cd'+'\n\nquestion\none\ntwo', cmds.Execute('?'))

    def test_20_regex(self):
        cmds = deity.commands.CommandDispatch()
        cmds.Register('a',  self.cmd_a)
        cmds.Register('aa', self.cmd_aa)
        cmds.Match('(\d*)d(\d+)',  self.cmd_re)
        cmds.Execute('3d6')
        self.assertEqual(None, self.a)
        self.assertEqual('3d636', self.re)

    def test_30_cmd_1char(self):
        cmds = deity.commands.CommandDispatch()
        cmds.Register('+', self.cmd_a)
        self.assertRaises(Exception, cmds.Register, '+', self.cmd_a)
        self.assertEqual(1, len(cmds.funcs.keys()))
        # dispatch
        self.assertEqual('a', cmds.Dispatch('+', '1'))
        self.assertEqual('+1', self.a)
        self.assertEqual('a', cmds.Execute('+ 2'))
        self.assertEqual('+2', self.a)
        self.assertEqual('a', cmds.Execute('+3'))
        self.assertEqual('+3', self.a)


###############################################################################


if __name__ == '__main__':
    unittest.main()
