#! /usr/bin/python -t
###############################################################################
#
# $Id: deity,v 1.1 2005/10/12 18:49:31 bcwhite Exp $
#
# Deity GameMaster Utility Program (test world module)
# Copyright (C) 2005-2013 by Brian White <bcwhite@pobox.com>
#
###############################################################################
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307 USA
#
###############################################################################


import unittest
from test import test_support

import deity
import deity.test
import deity.world

from deity.xtype import *
from deity.exceptions import *


###############################################################################


class World_00_Test(deity.test.TestCase):

    def test_100_init(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.30'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.EnvironSet('urban+')
        w._TimeOffset('+0s')
        t = w.TimeGetCurrent()
        self.assertEqual(720, t['year'])
        self.assertEqual(  1, t['month'])
        self.assertEqual(  2, t['day'])
        self.assertEqual(  8, t['hour'])
        self.assertEqual( 10, t['minute'])
        self.assertEqual( 20, t['second'])
        self.assertEqual( 30, t['nanosec'])
        self.assertEqual(  2, t['doy'])
        self.assertEqual(  0, t['season'])
        self.assertEqual(490, t['dayincr'])
        self.assertAlmostEqual(259202.3405, t['abs'], places=4)
        self.assertEqual('day', t['daytime'])

    def test_101_baddate(self):
        GDB['/World/Test/CurTime'] = 'blah blah'
        GDB['/World/Test/TimeZone'] = '+xyz'
        w = deity.world.World('Test')
        t = w.TimeGetCurrent()
        self.assertEqual(0, t['year'])
        self.assertEqual(1, t['month'])
        self.assertEqual(1, t['day'])
        self.assertEqual(0, t['hour'])
        self.assertEqual(0, t['minute'])
        self.assertEqual(0, t['second'])
        self.assertEqual(0, t['nanosec'])

    def test_102_tz(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.30'
        GDB['/World/Test/TimeZone'] = '+0130'
        w = deity.world.World('Test')
        t = w.TimeGetCurrent()
        self.assertEqual(720, t['year'])
        self.assertEqual(  1, t['month'])
        self.assertEqual(  2, t['day'])
        self.assertEqual(  9, t['hour'])
        self.assertEqual( 40, t['minute'])
        self.assertEqual( 20, t['second'])
        self.assertEqual( 30, t['nanosec'])

        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.30'
        GDB['/World/Test/TimeZone'] = '-0130'
        w = deity.world.World('Test')
        t = w.TimeGetCurrent()
        self.assertEqual(720, t['year'])
        self.assertEqual(  1, t['month'])
        self.assertEqual(  2, t['day'])
        self.assertEqual(  6, t['hour'])
        self.assertEqual( 40, t['minute'])
        self.assertEqual( 20, t['second'])
        self.assertEqual( 30, t['nanosec'])

    def test_103_adj(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.30'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.TimeAdjust('1year 2 mo 3d -1 h -2mi 3 sec +4ms -5us +6ns')
        t = w.TimeGetCurrent()
        self.assertEqual(721, t['year'])
        self.assertEqual(  3, t['month'])
        self.assertEqual(  5, t['day'])
        self.assertEqual(  7, t['hour'])
        self.assertEqual(  8, t['minute'])
        self.assertEqual( 17, t['second'])
        self.assertEqual(3995036, t['nanosec'])

        GDB['/World/Test/CurTime'] = '719-12-30 23:00:00.00'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.TimeAdjust('+1h')
        t = w.TimeGetCurrent()
        self.assertEqual(720, t['year'])
        self.assertEqual(  1, t['month'])
        self.assertEqual(  1, t['day'])
        self.assertEqual(  0, t['hour'])
        self.assertEqual(  0, t['minute'])
        self.assertEqual(  0, t['second'])

    def test_104_adj_round(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.30'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.TimeAdjust('1year 2 mo 3d -1 h: +10m')
        t = w.TimeGetCurrent()
        self.assertEqual(721, t['year'])
        self.assertEqual(  3, t['month'])
        self.assertEqual(  5, t['day'])
        self.assertEqual(  8, t['hour'])
        self.assertEqual( 10, t['minute'])
        self.assertEqual(  0, t['second'])
        self.assertEqual(  0, t['nanosec'])
        self.assertEqual( 65, t['doy'])

        w.TimeAdjust('-10m')
        w.TimeAdjust('-1 h: 20m')
        t = w.TimeGetCurrent()
        self.assertEqual(721, t['year'])
        self.assertEqual(  3, t['month'])
        self.assertEqual(  5, t['day'])
        self.assertEqual(  6, t['hour'])
        self.assertEqual( 40, t['minute'])
        self.assertEqual(  0, t['second'])
        self.assertEqual(  0, t['nanosec'])

    def test_105_adj_default(self):
        GDB['/World/Test/CurTime'] = '720-01-02 09:10:20.30'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.timeUnitDefault='hours'; w.timeIncDefault=4
        w.TimeAdjust('+')
        t = w.TimeGetCurrent()
        self.assertEqual(720, t['year'])
        self.assertEqual(  1, t['month'])
        self.assertEqual(  2, t['day'])
        self.assertEqual( 12, t['hour'])
        self.assertEqual(  0, t['minute'])
        self.assertEqual(  0, t['second'])

        GDB['/World/Test/CurTime'] = '720-01-02 09:10:20.30'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.timeUnitDefault='hours'; w.timeIncDefault=4
        w.TimeAdjust('-2')
        t = w.TimeGetCurrent()
        self.assertEqual(720, t['year'])
        self.assertEqual(  1, t['month'])
        self.assertEqual(  2, t['day'])
        self.assertEqual(  4, t['hour'])
        self.assertEqual(  0, t['minute'])
        self.assertEqual(  0, t['second'])

    def test_106_adj_round_tz(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.30'
        GDB['/World/Test/TimeZone'] = '-0130'
        w = deity.world.World('Test')
        w.TimeAdjust('1year 2 mo 3d -1 h: +45m')
        t = w.TimeGetCurrent()
        #print t['utc']
        self.assertEqual(721, t['year'])
        self.assertEqual(  3, t['month'])
        self.assertEqual(  5, t['day'])
        self.assertEqual(  6, t['hour'])
        self.assertEqual( 45, t['minute'])
        self.assertEqual(  0, t['second'])
        self.assertEqual(  0, t['nanosec'])

    def test_109_adj_bad(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.0'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        self.assertRaises(BadParameter, w.TimeAdjust, '+=')

    def test_110_time_set_date(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.0'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        w.TimeSetDate('200+2m20')
        t = w.TimeGetCurrent()
        self.assertEqual(200, t['year'])
        self.assertEqual(  2, t['month'])
        self.assertEqual( 20, t['day'])
        self.assertEqual(  8, t['hour'])
        self.assertEqual( 10, t['minute'])
        self.assertEqual( 20, t['second'])
        self.assertEqual(  0, t['nanosec'])
        self.assertEqual('day', t['daytime'])
        w.TimeSetTime('19h18m17,161514')
        t = w.TimeGetCurrent()
        self.assertEqual(200, t['year'])
        self.assertEqual(  2, t['month'])
        self.assertEqual( 20, t['day'])
        self.assertEqual( 19, t['hour'])
        self.assertEqual( 18, t['minute'])
        self.assertEqual( 17, t['second'])
        self.assertEqual(161514000, t['nanosec'])
        self.assertEqual('night', t['daytime'])

    def test_120_time_output(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.300000000'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        self.assertEqual('720-01-02 08:10:20.300.300000', w.TimeGetOutput('%Y-%02M-%02D %02h:%02m:%02s.%03i.%06u'))
        self.assertEqual('month01 2, 720', w.TimeGetOutput('%N %D, %Y'))
        self.assertEqual('720/M01/2', w.TimeGetOutput('%Y/%S/%D'))
        self.assertEqual('+0000', w.TimeGetOutput('%z'))

    def test_150_time_compare(self):
        GDB['/World/Test/CurTime'] = '720-01-02 08:10:20.300000000'
        GDB['/World/Test/TimeZone'] = '+0000'
        w = deity.world.World('Test')
        for p in w.kTimeParts:
            tnow = w.TimeGetCurrent()
            tpls = w.TimeGetCurrent()
            tmin = w.TimeGetCurrent()
            w.TimeAdjust('+1%s' % p, tpls)
            w.TimeAdjust('-1%s' % p, tmin)
            self.assertEqual( 0, w.TimeCompare(tnow))
            self.assertEqual(-1, w.TimeCompare(tmin))
            self.assertEqual(+1, w.TimeCompare(tpls))
            self.assertEqual( 0, w.TimeCompare(tpls, tpls))
            self.assertEqual(-1, w.TimeCompare(tmin, tpls))
            self.assertEqual(+1, w.TimeCompare(tpls, tmin))

    def test_200_env_lookup(self):
        w = deity.world.World('Test')
        self.assertEqual('safe', w.EnvironLookup('safe'))
        self.assertEqual('urban+', w.EnvironLookup('urban+'))
        self.assertEqual('rural-', w.EnvironLookup('rural-'))
        self.assertEqual('safe', w.EnvironLookup('s'))
        self.assertEqual('urban-', w.EnvironLookup('u-'))
        self.assertEqual('rural+', w.EnvironLookup('ru+'))
        self.assertEqual('road+', w.EnvironLookup('ro+'))
        self.assertRaises(BadParameter, w.EnvironLookup, 'u')
        self.assertRaises(BadParameter, w.EnvironLookup, 'r')
        self.assertRaises(BadParameter, w.EnvironLookup, 'r+')
        self.assertRaises(BadParameter, w.EnvironLookup, 'r-')
        self.assertRaises(BadParameter, w.EnvironLookup, 'blah')


###############################################################################


if __name__ == '__main__':
    unittest.main()
