Get the Python v2.7 "Windows Installer" from http://www.python.org/download/releases/2.7/

Go to http://wxpython.org/download.php and get the latest 2.8 version of
wxPython for Python v2.7.  Yes, wxPython and Python have slightly different
version numbers.

Install both of these packages.

Get the zip archive of the Deity program and unpack it where you like.
http://sourceforge.net/projects/deity/files/deity/

Explore the directory where you installed Deity and execute the "make-beastiary.bat"
file.  This will create the database of available creatures and equipment.  If
it fails, then something isn't installed correctly.

Execute the "deity.bat" file to run the program.  That's all there is to it.




==== IGNORE THIS -- IT'S FOR THE FUTURE ====

For the map support, you need PyOpenGL from http://pyopengl.sourceforge.net/
starting with http://peak.telecommunity.com/dist/ez_setup.py (save in C:\Temp)
and the following commands:

  c:
  \Temp\ez_setup.py
  \Python26\Scripts\easy_install PyOpenGL

